# GenPlus
[![pipeline status](https://gitlab.com/kaipho/GenPlus/badges/master/pipeline.svg)](https://gitlab.com/kaipho/GenPlus/commits/master)
[![coverage report](https://gitlab.com/kaipho/GenPlus/badges/master/coverage.svg)](https://gitlab.com/kaipho/GenPlus/commits/master)

Gen+ ist ein Generator für Informationssysteme. Für weitere Informationen kann die 
[Dokumentation](https://github.com/Neo11/GenPlus/blob/master/Dokumentation.pdf) und die [Beispielanwendung](https://github.com/Neo11/univerwaltung) betrachtet werden.

Zum Starten des Generators `mvn package` ausführen. Unter ./target wird nun die genplus-jar-with-dependencies.jar erzeugt. 
