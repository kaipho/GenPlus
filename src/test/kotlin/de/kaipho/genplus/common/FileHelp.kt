package de.kaipho.genplus.common

object FileHelp {

    /**
     * Checks if the line is in the file, ignores spacing.
     */
    fun contains(file: List<String>, line: String) : Boolean{
        return file.any { it.trim() == line }
    }

}