package de.kaipho.genplus.common

import de.kaipho.genplus.generator.Model
import de.kaipho.genplus.generator.commands.scanFiles
import de.kaipho.genplus.generator.store.ClassStore
import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.stream.Collectors

object Models {

    fun loadModelWithConcreteOnly() {
        ClassStore.reset()
        Model.loader = this::modelWithConcreteOnly

        val files = Model.loader.invoke()
        scanFiles(files)
    }

    fun modelWithConcreteOnly(): List<Pair<String, String>> {
        val result: MutableList<Pair<String, String>> = java.util.ArrayList()

        val framework = BufferedReader(InputStreamReader(javaClass.getResourceAsStream("/model/framework"))).lines().collect(Collectors.joining("\n"))
        result.add(Pair("framework", framework))

        val model = """
        domain {
            Person {
                String vorname
                String nachname
                Geschlecht geschlecht
                Adresse adresse
            }

            Adresse {
                String strasse
                Optional<Stadt> stadt
            }

            Stadt {
                String plz
                String name
            }

            Arbeitgeber {
                String name
            }
        }

        observations {
            Geschlecht
        }

        service {
            export ServiceClass {
                Person createPerson(String nachname, String vorname)
                Adresse createAdresse(String strasse)
                Stadt createStadt(String plz, String name)
            }
        }
        """
        result.add(Pair("testLoaderFunctionsGetResolvedCorrect", model))
        return result
    }

    fun loadModelWithConcreteAndAbstract() {
        ClassStore.reset()
        Model.loader = this::modelWithConcreteAndAbstract

        val files = Model.loader.invoke()
        scanFiles(files)
    }

    fun modelWithConcreteAndAbstract(): List<Pair<String, String>> {
        val result: MutableList<Pair<String, String>> = java.util.ArrayList()

        val framework = BufferedReader(InputStreamReader(javaClass.getResourceAsStream("/model/framework"))).lines().collect(Collectors.joining("\n"))
        result.add(Pair("framework", framework))

        val model = """
        domain {
            abstract Person {
                String vorname
                String nachname
            }

            Student : Person {
                String matNr
            }

            Dozent : Person {
                String persNr
            }

            Adresse {
                String strasse
            }

            Test {

            }
        }

        service {
            export ServiceClass {
                Dozent createDozent(String nachname, String vorname, String matNr)
                Student createStudent(String persNr)
                Adresse createAdresse(String strasse)
            }
        }
        """
        result.add(Pair("testLoaderFunctionsGetAllCreateFunctions", model))
        return result
    }

}