package de.kaipho.genplus.generator.parser

import com.google.common.collect.Lists
import de.kaipho.genplus.generator.domain.obj.ConstantClass
import de.kaipho.genplus.generator.store.ClassStore
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import kotlin.test.assertTrue

internal class ParserFacadeTest {

    @BeforeEach
    fun before() {
        ClassStore.reset()
    }

    @Test
    fun validateFiles() {
        val result = ParserFacade.validateFiles(Lists.newArrayList("./src/test/resources/models/uni/uni.genplus"))

        assertEquals(1, result.keys.size)

        print(result)

        assertTrue(result[result.keys.first()]!!.isEmpty())
    }

    @Test
    fun parseFiles() {
        ParserFacade.parseFilesToClassStore(Lists.newArrayList("./src/test/resources/models/uni/uni.genplus"))

        assertEquals(1, ClassStore.constants.size)

        val parsedConstant = ClassStore.constants[0] as ConstantClass

        assertEquals("RepVorlesung", parsedConstant.name)
        assertEquals("bezeichnung", parsedConstant.parameter[0])
        assertEquals("dozent", parsedConstant.parameter[1])
        assertEquals("anzStudenten", parsedConstant.parameter[2])

        assertEquals(1, ClassStore.domain.domainClasses.size)
        val types = ClassStore.domain.domainClasses

        val personRolle = types.find { "PersonRolle" == it.name }
        assertTrue(personRolle != null)
        assertEquals("uni", personRolle!!.prefix)
    }
}
