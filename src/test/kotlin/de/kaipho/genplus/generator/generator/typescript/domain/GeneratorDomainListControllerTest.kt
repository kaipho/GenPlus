package de.kaipho.genplus.generator.generator.typescript.domain

import de.kaipho.genplus.common.FileHelp
import de.kaipho.genplus.common.Models
import de.kaipho.genplus.generator.core.obj.options.Option
import de.kaipho.genplus.generator.generator.core.GenerationPool
import de.kaipho.genplus.generator.io.MemoryFileSystem
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.store.SettingsStore
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class GeneratorDomainListControllerTest {
    @Test
    internal fun testCreateFunctionGetsGenerated() {
        Models.loadModelWithConcreteAndAbstract()
        SettingsStore.options.add(Option.PREFIX, "de.kaipho.test")

        val clazz = ClassStore.getDomainClassByName("Person")
        val student = ClassStore.getDomainClassByName("Student")
        val dozent = ClassStore.getDomainClassByName("Dozent")

        val fileSystem = MemoryFileSystem()
        GenerationPool.callSimpleExternalGenerator(GeneratorDomainListController(clazz), fileSystem)

        val linesOfFile = fileSystem.files["./web/src/app/components/domain/person.list.component.ts"]!!

        // create function
        assertTrue(FileHelp.contains(linesOfFile, """createDozent() {"""))
        assertTrue(FileHelp.contains(linesOfFile, """this.create(ServiceClassCreateDozentDozentComponent, ${dozent.id})"""))
        // import
        assertTrue(FileHelp.contains(linesOfFile, """import { ServiceClassCreateDozentDozentComponent } from '../service/ServiceClass.createDozent.Dozent.component';"""))

        assertTrue(FileHelp.contains(linesOfFile, """createStudent() {"""))
        assertTrue(FileHelp.contains(linesOfFile, """this.create(ServiceClassCreateStudentStudentComponent, ${student.id})"""))

        assertTrue(FileHelp.contains(linesOfFile, """import { ServiceClassCreateStudentStudentComponent } from '../service/ServiceClass.createStudent.Student.component';"""))
    }
}