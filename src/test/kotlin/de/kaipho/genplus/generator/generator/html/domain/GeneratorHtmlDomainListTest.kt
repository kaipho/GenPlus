package de.kaipho.genplus.generator.generator.html.domain

import de.kaipho.genplus.common.FileHelp
import de.kaipho.genplus.common.Models
import de.kaipho.genplus.generator.core.obj.options.Option
import de.kaipho.genplus.generator.generator.core.GenerationPool
import de.kaipho.genplus.generator.io.MemoryFileSystem
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.store.SettingsStore
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

internal class GeneratorHtmlDomainListTest {

    @Test
    internal fun testSingleCreateOptionGetsCreatedCorrect() {
        Models.loadModelWithConcreteAndAbstract()
        SettingsStore.options.add(Option.PREFIX, "de.kaipho.test")

        val clazz = ClassStore.getDomainClassByName("Student")

        val fileSystem = MemoryFileSystem()
        GenerationPool.callSimpleExternalGenerator(GeneratorHtmlDomainList(clazz), fileSystem)

        val linesOfFile = fileSystem.files["./web/src/app/components/domain/student.list.component.html"]!!

        assertTrue(FileHelp.contains(linesOfFile, """<button [matTooltip]="'functions.createStudent.title' | translate" mat-icon-button (click)="createStudent()">"""))
        assertTrue(FileHelp.contains(linesOfFile, """<mat-icon>add</mat-icon>"""))
        assertTrue(FileHelp.contains(linesOfFile, """</button>"""))
    }

    @Test
    internal fun testNoCreateOptionGetsCreatedCorrect() {
        Models.loadModelWithConcreteAndAbstract()
        SettingsStore.options.add(Option.PREFIX, "de.kaipho.test")

        val clazz = ClassStore.getDomainClassByName("Test")

        val fileSystem = MemoryFileSystem()
        GenerationPool.callSimpleExternalGenerator(GeneratorHtmlDomainList(clazz), fileSystem)

        val linesOfFile = fileSystem.files["./web/src/app/components/domain/test.list.component.html"]!!

        assertFalse(FileHelp.contains(linesOfFile, """<mat-icon>add</mat-icon>"""))
    }

    @Test
    internal fun testMultipleCreateOptionGetsCreatedCorrect() {
        Models.loadModelWithConcreteAndAbstract()
        SettingsStore.options.add(Option.PREFIX, "de.kaipho.test")

        val clazz = ClassStore.getDomainClassByName("Person")

        val fileSystem = MemoryFileSystem()
        GenerationPool.callSimpleExternalGenerator(GeneratorHtmlDomainList(clazz), fileSystem)

        val linesOfFile = fileSystem.files["./web/src/app/components/domain/person.list.component.html"]!!

        assertTrue(FileHelp.contains(linesOfFile, """<button mat-menu-item (click)="createStudent()">"""))
        assertTrue(FileHelp.contains(linesOfFile, """{{ 'functions.createStudent.title' | translate }}"""))
        assertTrue(FileHelp.contains(linesOfFile, """</button>"""))
        assertTrue(FileHelp.contains(linesOfFile, """<button mat-menu-item (click)="createDozent()">"""))
        assertTrue(FileHelp.contains(linesOfFile, """{{ 'functions.createDozent.title' | translate }}"""))
        assertTrue(FileHelp.contains(linesOfFile, """</button>"""))
        assertTrue(FileHelp.contains(linesOfFile, """<button [matTooltip]="'common.createObj' | translate" mat-icon-button [mat-menu-trigger-for]="createMenu">"""))
        assertTrue(FileHelp.contains(linesOfFile, """<mat-icon>add</mat-icon>"""))
        assertTrue(FileHelp.contains(linesOfFile, """</button>"""))
    }
}
