package de.kaipho.genplus.generator.store

import de.kaipho.genplus.common.Models
import de.kaipho.genplus.generator.Model
import de.kaipho.genplus.generator.commands.scanFiles
import de.kaipho.genplus.generator.core.obj.AssoziationTypeString
import de.kaipho.genplus.generator.core.obj.AssoziationTypeUser
import de.kaipho.genplus.generator.core.obj.service.Function
import de.kaipho.genplus.generator.core.obj.service.ServiceClass
import de.kaipho.genplus.generator.domain.obj.AbstractDomainClass
import de.kaipho.genplus.generator.domain.obj.DomainClass
import de.kaipho.genplus.generator.domain.obj.DomainRepository
import de.kaipho.genplus.generator.store.ClassStore.addFunction
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.stream.Collectors

internal class ClassStoreTest {
    @Test
    fun addFunction() {
        ClassStore.reset()

        val serviceClass = ServiceClass()
        serviceClass.name = "Service1"

        val f1 = Function()
        f1.name = "f1"
        f1.returnType = AssoziationTypeString
        f1.owner = serviceClass

        val f2 = Function()
        f2.name = "f2"
        f2.returnType = AssoziationTypeString
        f2.owner = serviceClass

        assertEquals("Service1F1String", f1.getQualifiedClassName())
        assertEquals("Service1F2String", f2.getQualifiedClassName())
        serviceClass.addFunction(f1)
        serviceClass.addFunction(f2)

        val f3 = Function()
        f3.name = "f2"
        f3.returnType = AssoziationTypeString
        f3.owner = serviceClass

        assertThrows(FunctionUniquesViolatedException::class.java) { serviceClass.addFunction(f3) }
    }

    @Test
    fun addCreateFunction() {
        ClassStore.reset()

        val serviceClass = ServiceClass()
        serviceClass.name = "Service1"

        val f1 = Function()
        f1.name = "createPerson"
        f1.returnType = AssoziationTypeUser("Person")
        f1.owner = serviceClass

        val f2 = Function()
        f2.name = "createStudent"
        f2.returnType = AssoziationTypeUser("Student")
        f2.owner = serviceClass

        serviceClass.addFunction(f1)
        serviceClass.addFunction(f2)

        // Die Definition folgt nicht der Konvention <returnType> create<returnType>(<parameter>)
        val f3 = Function()
        f3.name = "createStadt"
        f3.returnType = AssoziationTypeUser("City")
        f3.owner = serviceClass
        assertThrows(FunctionUniquesViolatedException::class.java) { serviceClass.addFunction(f3) }

        // Die Definition folgt nicht der Konvention <returnType> create<returnType>(<parameter>)
        val f4 = Function()
        f4.name = "createStadt"
        f4.returnType = AssoziationTypeUser("Stadt")
        f4.owner = serviceClass
        f4.extends = AbstractDomainClass(ArrayList(), DomainRepository())
        assertThrows(FunctionUniquesViolatedException::class.java) { serviceClass.addFunction(f4) }

        // Es gibt mehr als eine create Operation für ein UserType
        assertThrows(FunctionUniquesViolatedException::class.java) { serviceClass.addFunction(f2) }

        assertEquals(2, ClassStore.createFunctions.size)
        assertTrue(ClassStore.createFunctions.contains("Person"))
        assertTrue(ClassStore.createFunctions.contains("Student"))
    }

    @Test
    fun functionsGetResolvedCorrect() {
        Models.loadModelWithConcreteOnly()

        assertEquals(3, ClassStore.createFunctions.size)

        val map = HashMap<String, DomainClass>()

        ClassStore.domain.domainClasses.forEach { map.put(it.name, it) }

        assertNull(map["Arbeitgeber"]!!.createOperation)
        assertEquals(2, map["Stadt"]!!.createOperation!!.params.size)
        assertEquals(1, map["Adresse"]!!.createOperation!!.params.size)
        assertEquals(3, map["Person"]!!.createOperation!!.params.size)

        assertEquals("strasse", map["Person"]!!.createOperation!!.params[2].name)
    }

    @Test
    fun functionsGetAllCreateFunctions() {
        Models.loadModelWithConcreteAndAbstract()

        assertEquals(3, ClassStore.createFunctions.size)
        val person = ClassStore.getDomainClassByName("Person")
        val student = ClassStore.getDomainClassByName("Student")
        val adresse = ClassStore.getDomainClassByName("Adresse")

        assertEquals(2, person.getAllCreateFunctions().size)
        assertEquals(1, student.getAllCreateFunctions().size)
        assertEquals(1, adresse.getAllCreateFunctions().size)
    }
}