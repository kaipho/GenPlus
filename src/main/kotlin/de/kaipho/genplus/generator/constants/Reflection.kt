package de.kaipho.genplus.generator.constants

import de.kaipho.genplus.generator.commands.GenCommand
import de.kaipho.genplus.generator.core.scanner.Token
import de.kaipho.genplus.generator.generator.core.Generator
import de.kaipho.genplus.generator.io.loader.FileLoader
import de.kaipho.genplus.generator.io.merger.FileMerger
import de.kaipho.genplus.generator.template.replacer.Replacer
import org.reflections.ReflectionUtils
import org.reflections.Reflections
import kotlin.reflect.jvm.kotlinProperty

object Reflection {
    private val base = "de.kaipho.genplus"

    val LOADER = Pair("de.kaipho.genplus.generator.io.loader", FileLoader::class.java)
    val MERGER = Pair("de.kaipho.genplus.generator.io.merger", FileMerger::class.java)
    val REPLACER = Pair("de.kaipho.genplus.generator.template.replacer", Replacer::class.java)
    val GENERATOR = Pair("de.kaipho.genplus.generator.generator", Generator::class.java)
    val TOKEN = Pair("de.kaipho.genplus.generator.core.scanner.token", Token::class.java)
    val COMMANDS = Pair("de.kaipho.genplus.generator.commands", GenCommand::class.java)


    fun <A : Annotation, F> forEachClazzInstance(annotation: Pair<String, Class<A>>, instanceClazz: Class<F>, inline: (F, A) -> Unit) {
        val classes = Reflections(annotation.first).getTypesAnnotatedWith(annotation.second)
        classes.forEach { clazz ->
            ReflectionUtils.getConstructors(clazz).forEach {
                val instance = it.newInstance()
                val clazzAnnotation = clazz.getAnnotation(annotation.second)
                inline(instance as F, clazzAnnotation)
            }
        }
    }

    fun <F> forEachClazzInstance(instanceClazz: Class<F>, inline: (F) -> Unit) {
        val classes = Reflections(base).getSubTypesOf(instanceClazz)
        classes.forEach { clazz ->
            inline(clazz.getField("INSTANCE").get(clazz) as F)
        }
    }
}