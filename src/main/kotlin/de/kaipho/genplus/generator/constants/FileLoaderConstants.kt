package de.kaipho.genplus.generator.constants

/**
 * Created by tom on 21.07.16.
 */
object FileLoaderConstants {
    val IMPORTS = "imports"
    val IMPORTS_END = "${IMPORTS}_end"

    val EDITABLE_AREA = "editable_area"
    val EDITABLE_AREA_START = "${EDITABLE_AREA}_start"
    val EDITABLE_AREA_END = "${EDITABLE_AREA}_end"

    val PRIVATE_AREA = "private_area"
    val PRIVATE_AREA_START = "${PRIVATE_AREA}_start"
    val PRIVATE_AREA_END = "${PRIVATE_AREA}_end"
}