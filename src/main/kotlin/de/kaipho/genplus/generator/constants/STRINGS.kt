package de.kaipho.genplus.generator.constants

/**
 * Created by tom on 14.07.16.
 */
object STRINGS {
    fun FOLDER_NOT_EXIST(string: String) = "Folder '$string' not found, creating..."
    fun FOLDER_EXIST(string: String) = "Folder '$string' found :)"

    val MODEL_DIR = "/model"
    val BACKUP_DIR = "/backup"
    val SERVER_DIR = "/server"

    val PREFIX_PLACEHOLDER = "#PREFIX#"

    fun FILE_EXISTS(file: String) = "File $file already exists"

    const val RULE_VALIDATE = "#validate"
    const val RULE_INLINE = "#inline"
    const val RULE_LOCAL = "#local"
    const val RULE_DERIVED = "#derived"
    const val RULE_SECURED = "#secured"
    val IGNORED_CASE = "One case is ignored"
    val GET_NO_GENERICS = "Get no generic type, but needed one"
}

enum class Language {
    JAVA, TYPESCRIPT, HTML, SASS, ESP32, ALL, POM, SQL
}