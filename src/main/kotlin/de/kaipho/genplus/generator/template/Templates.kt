package de.kaipho.genplus.generator.template

import de.kaipho.genplus.generator.constants.Language

/**
 * Created by tom on 13.07.16.
 */
interface Template {
    val path: String
    val lang: Language
}

enum class Templates(override val path: String, override val lang: Language) : Template {
    // CODE
    GETTER("java/core/getter", Language.JAVA),
    SETTER("java/core/setter", Language.JAVA),

    EQUALS_HASHCODE("java/core/hashCodeEquals", Language.JAVA),
    HASHCODE_LINE("java/core/hashCodeLine", Language.JAVA),
    EQUALS_LINE("java/core/equalsLine", Language.JAVA),

    // DOMAIN
    DOMAIN_CLASS("java/domain/domainClass", Language.JAVA),
    DOMAIN_CLASS_REPOSITORY_PROXY("java/domain/domainClassRepositoryProxy", Language.JAVA),
    DOMAIN_CLASS_INTERFACE("java/domain/domainClassInterface", Language.JAVA),
    DOMAIN_CLASS_INTERFACE2("java/domain/domainClassInterface2", Language.JAVA),
    DOMAIN_CLASS_VISITOR("java/domain/domainClassVisitor", Language.JAVA),
    DOMAIN_CLASS_COMPLETE_VISITOR("java/domain/domainClassCompleteVisitor", Language.JAVA),
    DOMAIN_CLASS_SIMPLE_VISITOR("java/domain/domainClassSimpleVisitor", Language.JAVA),
    ABSTRACT_DOMAIN_CLASS("java/domain/abstractDomainClass", Language.JAVA),
    DOMAIN_REPOSITORY("java/domain/domainRepository", Language.JAVA),
    DOMAIN_ASSOCIATION("java/domain/domainAssociation", Language.JAVA),
    DOMAIN_BUILDER("java/domain/domainBuilder", Language.JAVA),
    DOMAIN_BUILDER_METHOD("java/domain/domainBuilderMethod", Language.JAVA),
    JAVA_DOMAIN_DERIVED_MANAGER("java/domain/derivedManager", Language.JAVA),
    JAVA_SERVICE("java/service/serviceInterface", Language.JAVA),
    JAVA_SERVICE_IMPL("java/service/service", Language.JAVA),
    JAVA_SERVICE_TEST("java/service/serviceTest", Language.JAVA),
    JAVA_CONTROLLER("java/service/controller", Language.JAVA),
    JAVA_MAIN("java/core/main", Language.JAVA),
    JAVA_DATA_REST_CONFIG("java/core/dataRest", Language.JAVA),
    JAVA_SECURITY_CONFIG("java/security/securityConfigJwt", Language.JAVA),
    JAVA_JWT_SECURITY_FILTER("java/security/jwtSecurityFilter", Language.JAVA),
    JAVA_JWT_TOKEN_PROVIDER("java/security/jwtTokenProvider", Language.JAVA),
    JAVA_USER_VM("java/security/loginVM", Language.JAVA),

    TS_DOMAIN_SUPER_HTTP_SERVICE("typescript/core/httpService", Language.TYPESCRIPT),
    TS_HTTP_PAGEABLE("typescript/core/pageable", Language.TYPESCRIPT),
    TS_HTTP_SERVICE_CONSTANTS("typescript/core/httpServiceConstants", Language.TYPESCRIPT),
    TS_COMPONENT_CONSTANTS("typescript/core/componentConstants", Language.TYPESCRIPT),
    TS_DOMAIN_HTTP_SERVICE("typescript/domain/domainService", Language.TYPESCRIPT),
    TS_DOMAIN_ABSTRACT_HTTP_SERVICE("typescript/domain/domainAbstractService", Language.TYPESCRIPT),
    TS_DOMAIN_CLASS("typescript/domain/domainClass", Language.TYPESCRIPT),
    TS_DOMAIN_ABSTRACT_CLASS("typescript/domain/domainAbstractClass", Language.TYPESCRIPT),
    TS_DOMAIN_CONTROLLER("typescript/domain/domainController", Language.TYPESCRIPT),
    TS_DOMAIN_LIST_CONTROLLER("typescript/domain/domainListController", Language.TYPESCRIPT),
    TS_DOMAIN_DETAILS_CONTROLLER("typescript/domain/domainDetailsController", Language.TYPESCRIPT),
    TS_DOMAIN_ROUTES("typescript/domain/domainRoutes", Language.TYPESCRIPT),
    TS_DOMAIN_ROUTE("typescript/domain/domainRoute", Language.TYPESCRIPT),
    TS_DOMAIN_OBJECT("typescript/domain/domainObject", Language.TYPESCRIPT),
    TS_DOMAIN_OBJECT_VISITOR("typescript/domain/domainObjectVisitor", Language.TYPESCRIPT),
    TS_DOMAIN_VISITOR("typescript/domain/visitor", Language.TYPESCRIPT),
    TS_DOMAIN_REP_VISITOR("typescript/domain/domainRepVisitor", Language.TYPESCRIPT),
    TS_DOMAIN_TYPE_VISITOR("typescript/domain/domainTypeVisitor", Language.TYPESCRIPT),
    TS_SERVICE_CONTROLLER("typescript/service/service", Language.TYPESCRIPT),
    TS_HTTP_SERVICE("typescript/service/serviceHttp", Language.TYPESCRIPT),
    TS_MODULE("typescript/core/module", Language.TYPESCRIPT),

    HTML_DOMAIN_LIST("html/domain/domainList", Language.HTML),
    HTML_DOMAIN_FORM("html/domain/domainForm", Language.HTML),
    HTML_DOMAIN_DETAIL("html/domain/domainDetail", Language.HTML),
    HTML_SERVICE_COMPONENT("html/service/serviceComponent", Language.HTML),
    HTML_SERVICE_DELETE_COMPONENT("html/service/serviceDeleteComponent", Language.HTML),

    SASS_COLOR_FILE("scss/core/colors", Language.SASS),

    C_ESP32_DOMAIN_CLASS("esp32/domain/domainClass", Language.ESP32),
    C_ESP32_DOMAIN_JSON_SERVICE("esp32/domain/json", Language.ESP32),



}