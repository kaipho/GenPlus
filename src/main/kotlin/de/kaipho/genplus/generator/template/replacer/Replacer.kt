package de.kaipho.genplus.generator.template.replacer

import de.kaipho.genplus.generator.constants.Language

@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE)
@Retention(AnnotationRetention.RUNTIME)
annotation class Replacer(val name: String, val lang: Array<Language>)
