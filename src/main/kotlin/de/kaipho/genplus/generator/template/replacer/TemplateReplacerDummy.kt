package de.kaipho.genplus.generator.template.replacer

import de.kaipho.genplus.generator.template.replacer.TemplateReplacerVisitor
import de.kaipho.genplus.generator.template.TemplateReplacer

/**
 * Created by tom on 14.07.16.
 */
object TemplateReplacerDummy: TemplateReplacer<String>("dummy") {
    override fun insert(input: String, result: MutableList<String>, data: String) {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun accept(visitor: TemplateReplacerVisitor) {

    }
}