package de.kaipho.genplus.generator.template

import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.constants.Reflection
import de.kaipho.genplus.generator.template.TemplateReplacer
import de.kaipho.genplus.generator.template.replacer.*
import de.kaipho.genplus.generator.template.replacer.*
import java.util.*

/**
 * Created by tom on 13.07.16.
 */
object Placeholder {
    val placeholder: MutableMap<String, TemplateReplacer<*>> = HashMap()

    init {
        listOf(
                TemplateReplacerImplements,
                TemplateReplacerPackage,
                TemplateReplacerTable,
                TemplateReplacerValidators,
                TemplateReplacerEqualsHashCode,
                TemplateReplacerGetter,
                TemplateReplacerSetter,
                TemplateReplacerEqualsLine,
                TemplateReplacerHashCodeLine
        ).forEach {
            placeholder.put(it.name, it)
        }

        Reflection.forEachClazzInstance(Reflection.REPLACER, TemplateReplacer::class.java) { replacer, annotation ->
            replacer.name = annotation.name
            placeholder.put(replacer.name, replacer)
        }
    }

    fun get(name: String, lang: Language): TemplateReplacer<*> {
        val first = name.split("(")[0]
        if (!placeholder.contains(first)) {
            throw RuntimeException("Placeholder $first not supported at all -> $name" )
        }
        var result = this.placeholder[first]!!
        if (result.lang != lang && result.lang != Language.ALL) {
            throw RuntimeException("Placeholder $first not supported in $lang")
        }
        if (name.split("(").size > 1) {
            val newResult = result.javaClass.newInstance()
            newResult.name = result.name
            newResult.lang = result.lang
            result = newResult
            result.id = name.split("(")[1].removeSuffix(")").toInt()
        }
        return result
    }
}