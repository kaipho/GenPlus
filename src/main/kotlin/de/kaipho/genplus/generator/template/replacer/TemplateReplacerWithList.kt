package de.kaipho.genplus.generator.template.replacer

import de.kaipho.genplus.generator.template.TemplateReplacer

/**
 * Created by tom on 17.07.16.
 */
abstract class TemplateReplacerWithList(name: String = "") : TemplateReplacer<String>(name) {
    fun insert(input: String, result: MutableList<String>, data: List<String>) {
        data.forEach {
            result.add(this.replaceOneLine(input, it))
        }
    }
}