package de.kaipho.genplus.generator.template.replacer

import de.kaipho.genplus.generator.store.SettingsStore
import de.kaipho.genplus.generator.template.TemplateReplacer

/**
 * Created by tom on 14.07.16.
 */
object TemplateReplacerPackage : TemplateReplacer<String>("package") {
    override fun insert(input: String, result: MutableList<String>, data: String) {
        result.add(this.replaceOneLine(name, SettingsStore.getPrefixPackage()))
    }

    override fun accept(visitor: TemplateReplacerVisitor) {
        visitor.visit(this)
    }
}