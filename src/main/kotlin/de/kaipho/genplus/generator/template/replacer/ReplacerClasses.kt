package de.kaipho.genplus.generator.template.replacer

import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.generator.i18n.JsonStore
import de.kaipho.genplus.generator.template.TemplateReplacer

/**
 * Replacer für die Templates
 */

@Replacer(name = "comment", lang = arrayOf(Language.JAVA, Language.TYPESCRIPT))
class TemplateReplacerComment : TemplateReplacer<String>() {
    override fun insert(input: String, result: MutableList<String>, data: String) {
        if (data.isEmpty()) {
            return
        }
        val actual = data.removeSurrounding("/*", "*/").trim().removeSurrounding("\n").split("\n")

        result.add("/**")
        actual.forEach {
            result.add(" * $it")
        }
        result.add(" */")
    }

    override fun accept(visitor: TemplateReplacerVisitor) {
        visitor.visit(this)
    }
}

@Replacer(name = "annotations", lang = arrayOf(Language.JAVA))
class TemplateReplacerAnnotations : TemplateReplacer<String>() {
    override fun accept(visitor: TemplateReplacerVisitor) {
        visitor.visit(this)
    }
}

@Replacer(name = "body", lang = arrayOf(Language.JAVA))
class TemplateReplacerBody : TemplateReplacer<String>() {
    override fun accept(visitor: TemplateReplacerVisitor) {
        visitor.visit(this)
    }
}

@Replacer(name = "userattribute", lang = arrayOf(Language.JAVA))
class TemplateReplacerUserattribute : TemplateReplacer<String>() {
    override fun accept(visitor: TemplateReplacerVisitor) {
        visitor.visit(this)
    }
}

@Replacer(name = "params", lang = arrayOf(Language.JAVA))
class TemplateReplacerParams : TemplateReplacer<String>() {
    override fun accept(visitor: TemplateReplacerVisitor) {
        visitor.visit(this)
    }
}

@Replacer(name = "i18n", lang = arrayOf(Language.HTML, Language.TYPESCRIPT))
class TemplateReplacerI18n : TemplateReplacer<String>() {
    override fun insert(input: String, result: MutableList<String>, data: String) {
        JsonStore.add(data)
        result.add(this.replaceOneLine(input, data))
    }

    override fun accept(visitor: TemplateReplacerVisitor) {
        visitor.visit(this)
    }
}

@Replacer(name = "imports", lang = arrayOf(Language.JAVA, Language.TYPESCRIPT))
class TemplateReplacerImports : TemplateReplacer<String>() {
    override fun accept(visitor: TemplateReplacerVisitor) {
        visitor.visit(this)
    }
}

@Replacer(name = "path", lang = arrayOf(Language.JAVA, Language.TYPESCRIPT))
class TemplateReplacerPath : TemplateReplacer<String>() {
    override fun accept(visitor: TemplateReplacerVisitor) {
        visitor.visit(this)
    }
}

@Replacer(name = "generic", lang = arrayOf(Language.JAVA, Language.TYPESCRIPT))
class TemplateReplacerGeneric : TemplateReplacer<String>() {
    override fun accept(visitor: TemplateReplacerVisitor) {
        visitor.visit(this)
    }
}

@Replacer(name = "generated", lang = arrayOf(Language.JAVA))
class TemplateReplacerGenerated : TemplateReplacer<String>() {
    override fun accept(visitor: TemplateReplacerVisitor) {
        visitor.visit(this)
    }
}

@Replacer(name = "subpackage", lang = arrayOf(Language.JAVA))
class TemplateReplacerSubpackage : TemplateReplacer<String>() {
    override fun accept(visitor: TemplateReplacerVisitor) {
        visitor.visit(this)
    }
}

@Replacer(name = "id", lang = arrayOf(Language.JAVA))
class TemplateReplacerId : TemplateReplacer<String>() {
    override fun accept(visitor: TemplateReplacerVisitor) {
        visitor.visit(this)
    }
}

@Replacer(name = "accept", lang = arrayOf(Language.JAVA))
class TemplateReplacerAccept : TemplateReplacer<String>() {
    override fun accept(visitor: TemplateReplacerVisitor) {
        visitor.visit(this)
    }
}

@Replacer(name = "builder", lang = arrayOf(Language.JAVA))
class TemplateReplacerDomainBuilder : TemplateReplacerWithList() {
    override fun accept(visitor: TemplateReplacerVisitor) {
        visitor.visit(this)
    }
}

@Replacer(name = "builder_methods", lang = arrayOf(Language.JAVA))
class TemplateReplacerDomainBuilderMethod : TemplateReplacerWithList() {
    override fun accept(visitor: TemplateReplacerVisitor) {
        visitor.visit(this)
    }
}

@Replacer(name = "name", lang = arrayOf(Language.JAVA, Language.TYPESCRIPT))
class TemplateReplacerName : TemplateReplacer<String>() {
    override fun accept(visitor: TemplateReplacerVisitor) {
        visitor.visit(this)
    }
}

@Replacer(name = "associations", lang = arrayOf(Language.JAVA, Language.TYPESCRIPT))
class TemplateReplacerAssociations : TemplateReplacerWithList() {
    override fun accept(visitor: TemplateReplacerVisitor) {
        visitor.visit(this)
    }
}

@Replacer(name = "line", lang = arrayOf(Language.JAVA, Language.TYPESCRIPT))
class TemplateReplacerLine : TemplateReplacerWithList() {
    override fun accept(visitor: TemplateReplacerVisitor) {
        visitor.visit(this)
    }
}

object TemplateReplacerValidators : TemplateReplacer<String>("validators") {
    override fun accept(visitor: TemplateReplacerVisitor) {
        visitor.visit(this)
    }
}

object TemplateReplacerTable : TemplateReplacer<String>("table") {
    override fun accept(visitor: TemplateReplacerVisitor) {
        visitor.visit(this)
    }
}

object TemplateReplacerGetter : TemplateReplacerWithList("getter") {
    override fun accept(visitor: TemplateReplacerVisitor) {
        visitor.visit(this)
    }
}

object TemplateReplacerSetter : TemplateReplacerWithList("setter") {
    override fun accept(visitor: TemplateReplacerVisitor) {
        visitor.visit(this)
    }
}

object TemplateReplacerEqualsHashCode : TemplateReplacerWithList("equals_hashCode") {
    override fun accept(visitor: TemplateReplacerVisitor) {
        visitor.visit(this)
    }
}

@Replacer(name = "lower_class_name", lang = arrayOf(Language.JAVA, Language.TYPESCRIPT))
class TemplateReplacerLowerClassName : TemplateReplacerWithList() {
    override fun accept(visitor: TemplateReplacerVisitor) {
        visitor.visit(this)
    }
}

@Replacer(name = "upper_name", lang = arrayOf(Language.JAVA))
class TemplateReplacerUpperName : TemplateReplacer<String>() {
    override fun accept(visitor: TemplateReplacerVisitor) {
        visitor.visit(this)
    }
}

@Replacer(name = "class_name", lang = arrayOf(Language.JAVA, Language.TYPESCRIPT))
class TemplateReplacerClassName : TemplateReplacer<String>() {
    override fun accept(visitor: TemplateReplacerVisitor) {
        visitor.visit(this)
    }
}

object TemplateReplacerEqualsLine : TemplateReplacerWithList("equals_line") {
    override fun accept(visitor: TemplateReplacerVisitor) {
        visitor.visit(this)
    }
}

object TemplateReplacerHashCodeLine : TemplateReplacerWithList("hash_code_line") {
    override fun accept(visitor: TemplateReplacerVisitor) {
        visitor.visit(this)
    }
}

