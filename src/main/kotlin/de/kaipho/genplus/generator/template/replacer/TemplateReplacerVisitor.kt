package de.kaipho.genplus.generator.template.replacer

/**
 * Created by tom on 14.07.16.
 */
interface TemplateReplacerVisitor {
    fun visit(replacer: TemplateReplacerAssociations)
    fun visit(replacer: TemplateReplacerImplements)
    fun visit(replacer: TemplateReplacerClassName)
    fun visit(replacer: TemplateReplacerPackage)
    fun visit(replacer: TemplateReplacerComment)
    fun visit(replacer: TemplateReplacerAnnotations)
    fun visit(replacer: TemplateReplacerImports)
    fun visit(replacer: TemplateReplacerSubpackage)
    fun visit(replacer: TemplateReplacerValidators)
    fun visit(replacer: TemplateReplacerName)
    fun visit(replacer: TemplateReplacerTable)
    fun visit(replacer: TemplateReplacerGetter)
    fun visit(replacer: TemplateReplacerSetter)
    fun visit(replacer: TemplateReplacerEqualsHashCode)
    fun visit(replacer: TemplateReplacerUpperName)
    fun visit(replacer: TemplateReplacerEqualsLine)
    fun visit(replacer: TemplateReplacerHashCodeLine)
    fun visit(replacer: TemplateReplacerLowerClassName)
    fun visit(replacer: TemplateReplacerDomainBuilderMethod)
    fun visit(replacer: TemplateReplacerDomainBuilder)
    fun visit(replacer: TemplateReplacerLine)
    fun visit(replacer: TemplateReplacerI18n)
    fun visit(replacer: TemplateReplacerGeneric)
    fun visit(replacer: TemplateReplacerId)
    fun visit(replacer: TemplateReplacerAccept)
    fun visit(replacer: TemplateReplacerPath)
    fun visit(replacer: TemplateReplacerBody)
    fun visit(replacer: TemplateReplacerParams)
    fun visit(replacer: TemplateReplacerUserattribute)
    fun visit(replacer: TemplateReplacerGenerated)
}
