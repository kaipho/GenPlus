package de.kaipho.genplus.generator.template

import de.kaipho.genplus.generator.generator.SpecialGenerator
import de.kaipho.genplus.generator.generator.java.core.ExternalJavaGenerator
import de.kaipho.genplus.generator.generator.java.core.ExternalTemplate
import de.kaipho.genplus.generator.io.TemplateLoader
import java.util.*

/**
 * Created by tom on 13.07.16.
 */
object TemplateParser {

    fun parseTemplate(template: Template, generator: SpecialGenerator): MutableList<String> {
        val lines: MutableList<Pair<String, MutableList<TemplateReplacer<*>>>> = ArrayList()

        for(line:String in TemplateLoader.loadTemplate(template)) {
            if(line.startsWith("//STOP"))
                break
            lines.add(parseLine(line, template))
        }
        return generator.generate(lines)
    }
    fun parseTemplate(generator: ExternalJavaGenerator): MutableList<String> {
        return parseTemplate(ExternalTemplate(generator.templateString, generator.lang), generator)
    }

    private fun parseLine(line: String, template: Template): Pair<String, MutableList<TemplateReplacer<*>>> {
        var placeholder = ""
        var active = false

        val result: Pair<String, MutableList<TemplateReplacer<*>>> = Pair(line, ArrayList())

        line.forEach {
            if (it == '#') {
                if (!active) {
                    active = true
                } else {
                    active = false
                    val replacer = Placeholder.get(placeholder, template.lang)
                    result.second.add(replacer)
                    placeholder = ""
                }
            } else if (active) {
                placeholder += it
            }
        }

        return result
    }

}
