package de.kaipho.genplus.generator.template

import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerVisitor

/**
 * Created by tom on 14.07.16.
 */
abstract class TemplateReplacer<in D>(
        var name: String = "",
        var lang: Language = Language.ALL,
        var id: Int = -1
) {
    fun replaceOneLine(line: String, new: String): String {
        if (id == -1)
            return line.replace("#$name#", new)
        else
            return line.replace("#$name($id)#", new)
    }

    open fun insert(input: String, result: MutableList<String>, data: D) {
        result.add(this.replaceOneLine(input, "$data"))
    }

    abstract fun accept(visitor: TemplateReplacerVisitor)
}