package de.kaipho.genplus.generator.template.replacer

import de.kaipho.genplus.generator.template.replacer.TemplateReplacerVisitor
import de.kaipho.genplus.generator.template.TemplateReplacer

/**
 * Created by tom on 14.07.16.
 */
object TemplateReplacerImplements: TemplateReplacer<String>("implements") {
    override fun accept(visitor: TemplateReplacerVisitor) {
        visitor.visit(this)
    }
}