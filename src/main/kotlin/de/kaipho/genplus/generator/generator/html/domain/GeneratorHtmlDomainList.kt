package de.kaipho.genplus.generator.generator.html.domain

import de.kaipho.genplus.generator.common.convertPackageToFileSystem
import de.kaipho.genplus.generator.common.lowerFirstLetter
import de.kaipho.genplus.generator.constants.STRINGS
import de.kaipho.genplus.generator.core.obj.AssoziationTypeUser
import de.kaipho.genplus.generator.domain.obj.DomainClass
import de.kaipho.genplus.generator.generator.recursiveAssociationFetcher
import de.kaipho.genplus.generator.generator.typescript.ExternalTsGenerator
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerI18n
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLine
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerName

class GeneratorHtmlDomainList(val clazz: DomainClass) : ExternalTsGenerator() {
    override val templateString = "src/app/components/domain/xxx/xxx.list.component.html"
    override val replacer = arrayListOf(
            clazz.prefix.convertPackageToFileSystem().removeSuffix("/"),
            clazz.name.lowerFirstLetter()
    )

    override fun visit(replacer: TemplateReplacerName) {
        replacer.insert(clazz.prefix.convertPackageToFileSystem())
    }

    override fun visit(replacer: TemplateReplacerI18n) {
        when (replacer.id) {
            0 -> replacer.insert("title${clazz.prefix}.list${clazz.name.lowerFirstLetter()}")
            1 -> replacer.insert("prev")
            2 -> replacer.insert("results")
            3 -> replacer.insert("next")
            else -> throw RuntimeException(STRINGS.IGNORED_CASE)
        }
    }

    override fun visit(replacer: TemplateReplacerLine) {
        when (replacer.id) {
            0 -> {
                recursiveAssociationFetcher(clazz, false).filter {
                    it.type !is AssoziationTypeUser
                }.forEach {
                    replacer.insert("<mat-option value=\"${it.id}\">{{ 'domain${clazz.prefix}.${clazz.name.lowerFirstLetter()}.${it.name}' | translate }}</mat-option>")
                }
            }
            1 -> createMenu(replacer, clazz)
        }
    }

    private fun createMenu(replacer: TemplateReplacerLine, clazz: DomainClass) {
        val createFunctions = clazz.getAllCreateFunctions()
        if (createFunctions.isEmpty()) return

        if (createFunctions.size == 1) {
            val function = createFunctions[0]
            createSingleMethod(function.owner.prefix, function.name).insertInto(replacer)
            return
        }

        val functionStrings = createFunctions.map { createMultipleMenuEntry(it.owner.prefix, it.name) }
        createMultipleMethod(functionStrings).insertInto(replacer)
    }

    fun createSingleMethod(packagee: String, name: String) = """
    |<button [matTooltip]="'functions$packagee.$name.title' | translate" mat-icon-button (click)="$name()">
    |    <mat-icon>add</mat-icon>
    |</button>
    """

    fun createMultipleMethod(entries: List<String>): String {
        val builder = StringBuilder("""
        |<span>
        |   <mat-menu #createMenu>""")

        entries.forEach {
            builder.append(it)
        }

        return builder.append("""
        |   </mat-menu>
        |   <button [matTooltip]="'common.createObj' | translate" mat-icon-button [mat-menu-trigger-for]="createMenu">
        |       <mat-icon>add</mat-icon>
        |   </button>
        |</span>
        """).toString()
    }

    fun createMultipleMenuEntry(packagee: String, name: String) = """
    |       <button mat-menu-item (click)="$name()">
    |           {{ 'functions$packagee.$name.title' | translate }}
    |       </button>
    """
}
