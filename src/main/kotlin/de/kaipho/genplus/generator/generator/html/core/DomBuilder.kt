package de.kaipho.genplus.generator.generator.html.core

import de.kaipho.genplus.generator.generator.i18n.JsonStore
import java.util.*

fun tag(name: String, vararg attributes: Attribute, inline: Tag.()->Unit): Tag {
    return tagArray(name, attributes, inline)
}
fun tagArray(name: String, attributes:Array<out Attribute>, inline: Tag.()->Unit): Tag {
    val tag = Tag(name, attributes)
    inline.invoke(tag)
    return tag
}

fun Tag.tag(name: String, vararg attributes: Attribute, inline: Tag.()->Unit) {
    this.tagArray(name, attributes, inline)
}
fun Tag.tag(tag: Tag) {
    this.inner.add(tag)
}
fun Tag.tagArray(name: String, attributes:Array<out Attribute>, inline:(Tag.()->Unit)?): Tag {
    val tag = Tag(name, attributes)
    inline?.invoke(tag)
    this.inner.add(tag)
    return this
}

fun Tag.text(text:String) {
    this.inner.add(Text(text))
}
fun Tag.translate(text:String) {
    JsonStore.add(text)
    this.inner.add(Text("{{ '$text' | translate }}"))
}

fun tagOpen(name: String, vararg attributes: Attribute): String {
    return tagOpenArray(name, attributes)
}

fun tagOpenArray(name: String, attributes:Array<out Attribute>): String {
    if(attributes.isEmpty()) {
        return "<$name>"
    }
    return "<$name ${attributes.joinToString(separator = " ")}>"
}

fun tagClose(name: String): String {
    return "</$name>"
}

data class Attribute(val name:String, val value:String? = null) {
    override fun toString(): String {
        var result = name
        if(value != null) {
            result += "=\"$value\""
        }
        return result
    }
}

fun event(name:String, value:String? = null): Attribute {
    return Attribute("($name)", value)
}

fun onClick(value:String? = null): Attribute {
    return Attribute("(click)", value)
}

fun databind(name:String, value:String? = null): Attribute {
    return Attribute("[$name]", value)
}

fun fullbind(name:String, value:String? = null): Attribute {
    return Attribute("[($name)]", value)
}

fun model(value:String? = null): Attribute {
    return fullbind("ngModel", value)
}

fun placeholder(value:String? = null): Attribute {
    return databind("placeholder", value)
}

fun nobind(name:String, value:String? = null): Attribute {
    return Attribute("$name", value)
}

fun Tag.button(vararg attributes: Attribute, inline: Tag.()->Unit) {
    this.tagArray("button", attributes, inline)
}
fun Tag.input(vararg attributes: Attribute, inline: Tag.()->Unit): Tag {
    return tagArray("matInput", attributes, inline)
}
fun Tag.col(vararg attributes: Attribute, inline: Tag.()->Unit): Tag {
    return tagArray("form-col", attributes, inline)
}
fun Tag.filler(vararg attributes: Attribute) {
    this.tagArray("filler", attributes) {}
}
fun Tag.textarea(vararg attributes: Attribute, inline:(Tag.()->Unit)? = null) {
    this.tagArray("textarea", attributes, inline)
}
fun button(vararg attributes: Attribute, inline: Tag.()->Unit): Tag {
    return tagArray("button", attributes, inline)
}
fun col(vararg attributes: Attribute, inline: Tag.()->Unit): Tag {
    return tagArray("form-col", attributes, inline)
}
fun menu(vararg attributes: Attribute, inline: Tag.()->Unit): Tag {
    return tagArray("mat-menu", attributes, inline)
}
fun input(vararg attributes: Attribute, inline: Tag.()->Unit): Tag {
    return tagArray("matInput", attributes, inline)
}

interface Element {
    val name: String

    fun out(deep: Int = 0):List<String>
}

data class Text(override val name: String) : Element {
    override fun out(deep: Int): List<String> {
        return arrayListOf(tabbing(deep) + name)
    }

    override fun toString(): String {
        return name
    }
}

data class Tag(override val name: String, val attributes:Array<out Attribute>, val inner:MutableList<Element> = ArrayList()) : Element {
    override fun toString(): String {
        return tagOpenArray(name, attributes) + inner.joinToString(separator = "") + tagClose(name)
    }

    override fun out(deep: Int): List<String> {
        val list = arrayListOf(tabbing(deep) + tagOpenArray(name, attributes))
        inner.forEach {
            list.addAll(it.out(deep + 1))
        }
        list.add(tabbing(deep) + tagClose(name))
        return list
    }
}

fun tabbing(deep: Int): String {
    var actualDeep = deep
    var result = ""
    while(actualDeep > 0) {
        result += "\t"
        actualDeep --
    }
    return result
}
