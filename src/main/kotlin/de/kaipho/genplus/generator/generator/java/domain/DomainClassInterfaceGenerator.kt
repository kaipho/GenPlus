package de.kaipho.genplus.generator.generator.java.domain

import de.kaipho.genplus.generator.common.upperFirstLetter
import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.constants.STRINGS
import de.kaipho.genplus.generator.core.obj.AssoziationTypeCollection
import de.kaipho.genplus.generator.core.obj.AssoziationTypeInteger
import de.kaipho.genplus.generator.core.obj.AssoziationTypeOptional
import de.kaipho.genplus.generator.core.obj.AssoziationTypeUser
import de.kaipho.genplus.generator.core.scanner.KeywordHolder
import de.kaipho.genplus.generator.domain.obj.*
import de.kaipho.genplus.generator.generator.AbstractGenerator
import de.kaipho.genplus.generator.generator.core.File
import de.kaipho.genplus.generator.generator.core.Generator
import de.kaipho.genplus.generator.generator.core.RuleHelp
import de.kaipho.genplus.generator.generator.getInlinedAssociations
import de.kaipho.genplus.generator.generator.java.domain.DomainClassBuilderGenerator
import de.kaipho.genplus.generator.generator.java.domain.methods.MethodAddSingleToGenerator
import de.kaipho.genplus.generator.generator.java.domain.methods.MethodForI
import de.kaipho.genplus.generator.generator.java.domain.methods.MethodGetterGenerator
import de.kaipho.genplus.generator.generator.java.domain.methods.MethodSetterGenerator
import de.kaipho.genplus.generator.generator.java.service.CoreFramework
import de.kaipho.genplus.generator.generator.java.service.Imports
import de.kaipho.genplus.generator.generator.java.service.JavaImport
import de.kaipho.genplus.generator.generator.recursiveAssociationFetcher
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.store.SettingsStore
import de.kaipho.genplus.generator.template.TemplateParser
import de.kaipho.genplus.generator.template.Templates
import de.kaipho.genplus.generator.template.replacer.*
import de.kaipho.genplus.generator.domain.obj.*
import de.kaipho.genplus.generator.generator.java.core.*
import de.kaipho.genplus.generator.template.TemplateReplacer
import de.kaipho.genplus.generator.template.replacer.*
import java.util.*


@Generator(
        lang = Language.JAVA,
        template = Templates.DOMAIN_CLASS_INTERFACE,
        appliedOn = ExplicitDomainClass::class,
        file = File.JAVA_DOMAIN_CLASS_INTERFACE
)
class DomainClassInterfaceGenerator : AbstractGenerator<ExplicitDomainClass>() {
    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(self().name)
    }

    override fun visit(replacer: TemplateReplacerImplements) {
        var extends = "extends PersistentElement"
        if (self().extends.isNotEmpty()) {
            extends = extends + "," + self().extends.map { it.name }.joinToString()
        }
        replacer.insert(extends)
    }

    override fun visit(replacer: TemplateReplacerPackage) {
        lines.add(replacer.replaceOneLine(actual, SettingsStore.getPrefixPackage()))
    }

    override fun visit(replacer: TemplateReplacerComment) {
        replacer.insert(meta.data.comment)
    }

    override fun visit(replacer: TemplateReplacerImports) {
        val imports = Imports()

        if (RuleHelp.hasRule(self(), KeywordHolder.RULE_USER_ATTRIBUTE) != null) {
            imports.addCoreImport(CoreFramework.SECURITY_CONTEXT)
            imports.addJavaImport(JavaImport.DB_EXCEPTION)
        }

        if (SettingsStore.databaseEnabled()) {
            imports.addDomainRepository(self())
            imports.addJavaImport(JavaImport.LIST)
            imports.addJavaImport(JavaImport.DB_STATEMENT)

            imports.addCoreImport(CoreFramework.DB_CONNECTOR)
            imports.addCoreImport(CoreFramework.DB_FACADE)
            imports.addCoreImport(CoreFramework.DB_EXCEPTION)
        }

        self().extends.forEach {
            imports.addDomainImport(it)
        }
        fetchAllParents(self()).forEach {
            imports.addDomainCompleteVisitorImport(it)
        }

        fetchAllNonAbstractChildClassesRekursiv(self()).forEach {
            imports.addDomainRepository(it)
        }

        recursiveAssociationFetcher(self(), true).union(getInlinedAssociations(meta.data)).forEach(imports::addAssociationImport)

        recursiveAssociationFetcher(self(), false).forEach {
            if (it.type is AssoziationTypeUser && it.type.clazz !is AbstractDomainClass)
                imports.addDomainImport(it.type.clazz)
            if (it.type is AssoziationTypeCollection && it.type.inner is AssoziationTypeUser && it.type.inner.clazz !is AbstractDomainClass) {
                imports.addDomainImport(it.type.inner.clazz)
            }
        }

        ClassStore.domain.domainClasses.filter { it is AbstractDomainClass }.forEach { clazz ->
            fetchAllAssociationsAndChildClasses(clazz).filter { it.name == getData().name }.forEach {
                imports.addDomainVisitorImport(clazz)
            }
        }
        self().associations.filter { it.isDerived() }.forEach {
            imports.addImport("domain.core.DerivedManager")
        }
        replacer.insertAll(imports.getimports())
    }

    override fun visit(replacer: TemplateReplacerSubpackage) {
        replacer.insert(meta.data.prefix)
    }

    override fun visit(replacer: TemplateReplacerGetter) {
/*        meta.data.associations.filter {
            it.rules.contains(InlineRule())
        }.forEach { association ->
            if (association.type is AssoziationTypeUser) {
                recursiveAssociationFetcher(association.type.clazz as DomainClass, false).forEach {
                    val getterGenerator = MethodGetterGenerator(MethodForI(it.name, it.type.javaAbstractRep()))
                    replacer.insertAll(TemplateParser.parseTemplate(getterGenerator))
                }
            } else {
                throw RuntimeException(STRINGS.RULE_INLINE + " must be on UserTypes")
            }
        }*/
        self().associations.forEach { getterAssociationAbstract(it) { it -> replacer.insertAll(it)} }
    }

    override fun visit(replacer: TemplateReplacerLine) {
        when (replacer.id) {
            1 -> replacer.insert(recursiveAssociationFetcher(meta.data, false)
                    .filter { it.type !is AssoziationTypeCollection }
                    .map { "${it.type.javaAbstractRep()} ${it.name}" }
                    .joinToString())
            2 -> recursiveAssociationFetcher(meta.data, false)
                    .filter { it.type !is AssoziationTypeCollection }.forEach {
                replacer.insert("result.set${it.name.upperFirstLetter()}(${it.name});")
            }
            3 -> {
                val sql = if(RuleHelp.hasRule(self(), KeywordHolder.RULE_USER_ATTRIBUTE) != null) {
                    "SELECT obj.id, obj.instanceof FROM obj, link WHERE obj.instanceof = ${self().id} AND obj.id = link.toObj AND link.fromObj = ? AND link.instanceof = -1"
                } else {
                    "SELECT id, instanceof FROM obj WHERE instanceof = ${self().id}"
                }

                if (SettingsStore.databaseEnabled()) {
                    replacer.insert("static ${self().name} findOneById(Long id) {")
                    replacer.insert("   if(id == null) return null;")
                    replacer.insert("   return ${getClassCreation(self(), "id")};")
                    replacer.insert("}")
                    """
                    |static List<${self().name}> findAll() throws DbException {
                    """.insertInto(replacer)
                    if(RuleHelp.hasRule(self(), KeywordHolder.RULE_USER_ATTRIBUTE) != null) {
                        """
                        |try {
                        |    statement.setLong(1, SecurityContext.getUser().getId());
                        |} catch (SQLException e) {
                        |    throw new DbException(e);
                        |}
                        """.insertInto(replacer)
                    }
                    """
                    |    return DbFacade.getAll(${self().id}L, (classId, id) -> ${getClassCreation(self(), "id")});
                    |}
                    |static List<${self().name}> findBy(Long association, Object s) throws DbException {
                    |    return DbFacade.getByIdAndAssociationLike(${self().id}L, association, s)
                    |                   .stream()
                    |                   .map(${self().name}::findOneById) // TODO needs optimisation!
                    |                   .collect(Collectors.toList());
                    |}
                    """.insertInto(replacer)
                } else {
                    replacer.insert("static ${self().name} findOneById(Long id) {")
                    replacer.insert("   throw new RuntimeException(\"DB not supported!\");")
                    replacer.insert("}")
                }
            }
            4 -> replacer.insert(getClassCreation(self()))
        }
    }

    override fun visit(replacer: TemplateReplacerDomainBuilder) {
        replacer.insert(actual, lines, TemplateParser.parseTemplate(Templates.DOMAIN_BUILDER, DomainClassBuilderGenerator(meta.data)))
    }

    override fun visit(replacer: TemplateReplacerSetter) {
        self().associations.forEach { setterAssociationAbstract(it) { it -> replacer.insertAll(it)} }
    }

    override fun visit(replacer: TemplateReplacerAccept) {
        val visitable = ArrayList<IClass>()
        ClassStore.domain.domainClasses.filter { it is AbstractDomainClass }.forEach { clazz ->
            fetchAllAssociationsAndChildClasses(clazz).filter { it.name == getData().name }.forEach {
                addClassToList(visitable, clazz)
            }
        }
        visitable.forEach {
            replacer.insert("public <D> D accept(${it.name}Visitor<D> visitor);")
            replacer.insert("public void accept(${it.name}SimpleVisitor visitor);")
        }
        getAllParents(self().extends).forEach {
            replacer.insert("public <D> D accept(${it.name}CompleteVisitor<D> visitor);")
        }
    }

    fun getAllParents(clazz: List<AbstractClass>): MutableList<AbstractClass> {
        val list: MutableList<AbstractClass> = ArrayList()
        clazz.forEach {
            list.add(it)
            if (it.extends.isNotEmpty()) {
                list.addAll(getAllParents(it.extends))
            }
        }
        return list
    }
}
