package de.kaipho.genplus.generator.generator.typescript.service

import de.kaipho.genplus.generator.common.lowerFirstLetter
import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.core.obj.AssoziationTypeCollection
import de.kaipho.genplus.generator.core.obj.AssoziationTypeUser
import de.kaipho.genplus.generator.core.obj.UserClassVisitor
import de.kaipho.genplus.generator.core.obj.service.ClassDummy
import de.kaipho.genplus.generator.core.obj.service.Function
import de.kaipho.genplus.generator.core.obj.service.ServiceClass
import de.kaipho.genplus.generator.domain.obj.*
import de.kaipho.genplus.generator.generator.AbstractGenerator
import de.kaipho.genplus.generator.generator.core.File
import de.kaipho.genplus.generator.generator.core.Generator
import de.kaipho.genplus.generator.generator.core.Ref
import de.kaipho.genplus.generator.generator.typescript.core.Imports
import de.kaipho.genplus.generator.template.Templates
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerClassName
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerImports
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLine
import java.util.*

@Generator(
        lang = Language.TYPESCRIPT,
        template = Templates.TS_HTTP_SERVICE,
        appliedOn = ServiceClass::class,
        file = File.TS_HTTP_SERVICE,
        dependsOn = Ref.IS_SERVICECLASS_EXPORT
)
class GeneratorHttpService : AbstractGenerator<ServiceClass>() {
    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(getData().name)
    }

    override fun visit(replacer: TemplateReplacerImports) {
        val imports = Imports("service/functions/http")
        getData().functions.forEach {
            if (it.returnType is AssoziationTypeUser)
                imports.addDomainImport((it.returnType as AssoziationTypeUser).clazz)
            else if (it.returnType is AssoziationTypeCollection && (it.returnType as AssoziationTypeCollection).inner is AssoziationTypeUser) {
                imports.addDomainImport(((it.returnType as AssoziationTypeCollection).inner as AssoziationTypeUser).clazz)
            }
        }
        replacer.insertAll(imports.getImports())
    }

    override fun visit(replacer: TemplateReplacerLine) {
        when (replacer.id) {
            0 -> createMethod(replacer)
            1 -> createInstance(replacer)
        }
    }

    private fun createMethod(replacer: TemplateReplacerLine) {
        getData().functions.forEach { function ->
            replacer.insert("public ${function.name}(${getParams(function)}): Observable<any> {")
            replacer.insert("   return this._http.post(`${function.getFilePrefix()}/${function.name}`, ${getUrlParams(function)}).pipe(map(${getMapPart(function)}));")
            replacer.insert("}")
        }
    }

    private fun getMapPart(function: Function): String {
        val default = "res => res.body"
        if (function.returnType is AssoziationTypeUser) {
            return (function.returnType as AssoziationTypeUser).acceptClass(object : UserClassVisitor<String> {
                override fun visitPrimitive(association: AssoziationTypeUser, clazz: PrimitiveClass): String {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun visitCategory(association: AssoziationTypeUser, clazz: CategoryClass): String {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun visitDomain(association: AssoziationTypeUser, clazz: DomainClass): String {
                    return "res => ${self().name}HttpService._extractOne(res.body, new ${function.returnType.name}())"
                }

                override fun visitConstant(association: AssoziationTypeUser, clazz: Constant): String {
                    return default
                }

            })
        } else if (function.returnType is AssoziationTypeCollection) {
            if ((function.returnType as AssoziationTypeCollection).inner is AssoziationTypeUser) {
                return "res => ${self().name}HttpService._extractObjList(res, '${(function.returnType as AssoziationTypeCollection).inner.name.lowerFirstLetter()}')"
            } else {
                return "res => res.body"
            }
        } else {
            return default
        }
    }

    private fun getUrlParams(function: Function): String {
        var result = "{"
        if (function.extends !is ClassDummy) {
            result += "${function.extends.name.lowerFirstLetter()} : ${function.extends.name.lowerFirstLetter()}, "
        }
        function.params.forEach { param ->
            result += "${param.name} : ${param.name}, "
        }
        return result.removeSuffix(",") + "}"
    }


    private fun getParams(function: Function): String {
        var result = ""
        if (function.extends !is ClassDummy) {
            result += "${function.extends.name.lowerFirstLetter()}: number, "
        }
        function.params.forEach { param ->
            if (param.type is AssoziationTypeUser) {
                result += "${param.name}: number, "
            } else {
                result += "${param.name}: ${param.type.typescriptRep()}, "
            }
        }
        return result.removeSuffix(", ")
    }

    private fun createInstance(replacer: TemplateReplacerLine) {
        val lines = HashSet<String>()
        getData().functions.forEach {
            if (it.returnType is AssoziationTypeUser) {
                (it.returnType as AssoziationTypeUser).acceptClass(object : UserClassVisitor<Unit> {
                    override fun visitPrimitive(association: AssoziationTypeUser, clazz: PrimitiveClass) {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                    override fun visitCategory(association: AssoziationTypeUser, clazz: CategoryClass) {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                    override fun visitDomain(association: AssoziationTypeUser, clazz: DomainClass) {
                        if (clazz is ExplicitDomainClass) {
                            lines.add("case '${it.returnType.name.lowerFirstLetter()}': return new ${it.returnType.name}();")
                        } else {
                            lines.add("case '${it.returnType.name.lowerFirstLetter()}': return <${it.returnType.name}>{};")
                        }
                    }

                    override fun visitConstant(association: AssoziationTypeUser, clazz: Constant) {
                        // no obj creation
                    }

                })
            } else if (it.returnType is AssoziationTypeCollection && (it.returnType as AssoziationTypeCollection).inner is AssoziationTypeUser) {
                val clazz = (it.returnType as AssoziationTypeCollection).inner as AssoziationTypeUser
                if (clazz.clazz is ExplicitDomainClass) {
                    lines.add("case '${clazz.name.lowerFirstLetter()}': return new ${clazz.name}();")
                } else {
                    lines.add("case '${clazz.name.lowerFirstLetter()}': return <${clazz.name}>{};")
                }
            }
        }
        replacer.insertAll(lines.toList())
    }
}
