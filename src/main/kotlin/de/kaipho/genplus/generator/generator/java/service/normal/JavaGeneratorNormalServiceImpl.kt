package de.kaipho.genplus.generator.generator.java.service.normal

import de.kaipho.genplus.generator.common.convertPackageToFileSystem
import de.kaipho.genplus.generator.common.lowerFirstLetter
import de.kaipho.genplus.generator.core.obj.service.InjectableRepository
import de.kaipho.genplus.generator.core.obj.service.InjectableService
import de.kaipho.genplus.generator.core.obj.service.InjectableVisitor
import de.kaipho.genplus.generator.core.obj.service.ServiceClass
import de.kaipho.genplus.generator.domain.obj.SecuredRule
import de.kaipho.genplus.generator.generator.core.GeneratorType
import de.kaipho.genplus.generator.generator.java.core.ExternalJavaGenerator
import de.kaipho.genplus.generator.generator.java.core.Functions
import de.kaipho.genplus.generator.generator.java.service.Imports
import de.kaipho.genplus.generator.generator.java.service.getImportsForFunction
import de.kaipho.genplus.generator.template.replacer.*
import de.kaipho.genplus.generator.template.replacer.*

class JavaGeneratorNormalServiceImpl(val clazz: ServiceClass) : ExternalJavaGenerator() {
    override val templateString = "service/xxx/xxxImpl.java"
    override val replacer = arrayListOf(clazz.prefix.convertPackageToFileSystem().removeSuffix("/"),
                                        clazz.name)
    override val mode: GeneratorType = GeneratorType.MERGE

    override fun visit(replacer: TemplateReplacerSubpackage) {
        replacer.insert(clazz.prefix)
    }

    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(clazz.name)
    }

    override fun visit(replacer: TemplateReplacerAssociations) {
        clazz.injectable.forEach { injectable ->
            val className = injectable.forClass.name
            val varName = injectable.forClass.name.lowerFirstLetter()
            injectable.accept(object : InjectableVisitor {
                override fun visit(injectable: InjectableService) {
                    replacer.insert("private $className $varName = $className.getInstance();")
                }

                override fun visit(injectable: InjectableRepository) {
                    // TODO remove, not longer needed
                }
            })
        }
    }

    override fun visit(replacer: TemplateReplacerImports) {
        val imports = Imports()
        getImportsForFunction(clazz.functions, imports)
        clazz.injectable.forEach { injectable ->
            injectable.accept(object : InjectableVisitor {
                override fun visit(injectable: InjectableRepository) {
                    // TODO remove, not longer needed
                }

                override fun visit(injectable: InjectableService) {
                    imports.addServiceImport(injectable.forClass)
                }
            })
        }
        replacer.insertAll(imports.getimports())
    }

    override fun visit(replacer: TemplateReplacerLine) {
        clazz.functions.forEach { function ->
            val returnType = function.returnType.javaAbstractRep()
            val name = function.name
            val parameter = Functions.getParams(function.params, function.extends)
            if (function.comment != "") {
                replacer.insert("/**")
                replacer.insert(" * ${function.comment.removeSuffix("*/").removePrefix("/*").trim()}")
                replacer.insert(" */")
            }
            if (function.rules.filter { it is SecuredRule }.isNotEmpty()) {
                replacer.insert("// Secured by proxy...")
            }
            replacer.insert("public $returnType $name($parameter) {")
            replacer.insert("   // TODO implement $returnType $name(...)")
            replacer.insert("   throw new RuntimeException(\"Not implemented\");")
            replacer.insert("}")
        }
    }
}
