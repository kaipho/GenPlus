package de.kaipho.genplus.generator.generator.java.special

import de.kaipho.genplus.generator.common.upperFirstLetter
import de.kaipho.genplus.generator.core.exceptions.GenerationException
import de.kaipho.genplus.generator.domain.obj.*
import de.kaipho.genplus.generator.domain.obj.*
import de.kaipho.genplus.generator.generator.i18n.JsonStore
import de.kaipho.genplus.generator.generator.insertInto
import de.kaipho.genplus.generator.generator.java.persistence.ClassIdGenerator
import java.util.*

object ValidationGenerator {

    val supportedAnnotationValidators = hashMapOf(
            "min" to 1,
            "max" to 1,
            "notEmpty" to 0,
            "unique" to 0,
            "hierarchy" to 1,
            "allowedValues" to 1,
            "email" to 0,
            "pattern" to 1
    )

    fun getJavaValidationPreLogic(validators: ValidateRule, onClass: IClass, association: Association): List<String> {
        val result = ArrayList<String>()

        validators.rule.forEach {
            precheck(it)
            it.accept(object : RuleVisitor {
                override fun visit(rule: MinRule) {

                }

                override fun visit(rule: RuleWithParam) {
                    when (rule.name) {
                        "pattern" -> {
                            result.add("private static final Pattern P_${association.name.upperFirstLetter()} = Pattern.compile(\"${rule.params[0].replace("\\", "\\\\")}\");")
                        }
                    }
                }
            })
        }

        return result
    }

    fun getJavaValidationLogic(validators: ValidateRule, onClass: IClass, association: Association): List<String> {
        val result = ArrayList<String>()
        validators.rule.forEach {
            precheck(it)
            val error = JsonStore.add("validation.errors.${it.name}${onClass.prefix.replace("/", ".")}.${association.name}")
            val throwError = "throw new ValidationException(\"$error\");"
            it.accept(object : RuleVisitor {
                override fun visit(rule: MinRule) {
                    when (rule.name) {
                        "notEmpty" -> {
                            """
                            |   if(${association.name} == null || ${association.name}.trim().isEmpty()) {
                            |       $throwError
                            |   }
                            """.insertInto(result)
                        }
                        "unique" -> {
                            val type = onClass.name
                            """
                            |   List<$type> foundingOf$type = $type.findBy(MetadataKeys.${ClassIdGenerator.getAssociationValName(association)}, ${association.name});
                            |   if (!foundingOf$type.isEmpty()) {
                            |      $throwError
                            |   }
                            """.insertInto(result)
                        }
                    }
                }

                override fun visit(rule: RuleWithParam) {
                    when (rule.name) {
                        "pattern" -> {
                            """
                            |   if(!P_${association.name.upperFirstLetter()}.matcher(${association.name}).matches()) {
                            |       $throwError
                            |   }
                            """.insertInto(result)
                        }
                        "min" -> {
                            """
                            |   if(${association.name} < ${rule.params[0]}) {
                            |       $throwError
                            |   }
                            """.insertInto(result)
                        }
                        "max" -> {
                            """
                            |   if(${association.name} > ${rule.params[0]}) {
                            |       $throwError
                            |   }
                            """.insertInto(result)
                        }
                    }
                }
            })
        }
        return result
    }

    fun precheck(rule: Rule) {
        if(!supportedAnnotationValidators.contains(rule.name)) {
            throw GenerationException("${rule.name} it nicht erlaubt!")
        }
        rule.accept(object : RuleVisitor {
            override fun visit(rule: MinRule) {
                if(!supportedAnnotationValidators.contains(rule.name))
                if (!supportedAnnotationValidators.contains(rule.name) || supportedAnnotationValidators[rule.name] != 0) {
                    throw GenerationException("${rule.name} nur ohne Parameter erlaubt")
                }
            }

            override fun visit(rule: RuleWithParam) {
                if (!supportedAnnotationValidators.contains(rule.name) || supportedAnnotationValidators[rule.name] != rule.params.size) {
                    throw GenerationException("${rule.name} benötigt ${rule.params.size} Parameter")
                }
            }
        })
    }
}
