package de.kaipho.genplus.generator.generator.i18n

import de.kaipho.genplus.generator.core.obj.options.Option
import de.kaipho.genplus.generator.io.FileSystem
import de.kaipho.genplus.generator.store.SettingsStore
import org.json.JSONObject
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

/**
 * Loader to parse an existing JSON file.
 */
object JsonFileLoader {
    val src = "web/src/assets/i18n/"
    val json = ".json"

    fun loadI18nFiles(): MutableMap<String, JSONObject> {
        val result = HashMap<String, JSONObject>()
        SettingsStore.options.get(Option.USER_LANGS).forEach { lang ->
            FileSystem.createFile(SettingsStore.runningPath, src, lang, json)
            var file = String(Files.readAllBytes(Paths.get("${SettingsStore.runningPath}/$src$lang$json")))
            if (file.isEmpty()) {
                file = "{}"
            }
            val jsonObj = JSONObject(file)

            result.put(lang, jsonObj)
        }
        return result
    }

    fun writeAllI18nFiles() {
        JsonStore.files?.forEach { entry ->
            FileSystem.writeJsonFile(SettingsStore.runningPath, src, entry.key, listOf(entry.value.toString(4)))
        }
    }
}