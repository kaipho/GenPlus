package de.kaipho.genplus.generator.generator.java

import de.kaipho.genplus.generator.core.obj.options.Option
import de.kaipho.genplus.generator.domain.obj.*
import de.kaipho.genplus.generator.generator.core.GeneratorType
import de.kaipho.genplus.generator.generator.core.RuleHelp
import de.kaipho.genplus.generator.generator.java.controller.JavaGeneratorDomainRController
import de.kaipho.genplus.generator.generator.java.controller.JavaGeneratorDomainRPathResolver
import de.kaipho.genplus.generator.generator.java.controller.JavaGeneratorDomainRToJson
import de.kaipho.genplus.generator.generator.java.core.*
import de.kaipho.genplus.generator.generator.java.domain.GeneratorMetadataKeys
import de.kaipho.genplus.generator.generator.java.domain.JavaGeneratorValidationProxy
import de.kaipho.genplus.generator.generator.java.domain.category.JavaGeneratorCategory
import de.kaipho.genplus.generator.generator.java.domain.category.JavaGeneratorCategoryLanguage
import de.kaipho.genplus.generator.generator.java.domain.category.JavaGeneratorCategoryRole
import de.kaipho.genplus.generator.generator.java.domain.constant.JavaGeneratorConstantImpl
import de.kaipho.genplus.generator.generator.java.domain.constant.JavaGeneratorConstantInterface
import de.kaipho.genplus.generator.generator.java.domain.primitive.JavaGeneratorPrimitives
import de.kaipho.genplus.generator.generator.java.domain.primitive.JavaGeneratorUnitClass
import de.kaipho.genplus.generator.generator.java.domain.primitive.JavaGeneratorUnitInstance
import de.kaipho.genplus.generator.generator.java.domain.visitor.JavaGeneratorDomainMasterBuilder
import de.kaipho.genplus.generator.generator.java.resources.database.SqlGeneratorInitScript
import de.kaipho.genplus.generator.generator.java.service.domainr.DataRequestService
import de.kaipho.genplus.generator.generator.java.service.domainr.JavaGeneratorDomainRService
import de.kaipho.genplus.generator.generator.java.service.normal.CommandImplGenerator
import de.kaipho.genplus.generator.generator.java.service.normal.JavaGeneratorNormalService
import de.kaipho.genplus.generator.generator.java.service.normal.JavaGeneratorNormalServiceImpl
import de.kaipho.genplus.generator.generator.java.service.normal.JavaGeneratorNormalServiceSecurityProxy
import de.kaipho.genplus.generator.generator.java.tests.cucumber.GivenDomainStepDefsGenerator
import de.kaipho.genplus.generator.generator.recursiveAssociationFetcher
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.store.SettingsStore

class JavaManager : ExternalGeneratorManager {
    val domainClazzes = ClassStore.domain.domainClasses.filter { it !is ExternalClass }

    override fun go() {
        // main class
        runLater(JavaGeneratorApplicationClass())
        runLater(JavaGeneratorBootstrapContext())

        domainClazzes.forEach {
            runLater(JavaGeneratorDomainRController(it))
            runLater(JavaGeneratorDomainRService(it))

            if (hasAssociationWithValidator(it)) {
                runLater(JavaGeneratorValidationProxy(it))
            }
        }
        ClassStore.services.forEach {
            runLater(JavaGeneratorNormalService(it))
            runLater(JavaGeneratorNormalServiceImpl(it))
            runLater(JavaGeneratorNormalServiceSecurityProxy(it))

            it.functions.forEach {
                runLater(CommandImplGenerator(it))
            }
        }
        ClassStore.primitives.forEach { clazz ->
            runLater(JavaGeneratorUnitClass(clazz))
            clazz.units.forEach { type ->
                runLater(JavaGeneratorUnitInstance(clazz, type))
            }
        }
        runLater(JavaGeneratorPrimitives())

        // categories

        runLater(JavaFileWithPath("domain/category/PersistentCategory.java"))
        runLast(JavaGeneratorCategoryLanguage())
        runLast(JavaGeneratorCategoryRole())
        ClassStore.categories.forEach {
            runLater(JavaGeneratorCategory(it))
        }
        if (SettingsStore.options.given(Option.DEFAULTS)) {
            runLast(JavaFileWithPath("service/core/CategoryServiceImpl.java"))
            runLast(JavaFileWithPath("service/core/CategoryNotExistInClassException.java"))
            runLast(JavaFileWithPath("service/core/CategoryAlreadyExistInHierarchyException.java"))
            runLast(JavaFileWithPath("service/core/CategoryInUseException.java"))
            runLast(JavaFileWithPath("service/security/UserCache.java"))
        }

        // constants

        runLater(JavaFileWithPath("domain/constant/core/PersistentConstant.java"))
        ClassStore.constants.forEach {
            runLater(JavaGeneratorConstantInterface(it))
            it.accept(object : ConstantVisitor<Unit> {
                override fun visit(constantClass: ConstantClass) {
                    runLater(JavaGeneratorConstantImpl(constantClass))
                }

                override fun visit(constantContainer: ConstantContainer) {
                    //
                }
            })
        }

        // i18n
        runLater(JavaFileWithPath("i18n/I18nContext.java"))
        runLater(JavaFileWithPath("i18n/I18nInstance.java"))
        runLater(JavaFileWithPath("i18n/I18nModelContext.java"))
        runLater(JavaFileWithPath("i18n/I18nParser.java"))
        runLater(JavaFileWithPath("i18n/UserHasNoLanguageDefined.java"))
        runLater(JavaFileWithPath("i18n/I18nModelConstantContext.java"))


        runLater(JavaGeneratorDomainRPathResolver())
        runLater(JavaGeneratorDomainRToJson())
        runLater(JavaGeneratorDomainMasterBuilder())

        runLater(GeneratorMetadataKeys())
        runLater(JavaPersistentElement())
        runLater(JavaPersistentElementCompleteVisitor())

        runLater(JavaFileWithPath("core/Pair.java"))
        runLater(JavaFileWithPath("core/Command.java"))
        runLater(CommandVisitorGenerator())

        // converter for controller package
        runLater(JavaFileWithPath("core/converter/Updater.java"))
        runLater(JavaFileWithPath("core/converter/CrudUpdater.java"))
        runLater(JavaFileWithPath("core/converter/UpdaterCategory.java"))
        runLater(JavaFileWithPath("core/converter/FunctionParam.java"))


        runLater(JavaFileWithPath("core/db/cache/PrimitivesCache.java"))
        runLater(JavaFileWithPath("core/db/cache/SingletonCache.java"))
        runLater(JavaFileWithPath("core/db/primitives/PrimitivesStore.java"))
        runLater(JavaFileWithPath("core/db/primitives/PrimitivesStoreH2.java"))
        runLater(JavaFileWithPath("core/db/support/TransactionContext.java"))
        runLater(JavaFileWithPath("core/db/support/TransactionManager.java"))
        runLater(JavaFileWithPath("core/db/DbFacadeImpl.java"))
        runLater(JavaFileWithPath("core/db/DbFacadeImplH2.java"))
        runLater(JavaFileWithPath("core/db/DbFacadeImplPostgre.java"))
        runLater(JavaFileWithPath("core/db/DbLink.java"))
        runLater(JavaFileWithPath("core/db/blob/Blob.java"))

        runLater(JavaFileWithPath("core/exceptions/UnauthorizedException.java"))
        runLater(JavaFileWithPath("domain/core/primitive/PersistentCombinedPrimitive.java"))
        runLater(JavaFileWithPath("domain/core/primitive/PersistentElementaryPrimitive.java"))
        runLater(JavaFileWithPath("domain/core/primitive/PersistentEmptyPrimitive.java"))
        runLater(JavaFileWithPath("domain/core/primitive/PersistentPrimitive.java"))
        runLater(JavaFileWithPath("domain/core/primitive/PrimitiveState.java"))
        runLater(JavaFileWithPath("domain/core/primitive/PrimitiveStateVisitor.java"))
        runLater(JavaFileWithPath("domain/core/primitive/Rational.java"))
        runLater(JavaFileWithPath("domain/core/primitive/UnitClass.java"))
        runLater(JavaFileWithPath("domain/core/primitive/UnitInstance.java"))
        runLater(JavaFileWithPath("domain/core/primitive/UnitNotCovered.java"))


        runLater(JavaFileWithPath("core/exceptions/ValidationException.java"))
        runLater(JavaFileWithPath("core/transaction/Transaction.java"))
        runLater(JavaFileWithPath("core/transaction/TransactionContext.java"))
        runLater(JavaGeneratorCallable())
        runLater(JavaFileWithPath("core/transaction/LongTransaction.java"))
        runLater(JavaFileWithPath("core/transaction/ShortTransaction.java"))
        runLater(JavaFileWithPath("core/transaction/TransactionExecutor.java"))
        runLater(JavaFileWithPath("core/transaction/TransactionFilter.java"))

        runLater(JavaFileWithPath("domain/core/ToStringVisitor.java", GeneratorType.ONCE))

        if (SettingsStore.securityEnabled()) {
            runLater(JavaFileWithPath("core/security/SecurityContext.java"))
            runLater(JavaFileWithPath("core/security/JwtSecurityFilter.java"))

            // Exceptions
            runLater(JavaFileWithPath("service/security/PasswordsNotMatchException.java"))
            runLater(JavaFileWithPath("service/security/WrongLoginException.java"))

            // runLater(JavaGeneratorUserRoles())
        }
        if (SettingsStore.databaseEnabled()) {
            runLater(JavaFileWithPath("core/db/DbConnectionBuilder.java"))
            runLater(JavaFileWithPath("core/db/DbConnector.java"))
            runLater(JavaFileWithPath("core/db/DbException.java"))
            runLater(JavaFileWithPath("core/db/DbFacade.java"))
            runLater(JavaFileWithPath("core/db/DbModel.java"))
            runLater(JavaFileWithPath("core/db/DbStatistic.java"))
            runLater(JavaFileWithPath("core/db/SqlFunction.java"))
            runLater(JavaFileWithPath("core/db/StoredList.java"))
            runLater(JavaFileWithPath("core/db/TriConsumer.java"))

            runLater(JavaFileWithPath("controller/domainr/ListVM.java"))
            runLater(JavaFileWithPath("controller/domainr/ObjVM.java"))
            runLater(JavaFileWithPath("controller/domainr/JsonConverter.java"))

            runLater(JavaFileWithPath("controller/GlobalDefaultExceptionHandler.java"))
            runLater(JavaFileWithPath("controller/WebController.java"))

            runLater(DataRequestService())
            runLater(SqlGeneratorInitScript())
            runLater(SqlFileWithPath("h2/loadLinks.sql"))
            runLater(SqlFileWithPath("h2/findAll.sql"))
            runLater(SqlFileWithPath("h2/getByIdAndAssociationLike.sql"))
            runLater(SqlFileWithPath("h2/findOpenLongTransactionsForUser.sql"))
            runLater(SqlFileWithPath("h2/getObjectForAssociationsOptional.sql"))
        }

        if (SettingsStore.options.given(Option.DEFAULTS)) {
            runLast(JavaFileWithPath("service/security/BCryptProviderImpl.java"))
            runLast(JavaFileWithPath("service/security/JwtTokenServiceImpl.java"))
            runLast(JavaFileWithPath("service/security/UserServiceImpl.java"))
            runLast(JavaFileWithPath("service/transaction/TransactionServiceImpl.java"))
            runLast(JavaFileWithPath("service/core/NotificationServiceImpl.java"))
        }

        // cucumber Tests
        runLater(GivenDomainStepDefsGenerator())
    }
}

class JavaFileWithPath(override val templateString: String, override val mode: GeneratorType = GeneratorType.PROCESS) : ExternalJavaGenerator()

class SqlFileWithPath(override val templateString: String, override val mode: GeneratorType = GeneratorType.PROCESS) : ExternalSqlGenerator()

fun hasAssociationWithValidator(clazz: DomainClass): Boolean {
    return clazz is ExplicitDomainClass && recursiveAssociationFetcher(clazz, false).stream().anyMatch { RuleHelp.hasRuleValidation(it) != null }
}
