package de.kaipho.genplus.generator.generator.c_esp32.domain

import de.kaipho.genplus.generator.common.convertPackageToFileSystem
import de.kaipho.genplus.generator.common.upperFirstLetter
import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.core.obj.AssoziationTypeCollection
import de.kaipho.genplus.generator.core.obj.AssoziationTypeString
import de.kaipho.genplus.generator.core.obj.AssoziationTypeUser
import de.kaipho.genplus.generator.core.obj.options.Option
import de.kaipho.genplus.generator.domain.obj.ExplicitDomainClass
import de.kaipho.genplus.generator.domain.obj.IClass
import de.kaipho.genplus.generator.generator.AbstractGenerator
import de.kaipho.genplus.generator.generator.c_esp32.core.CImports
import de.kaipho.genplus.generator.generator.c_esp32.core.FrameworkImports
import de.kaipho.genplus.generator.generator.c_esp32.core.Imports
import de.kaipho.genplus.generator.generator.core.File
import de.kaipho.genplus.generator.generator.core.Generator
import de.kaipho.genplus.generator.generator.recursiveAssociationFetcher
import de.kaipho.genplus.generator.store.SettingsStore
import de.kaipho.genplus.generator.template.Templates
import de.kaipho.genplus.generator.template.replacer.*
import de.kaipho.genplus.generator.template.replacer.*


@Generator(
        lang = Language.ESP32,
        template = Templates.C_ESP32_DOMAIN_CLASS,
        appliedOn = ExplicitDomainClass::class,
        file = File.C_ESP32_DOMAIN_CLASS
)
class GeneratorDomainClass : AbstractGenerator<ExplicitDomainClass>() {
    override fun visit(replacer: TemplateReplacerGetter) {
        recursiveAssociationFetcher(getData()).forEach {
            replacer.insert("${it.type.cRep()} get${it.name.upperFirstLetter()}() { return this->${it.name}; }")
        }
    }

    override fun visit(replacer: TemplateReplacerSetter) {
        recursiveAssociationFetcher(getData()).forEach {
            replacer.insert("void set${it.name.upperFirstLetter()}(${it.type.cRep()} ${it.name}) { this->${it.name} = ${it.name}; }")
        }
    }

    override fun visit(replacer: TemplateReplacerImplements) {
        if(self().extends.isEmpty()) {
            replacer.insert(": public PersistentElement")
        } else {
            replacer.insert(": public ${seperateExtends(getData().extends)}")
        }
    }

    private fun seperateExtends(classes: List<IClass>): String {
        return classes.map { it.name }.joinToString()
    }

    override fun visit(replacer: TemplateReplacerUpperName) {
        replacer.insert("${SettingsStore.options.getFirst(Option.NAME)}_${getData().name}".toUpperCase())
    }

    override fun visit(replacer: TemplateReplacerAssociations) {
        recursiveAssociationFetcher(getData()).forEach {
            replacer.insert("${it.type.cRep()} ${it.name};")
        }
    }

    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(getData().name)
    }

    override fun visit(replacer: TemplateReplacerImports) {
        val imports = Imports("domain${getData().prefix.convertPackageToFileSystem()}")

        recursiveAssociationFetcher(getData()).forEach {
            if (it.type is AssoziationTypeString) {
                imports.addImport(CImports.STRING)
            } else if (it.type is AssoziationTypeCollection) {
                imports.addImport(CImports.VECTOR)
                if (it.type.inner is AssoziationTypeUser) {
                    imports.addDomainImport(it.type.inner.clazz)
                }
            } else if (it.type is AssoziationTypeUser) {
                imports.addDomainImport(it.type.clazz)
            }
        }

        getData().extends.forEach {
            imports.addDomainImport(it)
        }

        if(self().extends.isEmpty()) {
            imports.addImport(FrameworkImports.PERSISTENT_ELEMENT)
        }

        replacer.insertAll(imports.getimports())
    }

    override fun visit(replacer: TemplateReplacerLine) {
        when (replacer.id) {
            0 -> replacer.insert("#ifndef")
            1 -> replacer.insert("#define")
            2 -> replacer.insert("#endif")
        }
    }
}
