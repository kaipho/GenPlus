package de.kaipho.genplus.generator.generator.java.domain

import de.kaipho.genplus.generator.domain.obj.Association
import de.kaipho.genplus.generator.generator.ClassGenerator
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerClassName
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerName
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerTable
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerValidators

/**
 * Created by tom on 14.07.16.
 */
class DomainClassAssociationGenerator(domain: Association) : ClassGenerator<Association>(domain) {
    override fun visit(replacer: TemplateReplacerValidators) {
//        domain.rules.forEach {
//            if(it is ValidateRule) {
//                ValidationGenerator.getValidationAnnotation(it).forEach {
//                    replacer.insert(actual, lines, it)
//                }
//            }
//        }
    }

    override fun visit(replacer: TemplateReplacerName) {
        replacer.insert(domain.name)
    }

    override fun visit(replacer: TemplateReplacerTable) {
        replacer.insert("")
    }

    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(domain.type.javaAbstractRep())
    }
}
