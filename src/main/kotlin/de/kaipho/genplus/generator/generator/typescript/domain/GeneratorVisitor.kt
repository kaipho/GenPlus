package de.kaipho.genplus.generator.generator.typescript.domain

import de.kaipho.genplus.generator.common.convertPackageToFileSystem
import de.kaipho.genplus.generator.common.lowerFirstLetter
import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.domain.obj.AbstractDomainClass
import de.kaipho.genplus.generator.generator.AbstractGenerator
import de.kaipho.genplus.generator.generator.core.File
import de.kaipho.genplus.generator.generator.core.Generator
import de.kaipho.genplus.generator.generator.typescript.core.ImportHelp
import de.kaipho.genplus.generator.generator.typescript.core.Imports
import de.kaipho.genplus.generator.template.Templates
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerClassName
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerImports
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLine

@Generator(
        lang = Language.TYPESCRIPT,
        template = Templates.TS_DOMAIN_VISITOR,
        appliedOn = AbstractDomainClass::class,
        file = File.TS_DOMAIN_VISITOR
)
class GeneratorVisitor : AbstractGenerator<AbstractDomainClass>() {
    override fun visit(replacer: TemplateReplacerImports) {
        val import = Imports("domain${self().prefix.convertPackageToFileSystem()}")
        meta.data.childClasses.forEach { clazz ->
            import.addDomainImport(clazz)
        }
        replacer.insertAll(import.getImports())
    }

    override fun visit(replacer: TemplateReplacerLine) {
        meta.data.childClasses.forEach { clazz ->
            replacer.insert("visit${clazz.name}(obj: ${clazz.name}):D")
        }
    }

    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(meta.data.name)
    }
}