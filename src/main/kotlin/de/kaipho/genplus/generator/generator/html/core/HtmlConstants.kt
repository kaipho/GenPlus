package de.kaipho.genplus.generator.generator.html.core

val ICON_ADD = tag("mat-icon") { text("add") }
val ICON_BUTTON = nobind("mat-icon-button")
val MENU_ITEM = nobind("mat-menu-item")
