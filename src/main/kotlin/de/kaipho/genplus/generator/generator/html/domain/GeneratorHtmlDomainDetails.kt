package de.kaipho.genplus.generator.generator.html.domain

import de.kaipho.genplus.generator.common.convertPackageToFileSystem
import de.kaipho.genplus.generator.common.lowerFirstLetter
import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.domain.obj.DomainClass
import de.kaipho.genplus.generator.domain.obj.ExplicitDomainClass
import de.kaipho.genplus.generator.generator.AbstractGenerator
import de.kaipho.genplus.generator.generator.core.File
import de.kaipho.genplus.generator.generator.core.Generator
import de.kaipho.genplus.generator.generator.core.Ref
import de.kaipho.genplus.generator.template.Templates
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerI18n
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLowerClassName
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerName
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerSubpackage

@Generator(
        lang = Language.TYPESCRIPT,
        template = Templates.HTML_DOMAIN_DETAIL,
        appliedOn = ExplicitDomainClass::class,
        file = File.HTML_DOMAIN_DETAIL
)
class GeneratorHtmlDomainDetails : AbstractGenerator<DomainClass>() {
    override fun visit(replacer: TemplateReplacerName) {
        replacer.insert(meta.data.prefix.convertPackageToFileSystem())
    }

    override fun visit(replacer: TemplateReplacerSubpackage) {
        replacer.insert(meta.data.prefix)
    }

    override fun visit(replacer: TemplateReplacerLowerClassName) {
        replacer.insert(meta.data.name.lowerFirstLetter())
    }

    override fun visit(replacer: TemplateReplacerI18n) {
        replacer.insert("title${meta.data.prefix}.details")
    }
}