package de.kaipho.genplus.generator.generator.java.service.normal

import de.kaipho.genplus.generator.common.convertPackageToFileSystem
import de.kaipho.genplus.generator.core.obj.service.ServiceClass
import de.kaipho.genplus.generator.generator.core.RuleHelp
import de.kaipho.genplus.generator.generator.java.core.ExternalJavaGenerator
import de.kaipho.genplus.generator.generator.java.core.Functions
import de.kaipho.genplus.generator.generator.java.service.Imports
import de.kaipho.genplus.generator.generator.java.service.getImportsForFunction
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerClassName
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerImports
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLine
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerSubpackage

class JavaGeneratorNormalService(val clazz: ServiceClass) : ExternalJavaGenerator() {
    override val templateString = "service/xxx/xxx.java"
    override val replacer = arrayListOf(clazz.prefix.convertPackageToFileSystem().removeSuffix("/"),
                                        clazz.name)

    override fun visit(replacer: TemplateReplacerSubpackage) {
        replacer.insert(clazz.prefix)
    }

    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(clazz.name)
    }

    override fun visit(replacer: TemplateReplacerImports) {
        val imports = Imports()
        getImportsForFunction(clazz.functions, imports)
        replacer.insertAll(imports.getimports())
    }

    override fun visit(replacer: TemplateReplacerLine) {
        clazz.functions.forEach { function ->
            val returnType = function.returnType.javaAbstractRep()
            val name = function.name
            val parameter = Functions.getParams(function.params, function.extends)
            if(function.comment != "") {
                replacer.insert("/**")
                replacer.insert(" * ${function.comment.removeSuffix("*/").removePrefix("/*").trim()}")
                replacer.insert(" */")
            }

            if(RuleHelp.hasRuleSecured(function) != null) {
                replacer.insert("// Secured by proxy...")
            }
            replacer.insert("$returnType $name($parameter);")
        }
    }
}
