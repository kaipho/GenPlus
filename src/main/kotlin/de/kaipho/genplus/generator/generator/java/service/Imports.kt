package de.kaipho.genplus.generator.generator.java.service

import de.kaipho.genplus.generator.core.obj.*
import de.kaipho.genplus.generator.core.obj.service.ClassDummy
import de.kaipho.genplus.generator.core.obj.service.Function
import de.kaipho.genplus.generator.domain.obj.*
import de.kaipho.genplus.generator.store.SettingsStore
import java.util.*

class Imports {
    val imports: MutableSet<String> = HashSet()
    private val importTemplate = "import ${SettingsStore.getPrefixPackage()}#;"
    private fun importSimpleTemplate(string: String): String = "import $string;"

    fun addImport(import: String) {
        this.imports.add(this.importTemplate.replace("#", import))
    }

    fun addJavaImport(import: JavaImport) {
        this.imports.add(importSimpleTemplate(import.string))
    }

    fun addCoreImport(import: CoreFramework) {
        this.addImport(import.string)
    }

    fun addDomainImport(clazz: IClass) {
        clazz.acceptClass(object : TypeVisitor<Unit> {
            override fun visitPrimitive(clazz: PrimitiveClass) {
                addImport("domain.primitive.unit.classes.${clazz.name}")
                addImport("domain.core.primitive.PersistentPrimitive")
            }

            override fun visitCategory(clazz: CategoryClass) {
                addImport("domain.category.${clazz.name}")
            }

            override fun visitDomain(clazz: DomainClass) {
                addImport("domain${clazz.prefix}.${clazz.name}")
            }

            override fun visitConstant(clazz: Constant) {
                addImport("domain.constant.${clazz.prefix}${clazz.name}")
            }

            override fun visitOther(clazz: IClass) {
                // no domain...
            }

            override fun visitDummy(clazz: IClass) {
                throw RuntimeException("Dummy class not allowed in this stage!")
            }
        })
    }

    fun addDomainControllerImport(clazz: IClass) {
        clazz.acceptClass(object : TypeVisitor<Unit> {
            override fun visitPrimitive(clazz: PrimitiveClass) {
            }

            override fun visitCategory(clazz: CategoryClass) {
            }

            override fun visitDomain(clazz: DomainClass) {
                addImport("controller.domainr${clazz.prefix}.${clazz.name}Controller")
            }

            override fun visitConstant(clazz: Constant) {
            }

            override fun visitOther(clazz: IClass) {
                // no domain...
            }

            override fun visitDummy(clazz: IClass) {
                throw RuntimeException("Dummy class not allowed in this stage!")
            }
        })
    }

    fun addDomainServiceImport(clazz: IClass) {
        if (clazz !is ClassDummy)
            addImport("service.domainr${clazz.prefix}.${clazz.name}Service")
    }

    fun addDomainVisitorImport(clazz: IClass) {
        addImport("domain${clazz.prefix}.${clazz.name}Visitor")
        addImport("domain${clazz.prefix}.${clazz.name}SimpleVisitor")
    }

    fun addDomainCompleteVisitorImport(clazz: IClass) {
        addImport("domain${clazz.prefix}.${clazz.name}CompleteVisitor")
    }

    fun addDomainRepository(clazz: IClass) {
        addImport("domain${clazz.prefix}.impl.${clazz.name}RepositoryProxy")
    }

    fun addServiceImport(clazz: IClass) {
        addImport("service${clazz.prefix}.${clazz.name}")
    }

    fun getimports(): List<String> = imports.toList()

    fun addAssociationImport(association: Association) {
        addAssociationImport(association.type)
    }

    fun addAssociationImport(association: AssoziationType) {
        association.accept(object : AssociationVisitor<Unit> {
            override fun visit(associationTypeBlob: AssociationTypeBlob) {
                addImport("core.db.blob.Blob")
            }

            override fun visit(association: AssoziationTypeMap) {
                addJavaImport(JavaImport.MAP)
                addJavaImport(JavaImport.HASH_MAP)
                association.key.accept(this)
                association.value.accept(this)
            }

            override fun visit(association: AssoziationTypeSecure) = Unit
            override fun visit(association: AssoziationTypePassword) {

            }

            override fun visit(association: AssoziationTypeOptional) {
                addJavaImport(JavaImport.OPTIONAL)
                association.inner.accept(this)
            }

            override fun visit(association: AssoziationTypeText) = Unit

            override fun visit(association: AssoziationTypeBigNumber) {
                addJavaImport(JavaImport.BIG_INTEGER)
            }

            override fun visit(assoziation: AssoziationTypeUnit): Unit {
            }

            override fun visit(assoziation: AssoziationTypeDummy): Unit = throw UnsupportedOperationException("not implemented")
            override fun visit(association: AssoziationTypeString) {
            }

            override fun visit(association: AssoziationTypeInteger) {
            }

            override fun visit(association: AssoziationTypeDouble) {
            }

            override fun visit(association: AssoziationTypeDate) {
                addJavaImport(JavaImport.LOCAL_DATE)
            }

            override fun visit(association: AssoziationTypeBoolean) {
            }

            override fun visit(association: AssoziationTypeUser) {
                association.acceptClass(object : UserClassVisitor<Unit> {
                    override fun visitConstant(association: AssoziationTypeUser, clazz: Constant) {
                        addImport("domain.constant.${clazz.prefix}${clazz.name}")
                    }

                    override fun visitPrimitive(association: AssoziationTypeUser, clazz: PrimitiveClass) {
                        addImport("domain.primitive.unit.classes.${clazz.name}")
                        addImport("domain.core.primitive.PersistentPrimitive")
                    }

                    override fun visitCategory(association: AssoziationTypeUser, clazz: CategoryClass) {
                        addImport("domain.category.${clazz.name}")
                    }

                    override fun visitDomain(association: AssoziationTypeUser, clazz: DomainClass) {
                        addDomainImport(clazz)
                    }
                })
            }

            override fun visit(association: AssoziationTypeCollection) {
                addJavaImport(JavaImport.LIST)
                addJavaImport(JavaImport.ARRAY_LIST)
                association.inner.accept(this)
            }
        })
    }

    fun addFunctionCommandImport(it: Function) {
        addImport("service${it.owner.prefix}.command.${it.getQualifiedClassName()}Command")
    }
}

enum class CoreFramework(val string: String) {
    DB_EXCEPTION("core.db.DbException"),
    DB_FACADE("core.db.DbFacade"),
    DB_CONNECTOR("core.db.DbConnector"),
    SECURITY_CONTEXT("core.security.SecurityContext"),
    BCRYPT("service.security.BCryptProvider")
}

enum class JavaImport(val string: String) {
    MAP("java.util.Map"),
    HASH_MAP("java.util.HashMap"),
    LIST("java.util.List"),
    COLLECTORS("java.util.stream.Collectors"),
    ARRAYS("java.util.Arrays"),
    ARRAY_LIST("java.util.ArrayList"),
    OPTIONAL("java.util.Optional"),
    OPTIONAL_INT("java.util.OptionalInt"),
    LOCAL_DATE("java.time.LocalDate"),
    BIG_INTEGER("java.math.BigDecimal"),
    DB_STATEMENT("java.sql.PreparedStatement"),
    DB_RESULT("java.sql.ResultSet"),
    DB_EXCEPTION("java.sql.SQLException"),

    SPRING_REPOSITORY_REST_RESOURCE("org.springframework.data.rest.core.annotation.RepositoryRestResource"),
    SPRING_REPOSITORY("org.springframework.stereotype.Repository"),
}
