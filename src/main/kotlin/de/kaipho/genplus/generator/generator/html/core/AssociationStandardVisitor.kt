package de.kaipho.genplus.generator.generator.html.core

import de.kaipho.genplus.generator.core.obj.*

abstract class AssociationStandardVisitor<out T> : AssociationVisitor<T> {
    abstract fun standard(association: AssoziationType): T

    override fun visit(association: AssoziationTypeString): T {
        return standard(association)
    }

    override fun visit(association: AssoziationTypeInteger): T {
        return standard(association)
    }

    override fun visit(association: AssoziationTypeDate): T {
        return standard(association)
    }

    override fun visit(association: AssoziationTypeBoolean): T {
        return standard(association)
    }

    override fun visit(association: AssoziationTypeUser): T {
        return standard(association)
    }

    override fun visit(association: AssoziationTypeCollection): T {
        return standard(association)
    }

    override fun visit(association: AssoziationTypeDummy): T {
        return standard(association)
    }

    override fun visit(association: AssoziationTypeUnit): T {
        return standard(association)
    }

    override fun visit(association: AssoziationTypeDouble): T {
        return standard(association)
    }

    override fun visit(association: AssoziationTypeBigNumber): T {
        return standard(association)
    }

    override fun visit(association: AssoziationTypeText): T {
        return standard(association)
    }

    override fun visit(association: AssoziationTypeOptional): T {
        return standard(association)
    }

    override fun visit(association: AssoziationTypePassword): T {
        return standard(association)
    }
}