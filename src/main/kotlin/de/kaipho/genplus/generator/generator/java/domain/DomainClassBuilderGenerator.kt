package de.kaipho.genplus.generator.generator.java.domain

import de.kaipho.genplus.generator.common.lowerFirstLetter
import de.kaipho.genplus.generator.common.upperFirstLetter
import de.kaipho.genplus.generator.core.obj.AssoziationType
import de.kaipho.genplus.generator.core.obj.AssoziationTypeOptional
import de.kaipho.genplus.generator.core.obj.AssoziationTypeUser
import de.kaipho.genplus.generator.domain.obj.Association
import de.kaipho.genplus.generator.domain.obj.ExplicitDomainClass
import de.kaipho.genplus.generator.generator.ClassGenerator
import de.kaipho.genplus.generator.generator.java.hasAssociationWithValidator
import de.kaipho.genplus.generator.generator.recursiveAssociationFetcher
import de.kaipho.genplus.generator.template.TemplateParser
import de.kaipho.genplus.generator.template.Templates
import de.kaipho.genplus.generator.template.replacer.*

class DomainClassBuilderGenerator(domain: ExplicitDomainClass, val association: Association? = null) : ClassGenerator<ExplicitDomainClass>(domain) {

    override fun visit(replacer: TemplateReplacerLowerClassName) {
        replacer.insert(domain.name.lowerFirstLetter())
    }

    override fun visit(replacer: TemplateReplacerClassName) {
        if (association != null) {
            replacer.insert(association.type.javaAbstractRep())
        } else {
            replacer.insert(domain.name)
        }
    }

    override fun visit(replacer: TemplateReplacerDomainBuilderMethod) {
        when(replacer.id) {
            1 -> fakeBuilder(replacer)
            2 -> realBulder(replacer)
        }

    }

    private fun fakeBuilder(replacer: TemplateReplacerDomainBuilderMethod) {
        recursiveAssociationFetcher(meta.data, false).forEach {
            if (it.type !is AssoziationTypeUser) {
                replacer.insert("@Override")
                replacer.insert("public Doneable${self().name}Builder<R> ${it.name}(${it.type.javaAbstractRep()} ${it.name}) {")
                replacer.insert("   super.${it.name}(${it.name});")
                replacer.insert("   return this;")
                replacer.insert("}")
            }
        }
    }

    private fun realBulder(replacer: TemplateReplacerDomainBuilderMethod) {
        recursiveAssociationFetcher(meta.data, false).forEach {
/*            if (it.type is AssoziationTypeOptional) {
                """
                |public ${it.belongsTo.name}Builder ${it.name}(${it.type.inner.javaAbstractRep()} ${it.name}) {
                |    this.${it.belongsTo.name.lowerFirstLetter()}.set${it.name.upperFirstLetter()}(${it.name});
                |    return this;
                |}
                """.insertInto(replacer)
            }*/

            if (it.type is AssoziationTypeUser && it.type.clazz is ExplicitDomainClass) {
                // TODO!!!
//                val javaRep = it.type.javaRep()
//                val name = it.name.upperFirstLetter()
//                replacer.insert("public $javaRep.Doneable${javaRep}Builder<${self().name}Builder> with$name() {")
//                replacer.insert("   return new $javaRep.Doneable${javaRep}Builder(this);")
//                replacer.insert("}")
//                replacer.insert("@Override")
//                replacer.insert("public ${self().name}Builder visit($javaRep data) {")
//                replacer.insert("   this.${self().name.lowerFirstLetter()}.set$name(data);")
//                replacer.insert("   return this;")
//                replacer.insert("}")
            } else
                replacer.insert(actual, lines, TemplateParser.parseTemplate(Templates.DOMAIN_BUILDER_METHOD, DomainClassBuilderGenerator(domain, it)))
        }
    }

    override fun visit(replacer: TemplateReplacerAssociations) {
        if (association != null) {
            replacer.insert(association.name)
        }
    }

    override fun visit(replacer: TemplateReplacerDomainBuilder) {
        replacer.insert(domain.name)
    }

    override fun visit(replacer: TemplateReplacerUpperName) {
        if (association != null) {
            replacer.insert(association.name.upperFirstLetter())
        }
    }

    override fun visit(replacer: TemplateReplacerLine) {
        val name = self().name.lowerFirstLetter()
        if (hasAssociationWithValidator(self())) {
            replacer.insert("this.$name = new ${self().name}ValidationProxy(this.${name}RepositoryProxy);")
        } else {
            replacer.insert("this.$name = this.${name}RepositoryProxy;")
        }
    }
}
