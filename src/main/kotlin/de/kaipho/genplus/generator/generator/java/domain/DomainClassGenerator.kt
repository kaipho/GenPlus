package de.kaipho.genplus.generator.generator.java.domain

import de.kaipho.genplus.generator.common.upperFirstLetter
import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.constants.STRINGS
import de.kaipho.genplus.generator.core.obj.AssoziationTypeCollection
import de.kaipho.genplus.generator.core.obj.AssoziationTypeInteger
import de.kaipho.genplus.generator.core.obj.AssoziationTypeUser
import de.kaipho.genplus.generator.domain.obj.*
import de.kaipho.genplus.generator.generator.AbstractGenerator
import de.kaipho.genplus.generator.generator.core.File
import de.kaipho.genplus.generator.generator.core.Generator
import de.kaipho.genplus.generator.generator.core.RuleHelp
import de.kaipho.genplus.generator.generator.getInlinedAssociations
import de.kaipho.genplus.generator.generator.java.core.*
import de.kaipho.genplus.generator.generator.java.service.Imports
import de.kaipho.genplus.generator.generator.recursiveAssociationFetcher
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.store.SettingsStore
import de.kaipho.genplus.generator.template.TemplateParser
import de.kaipho.genplus.generator.template.Templates
import de.kaipho.genplus.generator.template.replacer.*
import java.util.*


@Generator(
        lang = Language.JAVA,
        template = Templates.DOMAIN_CLASS,
        appliedOn = ExplicitDomainClass::class,
        file = File.JAVA_DOMAIN_CLASS
)
class DomainClassGenerator : AbstractGenerator<ExplicitDomainClass>() {

    override fun visit(replacer: TemplateReplacerAssociations) {
        recursiveAssociationFetcher(meta.data, false).forEach {
            replacer.insert(actual, lines, TemplateParser.parseTemplate(Templates.DOMAIN_ASSOCIATION, DomainClassAssociationGenerator(it)))
        }
    }

    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(meta.data.name)
    }

    override fun visit(replacer: TemplateReplacerImplements) {
        if (meta.data.extends.size > 0) {
            lines.add(replacer.replaceOneLine(actual, "extends ${meta.data.extends[0].name}" + "Impl implements " + self().name))
        } else {
            lines.add(replacer.replaceOneLine(actual, "implements " + self().name))
        }
    }

    override fun visit(replacer: TemplateReplacerPackage) {
        lines.add(replacer.replaceOneLine(actual, SettingsStore.getPrefixPackage()))
    }

    override fun visit(replacer: TemplateReplacerComment) {
        replacer.insert(meta.data.comment)
    }

    override fun visit(replacer: TemplateReplacerAnnotations) {
//        ValidationGenerator.getClassValidationAnnotation(meta.data).forEach {
//            replacer.insert(actual, lines, it)
//        }
    }

    override fun visit(replacer: TemplateReplacerImports) {
        val imports = Imports()
//        ValidationGenerator.getValidationImports(meta.data).forEach {
//            replacer.insert(actual, lines, it)
//        }

        recursiveAssociationFetcher(meta.data, true).union(getInlinedAssociations(meta.data)).forEach(imports::addAssociationImport)

        recursiveAssociationFetcher(getData(), false).forEach {
            if (it.type is AssoziationTypeUser && it.type.clazz !is AbstractDomainClass)
                imports.addDomainImport(it.type.clazz)
            if (it.type is AssoziationTypeCollection && it.type.inner is AssoziationTypeUser && it.type.inner.clazz !is AbstractDomainClass) {
                imports.addDomainImport(it.type.inner.clazz)
            }
        }

        self().extends.forEach {
            imports.addImport("domain${it.prefix}.impl.${it.name}Impl")
        }
        fetchAllParents(self()).forEach {
            imports.addDomainCompleteVisitorImport(it)
        }

        ClassStore.domain.domainClasses.filter { it is AbstractDomainClass }.forEach { clazz ->
            fetchAllAssociationsAndChildClasses(clazz).filter { it.name == getData().name }.forEach {
                imports.addDomainVisitorImport(clazz)
            }
        }
        recursiveAssociationFetcher(meta.data).filter { it.isDerived() }.forEach {
            imports.addImport("domain.core.DerivedManager")
        }
        replacer.insertAll(imports.getimports())
    }

    override fun visit(replacer: TemplateReplacerSubpackage) {
        replacer.insert(meta.data.prefix)
    }

    override fun visit(replacer: TemplateReplacerGetter) {
        /*meta.data.associations.filter {
            it.rules.contains(InlineRule())
        }.forEach { assiciation ->
            if (assiciation.type is AssoziationTypeUser) {
                recursiveAssociationFetcher(assiciation.type.clazz as DomainClass, false).forEach {
                    replacer.insert("@Override")
                    replacer.insert(
                            "public ${it.type.javaAbstractRep()} get${it.name.upperFirstLetter()}() {return this.${assiciation.name}.get${it.name.upperFirstLetter()}(); }")
                }
            } else {
                throw RuntimeException(STRINGS.RULE_INLINE + " must be on UserTypes")
            }
        }*/
        val associations = recursiveAssociationFetcher(meta.data, false)
        associations.forEach {
            replacer.insert(actual, lines, TemplateParser.parseTemplate(Templates.GETTER, GetterSetterGenerator(it)))
            if (it.type is AssoziationTypeCollection) {
                replacer.insert("@Override")
                createMethod(
                        name1 = "addSingleTo",
                        name2 = it.name,
                        parameter = listOf(it.type.inner.javaAbstractRep() to it.name)) {
                    add("   this.${it.name}.add(${it.name});")
                }.apply { replacer.insertAll(this) }
            }
            if (it.type is AssoziationTypeInteger) {
                replacer.insert("@Override")
                createMethodAsso(name1 = "add", name2 = it.name, parameter = listOf(it)) {
                    add("   if(this.${it.name} == null) this.${it.name} = 0L;")
                    add("   this.${it.name} += ${it.name};")
                }.apply { replacer.insertAll(this) }
            }
        }
        recursiveAssociationFetcher(meta.data).filter { it.isDerived() }.forEach {
            replacer.insert("@Override")
            createMethod(name1 = "get", name2 = it.name, returntype = it.type) {
                add("   return DerivedManager.getInstance().derived${it.name.upperFirstLetter()}(this);")
            }.apply { replacer.insertAll(this) }
        }
    }

    override fun visit(replacer: TemplateReplacerSetter) {
        recursiveAssociationFetcher(meta.data).forEach { setterAssociationImpl(it) { it -> replacer.insertAll(it) } }
    }

    override fun visit(replacer: TemplateReplacerEqualsHashCode) {
        replacer.insert(actual, lines, TemplateParser.parseTemplate(Templates.EQUALS_HASHCODE, EqualsHashCodeGenerator(meta.data)))
    }

    override fun visit(replacer: TemplateReplacerDomainBuilder) {
        replacer.insert(actual, lines, TemplateParser.parseTemplate(Templates.DOMAIN_BUILDER, DomainClassBuilderGenerator(meta.data)))
    }

    override fun visit(replacer: TemplateReplacerLine) {
        when (replacer.id) {
            0 -> {
                recursiveAssociationFetcher(meta.data, false).filter { it.type is AssoziationTypeUser && it.type.clazz !is AbstractClass }.forEach {
                    replacer.insert("this.${it.name} = ${it.type}.createUnchecked();")
                }
                recursiveAssociationFetcher(meta.data, false).filter { it.type is AssoziationTypeCollection }.forEach {
                    replacer.insert("this.${it.name} = new ArrayList<>();")
                }
            }
            1 -> replacer.insert(recursiveAssociationFetcher(meta.data, false)
                    .filter { it.type !is AssoziationTypeCollection }
                    .map { "${it.type.javaAbstractRep()} ${it.name}" }
                    .joinToString())
            2 -> recursiveAssociationFetcher(meta.data, false)
                    .filter { it.type !is AssoziationTypeCollection }.forEach {
                replacer.insert("result.set${it.name.upperFirstLetter()}(${it.name});")
            }
        }
    }

    override fun visit(replacer: TemplateReplacerAccept) {
        val visitable = ArrayList<IClass>()
        ClassStore.domain.domainClasses.filter { it is AbstractDomainClass }.forEach { clazz ->
            fetchAllAssociationsAndChildClasses(clazz).filter { it.name == getData().name }.forEach {
                addClassToList(visitable, clazz)
            }
        }
        visitable.forEach {
            replacer.insert("@Override")
            replacer.insert("public <D> D accept(${it.name}Visitor<D> visitor) {")
            replacer.insert("   return visitor.visit(this);")
            replacer.insert("}")
            replacer.insert("@Override")
            replacer.insert("public void accept(${it.name}SimpleVisitor visitor) {")
            replacer.insert("   visitor.visit(this);")
            replacer.insert("}")
        }
        getAllParents(self().extends).forEach {
            replacer.insert("@Override")
            replacer.insert("public <D> D accept(${it.name}CompleteVisitor<D> visitor) {")
            replacer.insert("   return visitor.visit(this);")
            replacer.insert("}")
        }
    }

    fun getAllParents(clazz: List<AbstractClass>): MutableList<AbstractClass> {
        val list: MutableList<AbstractClass> = ArrayList()
        clazz.forEach {
            list.add(it)
            if (it.extends.isNotEmpty()) {
                list.addAll(getAllParents(it.extends))
            }
        }
        return list
    }
}
