package de.kaipho.genplus.generator.generator.typescript.domain

import de.kaipho.genplus.generator.common.upperFirstLetter
import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.generator.AbstractGenerator
import de.kaipho.genplus.generator.generator.core.File
import de.kaipho.genplus.generator.generator.core.Generator
import de.kaipho.genplus.generator.generator.typescript.core.Imports
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.template.Templates
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerImports
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLine

@Generator(
        lang = Language.TYPESCRIPT,
        template = Templates.TS_COMPONENT_CONSTANTS,
        appliedOn = ClassStore::class,
        file = File.TS_COMPONENT_CONSTANTS
)
class GeneratorComponentConstants : AbstractGenerator<ClassStore>() {
    override fun visit(replacer: TemplateReplacerImports) {
        val imports = Imports("/components")
        self().services.filter { it.export }.map { it.functions }.flatten().forEach {
            imports.addServiceComponentImport(it)
        }
        replacer.insertAll(imports.getImports())
        replacer.insertAll(imports.getExports())
    }

    override fun visit(replacer: TemplateReplacerLine) {
        self().services.filter { it.export }.map { it.functions }.flatten().forEach {
            replacer.insert("${it.getQualifiedClassName()}Component,")
        }
    }
}