package de.kaipho.genplus.generator.generator.typescript.domain

import de.kaipho.genplus.generator.common.convertPackageToFileSystem
import de.kaipho.genplus.generator.common.lowerFirstLetter
import de.kaipho.genplus.generator.common.upperFirstLetter
import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.core.obj.AssoziationTypeBoolean
import de.kaipho.genplus.generator.core.obj.AssoziationTypeCollection
import de.kaipho.genplus.generator.core.obj.AssoziationTypeUser
import de.kaipho.genplus.generator.core.obj.UserClassVisitor
import de.kaipho.genplus.generator.domain.obj.*
import de.kaipho.genplus.generator.generator.AbstractGenerator
import de.kaipho.genplus.generator.generator.core.File
import de.kaipho.genplus.generator.generator.core.Generator
import de.kaipho.genplus.generator.generator.core.RuleHelp.hasRuleValidation
import de.kaipho.genplus.generator.generator.core.hasAllowedValueContrained
import de.kaipho.genplus.generator.generator.recursiveAssociationFetcher
import de.kaipho.genplus.generator.generator.typescript.TsPath
import de.kaipho.genplus.generator.generator.typescript.core.Imports
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.template.Templates
import de.kaipho.genplus.generator.template.replacer.*
import java.util.*

@Generator(
        lang = Language.TYPESCRIPT,
        template = Templates.TS_DOMAIN_CONTROLLER,
        appliedOn = ExplicitDomainClass::class,
        file = File.TS_DOMAIN_CONTROLLER
)
class GeneratorDomainController() : AbstractGenerator<ExplicitDomainClass>() {

    override fun visit(replacer: TemplateReplacerUpperName) {
        replacer.insert(meta.data.name)
    }

    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(meta.data.name)
    }

    override fun visit(replacer: TemplateReplacerImports) {
        val ownPath = "components/domain/" + meta.data.prefix.convertPackageToFileSystem()
        val imports = Imports(ownPath)

        imports.addImport(TsPath.TITLE_SERVICE)
        imports.addImport(TsPath.TITLE_COMPONENT)
        imports.addImport(TsPath.DOMAIN_TYPE_VISITOR)
        imports.addImport(TsPath.MENU_SERVICE)
        imports.addDomainImport(self())

        imports.addHttpImport(meta.data)

        recursiveAssociationFetcher(meta.data).forEach {
            if (it.type is AssoziationTypeCollection && it.type.inner is AssoziationTypeUser) {
                importUserType(imports, it.type.inner)
            } else if (it.type is AssoziationTypeUser && it.isInline) {
                importUserType(imports, it.type)
            }
        }

        ClassStore.getAllFunctionExtensionsFor(getData()).forEach {
            imports.addFunctionImport(it)
        }

        replacer.insertAll(imports.getImports())
    }

    override fun visit(replacer: TemplateReplacerName) {
        replacer.insert(meta.data.name.lowerFirstLetter())
    }

    override fun visit(replacer: TemplateReplacerLowerClassName) {
        replacer.insert(meta.data.name.lowerFirstLetter())
    }

    override fun visit(replacer: TemplateReplacerI18n) {
        replacer.insert("title${meta.data.prefix}.${meta.data.name.lowerFirstLetter()}")
    }

    override fun visit(replacer: TemplateReplacerSubpackage) {
        replacer.insert(meta.data.prefix)
    }

    override fun visit(replacer: TemplateReplacerLine) {
        val insertions = arrayListOf("")
        recursiveAssociationFetcher(meta.data).forEach {
            if (it.type is AssoziationTypeCollection && it.type.inner is AssoziationTypeUser) {
                when (replacer.id) {
//                    0 -> {
//                        insertions.add("_${it.name}Http:${it.type.inner.javaRep()}HttpService;")
//                        insertions.add("${it.name}:Array<${it.type.inner.javaRep()}> = [];")
//                    }
//                    1 -> insertions[0] = insertions[0] + ", ${it.name}Http:${it.type.inner.typescriptRep()}HttpService"
//                    2 -> insertions.add("this._${it.name}Http = ${it.name}Http;")
                // 3 -> generateCallback(insertions, it)
//                    4 -> generateNavigate(insertions, it)
                }
            }
            if (it.type is AssoziationTypeUser && it.isInline) {
                when (replacer.id) {
//                    0 -> {
//                        insertions.add("_${it.name}Http:${it.type.typescriptRep()}HttpService;")
//                        insertions.add("${it.name}:${it.type.typescriptRep()} = <${it.type.typescriptRep()}>{};")
//                    }
//                    1 -> insertions[0] = insertions[0] + ", ${it.name}Http:${it.type.typescriptRep()}HttpService"
//                    2 -> insertions.add("this._${it.name}Http = ${it.name}Http;")
                // 3 -> generateCallbackSingle(insertions, it)
//                    4 -> generateNavigateSingle(insertions, it)
                }
            }
            if (replacer.id == 3) {
                generateCallback(insertions, it)
            }
            val rule = hasRuleValidation(it)?.hasAllowedValueContrained()
            if (rule != null) {
                when (replacer.id) {
                    0 -> {
                        insertions.add("${it.name}Options: Array<String> = [];")
                    }
                }
            }
        }



        if (replacer.id == 5) {
            generateMenuEntries(insertions)
        }
        replacer.insertAll(insertions)
    }

    private fun generateMenuEntries(list: MutableList<String>) {
        list.add("this.menu.addEntries(this, result.id, result,")
        ClassStore.getAllFunctionExtensionsFor(getData()).forEach {
            list.add("  { name: 'functions${it.getFilePrefix().replace("/", ".")}.${it.name}.title', dialog: ${it.getQualifiedClassName()}Component},")
        }
        list.add(");")
    }

    private fun generateCallback(list: ArrayList<String>, association: Association) {
        if (association.isDerived())
            list.add("${association.name}: [{value:'', disabled: true}],")
        else {
            if (association.type is AssoziationTypeBoolean)
                list.add("${association.name}: [false],")
            else
                list.add("${association.name}: [''],")
        }

    }

    private fun generateCallbackSingle(list: ArrayList<String>, association: Association) {
        list.add("this._${association.name}Http.get(result._links.${association.name}.href).subscribe(")
        list.add("    result => this.${association.name} = result,")
        list.add("    error => ''")
        list.add(");")
    }

    private fun generateNavigateSingle(list: ArrayList<String>, association: Association) {
        list.add("navigate${association.name.upperFirstLetter()}() {")
        list.add("  const type = this.${association.name}.accept(new TypeVisitor());")
        list.add(createRoute())
        list.add("  this._router.navigateByUrl(url);")
        list.add("}")
    }

    private fun createRoute(): String {
        if (!getData().hasDetailsPage())
            return "  const url = `web${getData().prefix.convertPackageToFileSystem()}/\${type}/detail/\${type}`;"
        else
            return "  const url = `web/domainr/${getData().id}/${getData().name.lowerFirstLetter()}_\${type}`;"
    }

    private fun importUserType(imports: Imports, association: AssoziationTypeUser) = association.acceptClass(object : UserClassVisitor<Unit> {
        override fun visitPrimitive(association: AssoziationTypeUser, clazz: PrimitiveClass) {
            // nothing to import :)
        }

        override fun visitCategory(association: AssoziationTypeUser, clazz: CategoryClass) {
            // nothing to import :)
        }

        override fun visitDomain(association: AssoziationTypeUser, clazz: DomainClass) {
            imports.addHttpImport(clazz)
            imports.addDomainImport(clazz)
            imports.addImport(TsPath.DOMAIN_TYPE_VISITOR)
        }

        override fun visitConstant(association: AssoziationTypeUser, clazz: Constant) {
            // nothing to import :)
        }
    })
}