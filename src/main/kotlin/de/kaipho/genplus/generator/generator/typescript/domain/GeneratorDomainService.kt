package de.kaipho.genplus.generator.generator.typescript.domain

import de.kaipho.genplus.generator.common.convertPackageToFileSystem
import de.kaipho.genplus.generator.common.lowerFirstLetter
import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.domain.obj.ExplicitDomainClass
import de.kaipho.genplus.generator.generator.AbstractGenerator
import de.kaipho.genplus.generator.generator.core.File
import de.kaipho.genplus.generator.generator.core.Generator
import de.kaipho.genplus.generator.generator.typescript.RelevantPathes
import de.kaipho.genplus.generator.generator.typescript.core.ImportHelp
import de.kaipho.genplus.generator.template.Templates
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerClassName
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerImports
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLine
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLowerClassName

@Generator(
        lang = Language.TYPESCRIPT,
        template = Templates.TS_DOMAIN_HTTP_SERVICE,
        appliedOn = ExplicitDomainClass::class,
        file = File.TS_DOMAIN_HTTP_SERVICE
)
class GeneratorDomainService : AbstractGenerator<ExplicitDomainClass>() {
    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(meta.data.name)
    }

    override fun visit(replacer: TemplateReplacerLine) {
        replacer.insert(self().id.toString())
    }

    override fun visit(replacer: TemplateReplacerImports) {
        replacer.insert(ImportHelp.createImport(meta.data.name,
                                                ImportHelp.createRelativePath("service/domain/http",
                                                                              "domain/${meta.data.prefix.convertPackageToFileSystem()}"),
                                                meta.data.name.lowerFirstLetter()))

        replacer.insert(ImportHelp.createImport("HttpService",
                                                ImportHelp.createRelativePath("service/domain/http", RelevantPathes.HTTP_SERVICE.path),
                                                RelevantPathes.HTTP_SERVICE.file))
    }

    override fun visit(replacer: TemplateReplacerLowerClassName) {
        replacer.insert(meta.data.name.lowerFirstLetter())
    }
}