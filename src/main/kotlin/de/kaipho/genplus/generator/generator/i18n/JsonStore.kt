package de.kaipho.genplus.generator.generator.i18n

import de.kaipho.genplus.generator.ContextSingleton
import org.json.JSONObject

object JsonStore : ContextSingleton {
    var files: MutableMap<String, JSONObject>? = null

    fun load() {
        this.files = JsonFileLoader.loadI18nFiles()

        add("button.addElement")
    }

    override fun reset() {
        files?.clear()
    }

    /**
     * adds the value to all language files. The key can be a path (e.g. el1.el2.key).
     */
    fun add(key: String, value: String = ""): String {
        val keyParts = key.split(".")
        this.files?.forEach { entry ->
            var actual = entry.value
            for (i in 0..keyParts.size - 2) {
                if (!actual.has(keyParts[i])) {
                    actual.put(keyParts[i], JSONObject())
                }
                actual = actual.getJSONObject(keyParts[i])
            }
            val text = if(value != "") value else keyParts.last()
            if (!actual.has(keyParts.last())) {
                actual.putOnce(keyParts.last(), text)
            } else if (actual.get(keyParts.last()) == "") {
                actual.put(keyParts.last(), text)
            }
        }
        return key
    }

    /**
     * saves all the changed data to the file system
     */
    fun finalize() {
        JsonFileLoader.writeAllI18nFiles()
    }
}