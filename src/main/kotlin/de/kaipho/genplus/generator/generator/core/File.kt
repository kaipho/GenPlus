package de.kaipho.genplus.generator.generator.core

import de.kaipho.genplus.generator.core.obj.options.Option
import de.kaipho.genplus.generator.io.AbstractFileSystem
import de.kaipho.genplus.generator.io.FileSystem
import de.kaipho.genplus.generator.io.TemplateLoader
import de.kaipho.genplus.generator.io.loader.FileStore
import de.kaipho.genplus.generator.store.SettingsStore
import de.kaipho.genplus.generator.template.Templates

/**
 * Created by tom on 22.07.16.
 */
enum class File(val path: String, val file: String, vararg val replacer: String) {
    TS_DOMAIN_OBJECT_VISITOR(PATH_TS, "domain/domain.object.visitor.ts"),
    TS_DOMAIN_VISITOR(PATH_TS, "domain/:${FILE_PREFIX}/:${LOWER_NAME}.visitor.ts", FILE_PREFIX, LOWER_NAME),
    TS_REP_VISITOR(PATH_TS, "domain/rep.visitor.ts"),
    TS_TYPE_VISITOR(PATH_TS, "domain/type.visitor.ts"),
    TS_DOMAIN_HTTP_SERVICE(PATH_TS, "service/domain/http/:${LOWER_NAME}.http.service.ts", LOWER_NAME),
    TS_HTTP_SERVICE_CONSTANTS(PATH_TS, "service/domain/http/services.ts"),
    TS_COMPONENT_CONSTANTS(PATH_TS, "components/components.ts"),
    TS_DOMAIN_CONTROLLER(PATH_TS, "components/domain/:${FILE_PREFIX}/:${LOWER_NAME}.component.ts", FILE_PREFIX, LOWER_NAME),
    TS_DOMAIN_ROUTES(PATH_TS, "components/domain/:${FILE_PREFIX}/:${LOWER_NAME}.routes.ts", FILE_PREFIX, LOWER_NAME),
    TS_DOMAIN_LIST_CONTROLLER(PATH_TS, "components/domain/:${FILE_PREFIX}/:${LOWER_NAME}.list.component.ts", FILE_PREFIX, LOWER_NAME),
    TS_DOMAIN_DETAILS_CONTROLLER(PATH_TS, "components/domain/:${FILE_PREFIX}/:${LOWER_NAME}.detail.component.ts", FILE_PREFIX, LOWER_NAME),
    TS_DOMAIN_CLASS(PATH_TS, "domain/:${FILE_PREFIX}/:${LOWER_NAME}.ts", FILE_PREFIX, LOWER_NAME),
    TS_DOMAIN_ABSTRACT_CLASS(PATH_TS, "domain/:${FILE_PREFIX}/:${LOWER_NAME}.ts", FILE_PREFIX, LOWER_NAME),
    TS_SERVICE_CONTROLLER(PATH_TS, "components/service/:${FILE_PREFIX}/:${NAME}.component.ts", FILE_PREFIX, NAME),
    TS_SERVICE_DELETE_CONTROLLER(PATH_TS, "components/service/:${FILE_PREFIX}/:${LOWER_NAME}.delete.component.ts", FILE_PREFIX, LOWER_NAME),
    TS_HTTP_SERVICE(PATH_TS, "service/functions/http/:${LOWER_NAME}.http.service.ts", LOWER_NAME),
    TS_MODULE(PATH_TS, "app.module.ts"),

    HTML_DOMAIN_FORM(PATH_TS, "components/domain/:${FILE_PREFIX}/:${LOWER_NAME}.component.html", FILE_PREFIX, LOWER_NAME),
    HTML_DOMAIN_LIST(PATH_TS, "components/domain/:${FILE_PREFIX}/:${LOWER_NAME}.list.component.html", FILE_PREFIX, LOWER_NAME),
    HTML_DOMAIN_DETAIL(PATH_TS, "components/domain/:${FILE_PREFIX}/:${LOWER_NAME}.detail.component.html", FILE_PREFIX, LOWER_NAME),
    HTML_SERVICE_COMPONENT(PATH_TS, "components/service/:${FILE_PREFIX}/:${NAME}.component.html", FILE_PREFIX, NAME),
    HTML_SERVICE_DELETE_COMPONENT(PATH_TS, "components/service/:${FILE_PREFIX}/:${LOWER_NAME}.delete.component.html", FILE_PREFIX, LOWER_NAME),

    JAVA_DOMAIN_CLASS(PATH_JAVA, "domain:${FILE_PREFIX}/impl/:${NAME}Impl.java", PREFIX, NAME, FILE_PREFIX),
    JAVA_DOMAIN_CLASS_REPOSITORY_PROXY(PATH_JAVA, "domain:${FILE_PREFIX}/impl/:${NAME}RepositoryProxy.java", PREFIX, NAME, FILE_PREFIX),
    JAVA_DOMAIN_CLASS_INTERFACE(PATH_JAVA, "domain:${FILE_PREFIX}/:${NAME}.java", PREFIX, NAME, FILE_PREFIX),
    JAVA_DOMAIN_CLASS_VISITOR(PATH_JAVA, "domain:${FILE_PREFIX}/:${NAME}Visitor.java", PREFIX, NAME, FILE_PREFIX),
    JAVA_DOMAIN_CLASS_COMPLETE_VISITOR(PATH_JAVA, "domain:${FILE_PREFIX}/:${NAME}CompleteVisitor.java", PREFIX, NAME, FILE_PREFIX),
    JAVA_DOMAIN_CLASS_SIMPLE_VISITOR(PATH_JAVA, "domain:${FILE_PREFIX}/:${NAME}SimpleVisitor.java", PREFIX, NAME, FILE_PREFIX),
    JAVA_DOMAIN_REPOSITORY(PATH_JAVA, "repository/:${NAME}Repository.java", PREFIX, NAME),
    JAVA_DOMAIN_DERIVED_MANAGER(PATH_JAVA, "domain/core/DerivedManager.java", PREFIX),
    JAVA_SERVICE(PATH_JAVA, "service:${FILE_PREFIX}/:${NAME}.java", PREFIX, NAME, FILE_PREFIX),
    JAVA_SERVICE_IMPL(PATH_JAVA, "service:${FILE_PREFIX}/:${NAME}Impl.java", PREFIX, NAME, FILE_PREFIX),
    JAVA_SERVICE_TEST(PATH_JAVA_TEST, "service:${FILE_PREFIX}/:${NAME}Test.java", PREFIX, NAME, FILE_PREFIX),
    JAVA_CONTROLLER(PATH_JAVA, "controller:${FILE_PREFIX}/:${NAME}Controller.java", PREFIX, NAME, FILE_PREFIX),
    JAVA_MAIN(PATH_JAVA, "Application.java", PREFIX),
    JAVA_DATA_REST_CONFIG(PATH_JAVA, "config/DataRestConfig.java", PREFIX),
    JAVA_SECURITY_CONFIG(PATH_JAVA, "config/SecurityConfig.java", PREFIX),
    JAVA_JWT_SECURITY_FILTER(PATH_JAVA, "core/security/JwtSecurityFilter.java", PREFIX),
    JAVA_JWT_TOKEN_PROVIDER(PATH_JAVA, "core/security/JwtTokenProvider.java", PREFIX),
    JAVA_USER_VM(PATH_JAVA, "controller/core/LoginVM.java", PREFIX),

    SASS_COLOR_FILE(PATH_TS_FOLDER, "src/assets/style/colors.scss"),

    C_ESP32_DOMAIN_CLASS(PATH_C_ESP32, "domain/:${FILE_PREFIX}/:${NAME}.h", FILE_PREFIX, NAME),
    C_ESP32_DOMAIN_JSON_SERVICE(PATH_C_ESP32, "domain/:${FILE_PREFIX}/:${NAME}JsonService.h", FILE_PREFIX, NAME),



}

interface IFile {
    val target: String
    val template: Templates
}


// TODO Condition
enum class FileToCopy(override val target: String, override val template: Templates) : IFile {
    F001("web/src/app/domain/domain.object.ts", Templates.TS_DOMAIN_OBJECT),
    // F003("web/src/app/service/domain/http/abstract.http.service.ts", Templates.TS_DOMAIN_SUPER_HTTP_SERVICE),
    F004("web/src/app/service/domain/http/pageable.ts", Templates.TS_HTTP_PAGEABLE),
 }

fun copyFiles(files: Array<out IFile>, fileSystem: AbstractFileSystem = FileSystem) {
    files.forEach { file ->
        if (SettingsStore.options.isSelected(Option.LANGS, file.template.lang.name)) {
            TemplateLoader.loadTemplate(file.template).apply {
                fileSystem.writeFile(SettingsStore.runningPath, "", file.target, this)
            }
        }
    }
}

const val PREFIX = "prefix"
const val NAME = "name"
const val LOWER_NAME = "lowerName"
const val FILE_PREFIX = "filePrefix"

const val PATH_TS = "web/src/app"
const val PATH_C_ESP32 = "esp32/src"
const val PATH_TS_FOLDER = "web"
const val PATH_JAVA = "server/src/main/java/:${PREFIX}"
const val PATH_JAVA_TEST = "server/src/test/java/:${PREFIX}"
const val PATH_POM = "server"
const val PATH_SQL = "server/src/main/resources/database"