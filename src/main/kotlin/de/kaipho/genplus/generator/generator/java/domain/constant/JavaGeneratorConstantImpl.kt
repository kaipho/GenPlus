package de.kaipho.genplus.generator.generator.java.domain.constant

import de.kaipho.genplus.generator.common.convertPackageToFileSystem
import de.kaipho.genplus.generator.domain.obj.Constant
import de.kaipho.genplus.generator.domain.obj.ConstantClass
import de.kaipho.genplus.generator.domain.obj.ConstantContainer
import de.kaipho.genplus.generator.domain.obj.ConstantVisitor
import de.kaipho.genplus.generator.generator.java.core.ExternalJavaGenerator
import de.kaipho.genplus.generator.generator.java.core.fetchHierarchy
import de.kaipho.genplus.generator.generator.java.core.fetchHierarchyWrongWay
import de.kaipho.genplus.generator.generator.java.core.fetchSimpleHierarchy
import de.kaipho.genplus.generator.template.replacer.*

class JavaGeneratorConstantImpl(val constant: ConstantClass) : ExternalJavaGenerator() {
    override val templateString = "domain/constant/xxx/xxxImpl.java"
    override val replacer = arrayListOf(constant.prefix.convertPackageToFileSystem().removeSuffix("/"), constant.name)


    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(constant.name)
    }

    override fun visit(replacer: TemplateReplacerParams) {
        replacer.insert(constant.parameter.joinToString(transform = { "Object $it" }))
    }

    override fun visit(replacer: TemplateReplacerSubpackage) {
        replacer.insert(constant.prefix.removeSuffix("."))
    }

    override fun visit(replacer: TemplateReplacerLine) {
        constant.parameter.forEach {
            replacer.insert("data.put(\"$it\", $it);")
        }
    }
}