package de.kaipho.genplus.generator.generator.java.service

import de.kaipho.genplus.generator.common.convertPackageToFileSystem
import de.kaipho.genplus.generator.common.lowerFirstLetter
import de.kaipho.genplus.generator.common.upperFirstLetter
import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.core.obj.*
import de.kaipho.genplus.generator.core.obj.service.ClassDummy
import de.kaipho.genplus.generator.core.obj.service.Function
import de.kaipho.genplus.generator.core.obj.service.ServiceClass
import de.kaipho.genplus.generator.domain.obj.*
import de.kaipho.genplus.generator.generator.AbstractGenerator
import de.kaipho.genplus.generator.generator.core.File
import de.kaipho.genplus.generator.generator.core.Generator
import de.kaipho.genplus.generator.generator.core.Ref
import de.kaipho.genplus.generator.store.SettingsStore
import de.kaipho.genplus.generator.template.Templates
import de.kaipho.genplus.generator.template.replacer.*

@Generator(
        lang = Language.JAVA,
        file = File.JAVA_CONTROLLER,
        appliedOn = ServiceClass::class,
        template = Templates.JAVA_CONTROLLER,
        dependsOn = Ref.IS_EXPORT
)
class GeneratorController : AbstractGenerator<ServiceClass>() {
    override fun visit(replacer: TemplateReplacerPackage) {
        lines.add(replacer.replaceOneLine(actual, SettingsStore.getPrefixPackage()))
    }

    override fun visit(replacer: TemplateReplacerSubpackage) {
        replacer.insert(getData().prefix)
    }

    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(getData().name)
    }

    override fun visit(replacer: TemplateReplacerLowerClassName) {
        replacer.insert(getData().name.lowerFirstLetter())
    }

    override fun visit(replacer: TemplateReplacerName) {
        replacer.insert(getData().prefix.convertPackageToFileSystem())
    }

    override fun visit(replacer: TemplateReplacerImports) {
        val import = Imports()
        import.addServiceImport(getData())

        import.addJavaImport(JavaImport.LIST)

        getAllParamClasses().forEach {
            import.addDomainImport(it)
        }



        getData().functions.forEach {
            import.addAssociationImport(it.returnType)
            import.addFunctionCommandImport(it)
            it.params.forEach { param ->
                import.addAssociationImport(param.type)
            }
        }

        replacer.insertAll(import.getimports())
    }

    override fun visit(replacer: TemplateReplacerAssociations) {
        replacer.insert("")

//        TODO getAllParamClasses().map { it.name }.toHashSet().forEach {
//            replacer.insert("@Autowired")
//            replacer.insert("private ${it}Repository ${it.lowerFirstLetter()}Repository;")
//        }
    }

    override fun visit(replacer: TemplateReplacerLine) {
        getData().functions.forEach { function ->
            replacer.insert("@PostMapping(\"/${function.name}\")")
            replacer.insert("public ${function.returnType.javaAbstractRep()} ${function.name}(@RequestBody Map<String, Object> obj) {")
            if (function.extends !is ClassDummy) {
                replacer.insert(
                        "    ${function.extends.name} ${function.extends.name.lowerFirstLetter()}1 = ${function.extends.name}.findOneById(Long.parseLong(obj.get(\"${function.extends.name.lowerFirstLetter()}\").toString()));")
            }
            function.params.filter { it.type is AssoziationTypeUser }.forEach {
                (it.type as AssoziationTypeUser).acceptClass(object : UserClassVisitor<Unit> {
                    override fun visitPrimitive(association: AssoziationTypeUser, clazz: PrimitiveClass) {
                        // no converting
                    }

                    override fun visitCategory(association: AssoziationTypeUser, clazz: CategoryClass) {
                        // no converting
                    }

                    override fun visitDomain(association: AssoziationTypeUser, clazz: DomainClass) {
                        replacer.insert("    ${it.type.javaRep()} ${it.name}1 = ${it.type.javaRep()}.findOneById(Long.parseLong(obj.get(\"${it.name}\").toString()));")
                    }

                    override fun visitConstant(association: AssoziationTypeUser, clazz: Constant) {
                        // no converting
                    }
                })
            }
            replacer.insert("    ${function.getQualifiedClassName()}Command command = new ${function.getQualifiedClassName()}Command(${getParamsInnerCall(function)}) {")
            replacer.insert("        @Override")

            if (function.returnType is AssoziationTypeUnit) {
                replacer.insert("        protected Void executeImpl() {")
                replacer.insert("            ${getData().name.lowerFirstLetter()}.${function.name}(${getGetterInnerCall(function)});")
                replacer.insert("            return null;")
                replacer.insert("        }")
            } else {
                replacer.insert("        protected ${function.returnType.javaAbstractRep()} executeImpl() {")
                replacer.insert("             return ${getData().name.lowerFirstLetter()}.${function.name}(${getGetterInnerCall(function)});")
                replacer.insert("        }")
            }
            replacer.insert("    };")
            if (function.returnType is AssoziationTypeUnit) {
                replacer.insert("    Transaction.getInstance().run(command).resolve();")
            } else {
                replacer.insert("    return Transaction.getInstance().run(command).resolve();")
            }
            replacer.insert("}")
        }
    }

//    private fun getParams(function: Function): String {
//        var result = ""
//        if (function.extends !is ClassDummy) {
//            result += "Long ${function.extends.name.lowerFirstLetter()}, "
//        }
//        function.params.forEach { param ->
//            if (param.type is AssoziationTypeUser) {
//                result += "Long ${param.name}, "
//            } else if (param.type is AssoziationTypeDate) {
//                result += "String ${param.name}, "
//            } else {
//                result += "${param.type.javaAbstractRep()} ${param.name}, "
//            }
//        }
//        return result.removeSuffix(", ")
//    }

    private fun getGetterInnerCall(function: Function): String {
        val result = ArrayList<String>()

        if(function.extends !is ClassDummy) {
            result.add("get${function.extends.name.upperFirstLetter()}()")
        }

        function.params.map {
            "get${it.name.upperFirstLetter()}()"
        }.forEach { result.add(it) }

        if(result.isEmpty()) {
            return ""
        }
        return "\n                        " + result.joinToString(separator = ",\n                        ")
    }

    private fun getParamsInnerCall(function: Function): String {
        val result = ArrayList<String>()
        if (function.extends !is ClassDummy) {
            result += "${function.extends.name.lowerFirstLetter()}1"
        }
        function.params.forEach { param ->
            param.type.accept(object : AssociationVisitor<Unit> {
                override fun visit(associationTypeBlob: AssociationTypeBlob) = standard("BLOB")

                private fun standard(inner: String) {
                    result += "PARAM_$inner.apply(obj.get(\"${param.name}\"))"
                }

                override fun visit(association: AssoziationTypeSecure) = standard("STRING")

                override fun visit(association: AssoziationTypeString) = standard("STRING")

                override fun visit(association: AssoziationTypeInteger) = standard("INTEGER")

                override fun visit(association: AssoziationTypeDate) = standard("DATE")

                override fun visit(association: AssoziationTypeBoolean) = standard("BOOLEAN")

                override fun visit(association: AssoziationTypeUser) {
                    association.acceptClass(object : UserClassVisitor<Unit> {
                        override fun visitPrimitive(association: AssoziationTypeUser, clazz: PrimitiveClass) {
                            standard("PRIMITIVE")
                        }

                        override fun visitCategory(association: AssoziationTypeUser, clazz: CategoryClass) {
                            result += "${clazz.name}.from(PARAM_STRING.apply(obj.get(\"${param.name}\")))"
                        }

                        override fun visitDomain(association: AssoziationTypeUser, clazz: DomainClass) {
                            result += "${param.name}1"
                        }

                        override fun visitConstant(association: AssoziationTypeUser, clazz: Constant) {
                            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                        }

                    })
                }

                override fun visit(association: AssoziationTypeCollection) {
                    // No Mapping
                }

                override fun visit(association: AssoziationTypeMap) {
                    // No Mapping
                }

                override fun visit(association: AssoziationTypeDummy) {
                    // No Mapping
                }

                override fun visit(association: AssoziationTypeUnit) {
                    // No Mapping
                }

                override fun visit(association: AssoziationTypeDouble) = standard("DOUBLE")

                override fun visit(association: AssoziationTypeBigNumber) {
                    standard("DECIMAL")
                }

                override fun visit(association: AssoziationTypeText) = standard("STRING")

                override fun visit(association: AssoziationTypeOptional) {
                    // No Mapping
                }

                override fun visit(association: AssoziationTypePassword) = standard("PASSWORD")
            })
        }
        return "\n                    " + result.joinToString(separator = ",\n                    ")
    }

    private fun getAllParamClasses(): List<IClass> {
        val classList = ArrayList<IClass>()
        self().functions.forEach { function ->
            addClassToList(classList, function.extends)
            function.params.filter { it.type is AssoziationTypeUser }.map { (it.type as AssoziationTypeUser).clazz }.forEach {
                addClassToList(classList, it)
            }
            function.params.filter {
                it.type is AssoziationTypeCollection && (it.type as AssoziationTypeCollection).inner is AssoziationTypeUser
            }.map {
                ((it.type as AssoziationTypeCollection).inner as AssoziationTypeUser).clazz
            }.forEach {
                addClassToList(classList, it)
            }
        }
        return classList
    }

    private fun addClassToList(classList: MutableList<IClass>, clazz: IClass) {
        if (clazz is ClassDummy) {
            return
        }
        if (classList.filter { it.name == clazz.name && it.prefix == clazz.prefix }.isEmpty()) {
            classList.add(clazz)
        }
    }
}
