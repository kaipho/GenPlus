package de.kaipho.genplus.generator.generator.java.domain

import de.kaipho.genplus.generator.common.upperFirstLetter
import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.constants.STRINGS
import de.kaipho.genplus.generator.core.obj.AssoziationTypeCollection
import de.kaipho.genplus.generator.core.obj.AssoziationTypeInteger
import de.kaipho.genplus.generator.core.obj.AssoziationTypeUser
import de.kaipho.genplus.generator.core.scanner.KeywordHolder
import de.kaipho.genplus.generator.domain.obj.*
import de.kaipho.genplus.generator.generator.AbstractGenerator
import de.kaipho.genplus.generator.generator.core.File
import de.kaipho.genplus.generator.generator.core.Generator
import de.kaipho.genplus.generator.generator.core.RuleHelp
import de.kaipho.genplus.generator.generator.getInlinedAssociations
import de.kaipho.genplus.generator.generator.java.core.addClassToList
import de.kaipho.genplus.generator.generator.java.core.fetchAllAssociationsAndChildClasses
import de.kaipho.genplus.generator.generator.java.core.fetchAllNonAbstractChildClassesRekursiv
import de.kaipho.genplus.generator.generator.java.core.fetchAllParents
import de.kaipho.genplus.generator.generator.java.domain.methods.MethodAddSingleToGenerator
import de.kaipho.genplus.generator.generator.java.domain.methods.MethodForI
import de.kaipho.genplus.generator.generator.java.domain.methods.MethodGetterGenerator
import de.kaipho.genplus.generator.generator.java.domain.methods.MethodSetterGenerator
import de.kaipho.genplus.generator.generator.java.service.CoreFramework
import de.kaipho.genplus.generator.generator.java.service.Imports
import de.kaipho.genplus.generator.generator.java.service.JavaImport
import de.kaipho.genplus.generator.generator.recursiveAssociationFetcher
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.store.SettingsStore
import de.kaipho.genplus.generator.template.TemplateParser
import de.kaipho.genplus.generator.template.Templates
import de.kaipho.genplus.generator.template.replacer.*
import de.kaipho.genplus.generator.template.replacer.*
import java.util.*


@Generator(
        lang = Language.JAVA,
        template = Templates.DOMAIN_CLASS_INTERFACE2,
        appliedOn = AbstractDomainClass::class,
        file = File.JAVA_DOMAIN_CLASS_INTERFACE
)
class DomainClassInterfaceAbstractClassGenerator : AbstractGenerator<AbstractDomainClass>() {
    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(self().name)
    }

    override fun visit(replacer: TemplateReplacerImplements) {
        var extends = "extends PersistentElement"
        if (self().extends.isNotEmpty()) {
            extends = extends + ", " + self().extends.map { it.name }.joinToString()
        }
        replacer.insert(extends)
    }

    override fun visit(replacer: TemplateReplacerPackage) {
        lines.add(replacer.replaceOneLine(actual, SettingsStore.getPrefixPackage()))
    }

    override fun visit(replacer: TemplateReplacerComment) {
        replacer.insert(meta.data.comment)
    }

    override fun visit(replacer: TemplateReplacerImports) {
        val imports = Imports()

        if (SettingsStore.databaseEnabled()) {
            imports.addCoreImport(CoreFramework.DB_FACADE)
            imports.addCoreImport(CoreFramework.DB_EXCEPTION)
            imports.addCoreImport(CoreFramework.DB_CONNECTOR)
            imports.addJavaImport(JavaImport.OPTIONAL_INT)
            imports.addJavaImport(JavaImport.LIST)
            imports.addJavaImport(JavaImport.DB_STATEMENT)
            imports.addJavaImport(JavaImport.COLLECTORS)
            imports.addJavaImport(JavaImport.ARRAYS)
        }

        if (RuleHelp.hasRule(self(), KeywordHolder.RULE_USER_ATTRIBUTE) != null) {
            imports.addCoreImport(CoreFramework.SECURITY_CONTEXT)
            imports.addJavaImport(JavaImport.DB_EXCEPTION)
        }

        recursiveAssociationFetcher(meta.data, true).union(getInlinedAssociations(meta.data)).forEach(imports::addAssociationImport)

        recursiveAssociationFetcher(getData(), false).forEach {
            if (it.type is AssoziationTypeUser && it.type.clazz !is AbstractDomainClass)
                imports.addDomainImport(it.type.clazz)
            if (it.type is AssoziationTypeCollection && it.type.inner is AssoziationTypeUser && it.type.inner.clazz !is AbstractDomainClass) {
                imports.addDomainImport(it.type.inner.clazz)
            }
        }

        fetchAllNonAbstractChildClassesRekursiv(self()).forEach {
            imports.addDomainRepository(it)
        }

        ClassStore.domain.domainClasses.filter { it is AbstractDomainClass }.forEach { clazz ->
            fetchAllAssociationsAndChildClasses(clazz).filter { it.name == getData().name }.forEach {
                imports.addDomainVisitorImport(clazz)
            }
        }
        self().associations.filter { it.isDerived() }.forEach {
            imports.addImport("domain.core.DerivedManager")
        }
        replacer.insertAll(imports.getimports())
    }

    override fun visit(replacer: TemplateReplacerSubpackage) {
        replacer.insert(meta.data.prefix)
    }

    override fun visit(replacer: TemplateReplacerGetter) {
/*        self().associations.filter {
            it.rules.contains(InlineRule())
        }.forEach { assiciation ->
            if (assiciation.type is AssoziationTypeUser) {
                self().associations.filter { RuleHelp.hasRuleDerived(it) == null }.forEach {
                    val getterGenerator = MethodGetterGenerator(MethodForI(it.name, it.type.javaAbstractRep()))
                    replacer.insertAll(TemplateParser.parseTemplate(getterGenerator))
                }
            } else {
                throw RuntimeException(STRINGS.RULE_INLINE + " must be on UserTypes")
            }
        }*/
        self().associations.forEach { getterAssociationAbstract(it) { it -> replacer.insertAll(it) } }
    }

    override fun visit(replacer: TemplateReplacerSetter) {
        self().associations.forEach { setterAssociationAbstract(it) { it -> replacer.insertAll(it) } }
    }

    override fun visit(replacer: TemplateReplacerAccept) {
        val visitable = ArrayList<IClass>()
        ClassStore.domain.domainClasses.filter { it is AbstractDomainClass }.forEach { clazz ->
            fetchAllAssociationsAndChildClasses(clazz).filter { it.name == getData().name }.forEach {
                addClassToList(visitable, clazz)
            }
        }
        replacer.insert("<D> D accept(${self().name}Visitor<D> visitor);")
        replacer.insert("void accept(${self().name}SimpleVisitor visitor);")
        replacer.insert("<D> D accept(${self().name}CompleteVisitor<D> visitor);")

        visitable.forEach {
            replacer.insert("<D> D accept(${it.name}Visitor<D> visitor);")
            replacer.insert("void accept(${it.name}SimpleVisitor visitor);")
        }
        getAllParents(self().extends).forEach {
            replacer.insert("<D> D accept(${it.name}CompleteVisitor<D> visitor);")
        }
    }

    fun getAllParents(clazz: List<AbstractClass>): MutableList<AbstractClass> {
        val list: MutableList<AbstractClass> = ArrayList()
        clazz.forEach {
            list.add(it)
            if (it.extends.isNotEmpty()) {
                list.addAll(getAllParents(it.extends))
            }
        }
        return list
    }

    override fun visit(replacer: TemplateReplacerLine) {
        when (replacer.id) {
            3 -> {
                val ids = fetchAllNonAbstractChildClassesRekursiv(self()).map { it.id }.joinToString()
                val sql = if (RuleHelp.hasRule(self(), KeywordHolder.RULE_USER_ATTRIBUTE) != null) {
                    "SELECT obj.id, obj.instanceof FROM obj, link WHERE obj.instanceof IN ($ids) AND obj.id = link.toObj AND link.fromObj = ? AND link.instanceof = -1"
                } else {
                    "SELECT id, instanceof FROM obj WHERE instanceof IN ($ids)"
                }

                val classname = self().name
                replacer.apply {
                    if (SettingsStore.databaseEnabled()) {
                        insert("static $classname findOneById(Long id) throws DbException {")
                        insert("    if(id == null) return null;")
                        insert("    OptionalInt optionalClassId = DbFacade.getClassIdForInstanceId(id);")
                        insert("    int classId = optionalClassId.orElseThrow(DbException::new);")
                        insert("    return mapToInstance(classId, id);")
                        insert("}")
                        insert("")
                        insert("static $classname mapToInstance(Integer classId, Long id) {")
                        fetchAllNonAbstractChildClassesRekursiv(self()).forEach {
                            insert("    if(classId == ${it.id}) {")
                            insert("        return new ${it.name}RepositoryProxy(id);")
                            insert("    }")
                        }
                        insert("    throw new DbException(\"Class id not handled in the case '${self().name}'!\");")
                        insert("}")
                        insert("static List<$classname> findAll() throws DbException {")
//                        insert("    PreparedStatement statement = DbConnector.getConnector()")
//                        insert("                                             .getStatementFor(\"$sql\");")
                        if (RuleHelp.hasRule(self(), KeywordHolder.RULE_USER_ATTRIBUTE) != null) {
                            """
                            |try {
                            |    statement.setLong(1, SecurityContext.getUser().getId());
                            |} catch (SQLException e) {
                            |    throw new DbException(e);
                            |}
                            """.insertInto(replacer)
                        }
                        insert("    return DbFacade.getAll(${self().id}L, $classname::mapToInstance);")
                        insert("}")
                        insert("")
                        insert("static List<$classname> findBy(Long association, Object s) throws DbException {")
                        insert("    return DbFacade.getByIdAndAssociationLike(${self().id}L, association, s)")
                        insert("                   .stream()")
                        insert("                   .map($classname::findOneById)")
                        insert("                   .collect(Collectors.toList());")
                        insert("}")
                    } else {
                        insert("static $classname findOneById(Long id) {")
                        insert("    throw new RuntimeException(\"DB not supported!\");")
                        insert("}")
                    }
                }
            }
        }
    }
}
