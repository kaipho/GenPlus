package de.kaipho.genplus.generator.generator.java.domain

import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.domain.obj.AbstractDomainClass
import de.kaipho.genplus.generator.generator.AbstractGenerator
import de.kaipho.genplus.generator.generator.core.File
import de.kaipho.genplus.generator.generator.core.Generator
import de.kaipho.genplus.generator.generator.core.RuleHelp
import de.kaipho.genplus.generator.generator.java.core.fetchAllParents
import de.kaipho.genplus.generator.generator.java.service.Imports
import de.kaipho.genplus.generator.store.SettingsStore
import de.kaipho.genplus.generator.template.Templates
import de.kaipho.genplus.generator.template.replacer.*

@Generator(
        lang = Language.JAVA,
        template = Templates.ABSTRACT_DOMAIN_CLASS,
        appliedOn = AbstractDomainClass::class,
        file = File.JAVA_DOMAIN_CLASS
)
class AbstractDomainClassGenerator : AbstractGenerator<AbstractDomainClass>() {
    override fun visit(replacer: TemplateReplacerImports) {
        val imports = Imports()

        self().associations.filter { it.isDerived() }.forEach {
            imports.addImport("domain.core.DerivedManager")
        }
        replacer.insertAll(imports.getimports())
    }

    override fun visit(replacer: TemplateReplacerAnnotations) {

    }

    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(meta.data.name)
    }

    override fun visit(replacer: TemplateReplacerImplements) {
        if (getData().extends.isNotEmpty()) {
            replacer.insert("extends ${getData().extends[0].name}")
        } else
            lines.add(replacer.replaceOneLine(actual, ""))
    }

    override fun visit(replacer: TemplateReplacerPackage) {
        lines.add(replacer.replaceOneLine(actual, SettingsStore.getPrefixPackage()))
    }

    override fun visit(replacer: TemplateReplacerComment) {
        replacer.insert(meta.data.comment)
    }

    override fun visit(replacer: TemplateReplacerSubpackage) {
        replacer.insert(meta.data.prefix)
    }

    override fun visit(replacer: TemplateReplacerAccept) {
        fetchAllParents(self()).forEach {
            replacer.insert("@Override")
            replacer.insert("public <D> D accept(${it.name}Visitor<D> visitor) {")
            replacer.insert("   return visitor.visit(this);")
            replacer.insert("}")
            replacer.insert("@Override")
            replacer.insert("public void accept(${it.name}SimpleVisitor visitor) {")
            replacer.insert("   visitor.visit(this);")
            replacer.insert("}")
        }
    }
}
