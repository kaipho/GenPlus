package de.kaipho.genplus.generator.generator.core

import de.kaipho.genplus.generator.ContextSingleton
import de.kaipho.genplus.generator.common.upperFirstLetter
import de.kaipho.genplus.generator.constants.STRINGS
import de.kaipho.genplus.generator.generator.AbstractGenerator
import de.kaipho.genplus.generator.generator.java.core.ExternalTemplate
import de.kaipho.genplus.generator.generator.java.core.SimpleExternalGenerator
import de.kaipho.genplus.generator.io.AbstractFileSystem
import de.kaipho.genplus.generator.io.merger.FileMergerFaccade
import de.kaipho.genplus.generator.store.SettingsStore
import de.kaipho.genplus.generator.template.TemplateParser
import java.util.*

/**
 * This pool can be filled with generators, which should run after the generation is finished.
 * This helps to decide while generating which classes also should be generated!
 */
object GenerationPool : ContextSingleton {
    private val lateRunGenerators = ArrayList<AbstractGenerator<out Any>>()
    private val lastRunGenerators = ArrayList<AbstractGenerator<out Any>>()
    private val files = HashSet<String>()

    override fun reset() {
        this.lateRunGenerators.clear()
        this.lastRunGenerators.clear()
        this.files.clear()
    }

    fun addGenerator(generator: AbstractGenerator<out Any>, resultFile: String, last: Boolean = false) {
        if (!files.contains(resultFile)) {
            if (last)
                lastRunGenerators.add(generator)
            else
                lateRunGenerators.add(generator)
            files.add(resultFile)
        }
    }

    fun runGenerators(fileSystem: AbstractFileSystem) {
        lateRunGenerators.forEach {
            val annotation = it.javaClass.getAnnotation(Generator::class.java)

            if (it is SimpleExternalGenerator) {
                callSimpleExternalGenerator(it, fileSystem)
            } else
                callGenerator(annotation, it, fileSystem)
        }
        println("Generating default implementations...")
        lastRunGenerators.forEach {
            val annotation = it.javaClass.getAnnotation(Generator::class.java)

            if (it is SimpleExternalGenerator) {
                callSimpleExternalGenerator(it, fileSystem)
            } else
                callGenerator(annotation, it, fileSystem)
        }
    }

    fun callSimpleExternalGenerator(it: SimpleExternalGenerator, fileSystem: AbstractFileSystem) {
        val path = replacePlaceholder(it.getPath(), arrayOf(PREFIX), null)

        if (it.mode == GeneratorType.PROCESS) {
            TemplateParser.parseTemplate(ExternalTemplate(it.templateString, it.lang), it).apply {
                fileSystem.writeFile(SettingsStore.runningPath, path, it.getResultFile(), this)
            }
        } else if (it.mode == GeneratorType.MERGE) {
            TemplateParser.parseTemplate(ExternalTemplate(it.templateString, it.lang), it).apply {
                FileMergerFaccade.merge(it.getResultFile(), it.lang, this).apply {
                    fileSystem.writeFile(SettingsStore.runningPath, path, it.getResultFile(), this)
                }
            }
        } else if (it.mode == GeneratorType.ONCE) {
            if(fileSystem.exist(SettingsStore.runningPath, path, it.getResultFile())) return
            TemplateParser.parseTemplate(ExternalTemplate(it.templateString, it.lang), it).apply {
                fileSystem.writeFile(SettingsStore.runningPath, path, it.getResultFile(), this)
            }
        }
    }

    fun callGenerator(annotation: Generator, it: AbstractGenerator<*>, fileSystem: AbstractFileSystem) {
        val path = replacePlaceholder(annotation.file.path, annotation.file.replacer, it.getData()!!)
        val file = replacePlaceholder(annotation.file.file, annotation.file.replacer, it.getData()!!)

        if (annotation.type == GeneratorType.PROCESS) {
            TemplateParser.parseTemplate(annotation.template, it).apply {
                fileSystem.writeFile(SettingsStore.runningPath, path, file, this)
            }
        } else if (annotation.type == GeneratorType.MERGE) {
            TemplateParser.parseTemplate(annotation.template, it).apply {
                FileMergerFaccade.merge(file, annotation.lang, this).apply {
                    fileSystem.writeFile(SettingsStore.runningPath, path, file, this)
                }
            }
        }
    }

    fun replacePlaceholder(string: String, placeholder: Array<out String>, data: Any?): String {
        var result = string
        placeholder.filter { string.contains(it) }.forEach {
            if (staticReplacements.contains(it)) {
                result = result.replace(":$it", staticReplacements[it]!!)
            } else if (data != null) {
                val returnValue = data.javaClass.getMethod("get${it.upperFirstLetter()}").invoke(data)
                result = result.replace(":$it", returnValue.toString())
            }
        }
        return result
    }

    val staticReplacements = hashMapOf(
            PREFIX to STRINGS.PREFIX_PLACEHOLDER
    )
}