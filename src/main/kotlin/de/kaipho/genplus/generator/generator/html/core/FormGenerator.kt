package de.kaipho.genplus.generator.generator.html.core

import de.kaipho.genplus.generator.common.lowerFirstLetter
import de.kaipho.genplus.generator.common.upperFirstLetter
import de.kaipho.genplus.generator.core.exceptions.NotSupported
import de.kaipho.genplus.generator.core.obj.*
import de.kaipho.genplus.generator.core.obj.service.Function
import de.kaipho.genplus.generator.core.obj.service.Parameter
import de.kaipho.genplus.generator.domain.obj.*
import de.kaipho.genplus.generator.generator.core.RuleHelp
import de.kaipho.genplus.generator.generator.core.hasAllowedValueContrained
import de.kaipho.genplus.generator.generator.i18n.JsonStore

/**
 * Created by tom on 29.07.16.
 */
object FormGenerator {
    private val matInput = "matInput"

    val type_date = "type=\"date\""
    val type_number = "type=\"number\""

    fun getRequestFormForServiceParam(param: Parameter, function: Function): String {
        val placeholderInner = JsonStore.add("functions${function.getFilePrefix().replace("/", ".")}.${function.name}.${param.name}")
        val placeholder = "[placeholder]=\"'$placeholderInner' | translate\""

        when (param.type) {
            AssoziationTypeText, AssoziationTypePassword, AssoziationTypeDate, AssoziationTypeDouble, AssoziationTypeInteger, AssoziationTypeString, AssoziationTypeBigNumber -> {
                val builder = StringBuilder(MD_INPUT_CONTAINER_SERVICE)
                builder.addInputLine(param.type, param.name, placeholder, null)
                return builder.append(MD_INPUT_CONTAINER_END).toString()
            }
            AssoziationTypeBoolean -> {
                return "<mat-checkbox class=\"service\" formControlName=\"${param.name}\">{{ '$placeholderInner' | translate }}</mat-checkbox>"
            }
            is AssoziationTypeUser -> {
                val formControl = "formControlName=\"${param.name}\""
                return (param.type as AssoziationTypeUser).acceptClass(object : UserClassVisitor<String> {
                    override fun visitPrimitive(association: AssoziationTypeUser, clazz: PrimitiveClass) : String {
                        val clazz = (param.type as AssoziationTypeUser).clazz as PrimitiveClass
                        val units = clazz.units.map { it -> it.id }.joinToString()
                        return "<gen-primitive-s $formControl [unitClass]=\"${clazz.id}\" [units]=\"[$units]\" $placeholder></gen-primitive-s>"
                    }

                    override fun visitCategory(association: AssoziationTypeUser, clazz: CategoryClass) : String {
                        return "<gen-category $formControl $placeholder clazz=\"${clazz.id}\" ></gen-category>"
                    }

                    override fun visitDomain(association: AssoziationTypeUser, clazz: DomainClass) : String {
                        return "<button mat-button (click)=\"select${param.name.upperFirstLetter()}()\" style=\"width: 100%\">{{'$placeholderInner' | translate}} : {{serviceForm.get('${param.name}').value}}</button>"
                    }

                    override fun visitConstant(association: AssoziationTypeUser, clazz: Constant) : String {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                })
            }
            AssociationTypeBlob -> {
                return "<gen-blob formControlName=\"${param.name}\" $placeholder></gen-blob>"
            }
            else -> {
                val builder = StringBuilder()
                builder.addInputLine(param.type, param.name, placeholder, null)
                return builder.toString()
            }
        }


    }

    fun getResponseFormForServiceParam(function: Function): String {
        val placeholderInner = "functions${function.getFilePrefix().replace("/", ".")}.${function.name}.return"
        val placeholder = "[placeholder]=\"'$placeholderInner' | translate\""

        val builder = StringBuilder()

        function.returnType.accept(object : AssociationVisitor<Unit> {
            override fun visit(associationTypeBlob: AssociationTypeBlob) {
                throw NotSupported()
            }

            override fun visit(association: AssoziationTypeSecure) {
                throw NotSupported()
            }

            override fun visit(association: AssoziationTypeMap) {
                throw NotSupported()
            }

            private fun matInput(type: String): Unit {
                builder.append(MD_INPUT_CONTAINER_SERVICE)
                JsonStore.add(placeholderInner)
                builder.append("<input ${matInput} $type $placeholder formControlName=\"result\" disabled />")
                builder.append(MD_INPUT_CONTAINER_END)
            }

            override fun visit(association: AssoziationTypePassword): Unit {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun visit(association: AssoziationTypeText): Unit {
                builder.append(MD_INPUT_CONTAINER_SERVICE)
                JsonStore.add(placeholderInner)
                builder.append("<textarea ${matInput} matTextareaAutosize $placeholder formControlName=\"result\" disabled></textarea>")
                builder.append(MD_INPUT_CONTAINER_END)
            }

            override fun visit(association: AssoziationTypeOptional): Unit {
                association.inner.accept(this)
            }

            override fun visit(association: AssoziationTypeBigNumber): Unit {
                return matInput(type_number)
            }

            override fun visit(association: AssoziationTypeDouble): Unit {
                return matInput(type_number)
            }

            override fun visit(association: AssoziationTypeString): Unit {
                return matInput("")
            }

            override fun visit(association: AssoziationTypeInteger): Unit {
                return matInput(type_number)
            }

            override fun visit(association: AssoziationTypeDate): Unit {
                return matInput(type_date)
            }

            override fun visit(association: AssoziationTypeBoolean): Unit {
                JsonStore.add(placeholderInner)
                builder.append("<mat-checkbox formControlName=\"result\" disabled>{{ '$placeholderInner' | translate }}</mat-checkbox>")
            }

            override fun visit(association: AssoziationTypeUser): Unit {
                builder.append("<mat-list><mat-list-item>{{_toString(_result)}}</mat-list-item></mat-list>")
            }

            override fun visit(association: AssoziationTypeCollection): Unit {
                builder.append("<mat-list style=\"margin: -20px;margin-top: -45px;margin-bottom: -41px;\"><div *ngFor=\"let item of _result\"><mat-list-item>{{_toString(item)}}</mat-list-item><mat-divider></mat-divider></div></mat-list>")
            }

            override fun visit(assoziation: AssoziationTypeDummy): Unit = throw UnsupportedOperationException("not implemented")
            override fun visit(assoziation: AssoziationTypeUnit): Unit {}
        })

        return builder.toString()
    }

    /**
     * creates an input field, association should be of type string, number, date, ...
     */
    fun createInputFieldDepoendingOnRules(association: Association, self: ExplicitDomainClass): String {
        val formControl = "formControlName=\"${association.name}\""
        val placeholder = "[placeholder]=\"'domain${self.prefix}.${self.name.lowerFirstLetter()}.${association.name}' | translate\""
        val allowedValue = RuleHelp.hasRuleValidation(association)?.hasAllowedValueContrained()
        if (allowedValue != null) {
            return "<gen-category $formControl $placeholder clazz=\"$allowedValue\" ></gen-category>"
        }
        if (association.isDerived()) {
            return "   $MD_INPUT_CONTAINER<input $matInput $formControl $placeholder/>$MD_INPUT_CONTAINER_END"
        }
        return createInputContainer(association.type) {
            this.addInputLine(association.type, association.name, placeholder, association)
        }
    }

    private fun StringBuilder.addInputLine(type: AssoziationType, name: String, placeholder: String, association: Association?): Boolean {
        val formControl = "formControlName=\"$name\""
        val fullWidth = "class=\"full-width\""
        when (type) {
            AssoziationTypeText -> {
                this.append("")
                this.append("<textarea ${de.kaipho.genplus.generator.generator.html.core.FormGenerator.matInput} matTextareaAutosize $formControl $placeholder></textarea>")
            }
            AssoziationTypePassword -> {
                this.append("<input type=\"password\" ${de.kaipho.genplus.generator.generator.html.core.FormGenerator.matInput} $formControl $placeholder/>")
            }
            AssoziationTypeBoolean -> {
                this.append("<mat-checkbox $formControl>{{${placeholder.removePrefix("[placeholder]=\"").removeSuffix("\"")}}}</mat-checkbox>")
                return false
            }
            AssoziationTypeDate -> {
                this.append("<input ${de.kaipho.genplus.generator.generator.html.core.FormGenerator.type_date} ${de.kaipho.genplus.generator.generator.html.core.FormGenerator.matInput} $formControl $placeholder/>")
            }
            AssoziationTypeDouble, AssoziationTypeInteger, is AssoziationTypeUser -> {
                this.append("<input ${de.kaipho.genplus.generator.generator.html.core.FormGenerator.type_number} ${de.kaipho.genplus.generator.generator.html.core.FormGenerator.matInput} $formControl $placeholder/>")
            }
            is AssoziationTypeCollection -> {

                return false
            }
            is AssoziationTypeMap -> {
                this.append("<gen-obj-list name=\"${placeholder.removePrefix("[placeholder]=\"'").removeSuffix("' | translate\"")}\" [obj]=\"crudForm.getRawValue().$name\"></gen-obj-list>")
                return false
            }
            else -> {
                this.append("<input ${de.kaipho.genplus.generator.generator.html.core.FormGenerator.matInput} $formControl $placeholder/>")
            }
        }
        return true
    }

    private val MD_INPUT_CONTAINER = "<mat-form-field>"
    private val MD_INPUT_CONTAINER_TEXTAREA = "<mat-form-field class=\"textarea\">"
    private val MD_INPUT_CONTAINER_SERVICE = "<mat-form-field class=\"service\">"
    private val MD_INPUT_CONTAINER_END = "</mat-form-field>"
    private fun createInputContainer(type: AssoziationType, inline: StringBuilder.() -> Boolean): String {
        val result = StringBuilder("")

        type.accept(object : AssociationStandardVisitor<Unit>() {
            override fun visit(associationTypeBlob: AssociationTypeBlob) {
                throw NotSupported()
            }

            override fun visit(association: AssoziationTypeSecure) {
                throw NotSupported()
            }

            override fun visit(association: AssoziationTypeMap) { }

            override fun standard(association: AssoziationType) {
                result.append(MD_INPUT_CONTAINER)
            }

            override fun visit(association: AssoziationTypeBoolean) {
            }

            override fun visit(association: AssoziationTypeUser) {
                TODO("not implemented: visit(association: AssoziationTypeUser)")
            }

            override fun visit(association: AssoziationTypeCollection) {}

            override fun visit(assoziation: AssoziationTypeDummy) {
                throw NotSupported()
            }

            override fun visit(assoziation: AssoziationTypeUnit) {
                throw NotSupported()
            }

            override fun visit(association: AssoziationTypeText) {
                result.append(MD_INPUT_CONTAINER_TEXTAREA)
            }

            override fun visit(association: AssoziationTypeOptional) {
                result.append(MD_INPUT_CONTAINER)
            }
        })

        if (inline.invoke(result))
            return result.append(MD_INPUT_CONTAINER_END).toString()
        return result.toString()
    }
}
