package de.kaipho.genplus.generator.generator.java.core

import de.kaipho.genplus.generator.common.upperFirstLetter
import de.kaipho.genplus.generator.core.obj.AssoziationType
import de.kaipho.genplus.generator.domain.obj.Association
import java.util.*

/**
 * Helper functions to create Methods
 */


fun createMethod(name: String, parameter:List<Pair<String, String>> = ArrayList(), returntype: String = "void", body: (MutableList<String>.() -> Unit)?):List<String>  {
    val result: MutableList<String> = ArrayList()

    result.add("public $returntype $name(${parameter.map { "${it.first} ${it.second}" }.joinToString()}) {")
    body?.invoke(result)
    result.add("}")

    return result
}
fun createMethodAbstract(name: String, parameter:List<Pair<String, String>>, returntype: String):List<String> {
    val result: MutableList<String> = ArrayList()

    result.add("public $returntype $name(${parameter.map { "${it.first} ${it.second}" }.joinToString()});")

    return result
}
fun createMethodInterface(name: String, parameter:List<Pair<String, String>>, returntype: String):List<String> {
    val result: MutableList<String> = ArrayList()

    result.add("$returntype $name(${parameter.map { "${it.first} ${it.second}" }.joinToString()});")

    return result
}

fun createMethod(name: String, parameter:List<Pair<String, String>> = ArrayList(), returntype: AssoziationType, body: (MutableList<String>.() -> Unit)?):List<String>  {
    return createMethod(name, parameter, returntype.javaAbstractRep(), body)
}
fun createMethod(name1: String, name2: String, parameter:List<Pair<String, String>> = ArrayList(), returntype: AssoziationType, body: (MutableList<String>.() -> Unit)?):List<String>  {
    return createMethod(name1 + name2.upperFirstLetter(), parameter, returntype.javaAbstractRep(), body)
}
fun createMethod(name1: String, name2: String, parameter:List<Pair<String, String>> = ArrayList(), returntype: String = "void", body: (MutableList<String>.() -> Unit)?):List<String>  {
    return createMethod(name1 + name2.upperFirstLetter(), parameter, returntype, body)
}
fun createMethodAsso(name: String, parameter:List<Association> = ArrayList(), returntype: AssoziationType, body: (MutableList<String>.() -> Unit)?):List<String>  {
    return createMethod(name, mapParameter(parameter), returntype, body)
}
fun createMethodAsso(name1: String, name2: String, parameter:List<Association> = ArrayList(), returntype: AssoziationType, body: (MutableList<String>.() -> Unit)?):List<String>  {
    return createMethod(name1 + name2.upperFirstLetter(), mapParameter(parameter), returntype, body)
}
fun createMethodAsso(name1: String, name2: String, parameter:List<Association> = ArrayList(), returntype: String = "void", body: (MutableList<String>.() -> Unit)?):List<String>  {
    return createMethod(name1 + name2.upperFirstLetter(), mapParameter(parameter), returntype, body)
}

private fun mapParameter(parameter:List<Association>): List<Pair<String, String>> {
    return parameter.map {
        it.type.javaAbstractRep() to it.name
    }
}


