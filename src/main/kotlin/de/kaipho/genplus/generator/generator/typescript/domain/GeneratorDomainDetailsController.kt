package de.kaipho.genplus.generator.generator.typescript.domain

import de.kaipho.genplus.generator.common.convertPackageToFileSystem
import de.kaipho.genplus.generator.common.forEachAssociationWithUserType
import de.kaipho.genplus.generator.common.lowerFirstLetter
import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.core.obj.AssoziationTypeUser
import de.kaipho.genplus.generator.core.obj.UserClassVisitor
import de.kaipho.genplus.generator.domain.obj.*
import de.kaipho.genplus.generator.generator.AbstractGenerator
import de.kaipho.genplus.generator.generator.core.File
import de.kaipho.genplus.generator.generator.core.Generator
import de.kaipho.genplus.generator.generator.i18n.JsonStore
import de.kaipho.genplus.generator.generator.recursiveAssociationFetcher
import de.kaipho.genplus.generator.generator.typescript.RelevantPathes
import de.kaipho.genplus.generator.generator.typescript.TsPath
import de.kaipho.genplus.generator.generator.typescript.core.ImportHelp
import de.kaipho.genplus.generator.generator.typescript.core.Imports
import de.kaipho.genplus.generator.template.Templates
import de.kaipho.genplus.generator.template.replacer.*

@Generator(
        lang = Language.TYPESCRIPT,
        template = Templates.TS_DOMAIN_DETAILS_CONTROLLER,
        appliedOn = ExplicitDomainClass::class,
        file = File.TS_DOMAIN_DETAILS_CONTROLLER
)
class GeneratorDomainDetailsController : AbstractGenerator<DomainClass>() {
    override fun visit(replacer: TemplateReplacerImports) {
        val imports = Imports("components/domain/" + meta.data.prefix.convertPackageToFileSystem())

        val ownPath = "components/domain/" + meta.data.prefix.convertPackageToFileSystem()
        replacer.insert(ImportHelp.createImport("LoginService",
                                                ImportHelp.createRelativePath(ownPath, RelevantPathes.LOGIN_SERVICE.path),
                                                RelevantPathes.LOGIN_SERVICE.file))
        imports.addImport(TsPath.TITLE_COMPONENT)
        imports.addImport(TsPath.TITLE_SERVICE)
//        imports.addImport(TsPath.MENU_SERVICE_OPERATIONS)
//        imports.addImport(TsPath.NAVIGATION_SERVICE)
        imports.addImport(TsPath.HTTP_SERVICE)

        imports.addHttpImport(getData())
        imports.addDomainImport(getData())

        replacer.insertAll(imports.getImports())
    }

    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(meta.data.name)
    }

    override fun visit(replacer: TemplateReplacerLowerClassName) {
        replacer.insert(meta.data.name.lowerFirstLetter())
    }

    override fun visit(replacer: TemplateReplacerName) {
        replacer.insert(meta.data.prefix.convertPackageToFileSystem())
    }

    override fun visit(replacer: TemplateReplacerLine) {
        when (replacer.id) {
            0 -> recursiveAssociationFetcher(getData()).forEachAssociationWithUserType { association, clazz ->
                clazz.acceptClass(object : TypeVisitor<Unit> {
                    override fun visitPrimitive(clazz: PrimitiveClass) {
                        // no route
                    }

                    override fun visitCategory(clazz: CategoryClass) {
                        // no route
                    }

                    override fun visitDomain(clazz: DomainClass) {
                        val key = JsonStore.add("title${getData().prefix}.${association.name}")
                        replacer.insert(
                                "{route: '${association.id}', title: '$key', name: '${association.name}', realname: '${association.type.name.lowerFirstLetter()}', area: '${clazz.prefix}'},")
                    }

                    override fun visitConstant(clazz: Constant) {
                        // no route
                    }

                    override fun visitOther(clazz: IClass) {
                        // no route
                    }

                    override fun visitDummy(clazz: IClass) {
                        // no route
                    }

                })
            }
            1 -> recursiveAssociationFetcher(getData()).forEachAssociationWithUserType { association, clazz ->
                val associationType = association.type as AssoziationTypeUser
                associationType.acceptClass(object : UserClassVisitor<Unit> {
                    override fun visitPrimitive(association: AssoziationTypeUser, clazz: PrimitiveClass) {

                    }

                    override fun visitCategory(association: AssoziationTypeUser, clazz: CategoryClass) {

                    }

                    override fun visitDomain(association: AssoziationTypeUser, clazz: DomainClass) {
                        if (!clazz.hasDetailsPage()) {
                            replacer.insert("if (this.tabs[to.index].realname == '${clazz.name.lowerFirstLetter()}') {")
                            replacer.insert("   this.httpService.get('/api/' + this.route.snapshot.url.join('/') + '/' + this.tabs[to.index].route).subscribe(")
                            replacer.insert("       data => this.router.navigateByUrl('web/domainr/${clazz.id}/' + data.body._embedded.id)")
                            replacer.insert("   );")
                            replacer.insert("   return;")
                            replacer.insert("}")
                        }
                    }

                    override fun visitConstant(association: AssoziationTypeUser, clazz: Constant) {

                    }

                })
            }
        }
    }

    override fun visit(replacer: TemplateReplacerI18n) {
        replacer.insert("title${meta.data.prefix}.${meta.data.name.lowerFirstLetter()}")
    }

    override fun visit(replacer: TemplateReplacerSubpackage) {
        replacer.insert(meta.data.prefix)
    }
}
