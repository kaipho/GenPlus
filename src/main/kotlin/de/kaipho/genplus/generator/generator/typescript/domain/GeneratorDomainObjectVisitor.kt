package de.kaipho.genplus.generator.generator.typescript.domain

import de.kaipho.genplus.generator.common.convertPackageToFileSystem
import de.kaipho.genplus.generator.common.lowerFirstLetter
import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.domain.obj.Domain
import de.kaipho.genplus.generator.generator.AbstractGenerator
import de.kaipho.genplus.generator.generator.core.File
import de.kaipho.genplus.generator.generator.core.Generator
import de.kaipho.genplus.generator.generator.typescript.core.ImportHelp
import de.kaipho.genplus.generator.template.Templates
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerImports
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLine

@Generator(
        lang = Language.TYPESCRIPT,
        template = Templates.TS_DOMAIN_OBJECT_VISITOR,
        appliedOn = Domain::class,
        file = File.TS_DOMAIN_OBJECT_VISITOR
)
class GeneratorDomainObjectVisitor : AbstractGenerator<Domain>() {
    override fun visit(replacer: TemplateReplacerImports) {
        meta.data.domainClasses.forEach { clazz ->
            replacer.insert(ImportHelp.createImport(clazz.name,
                                                    ImportHelp.createRelativePath("", clazz.prefix.convertPackageToFileSystem()),
                                                    clazz.name.lowerFirstLetter()))
        }
    }

    override fun visit(replacer: TemplateReplacerLine) {
        meta.data.domainClasses.forEach { clazz ->
            replacer.insert("visit${clazz.name}(obj: ${clazz.name}):D")
        }
    }
}