package de.kaipho.genplus.generator.generator.core

import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.generator.core.GeneratorType
import de.kaipho.genplus.generator.generator.core.Ref
import de.kaipho.genplus.generator.core.obj.AstElement
import de.kaipho.genplus.generator.template.Templates
import de.kaipho.genplus.generator.generator.core.File
import kotlin.reflect.KClass

@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE)
@Retention(AnnotationRetention.RUNTIME)
annotation class Generator(
        val lang: Language,
        val type: GeneratorType = GeneratorType.PROCESS,
        val template: Templates,
        val file: File,
        val dependsOn: Ref = Ref.NOTHING,
        val dependsOnNot: Ref = Ref.NOTHING,
        val appliedOn: KClass<out AstElement>
)