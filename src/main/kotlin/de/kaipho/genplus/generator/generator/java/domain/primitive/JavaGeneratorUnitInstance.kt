package de.kaipho.genplus.generator.generator.java.domain.primitive

import de.kaipho.genplus.generator.domain.obj.PrimitiveClass
import de.kaipho.genplus.generator.domain.obj.PrimitiveType
import de.kaipho.genplus.generator.generator.java.core.ExternalJavaGenerator
import de.kaipho.genplus.generator.template.replacer.*

class JavaGeneratorUnitInstance(val clazz: PrimitiveClass, val type: PrimitiveType) : ExternalJavaGenerator() {
    override val templateString = "domain/primitive/unit/instances/xxx/xxx.java"
    override val replacer = arrayListOf(clazz.name.toLowerCase(), type.name)

    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(type.name)
    }

    override fun visit(replacer: TemplateReplacerName) {
        replacer.insert(clazz.name)
    }

    override fun visit(replacer: TemplateReplacerLowerClassName) {
        replacer.insert(clazz.name.toLowerCase())
    }

    override fun visit(replacer: TemplateReplacerLine) {
        replacer.insert("defaultInstance = ${clazz.default!!.name}.getUnit();")
        clazz.units.forEach {
            replacer.insert("instances.add(${it.name}.getUnit());")
        }
    }

    override fun visit(replacer: TemplateReplacerId) = replacer.insert(type.id.toString())
}