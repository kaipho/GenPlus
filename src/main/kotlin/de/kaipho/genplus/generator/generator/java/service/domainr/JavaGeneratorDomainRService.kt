package de.kaipho.genplus.generator.generator.java.service.domainr

import de.kaipho.genplus.generator.common.convertPackageToFileSystem
import de.kaipho.genplus.generator.common.lowerFirstLetter
import de.kaipho.genplus.generator.core.obj.getAllNonAbstractChildClasses
import de.kaipho.genplus.generator.domain.obj.AbstractDomainClass
import de.kaipho.genplus.generator.domain.obj.DomainClass
import de.kaipho.genplus.generator.generator.java.core.ExternalJavaGenerator
import de.kaipho.genplus.generator.generator.java.service.Imports
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerClassName
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerImports
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLine
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerPath

class JavaGeneratorDomainRService(val clazz: DomainClass) : ExternalJavaGenerator() {
    override val templateString = "service/domainr/xxx/xxxService.java"
    override val replacer = arrayListOf(clazz.prefix.convertPackageToFileSystem().removeSuffix("/"),
                                        clazz.name)

    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(clazz.name)
    }

    override fun visit(replacer: TemplateReplacerPath) {
        replacer.insert(clazz.prefix)
    }

    override fun visit(replacer: TemplateReplacerLine) {
        if (clazz is AbstractDomainClass) {
            replacer.insert("it.accept(new ${clazz.name}CompleteVisitor<${clazz.name}>() {")
            clazz.getAllNonAbstractChildClasses().forEach {
                """
                |@Override
                |public ${clazz.name} visit(${it.name} it) {
                |    result.addSingleToData("${it.name.lowerFirstLetter()}", new ObjVM<>(it));
                |    return it;
                |}
                """.insertInto(replacer)
            }
            replacer.insert("}")
        } else {
            replacer.insert("result.addSingleToData(\"${clazz.name.lowerFirstLetter()}\", new ObjVM<>(it)")
        }
    }

    override fun visit(replacer: TemplateReplacerImports) {
        val import = Imports()
        import.addDomainImport(clazz)
        if (clazz is AbstractDomainClass) {
            import.addDomainCompleteVisitorImport(clazz)
        }
        clazz.getAllNonAbstractChildClasses().forEach {
            import.addDomainImport(it)
        }
        replacer.insertAll(import.getimports())
    }
}
