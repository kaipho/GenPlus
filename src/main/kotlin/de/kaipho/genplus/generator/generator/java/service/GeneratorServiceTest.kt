package de.kaipho.genplus.generator.generator.java.service

import de.kaipho.genplus.generator.common.lowerFirstLetter
import de.kaipho.genplus.generator.core.obj.service.ServiceClass
import de.kaipho.genplus.generator.generator.AbstractGenerator
import de.kaipho.genplus.generator.store.SettingsStore
import de.kaipho.genplus.generator.template.replacer.*

//@Generator(
//        lang = Language.JAVA,
//        type = GeneratorType.MERGE,
//        file = File.JAVA_SERVICE_TEST,
//        appliedOn = ServiceClass::class,
//        template = Templates.JAVA_SERVICE_TEST
//)
class GeneratorServiceTest : AbstractGenerator<ServiceClass>() {
    override fun visit(replacer: TemplateReplacerPackage) {
        lines.add(replacer.replaceOneLine(actual, "${SettingsStore.getPrefixPackage()}"))
    }

    override fun visit(replacer: TemplateReplacerSubpackage) {
        replacer.insert(getData().prefix)
    }

    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(getData().name)
    }
    override fun visit(replacer: TemplateReplacerLowerClassName) {
        replacer.insert(getData().name.lowerFirstLetter())
    }

    override fun visit(replacer: TemplateReplacerLine) {
        getData().functions.forEach { function ->
            replacer.insert("@Test")
            replacer.insert("public void ${function.name}() throws Exception {")
            replacer.insert("   // TODO implement testcase for ${function.name}(...)")
            replacer.insert("   throw new RuntimeException(\"Needs test\");")
            replacer.insert("}")
        }
    }
}
