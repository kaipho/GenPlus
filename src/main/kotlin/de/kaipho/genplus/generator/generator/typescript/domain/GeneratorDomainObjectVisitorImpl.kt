package de.kaipho.genplus.generator.generator.typescript.domain

import de.kaipho.genplus.generator.common.convertPackageToFileSystem
import de.kaipho.genplus.generator.common.lowerFirstLetter
import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.domain.obj.Domain
import de.kaipho.genplus.generator.generator.AbstractGenerator
import de.kaipho.genplus.generator.generator.core.File
import de.kaipho.genplus.generator.generator.core.Generator
import de.kaipho.genplus.generator.generator.core.GeneratorType
import de.kaipho.genplus.generator.generator.typescript.core.ImportHelp
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.template.Templates
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerGeneric
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerImports
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLine

/**
 * Created by tom on 20.07.16.
 */
abstract class GeneratorDomainObjectVisitorImpl(val generic: String) : AbstractGenerator<Domain>() {
    override fun visit(replacer: TemplateReplacerImports) {
        ClassStore.domain.domainClasses.forEach { clazz ->
            replacer.insert(ImportHelp.createImport(clazz.name,
                    ImportHelp.createRelativePath("", clazz.prefix.convertPackageToFileSystem()),
                    clazz.name.lowerFirstLetter()))
        }
    }

    override fun visit(replacer: TemplateReplacerLine) {
        ClassStore.domain.domainClasses.forEach { clazz ->
            replacer.insert("public visit${clazz.name}(obj: ${clazz.name}):$generic {")
            replacer.insert("   // TODO implement visit${clazz.name}(obj: ${clazz.name}):$generic")
            if (generic == "string")
                replacer.insert("   return 'todo';")
            else
                replacer.insert("   return null;")
            replacer.insert("}")
        }
    }

    override fun visit(replacer: TemplateReplacerGeneric) {
        replacer.insert(generic)
    }
}

@Generator(
        lang = Language.TYPESCRIPT,
        type = GeneratorType.MERGE,
        template = Templates.TS_DOMAIN_REP_VISITOR,
        appliedOn = Domain::class,
        file = File.TS_REP_VISITOR
)
class GeneratorDomainObjectVisitorImplString : GeneratorDomainObjectVisitorImpl("string") {

}

@Generator(
        lang = Language.TYPESCRIPT,
        template = Templates.TS_DOMAIN_TYPE_VISITOR,
        appliedOn = Domain::class,
        file = File.TS_TYPE_VISITOR
)
class GeneratorDomainObjectVisitorImplType : GeneratorDomainObjectVisitorImpl("string") {
    override fun visit(replacer: TemplateReplacerLine) {
        ClassStore.domain.domainClasses.forEach { clazz ->
            replacer.insert("public visit${clazz.name}(obj: ${clazz.name}):$generic {")
            replacer.insert("   return '${clazz.name.lowerFirstLetter()}';")
            replacer.insert("}")
        }
    }
}

