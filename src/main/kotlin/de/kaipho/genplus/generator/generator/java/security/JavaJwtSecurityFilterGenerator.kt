package de.kaipho.genplus.generator.generator.java.security

import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.generator.core.File
import de.kaipho.genplus.generator.generator.core.Generator
import de.kaipho.genplus.generator.generator.core.Ref
import de.kaipho.genplus.generator.generator.java.JavaGenerator
import de.kaipho.genplus.generator.store.SettingsStore
import de.kaipho.genplus.generator.template.Templates

@Generator(
        lang = Language.JAVA,
        file = File.JAVA_JWT_SECURITY_FILTER,
        appliedOn = SettingsStore::class,
        template = Templates.JAVA_JWT_SECURITY_FILTER,
        dependsOn = Ref.SECURITY_ENABLED
)
class JavaJwtSecurityFilterGenerator : JavaGenerator<SettingsStore>() {}