package de.kaipho.genplus.generator.generator.typescript.core

import de.kaipho.genplus.generator.core.obj.options.Option
import de.kaipho.genplus.generator.core.scanner.KeywordHolder
import de.kaipho.genplus.generator.domain.obj.DomainClass
import de.kaipho.genplus.generator.domain.obj.ExplicitDomainClass
import de.kaipho.genplus.generator.generator.core.RuleHelp
import de.kaipho.genplus.generator.generator.i18n.JsonStore
import de.kaipho.genplus.generator.generator.typescript.ExternalTsGenerator
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.store.SettingsStore
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerId
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerImports
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLine
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerName

class GeneratorTsAppComponentHtml : ExternalTsGenerator() {
    override val templateString = "src/app/app.component.html"

    init {
        JsonStore.add("button.logout", "Logout")
    }

    override fun visit(replacer: TemplateReplacerName) {
        replacer.insert(SettingsStore.options.getFirst(Option.NAME))
    }
}

class GeneratorTsAppComponentTs : ExternalTsGenerator() {
    override val templateString = "src/app/app.component.ts"

    override fun visit(replacer: TemplateReplacerLine) {
        ClassStore.domain.domainClasses.filter { !RuleHelp.ruleExist(it, KeywordHolder.RULE_VIEWMODEL) }.forEach {
            """
            |{
            |    name: '${it.name}',
            |    link: ['/web/domainr/${it.id}'],
            |    icon: 'build'
            |},
            """.insertInto(replacer)
        }
    }

    override fun visit(replacer: TemplateReplacerId) {
        replacer.insert(ClassStore.domain.domainClasses.filter { it.name == "Notification" }[0].id.toString())
    }
}

class GeneratorTsAppRoutes : ExternalTsGenerator() {
    override val templateString = "src/app/app.routes.ts"

    val exportClasses = ClassStore.domain.domainClasses.filter { it.export }

    override fun visit(replacer: TemplateReplacerLine) {
        exportClasses.forEach {
            replacer.insert("...${it.name}DomainRoutes,")
        }
    }

    override fun visit(replacer: TemplateReplacerImports) {
        val imports = Imports("/")

        exportClasses.forEach {
            imports.addDomainRoutesImport(it)
        }

        replacer.insertAll(imports.getImports())
    }
}

class GeneratorTsAppModule : ExternalTsGenerator() {
    override val templateString = "src/app/app.module.ts"

    companion object {
        val listComponents = ArrayList<DomainClass>()
    }

    override fun visit(replacer: TemplateReplacerImports) {
        val imports = Imports("")
        ClassStore.getAllNotAbstractDomainClasses().forEach {
            imports.addDomainComponentImport(it)
            imports.addDetailComponentImport(it)
        }

        ClassStore.domain.domainClasses.forEach(imports::addListComponentImport)

        replacer.insertAll(imports.getImports())
    }

    override fun visit(replacer: TemplateReplacerLine) {
        when (replacer.id) {
            0 -> insertComponents(replacer)
        }
    }

    private fun insertComponents(replacer: TemplateReplacerLine) {
        ClassStore.getAllNotAbstractDomainClasses().forEach {
            replacer.insert(it.name + "Component,")
            replacer.insert(it.name + "DetailComponent,")
        }
        ClassStore.domain.domainClasses.forEach {
            replacer.insert(it.name + "ListComponent,")
        }
    }
}

class GeneratorTsAbstractHttpService : ExternalTsGenerator() {
    override val templateString = "src/app/service/domain/http/abstract.http.service.ts"

    val domainClasses = ClassStore.domain.domainClasses.filter { it is ExplicitDomainClass }

    override fun visit(replacer: TemplateReplacerLine) {
        domainClasses.forEach {
            replacer.insert("case ${it.id}:")
            replacer.insert("   return AbstractHttpService.extractOne(item, new ${it.name}());")
        }
    }

    override fun visit(replacer: TemplateReplacerImports) {
        val imports = Imports("service/domain/http")

        domainClasses.forEach {
            imports.addDomainImport(it)
        }

        replacer.insertAll(imports.getImports())
    }
}
