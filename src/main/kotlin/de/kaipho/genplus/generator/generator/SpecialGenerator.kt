package de.kaipho.genplus.generator.generator

import de.kaipho.genplus.generator.template.TemplateReplacer

/**
 * Created by tom on 14.07.16.
 */
interface SpecialGenerator {

    fun generate(template: MutableList<Pair<String, MutableList<TemplateReplacer<*>>>>): MutableList<String>

}