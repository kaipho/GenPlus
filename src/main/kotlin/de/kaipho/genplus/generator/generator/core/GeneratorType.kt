package de.kaipho.genplus.generator.generator.core

/**
 * Created by tom on 21.07.16.
 */
enum class GeneratorType {
    COPY, PROCESS, MERGE, ONCE
}