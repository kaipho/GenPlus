package de.kaipho.genplus.generator.generator.java.service.normal

import de.kaipho.genplus.generator.common.convertPackageToFileSystem
import de.kaipho.genplus.generator.common.upperFirstLetter
import de.kaipho.genplus.generator.core.obj.AssoziationTypeUnit
import de.kaipho.genplus.generator.core.obj.service.Function
import de.kaipho.genplus.generator.generator.java.core.ExternalJavaGenerator
import de.kaipho.genplus.generator.generator.java.service.Imports
import de.kaipho.genplus.generator.generator.java.service.getFunctionShortcut
import de.kaipho.genplus.generator.template.replacer.*

class CommandImplGenerator(val function: Function) : ExternalJavaGenerator() {
    override val templateString = "service/xxx/command/xxxCommand.java"

    override val replacer = arrayListOf(
            function.owner.prefix.convertPackageToFileSystem().removeSuffix("/"),
            function.getQualifiedClassName()
    )

    val allParams = function.getAllParams()

    override fun visit(replacer: TemplateReplacerLine) {
        when (replacer.id) {
            1 -> {
                allParams.forEach {
                    replacer.insert("private final ${it.type.javaRep()} ${it.name};")
                }
            }
            2 -> {
                replacer.insert(allParams.map {
                    "${it.type.javaRep()} ${it.name}"
                }.joinToString(separator = ",\n            "))
            }
            3 -> {
                allParams.forEach {
                    replacer.insert("this.${it.name} = ${it.name};")
                }
            }
            4 -> {
                allParams.forEach {
                    replacer.insert("public ${it.type.javaAbstractRep()} get${it.name.upperFirstLetter()}() {")
                    replacer.insert("    return this.${it.name};")
                    replacer.insert("}")
                    replacer.insert("")
                }
            }
        }
    }

    override fun visit(replacer: TemplateReplacerSubpackage) {
        replacer.insert(function.owner.prefix)
    }

    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(function.getQualifiedClassName())
    }

    override fun visit(replacer: TemplateReplacerName) {
        replacer.insert(getFunctionShortcut(function))
    }

    override fun visit(replacer: TemplateReplacerGeneric) {
        if (function.returnType === AssoziationTypeUnit) {
            replacer.insert("Void")
        } else {
            replacer.insert(function.returnType.javaAbstractRep())
        }
    }

    override fun visit(replacer: TemplateReplacerImports) {
        val import = Imports()

        allParams.forEach {
            import.addAssociationImport(it.type)
        }
        import.addAssociationImport(function.returnType)

        replacer.insertAll(import.getimports())
    }
}
