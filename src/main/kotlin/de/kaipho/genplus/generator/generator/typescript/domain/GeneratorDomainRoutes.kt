package de.kaipho.genplus.generator.generator.typescript.domain

import de.kaipho.genplus.generator.common.convertPackageToFileSystem
import de.kaipho.genplus.generator.common.lowerFirstLetter
import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.core.obj.AssoziationTypeCollection
import de.kaipho.genplus.generator.core.obj.AssoziationTypeUser
import de.kaipho.genplus.generator.core.obj.UserClassVisitor
import de.kaipho.genplus.generator.core.obj.getConcreteClasses
import de.kaipho.genplus.generator.domain.obj.*
import de.kaipho.genplus.generator.generator.AbstractGenerator
import de.kaipho.genplus.generator.generator.core.File
import de.kaipho.genplus.generator.generator.core.Generator
import de.kaipho.genplus.generator.generator.core.Ref
import de.kaipho.genplus.generator.generator.recursiveAssociationFetcher
import de.kaipho.genplus.generator.generator.typescript.core.Imports
import de.kaipho.genplus.generator.generator.typescript.core.RoutingHelp
import de.kaipho.genplus.generator.template.TemplateParser
import de.kaipho.genplus.generator.template.Templates
import de.kaipho.genplus.generator.template.replacer.*

@Generator(
        lang = Language.TYPESCRIPT,
        template = Templates.TS_DOMAIN_ROUTES,
        appliedOn = DomainClass::class,
        file = File.TS_DOMAIN_ROUTES,
        dependsOn = Ref.IS_EXPORT
)
class GeneratorDomainRoutes : AbstractGenerator<DomainClass>() {
    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(meta.data.name)
    }

    override fun visit(replacer: TemplateReplacerImports) {
        val imports = Imports("components/domain/" + getData().prefix.convertPackageToFileSystem())
        imports.addListComponentImport(getData())

        getData().getConcreteClasses().forEach {
            imports.addDetailComponentImport(it)
            imports.addDomainComponentImport(it)
        }
        // TODO path von association!
        recursiveAssociationFetcher(self()).forEach {
            createAssociationImports(it, imports)
        }

        if (getData() is AbstractDomainClass) {
            (getData() as AbstractDomainClass).getConcreteClasses().map { (it as DomainClass).associations }.flatten().forEach {
                createAssociationImports(it, imports)
            }
        }
        replacer.insertAll(imports.getImports())
    }

    override fun visit(replacer: TemplateReplacerLine) {
        getData().getConcreteClasses().forEach {
            replacer.insertAll(
                    TemplateParser.parseTemplate(Templates.TS_DOMAIN_ROUTE, GeneratorDomainRoute(it as DomainClass, it.name.lowerFirstLetter())))
        }
    }

    override fun visit(replacer: TemplateReplacerPath) {
        replacer.insert(RoutingHelp.getRouteToList(self()))
    }

    override fun visit(replacer: TemplateReplacerLowerClassName) {
        replacer.insert(meta.data.name.lowerFirstLetter())
    }

    private fun createAssociationImports(it: Association, imports: Imports) {
        if (it.type is AssoziationTypeUser) {
            it.type.acceptClass(object : UserClassVisitor<Unit> {
                override fun visitPrimitive(association: AssoziationTypeUser, clazz: PrimitiveClass) {
                    // nothing to import
                }

                override fun visitCategory(association: AssoziationTypeUser, clazz: CategoryClass) {
                    // nothing to import
                }

                override fun visitDomain(association: AssoziationTypeUser, clazz: DomainClass) {
                    if (clazz !is AbstractDomainClass)
                        imports.addDomainComponentImport(clazz)
                }

                override fun visitConstant(association: AssoziationTypeUser, clazz: Constant) {
                    // nothing to import
                }

            })
        }
        if (it.type is AssoziationTypeCollection && it.type.inner is AssoziationTypeUser && it.type.inner.clazz !is AbstractClass)
            imports.addDomainComponentImport(it.type.inner.clazz)
    }
}

