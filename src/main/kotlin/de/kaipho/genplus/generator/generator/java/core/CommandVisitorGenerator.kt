package de.kaipho.genplus.generator.generator.java.core

import de.kaipho.genplus.generator.generator.java.service.Imports
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerImports
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLine

class CommandVisitorGenerator : ExternalJavaGenerator() {
    override val templateString = "core/CommandVisitor.java"

    val functions = ClassStore.services.flatMap { it.functions }

    override fun visit(replacer: TemplateReplacerLine) {
        functions.forEach {
            replacer.insert("R visit(${it.getQualifiedClassName()}Command command);")
        }
    }

    override fun visit(replacer: TemplateReplacerImports) {
        val import = Imports()

        functions.forEach {
            import.addFunctionCommandImport(it)
        }

        replacer.insertAll(import.getimports())
    }
}
