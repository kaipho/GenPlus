package de.kaipho.genplus.generator.generator.typescript.core

import de.kaipho.genplus.generator.common.convertPackageToFileSystem
import de.kaipho.genplus.generator.common.lowerFirstLetter
import de.kaipho.genplus.generator.common.upperFirstLetter
import de.kaipho.genplus.generator.core.obj.service.Function
import de.kaipho.genplus.generator.domain.obj.*
import de.kaipho.genplus.generator.generator.typescript.TsPath
import de.kaipho.genplus.generator.generator.typescript.core.ImportHelp
import java.util.*

/**
 * Created by tom on 26.07.16.
 */
class Imports(val self: String) {
    val imports: MutableSet<String> = HashSet()

    fun addImport(to: TsPath) {
        this.imports.add(ImportHelp.createImport(to.clazz, ImportHelp.createRelativePath(this.self, to.path), to.file))
    }

    fun addImport(clazz: String, path: String, file: String) {
        this.imports.add(ImportHelp.createImport(clazz, ImportHelp.createRelativePath(this.self, path), file))
    }

    fun addImport(file: String) {
        this.imports.add(file)
    }

    fun addDomainImport(clazz: IClass) {
        clazz.acceptClass(object : TypeVisitor<Unit> {
            override fun visitCategory(clazz: CategoryClass) {
                // No import
            }

            override fun visitDomain(clazz: DomainClass) {
                imports.add(ImportHelp.createImport(clazz.name,
                        ImportHelp.createRelativePath(self, "domain${clazz.prefix.convertPackageToFileSystem()}"),
                        clazz.name.lowerFirstLetter()))
            }

            override fun visitConstant(clazz: Constant) {
                // No import
            }

            override fun visitOther(clazz: IClass) {
                // No import
            }

            override fun visitDummy(clazz: IClass) {
                // No import
            }

            override fun visitPrimitive(clazz: PrimitiveClass) {
                imports.add(ImportHelp.createImport("Primitive",
                        ImportHelp.createRelativePath(self, "domain"),
                        "primitive"))
            }
        })
    }

    fun addHttpImport(clazz: IClass) {
        this.imports.add(ImportHelp.createImport(clazz.name + "HttpService",
                ImportHelp.createRelativePath(this.self, "service/domain/http"),
                clazz.name.lowerFirstLetter() + ".http.service"))
    }

    fun addFunctionsHttpImport(clazz: IClass) {
        this.imports.add(ImportHelp.createImport(clazz.name + "HttpService",
                ImportHelp.createRelativePath(this.self, "service/functions/http"),
                clazz.name.lowerFirstLetter() + ".http.service"))
    }

    fun addListComponentImport(clazz: IClass) {
        clazz.acceptClass(object : TypeVisitor<Unit> {
            override fun visitCategory(clazz: CategoryClass) {
                // No import
            }

            override fun visitDomain(clazz: DomainClass) {
                imports.add(ImportHelp.createImport(clazz.name + "ListComponent",
                        ImportHelp.createRelativePath(self, "components/domain${clazz.prefix.convertPackageToFileSystem()}"),
                        clazz.name.lowerFirstLetter() + ".list.component"))
            }

            override fun visitConstant(clazz: Constant) {
                // No import
            }

            override fun visitOther(clazz: IClass) {
                // No import
            }

            override fun visitDummy(clazz: IClass) {
                // No import
            }

            override fun visitPrimitive(clazz: PrimitiveClass) {
                // No import
            }
        })
    }

    fun addDetailComponentImport(clazz: IClass) {
        this.imports.add(ImportHelp.createImport(clazz.name + "DetailComponent",
                ImportHelp.createRelativePath(this.self, "components/domain${clazz.prefix.convertPackageToFileSystem()}"),
                clazz.name.lowerFirstLetter() + ".detail.component"))
    }

    fun addDomainComponentImport(clazz: IClass) {
        this.imports.add(ImportHelp.createImport(clazz.name + "Component",
                ImportHelp.createRelativePath(this.self, "components/domain${clazz.prefix.convertPackageToFileSystem()}"),
                clazz.name.lowerFirstLetter() + ".component"))
    }

    fun addDomainRoutesImport(clazz: IClass) {
        this.imports.add(ImportHelp.createImport(clazz.name + "DomainRoutes",
                ImportHelp.createRelativePath(this.self, "components/domain${clazz.prefix.convertPackageToFileSystem()}"),
                clazz.name.lowerFirstLetter() + ".routes"))
    }

    fun addFunctionImport(function: Function) {
        this.imports.add(ImportHelp.createImport(function.getQualifiedClassName() + "Component",
                ImportHelp.createRelativePath(this.self, "components/service${function.getFilePrefix()}"),
                function.getQualifiedName()+ ".component"))
    }

    fun addFunctionImportFromClass(clazz: IClass, name: String) {
        this.imports.add(ImportHelp.createImport(clazz.name + name + "Component",
                ImportHelp.createRelativePath(this.self, "components/service${clazz.prefix.convertPackageToFileSystem()}"),
                clazz.name.lowerFirstLetter() + "." + name.lowerFirstLetter() + ".component"))
    }

    fun getImports(): List<String> = imports.toList()

    fun getExports(): List<String> {
        return this.imports.map {
            it.replaceFirst("import", "export")
        }
    }

    fun addServiceComponentImport(function: Function) {
        this.imports.add(ImportHelp.createImport(function.getQualifiedClassName() + "Component",
                ImportHelp.createRelativePath(this.self,
                        "components/service${function.owner.prefix.convertPackageToFileSystem()}"),
                function.getQualifiedName() + ".component"))
    }

    fun addDeleteServiceComponentImport(domain: DomainClass) {
        this.imports.add(ImportHelp.createImport(domain.name + "DeleteComponent",
                ImportHelp.createRelativePath(this.self, "components/service${domain.prefix.convertPackageToFileSystem()}"),
                domain.name.lowerFirstLetter() + ".delete.component"))
    }
}