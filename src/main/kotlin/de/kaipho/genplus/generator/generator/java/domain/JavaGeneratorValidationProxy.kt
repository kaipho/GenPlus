package de.kaipho.genplus.generator.generator.java.domain

import de.kaipho.genplus.generator.common.convertPackageToFileSystem
import de.kaipho.genplus.generator.common.upperFirstLetter
import de.kaipho.genplus.generator.core.obj.AssoziationTypeCollection
import de.kaipho.genplus.generator.core.obj.AssoziationTypeInteger
import de.kaipho.genplus.generator.core.obj.AssoziationTypeOptional
import de.kaipho.genplus.generator.domain.obj.AbstractDomainClass
import de.kaipho.genplus.generator.domain.obj.DomainClass
import de.kaipho.genplus.generator.domain.obj.IClass
import de.kaipho.genplus.generator.generator.core.RuleHelp
import de.kaipho.genplus.generator.generator.getInlinedAssociations
import de.kaipho.genplus.generator.generator.java.core.*
import de.kaipho.genplus.generator.generator.java.domain.methods.MethodAddSingleToGenerator
import de.kaipho.genplus.generator.generator.java.domain.methods.MethodForO
import de.kaipho.genplus.generator.generator.java.service.Imports
import de.kaipho.genplus.generator.generator.java.special.ValidationGenerator
import de.kaipho.genplus.generator.generator.recursiveAssociationFetcher
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.template.TemplateParser
import de.kaipho.genplus.generator.template.replacer.*
import java.util.ArrayList

class JavaGeneratorValidationProxy(val clazz: DomainClass) : ExternalJavaGenerator() {
    override val templateString = "domain/xxx/impl/xxxValidationProxy.java"
    override val replacer = arrayListOf(clazz.prefix.convertPackageToFileSystem().removeSuffix("/"), clazz.name)

    private val associationsWithDerived = recursiveAssociationFetcher(clazz, true)
    private val associationsWithoutDerived = recursiveAssociationFetcher(clazz, false)


    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(clazz.name)
    }

    override fun visit(replacer: TemplateReplacerSubpackage) {
        replacer.insert(clazz.prefix)
    }

    override fun visit(replacer: TemplateReplacerComment) {
        replacer.insert(clazz.comment)
    }

    override fun visit(replacer: TemplateReplacerImports) {
        val imports = Imports()
        associationsWithDerived.union(getInlinedAssociations(clazz)).forEach(imports::addAssociationImport)
        ClassStore.domain.domainClasses.filter { it is AbstractDomainClass }.forEach { clazz ->
            fetchAllAssociationsAndChildClasses(clazz).filter { it.name == clazz.name }.forEach {
                imports.addDomainVisitorImport(clazz)
            }
        }

        val visitable = ArrayList<IClass>()
        ClassStore.domain.domainClasses.filter { it is AbstractDomainClass }.forEach { clazz ->
            fetchAllAssociationsAndChildClasses(clazz).filter { it.name == clazz.name }.forEach {
                addClassToList(visitable, clazz)
            }
        }
        getAllParents(clazz.extends).forEach {
            addClassToList(visitable, it)
        }

        visitable.forEach {
            imports.addDomainVisitorImport(it)
        }

        fetchAllParents(clazz).forEach {
            imports.addDomainCompleteVisitorImport(it)
        }

        replacer.insertAll(imports.getimports())
    }

    override fun visit(replacer: TemplateReplacerGetter) {
        associationsWithDerived.forEach {
            replacer.insert("@Override public ${it.type.javaAbstractRep()} get${it.name.upperFirstLetter()}() {")
            replacer.insert("   return real.get${it.name.upperFirstLetter()}();")
            replacer.insert("}")
        }
    }

    override fun visit(replacer: TemplateReplacerSetter) {
        associationsWithoutDerived.forEach {
            val validationRule = RuleHelp.hasRuleValidation(it)
            if (validationRule != null)
                replacer.insertAll(ValidationGenerator.getJavaValidationPreLogic(validationRule, clazz, it))
            replacer.insert("@Override public void set${it.name.upperFirstLetter()}(${it.type.javaAbstractRep()} ${it.name}) {")
            if (validationRule != null)
                replacer.insertAll(ValidationGenerator.getJavaValidationLogic(validationRule, clazz, it))
            replacer.insert("   real.set${it.name.upperFirstLetter()}(${it.name});")
            replacer.insert("}")

            // sonderformen der setter
            if (it.type is AssoziationTypeCollection) {
                val addSingleToGenerator = MethodForO(it.name, params = listOf(it.type.inner.javaAbstractRep() to it.name),
                        body = listOf("real.addSingleTo${it.name.upperFirstLetter()}(${it.name});"))
                replacer.insertAll(TemplateParser.parseTemplate(MethodAddSingleToGenerator(addSingleToGenerator)))
            }
            if (it.type is AssoziationTypeInteger) {
                replacer.insert("@Override public void add${it.name.upperFirstLetter()}(${it.type.javaAbstractRep()} ${it.name}) {")
                replacer.insert("   real.add${it.name.upperFirstLetter()}(${it.name});")
                replacer.insert("}")
            }
            if (it.type is AssoziationTypeOptional) {
                replacer.insert("@Override public void set${it.name.upperFirstLetter()}(${it.type.inner.javaAbstractRep()} ${it.name}) {")
                replacer.insert("   this.set${it.name.upperFirstLetter()}(Optional.of(${it.name}));")
                replacer.insert("}")
            }
        }
    }

    override fun visit(replacer: TemplateReplacerAccept) {
        val visitable = java.util.ArrayList<IClass>()
        ClassStore.domain.domainClasses.filter { it is AbstractDomainClass }.forEach { clazz ->
            fetchAllAssociationsAndChildClasses(clazz).filter { it.name == this@JavaGeneratorValidationProxy.clazz.name }.forEach {
                addClassToList(visitable, clazz)
            }
        }
        getAllParents(clazz.extends).forEach {
            addClassToList(visitable, it)
        }
        visitable.forEach {
            replacer.insert("public <D> D accept(${it.name}Visitor<D> visitor) {")
            replacer.insert("   return visitor.visit(this);")
            replacer.insert("}")
            replacer.insert("public void accept(${it.name}SimpleVisitor visitor) {")
            replacer.insert("   visitor.visit(this);")
            replacer.insert("}")
        }
        getAllParents(clazz.extends).forEach {
            replacer.insert("public <D> D accept(${it.name}CompleteVisitor<D> visitor) {")
            replacer.insert("   return visitor.visit(this);")
            replacer.insert("}")
        }
    }
}