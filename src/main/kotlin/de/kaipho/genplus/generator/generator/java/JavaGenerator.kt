package de.kaipho.genplus.generator.generator.java

import de.kaipho.genplus.generator.generator.AbstractGenerator
import de.kaipho.genplus.generator.store.SettingsStore
import de.kaipho.genplus.generator.template.replacer.*

/**
 * Created by tom on 14.07.16.
 */
abstract class JavaGenerator<E> : AbstractGenerator<E>() {
   override fun visit(replacer: TemplateReplacerPackage) {
        lines.add(replacer.replaceOneLine(actual, SettingsStore.getPrefixPackage()))
    }

    override fun visit(replacer: TemplateReplacerGenerated) {
        replacer.insert("import javax.annotation.Generated;")
        replacer.insert("")
        replacer.insert("@Generated(\"${javaClass.name}\")")
    }

    override fun visit(replacer: TemplateReplacerImplements) {
        super.visit(replacer)
    }

    override fun visit(replacer: TemplateReplacerPath) {
        super.visit(replacer)
    }

    override fun visit(replacer: TemplateReplacerGetter) {
        super.visit(replacer)
    }

    override fun visit(replacer: TemplateReplacerSetter) {
        super.visit(replacer)
    }

    override fun visit(replacer: TemplateReplacerEqualsHashCode) {
        super.visit(replacer)
    }

    override fun visit(replacer: TemplateReplacerUpperName) {
        super.visit(replacer)
    }

    override fun visit(replacer: TemplateReplacerEqualsLine) {
        super.visit(replacer)
    }

    override fun visit(replacer: TemplateReplacerHashCodeLine) {
        super.visit(replacer)
    }

    override fun visit(replacer: TemplateReplacerAssociations) {
        super.visit(replacer)
    }

    override fun visit(replacer: TemplateReplacerClassName) {
        super.visit(replacer)
    }

    override fun visit(replacer: TemplateReplacerComment) {
        super.visit(replacer)
    }

    override fun visit(replacer: TemplateReplacerAnnotations) {
        super.visit(replacer)
    }

    override fun visit(replacer: TemplateReplacerImports) {
        super.visit(replacer)
    }

    override fun visit(replacer: TemplateReplacerSubpackage) {
        super.visit(replacer)
    }

    override fun visit(replacer: TemplateReplacerValidators) {
        super.visit(replacer)
    }

    override fun visit(replacer: TemplateReplacerName) {
        super.visit(replacer)
    }

    override fun visit(replacer: TemplateReplacerTable) {
        super.visit(replacer)
    }

    override fun visit(replacer: TemplateReplacerLowerClassName) {
        super.visit(replacer)
    }

    override fun visit(replacer: TemplateReplacerDomainBuilderMethod) {
        super.visit(replacer)
    }

    override fun visit(replacer: TemplateReplacerDomainBuilder) {
        super.visit(replacer)
    }

    override fun visit(replacer: TemplateReplacerLine) {
        super.visit(replacer)
    }

    override fun visit(replacer: TemplateReplacerI18n) {
        super.visit(replacer)
    }

    override fun visit(replacer: TemplateReplacerGeneric) {
        super.visit(replacer)
    }

    override fun visit(replacer: TemplateReplacerId) {
        super.visit(replacer)
    }

    override fun visit(replacer: TemplateReplacerAccept) {
        super.visit(replacer)
    }
}
