package de.kaipho.genplus.generator.generator

import de.kaipho.genplus.generator.constants.Reflection
import de.kaipho.genplus.generator.constants.STRINGS
import de.kaipho.genplus.generator.core.obj.AstElementStandardVisitor
import de.kaipho.genplus.generator.core.obj.options.Option
import de.kaipho.genplus.generator.domain.obj.Domain
import de.kaipho.genplus.generator.generator.c_esp32.core.CCopyFilesManager
import de.kaipho.genplus.generator.generator.core.FileToCopy
import de.kaipho.genplus.generator.generator.core.Generator
import de.kaipho.genplus.generator.generator.core.copyFiles
import de.kaipho.genplus.generator.generator.java.JavaManager
import de.kaipho.genplus.generator.generator.java.core.JavaCopyFilesManager
import de.kaipho.genplus.generator.generator.typescript.TSManager
import de.kaipho.genplus.generator.io.FileSystem
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.store.SettingsStore
import java.util.*

/**
 * Der GeneratorFassade ist dafür zuständig, ein Model in Programmcode zu überführen.
 * Da der Code auf alle Fälle neu generiert wird, sollte darauf geachtet werden, nur geänderte Modelle neu zu generieren!
 */
object GeneratorFassade {
    var pathPrefix = ""

    const val PATH_JAVA = "server/src/main/java"

    val javaFolders = hashMapOf(
            "domain" to "server/src/main/java/${STRINGS.PREFIX_PLACEHOLDER}domain",
            "repository" to "server/src/main/java/${STRINGS.PREFIX_PLACEHOLDER}repository"
    )

    const val PATH_TS = "web/src/app"

    val typescriptFolders = hashMapOf(
            "domain" to "web/src/app/domain",
            "domain_service" to "web/src/app/service/domain/http",
            "domain_component" to "web/src/app/components/domain"
    )

    val generators: MutableMap<String, MutableList<Pair<AbstractGenerator<*>, Generator>>>

    init {
        generators = HashMap()

        Reflection.forEachClazzInstance(Reflection.GENERATOR, AbstractGenerator::class.java) { clazz, annotation ->
            if (SettingsStore.options.isSelected(Option.LANGS, annotation.lang.name)) {
                val name = annotation.appliedOn.qualifiedName!!
                if (!generators.contains(name)) {
                    generators.put(name, ArrayList())
                }
                generators[name]?.add(clazz to annotation)
            }
        }
    }

    fun startManager() {
        JavaCopyFilesManager().go()
        CCopyFilesManager().go()
        JavaManager().go()
        TSManager().go()
    }


    fun generate(path: String) {
        FileSystem.setBackupSequence(path)
        this.pathPrefix = path

        copyFiles(FileToCopy.values())

        generateDomain(ClassStore.domain)


        // TODO SECURITY

    }
    /**
     * Generiert für die Domain alle Dateien
     */
    private fun generateDomain(domain: Domain) {
        javaFolders.forEach {
            checkFolder(it.value)
        }

        ClassStore.accept(AstElementStandardVisitor())
//        SettingsStore.accept(AstElementStandardVisitor())
    }

    /**
     * Überprüft, ob der Ordner vorhanden ist, falls nicht wird dieser angelegt.
     */
    private fun checkFolder(path: String) {
//        val replacedPath = path.replace(STRINGS.PREFIX_PLACEHOLDER, SettingsStore.getPrefixFileSystem())
//        if (!Files.exists(Paths.get("${pathPrefix}/$replacedPath"))) {
//            println(STRINGS.FOLDER_NOT_EXIST(replacedPath))
//        }
    }
}
