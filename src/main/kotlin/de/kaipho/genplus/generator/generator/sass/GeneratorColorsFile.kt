package de.kaipho.genplus.generator.generator.sass

import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.core.obj.options.Option
import de.kaipho.genplus.generator.domain.obj.Domain
import de.kaipho.genplus.generator.domain.obj.ExplicitDomainClass
import de.kaipho.genplus.generator.generator.AbstractGenerator
import de.kaipho.genplus.generator.generator.core.File
import de.kaipho.genplus.generator.generator.core.Generator
import de.kaipho.genplus.generator.store.SettingsStore
import de.kaipho.genplus.generator.template.Templates
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLine

@Generator(
        lang = Language.SASS,
        template = Templates.SASS_COLOR_FILE,
        appliedOn = Domain::class,
        file = File.SASS_COLOR_FILE
)
class GeneratorColorsFile : AbstractGenerator<ExplicitDomainClass>() {
    override fun visit(replacer: TemplateReplacerLine) {
        when (replacer.id) {
            0 -> replacer.insert(SettingsStore.options.getFirst(Option.COLOR_MAIN))
            1 -> replacer.insert(SettingsStore.options.getFirst(Option.COLOR_SELECTED))
            2 -> replacer.insert(SettingsStore.options.getFirst(Option.COLOR_ACCENT))
            3 -> replacer.insert(SettingsStore.options.getFirst(Option.COLOR_ERROR))
        }
    }
}