package de.kaipho.genplus.generator.generator

import de.kaipho.genplus.generator.constants.STRINGS
import de.kaipho.genplus.generator.core.obj.AssoziationTypeUser
import de.kaipho.genplus.generator.domain.obj.Association
import de.kaipho.genplus.generator.domain.obj.DomainClass
import de.kaipho.genplus.generator.domain.obj.InlineRule
import de.kaipho.genplus.generator.generator.core.RuleHelp

fun recursiveAssociationFetcher(clazz: DomainClass, withDerived: Boolean = true): List<Association> {
    val toJoin = clazz.extends.filter {
        it is DomainClass
    }.map {
        it as DomainClass
    }.map {
        recursiveAssociationFetcher(it)
    }.flatten()
    if (withDerived) {
        return toJoin.union(clazz.associations).toList()
    } else {
        // TODO
        return toJoin.union(clazz.associations).toList().filter { !it.isDerived() }
    }
}

fun getInlinedAssociations(clazz: DomainClass): List<Association> {
    /*return clazz.associations.filter {
        it.rules.contains(InlineRule())
    }.map {
        if (it.type is AssoziationTypeUser) {
            return recursiveAssociationFetcher((it.type.clazz as DomainClass))
        } else {
            throw RuntimeException(STRINGS.RULE_INLINE + " must be on UserTypes")
        }
    }*/
    return ArrayList()
}
