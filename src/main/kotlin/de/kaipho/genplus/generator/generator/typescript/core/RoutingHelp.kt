package de.kaipho.genplus.generator.generator.typescript.core

import de.kaipho.genplus.generator.domain.obj.IClass

object RoutingHelp {

    private fun getRouteTo(clazz: IClass): String {
        return "domainr/${clazz.id}"
    }

    fun getRouteToList(clazz: IClass): String {
        return getRouteTo(clazz)
    }

    fun getRouteToDetail(clazz: IClass): String {
        return getRouteTo(clazz)
    }

}