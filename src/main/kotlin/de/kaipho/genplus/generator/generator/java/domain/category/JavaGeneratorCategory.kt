package de.kaipho.genplus.generator.generator.java.domain.category

import de.kaipho.genplus.generator.common.convertPackageToFileSystem
import de.kaipho.genplus.generator.domain.obj.CategoryClass
import de.kaipho.genplus.generator.generator.java.core.ExternalJavaGenerator
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerClassName
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerId


class JavaGeneratorCategory(val category: CategoryClass) : ExternalJavaGenerator() {
    override val templateString = "domain/category/xxx.java"
    override val replacer = arrayListOf(category.name)


    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(category.name)
    }

    override fun visit(replacer: TemplateReplacerId) {
        replacer.insert(category.id.toString())
    }
}