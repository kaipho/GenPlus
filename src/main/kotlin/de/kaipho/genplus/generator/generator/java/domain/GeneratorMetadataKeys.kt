package de.kaipho.genplus.generator.generator.java.domain

import de.kaipho.genplus.generator.generator.java.core.ExternalJavaGenerator
import de.kaipho.genplus.generator.generator.java.persistence.ClassIdGenerator
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLine

class GeneratorMetadataKeys: ExternalJavaGenerator() {

    override val templateString = "domain/MetadataKeys.java"

    override fun visit(replacer: TemplateReplacerLine) {
        ClassStore.domain.domainClasses.forEach { clazz ->
            replacer.insert("final Long ${ClassIdGenerator.getClassValName(clazz)} = ${clazz.id}L;")

            clazz.associations.forEach { association ->
                replacer.insert("final Long ${ClassIdGenerator.getAssociationValName(association)} = ${association.id}L;")
            }

            replacer.insert("")
        }
    }
}
