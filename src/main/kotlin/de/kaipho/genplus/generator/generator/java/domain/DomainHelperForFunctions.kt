package de.kaipho.genplus.generator.generator.java.domain

import de.kaipho.genplus.generator.common.upperFirstLetter
import de.kaipho.genplus.generator.core.obj.*
import de.kaipho.genplus.generator.domain.obj.*
import de.kaipho.genplus.generator.generator.ListReplacer
import de.kaipho.genplus.generator.generator.insertInto
import de.kaipho.genplus.generator.generator.java.persistence.ClassIdGenerator

/**
 * Helper to create functions (getter/setter) in domain classes.
 */


fun setterAssociationAbstract(association: Association, replacer: ListReplacer) {
    val upperName = association.name.upperFirstLetter()
    val name = association.name

    association.specialisation.accept(object : AssociationSpecialisationVisitor<Unit> {
        override fun visitDerived(specialisation: AssociationSpecialisation) {
            // Derived Associations have no setter!
        }

        override fun visitTimed(specialisation: AssociationSpecialisation) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun visitNamed(specialisation: AssociationSpecialisation) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun visitNormal(specialisation: AssociationSpecialisation) {
            """
            |void set$upperName(${association.type.javaAbstractRep()} $name);
            |
            """.insertInto(replacer)
            if (association.type is AssoziationTypeOptional) {
                """
                |void set$upperName(${association.type.inner.javaAbstractRep()} $name);
                |
                """.insertInto(replacer)
            }
            if (association.type is AssoziationTypeCollection) {
                """
                |void addSingleTo$upperName(${association.type.inner.javaAbstractRep()} $name);
                |
                """.insertInto(replacer)
            }
            if (association.type is AssoziationTypeInteger) {
                """
                |void add$upperName(${association.type.javaAbstractRep()} $name);
                |
                """.insertInto(replacer)
            }
        }
    })
}

fun setterAssociationImpl(association: Association, replacer: ListReplacer) {
    val upperName = association.name.upperFirstLetter()
    val name = association.name

    association.specialisation.accept(object : AssociationSpecialisationVisitor<Unit> {
        override fun visitDerived(specialisation: AssociationSpecialisation) {
            // Derived Associations have no setter!
        }

        override fun visitTimed(specialisation: AssociationSpecialisation) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun visitNamed(specialisation: AssociationSpecialisation) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun visitNormal(specialisation: AssociationSpecialisation) {
            """
            |@Override
            |public void set$upperName(${association.type.javaAbstractRep()} $name) {
            |    this.$name = $name;
            |}
            """.insertInto(replacer)
            if (association.type is AssoziationTypeOptional) {
                """
                |@Override
                |public void set$upperName(${association.type.inner.javaAbstractRep()} $name) {
                |    this.$name = Optional.of($name);
                |}
                """.insertInto(replacer)
            }
        }
    })
}

fun setterAssociationRepoProxy(association: Association, replacer: ListReplacer) {
    val upperName = association.name.upperFirstLetter()
    val name = association.name

    association.specialisation.accept(object : AssociationSpecialisationVisitor<Unit> {
        override fun visitDerived(specialisation: AssociationSpecialisation) {
            // Derived Associations have no setter!
        }

        override fun visitTimed(specialisation: AssociationSpecialisation) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun visitNamed(specialisation: AssociationSpecialisation) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun visitNormal(specialisation: AssociationSpecialisation) {
            if (association.type is AssoziationTypeOptional) {
                """
                |@Override
                |public void set$upperName(${association.type.javaAbstractRep()} $name) {
                |    setAssociation(real.getId(), $name.orElse(null), ${ClassIdGenerator.getAssociationValName(association)});
                |    real.set$upperName($name);
                |}
                """.insertInto(replacer)
                """
                |@Override
                |public void set$upperName(${association.type.inner.javaAbstractRep()} $name) {
                |    setAssociation(real.getId(), $name, ${ClassIdGenerator.getAssociationValName(association)});
                |    real.set$upperName($name);
                |}
                """.insertInto(replacer)
            } else if (association.type is AssoziationTypeUser && association.type.clazz is PrimitiveClass) {
                """
                |@Override
                |public void set$upperName(${association.type.javaAbstractRep()} $name) {
                |    primitivesStore.savePrimitive($name, real, ${ClassIdGenerator.getAssociationValName(association)});
                |    real.set$upperName($name);
                |}
                """.insertInto(replacer)
            } else {
                """
                |@Override
                |public void set$upperName(${association.type.javaAbstractRep()} $name) {
                |    setAssociation(real.getId(), $name, ${ClassIdGenerator.getAssociationValName(association)});
                |    real.set$upperName($name);
                |}
                """.insertInto(replacer)
                if (association.type is AssoziationTypeCollection) {
                    """
                    |@Override
                    |public void addSingleTo$upperName(${association.type.inner.javaAbstractRep()} $name) {
                    |   real.addSingleTo$upperName($name);
                    |}
                    """.insertInto(replacer)
                }
                if (association.type is AssoziationTypeInteger) {
                    """
                    |@Override
                    |public void add$upperName(${association.type.javaAbstractRep()} $name) {
                    |   real.add$upperName($name);
                    |}
                    """.insertInto(replacer)
                }
            }
        }
    })
}

fun getterAssociationAbstract(association: Association, replacer: ListReplacer) {
    val upperName = association.name.upperFirstLetter()

    association.specialisation.accept(object : AssociationSpecialisationVisitor<Unit> {
        override fun visitDerived(specialisation: AssociationSpecialisation) {
            defaultGetter()
        }

        override fun visitTimed(specialisation: AssociationSpecialisation) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun visitNamed(specialisation: AssociationSpecialisation) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun visitNormal(specialisation: AssociationSpecialisation) {
            defaultGetter()
        }

        fun defaultGetter() {
            """
            |${association.type.javaAbstractRep()} get$upperName();
            |
            """.insertInto(replacer)
        }
    })
}

fun getterAssociationRepoProxy(association: Association, replacer: ListReplacer) {
    val upperName = association.name.upperFirstLetter()
    val name = association.name

    association.specialisation.accept(object : AssociationSpecialisationVisitor<Unit> {
        override fun visitDerived(specialisation: AssociationSpecialisation) {
            """
            |@Override
            |public ${association.type.javaAbstractRep()} get$upperName() {
            |   return DerivedManager.getInstance().derived$upperName(this);
            |}
            """.insertInto(replacer)
        }

        override fun visitTimed(specialisation: AssociationSpecialisation) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun visitNamed(specialisation: AssociationSpecialisation) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun visitNormal(specialisation: AssociationSpecialisation) {
            if (association.type is AssoziationTypeUser) {
                val clazzName = association.type.clazz.name
                association.type.acceptClass(object : UserClassVisitor<Unit> {
                    override fun visitPrimitive(innerAssociation: AssoziationTypeUser, clazz: PrimitiveClass) {
                        """
                        |@Override public ${innerAssociation.javaAbstractRep()} get$upperName() {
                        |   if(real.get$upperName() == null) {
                        |       PersistentPrimitive<$clazzName> primitive = primitivesStore.loadPersistentPrimitive(real, ${ClassIdGenerator.getAssociationValName(association)}, $clazzName.getUnitClass());
                        |       real.set$upperName(primitive);
                        |   }
                        |   return real.get$upperName();
                        |}
                        """.insertInto(replacer)
                    }

                    override fun visitCategory(association: AssoziationTypeUser, clazz: CategoryClass) {
                        """
                        |@Override
                        |public ${association.javaAbstractRep()} get$upperName() {
                        |   return real.get$upperName();
                        |}
                        """.insertInto(replacer)
                    }

                    override fun visitDomain(innerAssociation: AssoziationTypeUser, clazz: DomainClass) {
                        """
                        |@Override public ${innerAssociation.javaAbstractRep()} get$upperName() {
                        |   if(real.get$upperName().getId() == null) {
                        |       real.set$upperName(${innerAssociation.javaRep()}.findOneById(getObjectForAssociations(real.getId(), ${ClassIdGenerator.getAssociationValName(association)})));
                        |   }
                        |   return real.get$upperName();
                        |}
                        """.insertInto(replacer)
                    }

                    override fun visitConstant(association: AssoziationTypeUser, clazz: Constant) {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }
                })
            } else if (association.type is AssoziationTypeOptional && association.type.inner is AssoziationTypeUser) {
                """
                |@Override public ${association.type.javaAbstractRep()} get$upperName() {
                |   if(real.get$upperName() == null) {
                |       real.set$upperName(Optional.ofNullable(${association.type.javaRep()}
                |           .findOneById(getObjectForAssociationsOptional(real.getId(), ${ClassIdGenerator.getAssociationValName(association)}))));
                |   }
                |   return real.get$upperName();
                |}
                """.insertInto(replacer)
            } else {
                """
                |@Override
                |public ${association.type.javaAbstractRep()} get$upperName() {
                |   return real.get$upperName();
                |}
                """.insertInto(replacer)
            }
        }
    })
}