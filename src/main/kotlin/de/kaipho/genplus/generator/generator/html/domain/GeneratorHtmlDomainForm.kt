package de.kaipho.genplus.generator.generator.html.domain

import de.kaipho.genplus.generator.common.lowerFirstLetter
import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.core.obj.*
import de.kaipho.genplus.generator.domain.obj.*
import de.kaipho.genplus.generator.generator.AbstractGenerator
import de.kaipho.genplus.generator.generator.core.File
import de.kaipho.genplus.generator.generator.core.Generator
import de.kaipho.genplus.generator.generator.html.core.FormGenerator
import de.kaipho.genplus.generator.generator.i18n.JsonStore
import de.kaipho.genplus.generator.generator.recursiveAssociationFetcher
import de.kaipho.genplus.generator.template.Templates
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLine

@Generator(
        lang = Language.TYPESCRIPT,
        template = Templates.HTML_DOMAIN_FORM,
        appliedOn = ExplicitDomainClass::class,
        file = File.HTML_DOMAIN_FORM
)
class GeneratorHtmlDomainForm : AbstractGenerator<ExplicitDomainClass>() {
    lateinit var association: Association
    lateinit var name: String

    var counter = 0

    override fun visit(replacer: TemplateReplacerLine) {
        counter = 85
        this.name = meta.data.name
        val iterator = recursiveAssociationFetcher(meta.data).filter { it.type !is AssoziationTypeCollection && !(it.type is AssoziationTypeOptional && it.type.inner is AssoziationTypeUser && !it.type.inner.noClazz())}.iterator()
        while (iterator.hasNext()) {
            association = iterator.next()
            replacer.insert(association.type.accept(visitor))
            JsonStore.add("domain${meta.data.prefix}.${meta.data.getLowerName()}.${association.name}")
            if (iterator.hasNext()) {
                association = iterator.next()
                replacer.insert(association.type.accept(visitor))
                JsonStore.add("domain${meta.data.prefix}.${meta.data.getLowerName()}.${association.name}")
            }
            replacer.insert("")
        }
        recursiveAssociationFetcher(meta.data).filter { it.type is AssoziationTypeOptional }.forEach {
            this@GeneratorHtmlDomainForm.association = it
            JsonStore.add("domain${meta.data.prefix}.${meta.data.getLowerName()}.${it.name}")
            replacer.insert(it.type.accept(visitor))
        }
        recursiveAssociationFetcher(meta.data).filter { it.type is AssoziationTypeCollection }.forEach {
            this@GeneratorHtmlDomainForm.association = it
            JsonStore.add("domain${meta.data.prefix}.${meta.data.getLowerName()}.${it.name}")
            replacer.insert(it.type.accept(visitor))
        }
    }

    val visitor = object : AssociationVisitor<String> {
        override fun visit(associationTypeBlob: AssociationTypeBlob): String {
            val associationName = this@GeneratorHtmlDomainForm.association.name
            val placeholder = "[placeholder]=\"'domain${self().prefix}.${self().name.lowerFirstLetter()}.$associationName' | translate\""
            return "<gen-blob formControlName=\"$associationName\" $placeholder></gen-blob>"
        }

        override fun visit(association: AssoziationTypeMap): String {
            return FormGenerator.createInputFieldDepoendingOnRules(this@GeneratorHtmlDomainForm.association, self())
        }

        override fun visit(association: AssoziationTypeSecure): String {
            return "    <gen-secure formControlName=\"${this@GeneratorHtmlDomainForm.association.name}\" [placeholder]=\"'domain${self().prefix}.${self().name.lowerFirstLetter()}.${this@GeneratorHtmlDomainForm.association.name}'\"></gen-secure>"
        }

        override fun visit(association: AssoziationTypeOptional): String {
            val associationName = this@GeneratorHtmlDomainForm.association.name
            if (association.inner is AssoziationTypeUser) {
                return association.inner.acceptClass(object : UserClassVisitor<String> {
                    override fun visitPrimitive(association: AssoziationTypeUser, clazz: PrimitiveClass): String {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                    override fun visitCategory(association: AssoziationTypeUser, clazz: CategoryClass): String {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                    override fun visitDomain(association: AssoziationTypeUser, clazz: DomainClass): String {
                        val result = "<gen-obj [obj]=\"crudForm.get('$associationName').value\" name=\"domain${self().prefix}.${self().name.lowerFirstLetter()}.$associationName\"></gen-obj>"
                        return result
                    }

                    override fun visitConstant(association: AssoziationTypeUser, clazz: Constant): String {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                })
            } else
                return association.inner.accept(this)
        }

        override fun visit(association: AssoziationTypeText): String {
            return FormGenerator.createInputFieldDepoendingOnRules(this@GeneratorHtmlDomainForm.association, self())
        }

        override fun visit(association: AssoziationTypeBigNumber): String {
            return FormGenerator.createInputFieldDepoendingOnRules(this@GeneratorHtmlDomainForm.association, self())
        }

        override fun visit(association: AssoziationTypeDouble): String {
            return FormGenerator.createInputFieldDepoendingOnRules(this@GeneratorHtmlDomainForm.association, self())
        }

        override fun visit(assoziation: AssoziationTypeUnit): String = throw UnsupportedOperationException("not implemented")

        override fun visit(assoziation: AssoziationTypeDummy): String = throw UnsupportedOperationException("not implemented")

        override fun visit(outerAssociation: AssoziationTypeCollection): String {
            val associationName = this@GeneratorHtmlDomainForm.association.name
            val placeholder = "[placeholder]=\"'domain${self().prefix}.${self().name.lowerFirstLetter()}.$associationName' | translate\""
            if (outerAssociation.inner is AssoziationTypeUser) {
                return outerAssociation.inner.acceptClass(object : UserClassVisitor<String> {
                    override fun visitPrimitive(association: AssoziationTypeUser, clazz: PrimitiveClass): String {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                    override fun visitCategory(association: AssoziationTypeUser, clazz: CategoryClass): String {
                        return "<gen-category-obj-list [data]=\"obj.$associationName\" $placeholder clazzId=\"${clazz.id}\" assId=\"${this@GeneratorHtmlDomainForm.association.id}\"></gen-category-obj-list>"
                    }

                    override fun visitDomain(association: AssoziationTypeUser, clazz: DomainClass): String {
                        val result = "<gen-obj-list style=\"margin-top: ${counter}px !important;\" name=\"${placeholder.removePrefix("[placeholder]=\"'").removeSuffix("' | translate\"")}\" [obj]=\"crudForm.getRawValue().$associationName\"></gen-obj-list>"
                        return result
                    }

                    override fun visitConstant(association: AssoziationTypeUser, clazz: Constant): String {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                })
            } else {
                if (association != null) {
                    return "<gen-simple-obj-list [data]=\"obj.${association.name}\" $placeholder [link]=\"obj.self\" [assId]=\"${association.id}\"></gen-simple-obj-list>"
                } else {
                    throw RuntimeException("element as parameter not supported: AssociationTypeCollection<SimpleType>")
                }
            }
        }

        override fun visit(association: AssoziationTypeString): String {
            return FormGenerator.createInputFieldDepoendingOnRules(this@GeneratorHtmlDomainForm.association, self())
        }

        override fun visit(association: AssoziationTypeInteger): String {
            return FormGenerator.createInputFieldDepoendingOnRules(this@GeneratorHtmlDomainForm.association, self())
        }

        override fun visit(association: AssoziationTypePassword): String {
            return FormGenerator.createInputFieldDepoendingOnRules(this@GeneratorHtmlDomainForm.association, self())
        }

        override fun visit(association: AssoziationTypeDate): String {
            return FormGenerator.createInputFieldDepoendingOnRules(this@GeneratorHtmlDomainForm.association, self())
        }

        override fun visit(association: AssoziationTypeBoolean): String {
            return FormGenerator.createInputFieldDepoendingOnRules(this@GeneratorHtmlDomainForm.association, self())
        }

        override fun visit(association: AssoziationTypeUser): String {
            val associationName = this@GeneratorHtmlDomainForm.association.name
            val placeholder = "[placeholder]=\"'domain${self().prefix}.${self().name.lowerFirstLetter()}.$associationName' | translate\""

            return association.acceptClass(object : UserClassVisitor<String> {
                override fun visitPrimitive(association: AssoziationTypeUser, clazz: PrimitiveClass): String {
                    val units = (association.clazz as PrimitiveClass).units.map { it -> it.id }.joinToString()
                    return "<gen-primitive formControlName=\"$associationName\" [unitClass]=\"${association.clazz.id}\" [units]=\"[$units]\" $placeholder></gen-primitive>"
                }

                override fun visitCategory(association: AssoziationTypeUser, clazz: CategoryClass): String {
                    return "<gen-category formControlName=\"$associationName\" $placeholder clazz=\"${clazz.id}\" ></gen-category>"
                }

                override fun visitDomain(association: AssoziationTypeUser, clazz: DomainClass): String {
                    return ""
                }

                override fun visitConstant(association: AssoziationTypeUser, clazz: Constant): String {
                    return ""
                }

            })
        }
    }
}
