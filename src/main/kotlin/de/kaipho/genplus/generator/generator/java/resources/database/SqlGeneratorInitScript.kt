package de.kaipho.genplus.generator.generator.java.resources.database

import de.kaipho.genplus.generator.core.obj.*
import de.kaipho.genplus.generator.domain.obj.*
import de.kaipho.genplus.generator.generator.java.core.ExternalSqlGenerator
import de.kaipho.genplus.generator.generator.java.core.fetchAllParents
import de.kaipho.genplus.generator.generator.java.core.fetchHierarchy
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLine

class SqlGeneratorInitScript : ExternalSqlGenerator() {
    override val templateString = "init.sql"

    override fun visit(replacer: TemplateReplacerLine) {
        ClassStore.getAllClasses().forEach { type ->
            type.acceptClass(object : TypeVisitor<Unit> {
                override fun visitPrimitive(clazz: PrimitiveClass) {
                    replacer.insert("INSERT INTO class(id, name, abstract, type) VALUES (${type.id}, '${type.name}', 0, 'primitive');")
                }

                override fun visitCategory(clazz: CategoryClass) {
                    replacer.insert("INSERT INTO class(id, name, abstract, type) VALUES (${type.id}, '${type.name}', 0, 'category_class');")
                }

                override fun visitDomain(clazz: DomainClass) {
                    val isAbstract = if (clazz is ExplicitDomainClass) 0 else 1
                    replacer.insert("INSERT INTO class(id, name, abstract, type) VALUES (${clazz.id}, '${clazz.name}', $isAbstract, 'class');")
                }

                override fun visitConstant(clazz: Constant) {
                    clazz.accept(object : ConstantVisitor<Unit> {
                        override fun visit(constantClass: ConstantClass) {
                            replacer.insert("INSERT INTO class(id, name, abstract, type) VALUES (${type.id}, '${type.name}', 0, 'constant');")
                        }

                        override fun visit(constantContainer: ConstantContainer) {
                            replacer.insert("INSERT INTO class(id, name, abstract, type) VALUES (${type.id}, '${type.name}', 0, 'constant-container');")
                        }
                    })
                }

                override fun visitOther(clazz: IClass) {
                    // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun visitDummy(clazz: IClass) {
                    // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

            })
        }

        ClassStore.domain.domainClasses.forEach {

        }
        ClassStore.primitives.forEach {

        }
        ClassStore.getAllClasses().forEach { type ->
            // reflexive pairs
            replacer.insert("INSERT INTO hierarchy(super, sub, derived) VALUES (${type.id}, ${type.id}, 0);")
            type.acceptClass(object : TypeVisitor<Unit> {
                override fun visitPrimitive(clazz: PrimitiveClass) {
                    // TODO
                }

                override fun visitCategory(clazz: CategoryClass) {
                    fetchHierarchy(clazz).forEach { sub ->
                        replacer.insert("INSERT INTO hierarchy(super, sub, derived) VALUES (${clazz.id}, ${sub.id}, 0);")
                    }
                }

                override fun visitDomain(clazz: DomainClass) {
                    replacer.insert("INSERT INTO hierarchy(super, sub, derived) VALUES (-1, ${clazz.id}, ${isDerived(clazz)});")
                    fetchAllParents(clazz).forEach { superClass ->
                        replacer.insert("INSERT INTO hierarchy(super, sub, derived) VALUES (${superClass.id}, ${clazz.id}, ${isDerived(superClass, clazz)});")
                    }
                }

                override fun visitConstant(clazz: Constant) {
                    // TODO
                }

                override fun visitOther(clazz: IClass) {
                    TODO("not implemented")
                }

                override fun visitDummy(clazz: IClass) {
                    throw RuntimeException("Dummy classes not allowed in this stage")
                }

            })
        }
        ClassStore.domain.domainClasses.forEach { clazz ->
            clazz.associations.forEach {
                when (it.type) {
                    is AssoziationTypeBoolean,
                    is AssoziationTypeString,
                    is AssoziationTypeInteger,
                    is AssoziationTypeDate,
                    is AssoziationTypeDouble,
                    is AssoziationTypeText,
                    is AssoziationTypePassword -> {
                        replacer.insert("INSERT INTO association (id, source, target, name) VALUES (${it.id}, ${clazz.id}, ${it.type.id}, '${it.name}');")
                    }
                    is AssoziationTypeUser -> {
                        replacer.insert("INSERT INTO association (id, source, target, name) VALUES (${it.id}, ${clazz.id}, ${it.type.clazz.id}, '${it.name}');")
                    }
                    is GenericAssoziationType -> {
                        when (it.type.inner) {
                            is AssoziationTypeBoolean,
                            is AssoziationTypeString,
                            is AssoziationTypeInteger,
                            is AssoziationTypeDate,
                            is AssoziationTypeDouble,
                            is AssoziationTypeText,
                            is AssoziationTypePassword -> {
                                replacer.insert("INSERT INTO association (id, source, target, name) VALUES (${it.id}, ${clazz.id}, -2, '${it.name}');")
                            }
                            is AssoziationTypeUser -> {
                                replacer.insert("INSERT INTO association (id, source, target, name) VALUES (${it.id}, ${clazz.id}, ${it.type.inner.clazz.id}, '${it.name}');")
                            }
                        }
                    }
                }
            }
        }

        ClassStore.services.forEach { service ->
            service.functions.forEach { function ->
                replacer.insert("INSERT INTO callable (id, source, target, name) VALUES (${function.id}, ${function.extends.id}, ${function.returnType.id}, '${function.name}');")
            }
        }
    }

    private fun isDerived(superClass: IClass, sub: IClass): Int {
        if (sub.extends.contains(superClass))
            return 0
        return 1
    }

    private fun isDerived(sub: IClass): Int {
        if (sub.extends.isEmpty())
            return 0
        return 1
    }
}
