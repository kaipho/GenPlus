package de.kaipho.genplus.generator.generator.java.tests.cucumber

import de.kaipho.genplus.generator.common.lowerFirstLetter
import de.kaipho.genplus.generator.common.upperFirstLetter
import de.kaipho.genplus.generator.core.obj.AssoziationTypeUser
import de.kaipho.genplus.generator.generator.java.core.ExternalJavaTestGenerator
import de.kaipho.genplus.generator.generator.java.service.Imports
import de.kaipho.genplus.generator.generator.recursiveAssociationFetcher
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerImports
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLine

class GivenDomainStepDefsGenerator : ExternalJavaTestGenerator() {
    override val templateString = "e2e/steps/GivenDomainStepDefs.java"
    val domainClasses = ClassStore.domain.domainClasses.sortedBy { it.name }

    override fun visit(replacer: TemplateReplacerImports) {
        val imports = Imports()

        domainClasses.forEach {
            // imports.addDomainImport(it)
        }

        replacer.insertAll(imports.getimports())
    }

    override fun visit(replacer: TemplateReplacerLine) {
        domainClasses.forEach {
            replacer.insert("@Given(\"^die ${it.name}-Liste:$\")")
            replacer.insert("public void given${it.name}Liste(DataTable voListe) throws Exception {")
            replacer.insert("}")
            replacer.insert("")

            recursiveAssociationFetcher(it, false).filter { association -> association.type is AssoziationTypeUser }.forEach {association ->
                replacer.insert("@Given(\"^das ${it.name}-Element (\\\\d) hat im Feld ${association.name} ein ${association.type.name}-Element mit ID (\\\\d)$\")")
                replacer.insert("public void givenElementFor${it.name}InField${association.name.upperFirstLetter()}(long ${it.name.lowerFirstLetter()}, long ${association.type.name.lowerFirstLetter()}) throws Exception {")
                replacer.insert("}")
                replacer.insert("")
            }
        }
    }
}
