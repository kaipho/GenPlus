package de.kaipho.genplus.generator.generator.java.service.domainr

import de.kaipho.genplus.generator.generator.java.core.ExternalJavaGenerator
import de.kaipho.genplus.generator.generator.java.service.Imports
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerImports
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLine

class DataRequestService : ExternalJavaGenerator() {
    override val templateString = "service/domainr/DataRequestService.java"

    val domainClazzes = ClassStore.domain.domainClasses

    override fun visit(replacer: TemplateReplacerLine) {
        domainClazzes.forEach {
            replacer.insert("requestServices.put(${it.name}.class, new ${it.name}Service());")
        }
    }

    override fun visit(replacer: TemplateReplacerImports) {
        val import = Imports()
        domainClazzes.forEach {
            import.addDomainImport(it)
            import.addDomainServiceImport(it)
        }
        replacer.insertAll(import.getimports())
    }
}
