package de.kaipho.genplus.generator.generator.core

import de.kaipho.genplus.generator.domain.obj.*
import de.kaipho.genplus.generator.domain.obj.*

object RuleHelp {

    fun hasRuleValidation(association: AstElementWithRules): ValidateRule? {
        return association.rules.find { it is ValidateRule } as? ValidateRule
    }

    fun hasRuleSecured(association: AstElementWithRules): SecuredRule? {
        return association.rules.find { it is SecuredRule } as? SecuredRule
    }

    fun hasRule(association: AstElementWithRules, name:String): CustomRule? {
        return association.rules.find { it is CustomRule && it.customName == name } as? CustomRule
    }

    fun ruleExist(association: AstElementWithRules, name:String): Boolean {
        return hasRule(association, name) != null
    }
}

val VALIDATION_RULE_ALLOWED_VALUES = "allowedValues"

fun ValidateRule.hasAllowedValueContrained(): String? {
    val rule = this.rule.find { it.name == VALIDATION_RULE_ALLOWED_VALUES } as? RuleWithParam
    if (rule != null) {
        return rule.params[0]
    }
    return null
}