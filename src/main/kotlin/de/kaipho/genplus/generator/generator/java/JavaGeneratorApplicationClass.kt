package de.kaipho.genplus.generator.generator.java

import de.kaipho.genplus.generator.generator.java.core.ExternalJavaGenerator
import de.kaipho.genplus.generator.store.SettingsStore
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerPackage

class JavaGeneratorApplicationClass : ExternalJavaGenerator() {
    override val templateString = "Application.java"

    override fun visit(replacer: TemplateReplacerPackage) {
        lines.add(replacer.replaceOneLine(actual, SettingsStore.getPrefixPackage().removeSuffix(".")))
    }
}
