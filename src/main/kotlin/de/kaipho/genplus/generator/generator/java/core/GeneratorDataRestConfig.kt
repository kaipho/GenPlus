package de.kaipho.genplus.generator.generator.java.core

import de.kaipho.genplus.generator.domain.obj.Domain
import de.kaipho.genplus.generator.domain.obj.IClass
import de.kaipho.genplus.generator.generator.AbstractGenerator
import de.kaipho.genplus.generator.generator.java.service.Imports
import de.kaipho.genplus.generator.store.SettingsStore
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerImports
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLine
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerPackage

//@Generator(
//        lang = Language.JAVA,
//        file = File.JAVA_DATA_REST_CONFIG,
//        appliedOn = Domain::class,
//        template = Templates.JAVA_DATA_REST_CONFIG
//)
class GeneratorDataRestConfig : AbstractGenerator<Domain>() {
    override fun visit(replacer: TemplateReplacerPackage) {
        lines.add(replacer.replaceOneLine(actual, SettingsStore.getPrefixPackage()))
    }

    override fun visit(replacer: TemplateReplacerImports) {
        val imports = Imports()

        getData().domainClasses.filter { filter(it) }.forEach {
            imports.addDomainImport(it)
        }

        replacer.insertAll(imports.getimports())
    }

    override fun visit(replacer: TemplateReplacerLine) {
        getData().domainClasses.filter { filter(it) }.forEachIndexed { index, actual ->
            if (index == getData().domainClasses.filter { filter(it) }.size - 1)
                replacer.insert("${actual.name}.class")
            else
                replacer.insert("${actual.name}.class,")
        }
    }

    fun filter(clazz: IClass) : Boolean {
        if(!SettingsStore.securityEnabled()) {
            if(clazz.name == "User") {
                return false
            }
        }
        return true
    }
}