package de.kaipho.genplus.generator.generator.core

import de.kaipho.genplus.generator.core.obj.service.Function
import de.kaipho.genplus.generator.core.obj.service.ServiceClass
import de.kaipho.genplus.generator.domain.obj.ClassImpl
import de.kaipho.genplus.generator.domain.obj.ExplicitDomainClass
import de.kaipho.genplus.generator.domain.obj.IClass
import de.kaipho.genplus.generator.domain.obj.isExportRecursive
import de.kaipho.genplus.generator.store.SettingsStore
import de.kaipho.genplus.generator.store.SettingsStore.securityEnabled
import kotlin.reflect.KCallable

/**
 * Created by tom on 22.07.16.
 */
enum class Ref(val method: KCallable<Boolean>) {
    IS_EXPORT(ExplicitDomainClass::isExportRecursive),
    IS_FUNCTION_EXPORT(Function::isWithExport),
    IS_SERVICECLASS_EXPORT(ServiceClass::export.getter),
    HAS_DETAIL_PAGE(IClass::hasDetailsPage),
    IS_LOCAL(IClass::isLocal),
    SECURITY_ENABLED(SettingsStore::securityEnabled),
    DATABASE_ENABLED(ClassImpl::databaseEnabled),
    NOTHING(::getTrue)
}

fun getTrue() = true

fun callClasses() {
    ExplicitDomainClass::class.java
    IClass::class.java
    SettingsStore::class.java
}