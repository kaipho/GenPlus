package de.kaipho.genplus.generator.generator.java.domain.category

import de.kaipho.genplus.generator.core.obj.options.Option
import de.kaipho.genplus.generator.generator.java.core.ExternalJavaGenerator
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.store.SettingsStore
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerId
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLine


class JavaGeneratorCategoryLanguage : ExternalJavaGenerator() {
    override val templateString = "domain/category/Language.java"

    override fun visit(replacer: TemplateReplacerId) {
        val language = ClassStore.categories.find { it.name == "Language" }
        if(language == null) throw RuntimeException("Language must be defined as category for framework!")
        replacer.insert(language.id.toString())
    }

    override fun visit(replacer: TemplateReplacerLine) {
        SettingsStore.options.get(Option.USER_LANGS).forEach {
            replacer.insert("public static Language ${it.toUpperCase()};")
        }
    }
}