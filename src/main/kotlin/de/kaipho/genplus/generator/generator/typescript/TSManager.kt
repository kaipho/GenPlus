package de.kaipho.genplus.generator.generator.typescript

import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.core.obj.options.Option
import de.kaipho.genplus.generator.generator.AbstractGenerator
import de.kaipho.genplus.generator.generator.core.GeneratorType
import de.kaipho.genplus.generator.generator.html.domain.GeneratorHtmlDomainList
import de.kaipho.genplus.generator.generator.html.service.GeneratorHtmlServiceCall
import de.kaipho.genplus.generator.generator.i18n.JsonStore
import de.kaipho.genplus.generator.generator.java.core.ExternalGeneratorManager
import de.kaipho.genplus.generator.generator.java.core.SimpleExternalGenerator
import de.kaipho.genplus.generator.generator.typescript.core.*
import de.kaipho.genplus.generator.generator.typescript.domain.GeneratorDomainListController
import de.kaipho.genplus.generator.generator.typescript.service.GeneratorServiceController
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.store.SettingsStore
import java.util.*

class TSManager : ExternalGeneratorManager {
    override fun go() {
        if (SettingsStore.options.isSelected(Option.LANGS, Language.TYPESCRIPT.name)) {
            runLater(GeneratorTsAngularCliJson())
            runLater(GeneratorTsPackageJson())
            runLater(GeneratorTsWithPath("protractor.conf.js"))
            runLater(GeneratorTsIndexHtml())
            runLater(GeneratorTsWithPath("karma.conf.js"))
            runLater(GeneratorTsWithPath("tslint.json"))
            runLater(GeneratorTsWithPath("src/styles.scss"))
            runLater(GeneratorTsWithPath("src/main.ts"))
            runLater(GeneratorTsWithPath("src/test.ts"))
            runLater(GeneratorTsWithPath("src/tsconfig.json"))
            runLater(GeneratorTsWithPath("src/polyfills.ts"))
            runLater(GeneratorTsWithPath("src/typings.d.ts"))

            runLater(GeneratorTsWithPath("src/environments/environment.prod.ts"))
            runLater(GeneratorTsWithPath("src/environments/environment.ts"))

            runLater(GeneratorTsWithPath("src/app/app.component.scss"))
            runLater(GeneratorTsAppComponentHtml())
            runLater(GeneratorTsAppComponentTs())
            runLater(GeneratorTsAppRoutes())
            // app.module, muss zuletzt laufen, damit alle klassen bekannt sind.
            runLast(GeneratorTsAppModule())

            runLater(GeneratorTsWithPath("src/app/service/core/title.service.ts"))
            runLater(GeneratorTsWithPath("src/app/service/core/login.service.ts"))
            runLater(GeneratorTsWithPath("src/app/service/core/http.service.ts"))
            runLater(GeneratorTsWithPath("src/app/service/core/token.store.ts"))
            runLater(GeneratorTsWithPath("src/app/service/core/transaction.store.ts"))

            runLater(GeneratorTsWithPath("src/app/components/admin/admin.components.ts"))
            runLater(GeneratorTsWithPath("src/app/components/admin/masterview/masterview.component.ts"))
            runLater(GeneratorTsWithPath("src/app/components/admin/masterview/masterview.component.scss"))
            runLater(GeneratorTsWithPath("src/app/components/admin/masterview/masterview.component.html"))
            runLater(GeneratorTsWithPath("src/app/components/admin/statistic/statistic.component.html"))
            runLater(GeneratorTsWithPath("src/app/components/admin/statistic/statistic.component.ts"))
            runLater(GeneratorTsWithPath("src/app/components/admin/statistic/statistic.component.scss"))
            runLater(GeneratorTsWithPath("src/app/components/admin/transactions/transactions.component.html"))
            runLater(GeneratorTsWithPath("src/app/components/admin/transactions/transactions.component.ts"))
            runLater(GeneratorTsWithPath("src/app/components/admin/transactions/transactions.component.scss"))

            runLater(GeneratorTsWithPath("src/app/components/framework/title/title.component.ts"))
            runLater(GeneratorTsWithPath("src/app/components/framework/title/list.component.ts"))
            runLater(GeneratorTsWithPath("src/app/components/framework/title/detail.component.ts"))
            runLater(GeneratorTsWithPath("src/app/components/framework/gen-service/menu.service.ts"))
            runLater(GeneratorTsWithPath("src/app/components/framework/gen-service/gen-service.component.ts"))
            runLater(GeneratorTsWithPath("src/app/components/framework/pipes/safeHtml.pipe.ts"))

            runLater(GeneratorTsWithPath("src/app/components/framework/gen-dialog/gen-dialog.component.ts"))
            runLater(GeneratorTsWithPath("src/app/components/framework/form/form.service.ts"))
            runLater(GeneratorTsWithPath("src/app/components/framework/form/gen-category.ts"))
            runLater(GeneratorTsWithPath("src/app/components/framework/gen-obj/gen-blob.ts"))
            runLater(GeneratorTsWithPath("src/app/components/framework/gen-obj/gen-obj.component.ts"))
            runLater(GeneratorTsWithPath("src/app/components/framework/gen-obj/gen-primitive-s.scss"))
            runLater(GeneratorTsWithPath("src/app/components/framework/gen-obj/gen-primitive-s.ts"))
            runLater(GeneratorTsWithPath("src/app/components/framework/gen-obj/gen-primitive.scss"))
            runLater(GeneratorTsWithPath("src/app/components/framework/gen-obj/gen-primitive.ts"))
            runLater(GeneratorTsWithPath("src/app/components/framework/gen-obj-list/gen-obj-list.component.ts"))
            runLater(GeneratorTsWithPath("src/app/components/framework/gen-obj-list/gen-simple-obj-list.component.ts"))
            runLater(GeneratorTsWithPath("src/app/components/framework/gen-obj-list/gen-domainr-list.component.ts"))
            runLater(GeneratorTsWithPath("src/app/components/framework/gen-obj-list/gen-category-obj-list.component.ts"))
            runLater(GeneratorTsWithPath("src/app/components/framework/gen-secure/gen-secure.component.ts"))
            runLater(GeneratorTsWithPath("src/app/components/framework/gen-secure/gen-secure.component.html"))
            runLater(GeneratorTsWithPath("src/app/components/framework/editor/monaco-editor.component.ts"))
            runLater(GeneratorTsWithPath("src/app/components/framework/editor/monaco-editor.component.html"))

            runLater(GeneratorTsWithPath("src/app/components/core/sidenav/sidenav.component.ts"))
            runLater(GeneratorTsWithPath("src/app/components/core/sidenav/sidenav.component.scss"))
            runLater(GeneratorTsWithPath("src/app/components/core/sidenav/sidenav.component.html"))
            runLater(GeneratorTsWithPath("src/app/components/core/sidenav/sidenav-area.component.ts"))
            runLater(GeneratorTsWithPath("src/app/components/core/sidenav/sidenav-area.component.scss"))
            runLater(GeneratorTsWithPath("src/app/components/core/sidenav/sidenav-area.component.html"))

            runLater(GeneratorTsAbstractHttpService())

            runLater(GeneratorTsWithPath("src/app/domain/blob.ts"))
            runLater(GeneratorTsWithPath("src/app/domain/primitive.ts"))

            runLater(GeneratorTsWithPath("src/app/guard/authentication.guard.ts"))
            runLater(GeneratorTsWithPath("src/app/guard/save.guard.ts"))

            runLater(GeneratorTsWithPath("src/assets/ui/sidenav.json", GeneratorType.ONCE))
        }
        if (SettingsStore.options.given(Option.DEFAULTS)) {
            runLast(GeneratorTsWithPath("src/app/components/service/db/ModelService.getModelForClass.String.component.ts"),
                    GeneratorTsWithPath("src/app/components/service/db/ModelService.getModelForClass.String.component.html"))
        }

        ClassStore.services.filter { it.export }.forEach { service ->
            service.functions.forEach { function ->
                runLater(GeneratorServiceController(function))
                runLater(GeneratorHtmlServiceCall(function))
            }
        }

        ClassStore.domain.domainClasses.forEach { clazz ->
            runLater(GeneratorHtmlDomainList(clazz))
            runLater(GeneratorDomainListController(clazz))
        }


        // Primitive types
        ClassStore.primitives.forEach {
            val outer = "primitives.${it.id}"
            JsonStore.add("$outer.name", it.name)
            it.units.forEach {
                JsonStore.add("$outer.units.${it.id}", it.name)
            }
        }

    }
}

abstract class ExternalTsGenerator : AbstractGenerator<Nothing>(), SimpleExternalGenerator {
    override val lang = Language.TYPESCRIPT
    override val replacer: List<String> = ArrayList()
    override val mode: GeneratorType = GeneratorType.PROCESS
}
