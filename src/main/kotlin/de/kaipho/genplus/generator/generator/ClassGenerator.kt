package de.kaipho.genplus.generator.generator

import de.kaipho.genplus.generator.generator.core.Wrapper
import de.kaipho.genplus.generator.generator.AbstractGenerator

/**
 * Created by tom on 14.07.16.
 */
abstract class ClassGenerator<E> : AbstractGenerator<E> {
    var domain: E

    constructor(domain: E) : super() {
        this.domain = domain
        this.meta = Wrapper(domain)
    }
}