package de.kaipho.genplus.generator.generator.java.domain.methods

import de.kaipho.genplus.generator.common.upperFirstLetter
import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.generator.java.core.ExternalJavaGenerator
import de.kaipho.genplus.generator.template.replacer.*
import de.kaipho.genplus.generator.template.replacer.*
import java.util.*

class MethodAddSingleToGenerator(method: Method) : MethodWithSuffixGenerator(method) {
    override fun visit(replacer: TemplateReplacerName) = replacer.insert("addSingleTo")
}

class MethodGetterGenerator(method: Method) : MethodWithSuffixGenerator(method) {
    override fun visit(replacer: TemplateReplacerName) = replacer.insert("get")
}

class MethodSetterGenerator(method: Method) : MethodWithSuffixGenerator(method) {
    override fun visit(replacer: TemplateReplacerName) = replacer.insert("set")
}

open class Method(
        val type: MethodType = MethodType.NORMAL,
        val name: String,
        val returnType: String = "void",
        val params: List<Pair<String, String>> = ArrayList(),
        val body: List<String> = ArrayList()
)

class MethodForI(name: String,
                 returnType: String = "void",
                 params: List<Pair<String, String>> = ArrayList()
) : Method(MethodType.INTERFACE, name, returnType, params)

class MethodForO(name: String,
                 returnType: String = "void",
                 params: List<Pair<String, String>> = ArrayList(),
                 body: List<String> = ArrayList()
) : Method(MethodType.OVERRIDE, name, returnType, params, body)

enum class MethodType {
    INTERFACE, ABSTRACT, NORMAL, OVERRIDE
}

abstract class MethodGenerator(val method: Method) : ExternalJavaGenerator() {
    override val lang = Language.JAVA
    override val templateString = "/methods/method.java"

    override fun visit(replacer: TemplateReplacerGeneric) {
        if (method.type == MethodType.OVERRIDE)
            replacer.insert("@Override public ")
        else if (method.type != MethodType.INTERFACE)
            replacer.insert("public ")
        else
            replacer.insert("")
    }

    abstract override fun visit(replacer: TemplateReplacerUpperName)

    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(method.returnType)
    }

    abstract override fun visit(replacer: TemplateReplacerName)

    override fun visit(replacer: TemplateReplacerBody) {
        when (replacer.id) {
            1 -> {
                if (method.type != MethodType.NORMAL && method.type != MethodType.OVERRIDE)
                    replacer.insert(";")
                else
                    replacer.insert(" {")
            }
            2 -> {
                if (method.type == MethodType.NORMAL || method.type == MethodType.OVERRIDE) {
                    method.body.forEach {
                        replacer.insert("\t$it")
                    }
                    replacer.insert("}")
                } else replacer.insert("")
            }
        }
    }

    override fun visit(replacer: TemplateReplacerParams) {
        replacer.insert(method.params.map { "${it.first} ${it.second}" }.joinToString())
    }
}

abstract class MethodWithSuffixGenerator(method: Method) : MethodGenerator(method) {
    override fun visit(replacer: TemplateReplacerUpperName) {
        replacer.insert(method.name.upperFirstLetter())
    }
}