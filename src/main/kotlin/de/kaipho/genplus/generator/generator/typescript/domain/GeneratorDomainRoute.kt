package de.kaipho.genplus.generator.generator.typescript.domain

import de.kaipho.genplus.generator.common.lowerFirstLetter
import de.kaipho.genplus.generator.common.upperFirstLetter
import de.kaipho.genplus.generator.core.obj.AssoziationTypeCollection
import de.kaipho.genplus.generator.core.obj.AssoziationTypeUser
import de.kaipho.genplus.generator.domain.obj.AbstractClass
import de.kaipho.genplus.generator.domain.obj.AbstractDomainClass
import de.kaipho.genplus.generator.domain.obj.DomainClass
import de.kaipho.genplus.generator.generator.AbstractGenerator
import de.kaipho.genplus.generator.generator.recursiveAssociationFetcher
import de.kaipho.genplus.generator.template.replacer.*
import java.util.*

class GeneratorDomainRoute(val clazz: DomainClass, val path: String) : AbstractGenerator<DomainClass>() {
    override fun visit(replacer: TemplateReplacerPath) {
        replacer.insert(clazz.id.toString())
    }

    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(clazz.name)
    }

    override fun visit(replacer: TemplateReplacerLine) {
        if (clazz is AbstractDomainClass) {
            val result = HashSet<String>()
            clazz.childClasses.forEach {
                result.addAll(routesForClass(it as DomainClass))
            }
            replacer.insertAll(result.toList())
        } else {
            replacer.insertAll(routesForClass(clazz).toList())
        }
    }

    override fun visit(replacer: TemplateReplacerLowerClassName) {
        replacer.insert(clazz.name.lowerFirstLetter())
    }

    fun routesForClass(clazz: DomainClass): Set<String> {
        val result = HashSet<String>()
        result.add("{path: '', component: ${clazz.name.upperFirstLetter()}Component, canDeactivate: [SaveGuard]},")
        recursiveAssociationFetcher(clazz).forEach {
            if (it.type is AssoziationTypeUser && it.type.clazz !is AbstractClass)
                result.add("{path: '${it.id}', component: ${it.type.name}Component, canDeactivate: [SaveGuard]},")
            if (it.type is AssoziationTypeCollection && it.type.inner is AssoziationTypeUser && it.type.inner.clazz !is AbstractClass)
                result.add("{path: '${it.id}', component: ${it.type.inner.name}Component, canDeactivate: [SaveGuard]},")
        }
        return result
    }

    override fun visit(replacer: TemplateReplacerName) {
        replacer.insert(clazz.name.lowerFirstLetter())
    }
}