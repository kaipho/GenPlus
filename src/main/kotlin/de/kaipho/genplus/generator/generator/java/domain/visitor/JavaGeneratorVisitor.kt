package de.kaipho.genplus.generator.generator.java.domain.visitor

import de.kaipho.genplus.generator.common.lowerFirstLetter
import de.kaipho.genplus.generator.domain.obj.AbstractDomainClass
import de.kaipho.genplus.generator.generator.AbstractGenerator
import de.kaipho.genplus.generator.store.SettingsStore
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerClassName
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerPackage
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerSubpackage

abstract class JavaGeneratorVisitor : AbstractGenerator<AbstractDomainClass>() {
    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(meta.data.name)
    }

    override fun visit(replacer: TemplateReplacerPackage) {
        lines.add(replacer.replaceOneLine(actual, "${SettingsStore.getPrefixPackage()}"))
    }

    override fun visit(replacer: TemplateReplacerSubpackage) {
        replacer.insert(meta.data.prefix)
    }

    fun visitorMethod(returnType: String, name: String) = "$returnType visit($name ${name.lowerFirstLetter()});"
}
