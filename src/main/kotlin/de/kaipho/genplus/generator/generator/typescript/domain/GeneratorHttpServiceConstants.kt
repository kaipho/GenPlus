package de.kaipho.genplus.generator.generator.typescript.domain

import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.domain.obj.Domain
import de.kaipho.genplus.generator.generator.AbstractGenerator
import de.kaipho.genplus.generator.generator.core.File
import de.kaipho.genplus.generator.generator.core.Generator
import de.kaipho.genplus.generator.generator.typescript.core.Imports
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.template.Templates
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerImports
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLine

@Generator(
        lang = Language.TYPESCRIPT,
        template = Templates.TS_HTTP_SERVICE_CONSTANTS,
        appliedOn = Domain::class,
        file = File.TS_HTTP_SERVICE_CONSTANTS
)
class GeneratorHttpServiceConstants : AbstractGenerator<Domain>() {
    override fun visit(replacer: TemplateReplacerImports) {
        val imports = Imports("/service/domain/http")
        getData().getInternalDomainClasses().forEach {
            imports.addHttpImport(it)
        }
        ClassStore.services.filter { it.export }.forEach { service ->
            imports.addFunctionsHttpImport(service)
        }
        replacer.insertAll(imports.getImports())
        replacer.insertAll(imports.getExports())
    }

    override fun visit(replacer: TemplateReplacerLine) {
        getData().getInternalDomainClasses().forEach {
            replacer.insert("${it.name}HttpService,")
        }
        ClassStore.services.filter { it.export }.forEach { service ->
            replacer.insert("${service.name}HttpService,")
        }
    }
}