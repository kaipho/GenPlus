package de.kaipho.genplus.generator.generator

import de.kaipho.genplus.generator.generator.core.Wrapper
import de.kaipho.genplus.generator.generator.html.core.Tag
import de.kaipho.genplus.generator.store.SettingsStore
import de.kaipho.genplus.generator.template.TemplateReplacer
import de.kaipho.genplus.generator.template.replacer.*
import java.util.*

/**
 * Created by tom on 14.07.16.
 */
abstract class AbstractGenerator<E> : TemplateReplacerVisitor, SpecialGenerator {
    lateinit var meta: Wrapper<E>

    fun getData(): E {
        return meta.data
    }

    fun self(): E {
        return meta.data
    }

    val lines: MutableList<String> = ArrayList()

    var actual = ""

    override fun generate(template: MutableList<Pair<String, MutableList<TemplateReplacer<*>>>>): MutableList<String> {
        template.forEach {
            if (it.second.isEmpty()) {
                lines.add(it.first)
            } else if (it.second.size == 1) {
                actual = it.first
                it.second[0].accept(this)
            } else {
                actual = it.first
                it.second.forEach { inner ->
                    inner.accept(this)
                    if (lines.isNotEmpty()) {
                        actual = lines.last()
                        lines.removeAt(lines.size - 1)
                    }
                }
                lines.add(actual)
            }
        }
        this.standardAction()
        return lines
    }

    open fun standardAction() {

    }

    override fun visit(replacer: TemplateReplacerImplements) {
        lines.add(replacer.replaceOneLine(actual, ""))
    }

    override fun visit(replacer: TemplateReplacerPackage) {
        lines.add(replacer.replaceOneLine(actual, SettingsStore.getPrefixPackage()))
    }

    override fun visit(replacer: TemplateReplacerPath) {
        throw UnsupportedOperationException("not implemented")
    }

    override fun visit(replacer: TemplateReplacerGenerated) {
        throw UnsupportedOperationException("not implemented")
    }

    override fun visit(replacer: TemplateReplacerGetter) {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun visit(replacer: TemplateReplacerSetter) {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun visit(replacer: TemplateReplacerEqualsHashCode) {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun visit(replacer: TemplateReplacerUpperName) {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun visit(replacer: TemplateReplacerEqualsLine) {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun visit(replacer: TemplateReplacerHashCodeLine) {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun visit(replacer: TemplateReplacerAssociations) {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun visit(replacer: TemplateReplacerClassName) {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun visit(replacer: TemplateReplacerComment) {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun visit(replacer: TemplateReplacerAnnotations) {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun visit(replacer: TemplateReplacerImports) {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun visit(replacer: TemplateReplacerSubpackage) {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun visit(replacer: TemplateReplacerValidators) {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun visit(replacer: TemplateReplacerName) {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun visit(replacer: TemplateReplacerTable) {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun visit(replacer: TemplateReplacerLowerClassName) {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun visit(replacer: TemplateReplacerDomainBuilderMethod) {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun visit(replacer: TemplateReplacerDomainBuilder) {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun visit(replacer: TemplateReplacerLine) {
        throw UnsupportedOperationException(
                "not implemented " + javaClass.simpleName) //To change body of created functions use File | Settings | File Templates.
    }

    override fun visit(replacer: TemplateReplacerI18n) {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun visit(replacer: TemplateReplacerGeneric) {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun visit(replacer: TemplateReplacerId) {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun visit(replacer: TemplateReplacerAccept) {
        throw UnsupportedOperationException("not implemented")
    }

    override fun visit(replacer: TemplateReplacerBody) {
        throw UnsupportedOperationException("not implemented")
    }

    override fun visit(replacer: TemplateReplacerParams) {
        throw UnsupportedOperationException("not implemented")
    }

    override fun visit(replacer: TemplateReplacerUserattribute) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun TemplateReplacer<String>.insert(line: String) {
        this.insert(actual, lines, line)
    }

    fun TemplateReplacer<String>.insertNothing() {
        this.insert(actual, lines, "")
    }


    fun TemplateReplacer<String>.insert(line: String, intend: Int) {
        if (intend != 0)
            this.insert("\t$line", intend - 1)
        else
            this.insert(actual, lines, line)

    }

    fun TemplateReplacer<String>.insertIf(condition: Boolean, vararg line: String) {
        if (condition) line.forEach { this.insert(actual, lines, it) }
    }

    fun TemplateReplacer<String>.insertAll(line: List<String>) {
        line.forEach {
            this.insert(actual, lines, it)
        }
    }

    fun TemplateReplacer<String>.insert(line: Tag) {
        this.insertAll(line.out())
    }

    fun String.insertInto(replacer: TemplateReplacer<String>) {
        this.trimMargin().split("\n").apply {
            replacer.insertAll(this)
        }
    }
}

typealias ListReplacer = (List<String>) -> Unit

fun String.insertInto(replacer: ListReplacer) {
    this.trimMargin().split("\n").apply {
        replacer.invoke(this)
    }
}

fun String.insertInto(replacer: MutableList<String>) {
    this.trimMargin().split("\n").apply {
        replacer.addAll(this)
    }
}
