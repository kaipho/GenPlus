package de.kaipho.genplus.generator.generator.typescript.domain

import de.kaipho.genplus.generator.common.convertPackageToFileSystem
import de.kaipho.genplus.generator.common.lowerFirstLetter
import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.core.obj.getAllNonAbstractChildClasses
import de.kaipho.genplus.generator.domain.obj.AbstractDomainClass
import de.kaipho.genplus.generator.generator.AbstractGenerator
import de.kaipho.genplus.generator.generator.core.File
import de.kaipho.genplus.generator.generator.core.Generator
import de.kaipho.genplus.generator.generator.typescript.RelevantPathes
import de.kaipho.genplus.generator.generator.typescript.core.ImportHelp
import de.kaipho.genplus.generator.template.Templates
import de.kaipho.genplus.generator.template.replacer.*
import de.kaipho.genplus.generator.template.replacer.*

@Generator(
        lang = Language.TYPESCRIPT,
        template = Templates.TS_DOMAIN_ABSTRACT_HTTP_SERVICE,
        appliedOn = AbstractDomainClass::class,
        file = File.TS_DOMAIN_HTTP_SERVICE
)
class GeneratorDomainAbstractService : AbstractGenerator<AbstractDomainClass>() {
    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(meta.data.name)
    }

    override fun visit(replacer: TemplateReplacerPath) {
        replacer.insert(self().id.toString())
    }

    override fun visit(replacer: TemplateReplacerImports) {
        replacer.insert(ImportHelp.createImport(meta.data.name,
                                                ImportHelp.createRelativePath("service/domain/http",
                                                                              "domain/${meta.data.prefix.convertPackageToFileSystem()}"),
                                                meta.data.getLowerName()))
        replacer.insert(ImportHelp.createImport("HttpService",
                                                ImportHelp.createRelativePath("service/domain/http", RelevantPathes.HTTP_SERVICE.path),
                                                RelevantPathes.HTTP_SERVICE.file))

        getData().getAllNonAbstractChildClasses().forEach { clazz ->
            replacer.insert(ImportHelp.createImport(clazz.name,
                                                    ImportHelp.createRelativePath("service/domain/http",
                                                                                  "domain/${clazz.prefix.convertPackageToFileSystem()}"),
                                                    clazz.name.lowerFirstLetter()))
        }
    }

    override fun visit(replacer: TemplateReplacerLowerClassName) {
        replacer.insert(meta.data.name.lowerFirstLetter())
    }

    override fun visit(replacer: TemplateReplacerLine) {
        getData().getAllNonAbstractChildClasses().forEach {
            replacer.insert("case '${it.name.lowerFirstLetter()}':")
            replacer.insert("   return new ${it.name}();")
        }
    }
}