package de.kaipho.genplus.generator.generator.typescript

/**
 * Pfade zu Klassen, die vom Framework gegeben sind.
 * Alle Pfade relativ zu app/
 */
object RelevantPathes {
    val HTTP_SERVICE = PathWithFileName("service/core", "http.service")
    val CONSTANTS = PathWithFileName("components", "constants")
    val TITLE_COMPONENT = PathWithFileName("components/allgemein", "title.component")
    val TITLE_SERVICE = PathWithFileName("service/support", "title.service")
    val SELECTION_SERVICE = PathWithFileName("service/domain", "selection.service")
    val LOGIN_SERVICE = PathWithFileName("service/core", "login.service")
    val PAGEABLE = PathWithFileName("service/domain/http", "pageable")
    val DOMAIN_OBJECT = PathWithFileName("domain", "domain.object")
    val DOMAIN_OBJECT_VISITOR = PathWithFileName("domain", "domain.object.visitor")
    val DOMAIN_REP_VISITOR = PathWithFileName("domain", "rep.visitor")
    val DOMAIN_TYPE_VISITOR = PathWithFileName("domain", "type.visitor")
}

enum class TsPath(val clazz: String, val path: String, val file: String) {
    DOMAIN_OBJECT("DomainObject", "domain", "domain.object"),
    DOMAIN_OBJECT_VISITOR("DomainObjectVisitor", "domain", "domain.object.visitor"),
    DOMAIN_REP_VISITOR("RepVisitor", "domain", "rep.visitor"),
    DOMAIN_TYPE_VISITOR("TypeVisitor", "domain", "type.visitor"),
    HTTP_SERVICE("HttpService", "service/core", "http.service"),

    MENU_SERVICE("MenuService", "components/framework/gen-service", "menu.service"),
    MENU_DIALOG_COMPONENT("GenDialogComponent", "components/framework/gen-dialog", "gen-dialog.component"),
    MENU_SERVICE_OPERATIONS("ServiceOperations", "components/material/menu", "menu.component"),

    TITLE_COMPONENT("TitleComponent", "components/framework/title", "title.component"),
    LIST_COMPONENT("ListComponent", "components/framework/title", "list.component"),
    TITLE_SERVICE("TitleService", "service/core", "title.service"),
    NAVIGATION_SERVICE("NavigationService", "service/support", "navigation.service"),

    CONSTANT_TOP_INPUT("TOP_INPUT", "components", "constants"),
}

data class PathWithFileName(
        val path: String,
        val file: String
) {}