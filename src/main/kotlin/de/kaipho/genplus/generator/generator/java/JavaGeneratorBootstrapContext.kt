package de.kaipho.genplus.generator.generator.java

import de.kaipho.genplus.generator.core.obj.options.Option
import de.kaipho.genplus.generator.generator.java.core.ExternalJavaGenerator
import de.kaipho.genplus.generator.store.SettingsStore
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLine
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerPackage

class JavaGeneratorBootstrapContext : ExternalJavaGenerator() {
    override val templateString = "BootstrapContext.java"

    override fun visit(replacer: TemplateReplacerPackage) {
        lines.add(replacer.replaceOneLine(actual, SettingsStore.getPrefixPackage().removeSuffix(".")))
    }

    override fun visit(replacer: TemplateReplacerLine) {
        when(replacer.id) {
            0 -> {
                SettingsStore.options.get(Option.USER_LANGS).forEach {
                    replacer.insert("Language.${it.toUpperCase()} = initLang(\"$it\");")
                }
            }
            1 -> {
                replacer.insert("Role.ADMIN = initRole(\"ADMIN\");")
                replacer.insert("Role.EDIT = initRole(\"EDIT\");")
                SettingsStore.options.get(Option.SECURITY_ROLES).forEach {
                    replacer.insert("Role.${it.toUpperCase()} = initRole(\"$it\");")
                }
            }
        }
    }
}
