package de.kaipho.genplus.generator.generator.html.service

import de.kaipho.genplus.generator.common.convertPackageToFileSystem
import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.core.obj.service.Function
import de.kaipho.genplus.generator.core.obj.service.Parameter
import de.kaipho.genplus.generator.generator.AbstractGenerator
import de.kaipho.genplus.generator.generator.core.File
import de.kaipho.genplus.generator.generator.core.Generator
import de.kaipho.genplus.generator.generator.html.core.FormGenerator
import de.kaipho.genplus.generator.generator.i18n.JsonStore
import de.kaipho.genplus.generator.generator.typescript.ExternalTsGenerator
import de.kaipho.genplus.generator.template.Templates
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerI18n
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLine

class GeneratorHtmlServiceCall (val function: Function) : ExternalTsGenerator() {
    override val templateString = "src/app/components/service/xxx/xxx.component.html"
    override val replacer = arrayListOf(
            function.owner.prefix.convertPackageToFileSystem().removeSuffix("/"),
            function.getQualifiedName()
    )

    override fun standardAction() {
        JsonStore.add("button.ok")
        JsonStore.add("button.cancel")
        JsonStore.add("button.done")
    }

    override fun visit(replacer: TemplateReplacerLine) {
        when (replacer.id) {
            0 -> replaceRequest(replacer)
            1 -> replaceResponse(replacer)
        }
    }

    fun replaceRequest(replacer: TemplateReplacerLine) {
        val iterator = function.params.iterator()
        // TODO Listen
        var actual: Parameter
        while (iterator.hasNext()) {
            actual = iterator.next()
            replacer.insert("<div>")
            replacer.insert(FormGenerator.getRequestFormForServiceParam(actual, function))
            if (iterator.hasNext()) {
                actual = iterator.next()
                replacer.insert(FormGenerator.getRequestFormForServiceParam(actual, function))
            }
            replacer.insert("</div>")
        }
    }

    fun replaceResponse(replacer: TemplateReplacerLine) {
        replacer.insert("<div>")
        replacer.insert(FormGenerator.getResponseFormForServiceParam(function))
        replacer.insert("</div>")
    }

    override fun visit(replacer: TemplateReplacerI18n) {
        JsonStore.add("functions${function.getFilePrefix().replace("/", ".")}.${function.name}.title", function.name)
        replacer.insert("functions${function.getFilePrefix().replace("/", ".")}.${function.name}.title")
    }
}