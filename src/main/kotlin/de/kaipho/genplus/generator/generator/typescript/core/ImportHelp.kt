package de.kaipho.genplus.generator.generator.typescript.core

import java.util.*

/**
 * Erzeugt einen relativen Pfad zwischen zwei absoluten Pfaden.
 */
object ImportHelp {

    fun createRelativePath(from: String, to: String): String {
        var result = ""
        removeSameStart(from, to).apply {
            // 1. Pfad liegt in 2.
            if (this.first.isEmpty()) {
                result = "./${listToPath(this.second)}"
            }
            // 1. Pfad muss hochgegangen werden
            else {
                this.first.forEach {
                    result += "../"
                }
                result += listToPath(this.second)
            }
        }
        return result
    }

    fun createImport(clazz: String, path: String, file: String): String {
        return "import { $clazz } from '$path$file';"
    }

    private fun removeSameStart(from: String, to: String): Pair<MutableList<String>, MutableList<String>> {
        val fromList = pathToList(from)
        val toList = pathToList(to)
        while (fromList.isNotEmpty() && toList.isNotEmpty() && fromList[0] == toList[0]) {
            fromList.removeAt(0)
            toList.removeAt(0)
        }
        return Pair(fromList, toList)
    }

    private fun listToPath(list: List<String>): String {
        var result = ""
        list.forEach {
            result += "$it/"
        }
        return result
    }

    private fun pathToList(path: String): MutableList<String> {
        val result = ArrayList(path.split("/"))
        result.removeIf {
            it == ""
        }
        return result
    }
}