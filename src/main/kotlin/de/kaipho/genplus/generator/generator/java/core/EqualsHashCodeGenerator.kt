package de.kaipho.genplus.generator.generator.java.core

import de.kaipho.genplus.generator.common.lowerFirstLetter
import de.kaipho.genplus.generator.common.upperFirstLetter
import de.kaipho.genplus.generator.domain.obj.DomainClass
import de.kaipho.genplus.generator.generator.ClassGenerator
import de.kaipho.genplus.generator.generator.recursiveAssociationFetcher
import de.kaipho.genplus.generator.template.TemplateParser
import de.kaipho.genplus.generator.template.Templates
import de.kaipho.genplus.generator.template.replacer.*
import de.kaipho.genplus.generator.template.replacer.*

/**
 * Created by tom on 17.07.16.
 */
class EqualsHashCodeGenerator(domain: DomainClass, var actualAssociation:String = "") : ClassGenerator<DomainClass>(domain) {
    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(actual, lines, domain.name)
    }

    override fun visit(replacer: TemplateReplacerLowerClassName) {
        replacer.insert(actual, lines, domain.name.lowerFirstLetter())
    }

    override fun visit(replacer: TemplateReplacerUpperName) {
        replacer.insert(actual, lines, actualAssociation.upperFirstLetter())
    }

    override fun visit(replacer: TemplateReplacerEqualsLine) {
        recursiveAssociationFetcher(meta.data).forEach {
            replacer.insert(actual, lines, TemplateParser.parseTemplate(Templates.EQUALS_LINE, EqualsHashCodeGenerator(domain, it.name)))
        }
    }

    override fun visit(replacer: TemplateReplacerHashCodeLine) {
        recursiveAssociationFetcher(meta.data).forEach {
            replacer.insert(actual, lines, TemplateParser.parseTemplate(Templates.HASHCODE_LINE, EqualsHashCodeGenerator(domain, it.name)))
        }
    }
}
