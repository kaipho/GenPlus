package de.kaipho.genplus.generator.generator.java.domain

import de.kaipho.genplus.generator.common.forEachAssociationCollection
import de.kaipho.genplus.generator.common.forEachAssociationWithUserType
import de.kaipho.genplus.generator.common.upperFirstLetter
import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.constants.STRINGS
import de.kaipho.genplus.generator.core.obj.*
import de.kaipho.genplus.generator.core.scanner.KeywordHolder.RULE_USER_ATTRIBUTE
import de.kaipho.genplus.generator.domain.obj.*
import de.kaipho.genplus.generator.generator.core.File
import de.kaipho.genplus.generator.generator.core.Generator
import de.kaipho.genplus.generator.generator.core.Ref
import de.kaipho.genplus.generator.generator.core.RuleHelp
import de.kaipho.genplus.generator.generator.getInlinedAssociations
import de.kaipho.genplus.generator.generator.java.JavaGenerator
import de.kaipho.genplus.generator.generator.java.core.*
import de.kaipho.genplus.generator.generator.java.domain.methods.MethodAddSingleToGenerator
import de.kaipho.genplus.generator.generator.java.domain.methods.MethodForO
import de.kaipho.genplus.generator.generator.java.domain.methods.MethodGetterGenerator
import de.kaipho.genplus.generator.generator.java.domain.methods.MethodSetterGenerator
import de.kaipho.genplus.generator.generator.java.persistence.ClassIdGenerator
import de.kaipho.genplus.generator.generator.java.service.Imports
import de.kaipho.genplus.generator.generator.recursiveAssociationFetcher
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.template.TemplateParser
import de.kaipho.genplus.generator.template.Templates
import de.kaipho.genplus.generator.template.replacer.*
import java.util.*

@Generator(
        lang = Language.JAVA,
        template = Templates.DOMAIN_CLASS_REPOSITORY_PROXY,
        appliedOn = ExplicitDomainClass::class,
        file = File.JAVA_DOMAIN_CLASS_REPOSITORY_PROXY,
        dependsOn = Ref.DATABASE_ENABLED
)
class DomainClassRepositoryProxyGenerator : JavaGenerator<ExplicitDomainClass>() {
    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(meta.data.name)
    }

    override fun visit(replacer: TemplateReplacerUserattribute) {
        if (RuleHelp.hasRule(self(), RULE_USER_ATTRIBUTE) != null) {
            replacer.insert(", true")
        } else
            replacer.insert("")
    }

    override fun visit(replacer: TemplateReplacerSubpackage) {
        replacer.insert(meta.data.prefix)
    }

    override fun visit(replacer: TemplateReplacerComment) {
        replacer.insert(meta.data.comment)
    }

    override fun visit(replacer: TemplateReplacerImports) {
        val imports = Imports()
        recursiveAssociationFetcher(meta.data, true).union(getInlinedAssociations(meta.data)).forEach(imports::addAssociationImport)
        recursiveAssociationFetcher(getData(), false).forEach {
            if (it.type is AssoziationTypeUser && it.type.clazz !is AbstractDomainClass)
                imports.addDomainImport(it.type.clazz)
            if (it.type is AssoziationTypeCollection && it.type.inner is AssoziationTypeUser && it.type.inner.clazz !is AbstractDomainClass) {
                imports.addDomainImport(it.type.inner.clazz)
            }
        }

        val visitable = ArrayList<IClass>()
        ClassStore.domain.domainClasses.filter { it is AbstractDomainClass }.forEach { clazz ->
            fetchAllAssociationsAndChildClasses(clazz).filter { it.name == getData().name }.forEach {
                addClassToList(visitable, clazz)
            }
        }
        getAllParents(self().extends).forEach {
            addClassToList(visitable, it)
        }

        visitable.forEach {
            imports.addDomainVisitorImport(it)
        }

        fetchAllParents(self()).forEach {
            imports.addDomainCompleteVisitorImport(it)
        }
        recursiveAssociationFetcher(meta.data).filter { it.isDerived() }.forEach {
            imports.addImport("domain.core.DerivedManager")
        }
        replacer.insertAll(imports.getimports())
    }

    override fun visit(replacer: TemplateReplacerGetter) {
        recursiveAssociationFetcher(meta.data).forEach { getterAssociationRepoProxy(it) { replacer.insertAll(it) } }
    }

    override fun visit(replacer: TemplateReplacerSetter) {
        recursiveAssociationFetcher(meta.data).forEach { setterAssociationRepoProxy(it) { replacer.insertAll(it) } }
    }

    override fun visit(replacer: TemplateReplacerLine) {
        when (replacer.id) {
            0 -> replacer.insert(self().id.toString())
            1 -> generateMapping(replacer)
            2 -> generateConstructAll(replacer)
            3 -> {
                recursiveAssociationFetcher(self(), false).forEachAssociationCollection { assoziation, _ ->
                    val name = assoziation.name.upperFirstLetter()
                    replacer.insert("real.set$name(new StoredList<>(${ClassIdGenerator.getAssociationValName(assoziation)}, this, real.getId(), real.get$name()));")
                }
            }
            4 -> {
                recursiveAssociationFetcher(self(), false).forEach { actual ->
                    if (actual.type is AssociationTypeBlob) {
                        val name = actual.name.upperFirstLetter()
                        val associationKey = ClassIdGenerator.getAssociationValName(actual)
                        replacer.insert("real.set$name(new Blob(initLink(id, $associationKey)));")
                    }
                }
            }
        }
    }

    private fun generateConstructAll(replacer: TemplateReplacerLine) {
        recursiveAssociationFetcher(self(), false).forEach { association ->
            val upperName = association.name.upperFirstLetter()

            if (association.type is AssoziationTypeUser) {
                val clazz = association.type.clazz
                clazz.acceptClass(object : TypeVisitor<Unit> {
                    override fun visitPrimitive(clazz: PrimitiveClass) {
                        // no initialisation
                    }

                    override fun visitCategory(clazz: CategoryClass) {
                        // no initialisation
                    }

                    override fun visitDomain(clazz: DomainClass) {
                        if (clazz !is AbstractDomainClass) {
                            replacer.insert("if(real.get$upperName().getId() == null) {")
                            replacer.insert("   this.set$upperName(${clazz.name}.builder().build());")
                            replacer.insert("}")
                        }
                    }

                    override fun visitConstant(clazz: Constant) {
                        // no initialisation
                    }

                    override fun visitOther(clazz: IClass) {
                        // no initialisation
                    }

                    override fun visitDummy(clazz: IClass) {
                        // no initialisation
                    }

                })
            }
            if (association.type is AssoziationTypeOptional) {
                """
                |if(real.get$upperName() == null) {
                |   this.set$upperName(Optional.empty());
                |}
                """.insertInto(replacer)
            }
        }
    }

    private fun generateMapping(replacer: TemplateReplacerLine) {
        recursiveAssociationFetcher(self(), false).forEach { actual ->
            val name = actual.name.upperFirstLetter()
            val associationKey = ClassIdGenerator.getAssociationValName(actual)

            actual.type.accept(object : AssociationVisitor<Unit> {
                override fun visit(associationTypeBlob: AssociationTypeBlob) {
                    replacer.insert("real.set$name(cache.getBlob(links, $associationKey));")
                }

                override fun visit(association: AssoziationTypeSecure) {
                    replacer.insert("real.set$name(cache.getString(links, $associationKey));")
                }

                override fun visit(association: AssoziationTypePassword) {
                    replacer.insert("real.set$name(cache.getString(links, $associationKey));")
                }

                override fun visit(association: AssoziationTypeString) {
                    replacer.insert("real.set$name(cache.getString(links, $associationKey));")
                }

                override fun visit(association: AssoziationTypeInteger) {
                    replacer.insert("real.set$name(cache.getInteger(links, $associationKey));")
                }

                override fun visit(association: AssoziationTypeDate) {
                    replacer.insert("real.set$name(cache.getLocalDate(links, $associationKey));")
                }

                override fun visit(association: AssoziationTypeBoolean) {
                    replacer.insert("real.set$name(cache.getBoolean(links, $associationKey));")
                }

                override fun visit(association: AssoziationTypeUser) {
                    association.acceptClass(object : UserClassVisitor<Unit> {
                        override fun visitPrimitive(association: AssoziationTypeUser, clazz: PrimitiveClass) {
                            // Nothing...
                        }

                        override fun visitCategory(association: AssoziationTypeUser, clazz: CategoryClass) {
                            replacer.insert("real.set$name(${clazz.name}.from(cache.getString(links, $associationKey)));")
                        }

                        override fun visitDomain(association: AssoziationTypeUser, clazz: DomainClass) {
                            // Nothing...
                        }

                        override fun visitConstant(association: AssoziationTypeUser, clazz: Constant) {
                            // Nothing...
                        }

                    })
                }

                override fun visit(association: AssoziationTypeMap) {
                    // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun visit(association: AssoziationTypeCollection) {
                    val map = when (association.inner) {
                        AssoziationTypeInteger -> "Integer"
                        AssoziationTypeDate -> "LocalDate"
                        AssoziationTypeString, AssoziationTypePassword, AssoziationTypeText -> "String"
                        is AssoziationTypeUser -> association.inner.acceptClass(object : UserClassVisitor<String> {
                            override fun visitPrimitive(association: AssoziationTypeUser, clazz: PrimitiveClass) = throw RuntimeException()

                            override fun visitCategory(association: AssoziationTypeUser, clazz: CategoryClass) = "String"

                            override fun visitDomain(association: AssoziationTypeUser, clazz: DomainClass) = ""

                            override fun visitConstant(association: AssoziationTypeUser, clazz: Constant) = throw RuntimeException()
                        })
                        else -> throw RuntimeException()
                    }
                    if (association.inner is AssoziationTypeUser) {
                        return association.inner.acceptClass(object : UserClassVisitor<Unit> {
                            override fun visitPrimitive(association: AssoziationTypeUser, clazz: PrimitiveClass) {
                                // Nothing...
                            }

                            override fun visitCategory(association: AssoziationTypeUser, clazz: CategoryClass) {
                                replacer.insert("links.getOrDefault($associationKey, new ArrayList<>()).forEach(link ->")
                                replacer.insert("       real.get$name().add(${clazz.name}.from(cache.get$map(link.getToObj())))")
                                replacer.insert(");")
                            }

                            override fun visitDomain(association: AssoziationTypeUser, clazz: DomainClass) {

                                replacer.insert("getObjList(real.getId(), $associationKey).forEach((key, val) ->")
                                replacer.insert("       real.get$name().add(${clazz.name}.findOneById(val))")
                                replacer.insert(");")
                            }

                            override fun visitConstant(association: AssoziationTypeUser, clazz: Constant) {
                                // Nothing...
                            }
                        })
                    }
                    replacer.insert("links.getOrDefault($associationKey, new ArrayList<>()).forEach(link ->")
                    replacer.insert("       real.get$name().add(cache.get$map(link.getToObj()))")
                    replacer.insert(");")
                }

                override fun visit(assoziation: AssoziationTypeDummy) {
                }

                override fun visit(assoziation: AssoziationTypeUnit) {
                }

                override fun visit(association: AssoziationTypeDouble) {
                    replacer.insert("real.set$name(cache.getDouble(links, $associationKey));")
                }

                override fun visit(association: AssoziationTypeBigNumber) {
                }

                override fun visit(association: AssoziationTypeText) {
                    replacer.insert("real.set$name(cache.getString(links, $associationKey));")
                }

                override fun visit(association: AssoziationTypeOptional) {
                }
            })
        }
    }

    override fun visit(replacer: TemplateReplacerAccept) {
        val visitable = ArrayList<IClass>()
        ClassStore.domain.domainClasses.filter { it is AbstractDomainClass }.forEach { clazz ->
            fetchAllAssociationsAndChildClasses(clazz).filter { it.name == getData().name }.forEach {
                addClassToList(visitable, clazz)
            }
        }
        getAllParents(self().extends).forEach {
            addClassToList(visitable, it)
        }
        visitable.forEach {
            replacer.insert("public <D> D accept(${it.name}Visitor<D> visitor) {")
            replacer.insert("   return visitor.visit(this);")
            replacer.insert("}")
            replacer.insert("public void accept(${it.name}SimpleVisitor visitor) {")
            replacer.insert("   visitor.visit(this);")
            replacer.insert("}")
        }
        getAllParents(self().extends).forEach {
            replacer.insert("public <D> D accept(${it.name}CompleteVisitor<D> visitor) {")
            replacer.insert("   return visitor.visit(this);")
            replacer.insert("}")
        }
    }

    override fun visit(replacer: TemplateReplacerId) {
        replacer.insert(ClassIdGenerator.getClassValName(self()))
    }
}
