package de.kaipho.genplus.generator.generator.java.core

import de.kaipho.genplus.generator.common.lowerFirstLetter
import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.core.obj.options.IOptions
import de.kaipho.genplus.generator.core.obj.options.Option
import de.kaipho.genplus.generator.domain.obj.ExplicitDomainClass
import de.kaipho.genplus.generator.generator.SpecialGenerator
import de.kaipho.genplus.generator.generator.core.*
import de.kaipho.genplus.generator.generator.java.JavaGenerator
import de.kaipho.genplus.generator.generator.java.service.Imports
import de.kaipho.genplus.generator.generator.typescript.ExternalTsGenerator
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.store.SettingsStore
import de.kaipho.genplus.generator.template.Template
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerClassName
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerImports
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLine
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerPath
import java.util.*

// Generators to simply copy the file

class JavaCopyFilesManager : ExternalGeneratorManager {
    val options: IOptions = SettingsStore.options

    override fun go() {

        runLater(JavaConstants(),
                 JavaPom())
    }
}

class JavaGeneratorUserRoles : ExternalJavaGenerator() {
    override val templateString = "domain/core/Roles.java"

    override fun visit(replacer: TemplateReplacerLine) {
        SettingsStore.options.get(Option.SECURITY_ROLES).forEach {
            val name = it.toUpperCase()
            replacer.insert("public static final String $name = ROLE + \"$name\";")
        }
    }
}

class JavaConstants : ExternalJavaGenerator() {
    override val lang = Language.JAVA
    override val templateString = "core/Constants.java"

    override fun visit(replacer: TemplateReplacerLine) {
        replacer.insert(SettingsStore.options.get(Option.USER_LANGS).map { it -> "\"$it\"" }.joinToString())
    }
}

class JavaPom : ExternalJavaGenerator() {
    override val lang = Language.POM
    override val templateString = "/m-pom.xml"

    override fun getResultFile(): String {
        return "pom.xml"
    }

    override fun visit(replacer: TemplateReplacerPath) {
        replacer.insert(SettingsStore.options.getFirst(Option.PREFIX))
    }

    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(SettingsStore.options.getFirst(Option.NAME))
    }

    override fun visit(replacer: TemplateReplacerLine) {
        // replacer.insertIf(SettingsStore.securityEnabled(),
        //                  "<dependency>",
        //                  "    <groupId>org.springframework.boot</groupId>",
        //                  "    <artifactId>spring-boot-starter-security</artifactId>",
        //                  "</dependency>")
    }
}

class JavaPersistentElement : ExternalJavaGenerator() {
    override val templateString = "domain/core/PersistentElement.java"
}
class JavaPersistentElementCompleteVisitor : ExternalJavaGenerator() {
    override val templateString = "domain/core/PersistentElementCompleteVisitor.java"

    val classes = ClassStore.domain.domainClasses.filter { it is ExplicitDomainClass }


    override fun visit(replacer: TemplateReplacerImports) {
        val imports = Imports()
        classes.forEach { clazz ->
            imports.addDomainImport(clazz)
        }
        replacer.insertAll(imports.getimports())
    }

    override fun visit(replacer: TemplateReplacerLine) {
        classes.forEach { clazz ->
            replacer.insert("T visit(${clazz.name} ${clazz.name.lowerFirstLetter()});")
        }
    }
}

class JavaAllowedValueRepository : ExternalJavaGenerator() {
    override val lang = Language.JAVA
    override val templateString = "/repository/AllowedValueRepository.java"
}

// TODO option for external generators!!!
class ExternalTemplate(override val path: String, override val lang: Language) : Template

abstract class ExternalJavaGenerator : JavaGenerator<Nothing>(), SimpleExternalGenerator {
    override val replacer: List<String> = ArrayList()
    override val lang = Language.JAVA
    override val mode: GeneratorType = GeneratorType.PROCESS
}
abstract class ExternalJavaTestGenerator : JavaGenerator<Nothing>(), SimpleExternalGenerator {
    override val replacer: List<String> = ArrayList()
    override val lang = Language.JAVA
    override val mode: GeneratorType = GeneratorType.PROCESS
    override fun getPath(): String {
        return PATH_JAVA_TEST
    }
}
abstract class ExternalSqlGenerator : JavaGenerator<Nothing>(), SimpleExternalGenerator {
    override val replacer: List<String> = ArrayList()
    override val lang = Language.SQL
    override val mode: GeneratorType = GeneratorType.PROCESS
}

interface ExternalGenerator : SpecialGenerator {
    val lang: Language
    val templateString: String
    val replacer: List<String>
    val mode: GeneratorType

    fun <T> visit(visitor: GeneratorVisitor<T>): T
}

interface SimpleExternalGenerator : ExternalGenerator {
    fun getResultFile(): String {
        var resultingFile = templateString
        replacer.forEach {
            resultingFile = resultingFile.replaceFirst("xxx", it)
        }
        return resultingFile
    }

    fun getPath(): String {
        when (lang) {
            Language.JAVA -> return PATH_JAVA
            Language.TYPESCRIPT -> return PATH_TS_FOLDER
            Language.HTML -> TODO()
            Language.SASS -> TODO()
            Language.ESP32 -> return PATH_C_ESP32
            Language.ALL -> TODO()
            Language.POM -> return PATH_POM
            Language.SQL -> return PATH_SQL
        }
    }

    override fun <T> visit(visitor: GeneratorVisitor<T>): T {
        return visitor.accept(this)
    }
}

interface GeneratorVisitor<out T> {
    fun accept(generator: SimpleExternalGenerator): T
}

interface ExternalGeneratorManager {
    fun go()

    fun runLaterIf(condition: Boolean, vararg generators: ExternalJavaGenerator) {
        if (condition) runLaterArray(generators)
    }

    fun runLater(vararg generators: ExternalJavaGenerator) {
        runLaterArray(generators)
    }

    fun runLater(vararg generators: ExternalJavaTestGenerator) {
        runLaterArray(generators)
    }

    fun runLast(vararg generators: ExternalJavaGenerator) {
        runLaterArray(generators, true)
    }

    fun runLaterArray(generators: Array<out ExternalJavaGenerator>, last: Boolean = false) {
        generators.forEach {
            GenerationPool.addGenerator(it, it.getResultFile(), last)
        }
    }

    fun runLaterArray(generators: Array<out ExternalJavaTestGenerator>, last: Boolean = false) {
        generators.forEach {
            GenerationPool.addGenerator(it, it.getResultFile(), last)
        }
    }

    fun runLaterIf(condition: Boolean, vararg generators: ExternalTsGenerator) {
        if (condition) runLaterArray(generators)
    }

    fun runLater(vararg generators: ExternalTsGenerator) {
        runLaterArray(generators)
    }

    fun runLast(vararg generators: ExternalTsGenerator) {
        runLaterArray(generators, true)
    }

    fun runLater(vararg generators: ExternalSqlGenerator) {
        runLaterArray(generators)
    }

    fun runLaterArray(generators: Array<out ExternalTsGenerator>, last: Boolean = false) {
        generators.forEach {
            GenerationPool.addGenerator(it, it.getResultFile(), last)
        }
    }
    fun runLaterArray(generators: Array<out ExternalSqlGenerator>, last: Boolean = false) {
        generators.forEach {
            GenerationPool.addGenerator(it, it.getResultFile(), last)
        }
    }
}
