package de.kaipho.genplus.generator.generator.java.core

import de.kaipho.genplus.generator.core.obj.AssoziationTypeCollection
import de.kaipho.genplus.generator.core.obj.AssoziationTypeUser
import de.kaipho.genplus.generator.domain.obj.*
import de.kaipho.genplus.generator.generator.java.hasAssociationWithValidator
import de.kaipho.genplus.generator.generator.recursiveAssociationFetcher
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.store.SettingsStore
import java.util.*

/**
 * This method tries to detect all classes recursive reachable by the given class.
 * Returns all child Classes (first level, if this is abstract second level has own structure / visitor)
 * and all classes reached over Associations from these classes.
 *
 * If the root class is not abstract, the result will be empty!
 */
fun fetchAllAssociationsAndChildClasses(clazz: IClass, result: MutableList<IClass> = ArrayList()): List<IClass> {
    addClassToListIgnoreAbstract(result, clazz)
    if (clazz is AbstractDomainClass) {
        clazz.childClasses.forEach { child ->
            addClassToList(result, child)
            recursiveAssociationFetcher(child as DomainClass).forEach {
                if (it.type is AssoziationTypeUser) {
                    if (addClassToListIgnoreAbstract(result, it.type.clazz)) {
                        fetchAllAssociationsAndChildClasses(it.type.clazz, result)
                    }
                }
                if (it.type is AssoziationTypeCollection && it.type.inner is AssoziationTypeUser) {
                    if (addClassToListIgnoreAbstract(result, it.type.inner.clazz)) {
                        fetchAllAssociationsAndChildClasses(it.type.inner.clazz, result)
                    }
                }
            }
        }
    }
    return result
}

/**
 * Liefert eine Liste aller nicht abstrakten Spezialisierungen dieser Klasse.
 */
fun fetchAllNonAbstractChildClassesRekursiv(clazz: IClass, result: MutableList<IClass> = ArrayList()): List<IClass> {
    addClassToListIgnoreAbstract(result, clazz)
    if (clazz is AbstractDomainClass) {
        clazz.childClasses.forEach { child ->
            addClassToListIgnoreAbstract(result, child)
            fetchAllNonAbstractChildClassesRekursiv(child, result)
        }
    }
    return result
}

/**
 * Liefert eine Liste aller Extend beziehungen dieser Klasse.
 */
fun fetchAllParents(clazz: IClass, result: MutableList<IClass> = ArrayList()): List<IClass> {
    clazz.extends.forEach { child ->
        addClassToList(result, child)
        fetchAllParents(child, result)
    }
    return result
}

/**
 * Liefert die gesammte Hierachie einer Kategorie
 */
fun fetchHierarchy(clazz: CategoryClass, result: MutableList<CategoryClass> = ArrayList()): List<CategoryClass> {
    clazz.hierarchy.forEach { child ->
        addClassToList(result, child)
        fetchHierarchy(child, result)
    }
    return result
}

/**
 * Liefert die gesammte Hierachie einer Konstante
 */
fun fetchHierarchy(clazz: Constant): List<Constant> {
    return ClassStore.constants.filter {
        fetchHierarchyWrongWay(it).filter { it.name == clazz.name }.isNotEmpty()
    }
}

/**
 * Liefert die gesammte Hierachie einer Konstante
 */
fun fetchSimpleHierarchy(clazz: Constant): List<Constant> {
    return ClassStore.constants.filter {
        fetchHierarchyWrongSimple(it).filter { it.name == clazz.name }.isNotEmpty()
    }
}

private fun fetchHierarchyWrongSimple(clazz: Constant, result: MutableList<Constant> = ArrayList()): List<Constant> {
    return clazz.accept(object : ConstantVisitor<List<Constant>> {
        override fun visit(constantClass: ConstantClass): List<Constant> {
            return result
        }

        override fun visit(constantContainer: ConstantContainer): List<Constant> {
            constantContainer.constants.forEach { child ->
                addClassToList(result, child)
            }
            return result
        }
    })
}

fun fetchHierarchyWrongWay(clazz: Constant, result: MutableList<Constant> = ArrayList()): List<Constant> {
    return clazz.accept(object : ConstantVisitor<List<Constant>> {
        override fun visit(constantClass: ConstantClass): List<Constant> {
            return result
        }

        override fun visit(constantContainer: ConstantContainer): List<Constant> {
            constantContainer.constants.forEach { child ->
                addClassToList(result, child)
                fetchHierarchyWrongWay(child, result)
            }
            return result
        }
    })
}

fun addClassToListIgnoreAbstract(classList: MutableList<IClass>, clazz: IClass): Boolean {
    if (clazz is AbstractDomainClass) {
        return false
    }
    if (classList.filter { it.name == clazz.name && it.prefix == clazz.prefix }.isEmpty()) {
        classList.add(clazz)
        return true
    }
    return false
}

fun <T : IClass> addClassToList(classList: MutableList<T>, clazz: T): Boolean {
    if (classList.filter { it.name == clazz.name && it.prefix == clazz.prefix }.isEmpty()) {
        classList.add(clazz)
        return true
    }
    return false
}

fun getAllParents(clazz: List<AbstractClass>): MutableList<AbstractClass> {
    val list: MutableList<AbstractClass> = ArrayList()
    clazz.forEach {
        list.add(it)
        if (it.extends.isNotEmpty()) {
            list.addAll(getAllParents(it.extends))
        }
    }
    return list
}

fun getClassCreation(clazz: DomainClass, id:String = ""):String {
    var result = ""
    if (hasAssociationWithValidator(clazz)) {
        result += "new ${clazz.name}ValidationProxy("
    }
    if (SettingsStore.databaseEnabled()) {
        result += "new ${clazz.name}RepositoryProxy($id)"
    } else {
        result += "new ${clazz.name}Impl()"
    }
    if (hasAssociationWithValidator(clazz)) {
        result += ")"
    }
    return result
}
