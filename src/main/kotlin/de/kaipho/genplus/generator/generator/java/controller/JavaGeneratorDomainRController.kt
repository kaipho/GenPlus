package de.kaipho.genplus.generator.generator.java.controller

import de.kaipho.genplus.generator.common.convertPackageToFileSystem
import de.kaipho.genplus.generator.common.forEachAssociationWithUserType
import de.kaipho.genplus.generator.common.upperFirstLetter
import de.kaipho.genplus.generator.core.obj.*
import de.kaipho.genplus.generator.domain.obj.*
import de.kaipho.genplus.generator.generator.java.core.ExternalJavaGenerator
import de.kaipho.genplus.generator.generator.java.core.fetchAllParents
import de.kaipho.genplus.generator.generator.java.persistence.ClassIdGenerator
import de.kaipho.genplus.generator.generator.java.service.CoreFramework
import de.kaipho.genplus.generator.generator.java.service.Imports
import de.kaipho.genplus.generator.generator.java.service.JavaImport
import de.kaipho.genplus.generator.generator.recursiveAssociationFetcher
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerClassName
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerImports
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLine
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerPath

class JavaGeneratorDomainRController(val clazz: DomainClass) : ExternalJavaGenerator() {
    override val templateString = "controller/domainr/xxx/xxxController.java"
    override val replacer = arrayListOf(clazz.prefix.convertPackageToFileSystem().removeSuffix("/"),
            clazz.name)

    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(clazz.name)
    }

    override fun visit(replacer: TemplateReplacerPath) {
        replacer.insert(clazz.id.toString())
    }

    override fun visit(replacer: TemplateReplacerLine) {
        when (replacer.id) {
            0 -> replacer.insert(clazz.prefix)
            1 -> insertAssociationGetMethods(replacer)
            2 -> insertLinks(replacer)
            3 -> insertUpdateMapper(replacer)
        }
    }

    private fun insertUpdateMapper(replacer: TemplateReplacerLine) {
        recursiveAssociationFetcher(clazz, false).forEach {
            it.type.accept(object : AssociationVisitor<Unit> {
                override fun visit(associationTypeBlob: AssociationTypeBlob) = standard("BLOB")

                override fun visit(association: AssoziationTypeMap) {
                    // No Mapping
                }

                private fun standard(inner: String) {
                    val name = it.name
                    val upperName = it.name.upperFirstLetter()
                    replacer.insert("updater.put(\"$name\", updater(UPDATER_$inner, ${it.belongsTo.name}::set$upperName, ${it.belongsTo.name}::get$upperName));")
                }

                override fun visit(association: AssoziationTypeSecure) = standard("STRING")

                override fun visit(association: AssoziationTypeString) = standard("STRING")

                override fun visit(association: AssoziationTypeInteger) = standard("INTEGER")

                override fun visit(association: AssoziationTypeDate) = standard("DATE")

                override fun visit(association: AssoziationTypeBoolean) = standard("BOOLEAN")

                override fun visit(association: AssoziationTypeUser) {
                    association.acceptClass(object : UserClassVisitor<Unit> {
                        override fun visitPrimitive(association: AssoziationTypeUser, clazz: PrimitiveClass) {
                            standard("PRIMITIVE")
                        }

                        override fun visitCategory(association: AssoziationTypeUser, clazz: CategoryClass) {
                            val name = it.name
                            val upperName = it.name.upperFirstLetter()
                            replacer.insert("updater.put(\"$name\", updaterCategory(${clazz.name}::from, ${it.belongsTo.name}::set$upperName, ${it.belongsTo.name}::get$upperName));")
                        }

                        override fun visitDomain(association: AssoziationTypeUser, clazz: DomainClass) {
                            // No Mapping
                        }

                        override fun visitConstant(association: AssoziationTypeUser, clazz: Constant) {
                            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                        }

                    })
                }

                override fun visit(association: AssoziationTypeCollection) {
                    // No Mapping
                }

                override fun visit(association: AssoziationTypeDummy) {
                    // No Mapping
                }

                override fun visit(association: AssoziationTypeUnit) {
                    // No Mapping
                }

                override fun visit(association: AssoziationTypeDouble) = standard("DOUBLE")

                override fun visit(association: AssoziationTypeBigNumber) {
                    standard("DECIMAL")
                }

                override fun visit(association: AssoziationTypeText) = standard("STRING")

                override fun visit(association: AssoziationTypeOptional) {
                    // No Mapping
                }

                override fun visit(association: AssoziationTypePassword) = standard("PASSWORD")
            })
        }
    }

    private fun insertLinks(replacer: TemplateReplacerLine) {
        recursiveAssociationFetcher(clazz).filter { it.type is AssoziationTypeUser }.forEach {
            replacer.insert("links.add(\"${it.name}\", \"domainr/\" + it.accept(new ResolvePathVisitor()) + \"/\" + it.getId() + \"/\" + ${ClassIdGenerator.getAssociationValName(it)});")
        }
    }

    private fun insertAssociationGetMethods(replacer: TemplateReplacerLine) {
        if (clazz is ExplicitDomainClass) {
            """
            |@GetMapping("/create")
            |public String createObj() throws DbException {
            |    ${clazz.name} obj = Transaction.getInstance().run(() -> ${clazz.name}.builder().build()).resolve();
            |    return "domainr/${clazz.id}/" + obj.getId();
            |}
            """.insertInto(replacer)
        }
        recursiveAssociationFetcher(clazz).forEach {
            if (it.type is AssoziationTypeCollection && it.type.inner !is AssoziationTypeUser) {
                it.type.inner.accept(object : AssociationVisitor<Unit> {
                    private fun standard(inner: String) {
                        """
                        |@PostMapping("/{id}/${it.id}/add")
                        |public void addSingleTo${it.id}(@PathVariable Long id, @RequestBody Map<String, Object> obj) throws DbException {
                        |    ${this@JavaGeneratorDomainRController.clazz.name} it = ${this@JavaGeneratorDomainRController.clazz.name}.findOneById(id);
                        |    it.addSingleTo${it.name.upperFirstLetter()}(UPDATER_$inner.apply(obj.get("newValue")));
                        |}
                        """.insertInto(replacer)
                    }

                    override fun visit(association: AssoziationTypeMap) {
                        // No Mapping
                    }

                    override fun visit(association: AssoziationTypeSecure) = standard("STRING")

                    override fun visit(association: AssoziationTypeString) = standard("STRING")

                    override fun visit(association: AssoziationTypeInteger) = standard("INTEGER")

                    override fun visit(association: AssoziationTypeDate) = standard("DATE")

                    override fun visit(association: AssoziationTypeBoolean) = standard("BOOLEAN")

                    override fun visit(associationTypeBlob: AssociationTypeBlob) = standard("BLOB")

                    override fun visit(association: AssoziationTypeUser) {
                        association.acceptClass(object : UserClassVisitor<Unit> {
                            override fun visitPrimitive(association: AssoziationTypeUser, clazz: PrimitiveClass) {
                                standard("PRIMITIVE")
                            }

                            override fun visitCategory(association: AssoziationTypeUser, clazz: CategoryClass) {
                                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                            }

                            override fun visitDomain(association: AssoziationTypeUser, clazz: DomainClass) {
                                // No Mapping
                            }

                            override fun visitConstant(association: AssoziationTypeUser, clazz: Constant) {
                                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                            }

                        })
                    }

                    override fun visit(association: AssoziationTypeCollection) {
                        // No Mapping
                    }

                    override fun visit(association: AssoziationTypeDummy) {
                        // No Mapping
                    }

                    override fun visit(association: AssoziationTypeUnit) {
                        // No Mapping
                    }

                    override fun visit(association: AssoziationTypeDouble) = standard("DOUBLE")

                    override fun visit(association: AssoziationTypeBigNumber) {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                    override fun visit(association: AssoziationTypeText) = standard("STRING")

                    override fun visit(association: AssoziationTypeOptional) {
                        // No Mapping
                    }

                    override fun visit(association: AssoziationTypePassword) = standard("PASSWORD")
                })

            }
        }

        recursiveAssociationFetcher(clazz).forEachAssociationWithUserType { association, clazz ->
            clazz.acceptClass(object : TypeVisitor<Unit> {
                override fun visitPrimitive(clazz: PrimitiveClass) {
                    // no method
                }

                override fun visitCategory(clazz: CategoryClass) {
                    // no method
                }

                override fun visitDomain(clazz: DomainClass) {
                    if (clazz.isExportRecursive()) {
                        """
                        |@GetMapping("/{id}/${association.id}")
                        |public String get${association.name.upperFirstLetter()}(@PathVariable Long id) throws DbException {
                        |   ${this@JavaGeneratorDomainRController.clazz.name} it = ${this@JavaGeneratorDomainRController.clazz.name}.findOneById(id);
                        |   return new ${association.type.javaRep()}Controller().findSingle(it.get${association.name.upperFirstLetter()}().getId());
                        |}
                        """.insertInto(replacer)
                    } else {
                        """
                        |@GetMapping("/{id}/${association.id}")
                        |public String get${association.name.upperFirstLetter()}(@PathVariable Long id) throws DbException {
                        |    ${this@JavaGeneratorDomainRController.clazz.name} it = ${this@JavaGeneratorDomainRController.clazz.name}.findOneById(id);
                        |    JsonObject obj = it.get${association.name.upperFirstLetter()}().accept(ToJsonVisitor.getInstance());
                        |
                        |    JsonObjectBuilder links = Json.createObjectBuilder();
                        |    links.add("self", "domainr/" + ${ClassIdGenerator.getClassValName(clazz)} + "/" + it.get${association.name.upperFirstLetter()}().getId());
                        |
                        |    JsonObjectBuilder objVm = Json.createObjectBuilder().add("_embedded", obj)
                        |                                                        .add("_links", links.build());
                        |    return objVm.build().toString();
                        |}
                        """.insertInto(replacer)
                    }
                }

                override fun visitConstant(clazz: Constant) {
                    // no method
                }

                override fun visitOther(clazz: IClass) {
                    // no method
                }

                override fun visitDummy(clazz: IClass) {
                    // no method
                }

            })
        }
    }

    override fun visit(replacer: TemplateReplacerImports) {
        val import = Imports()
        fetchAllParents(clazz).forEach {
            import.addDomainImport(it)
        }
        recursiveAssociationFetcher(clazz, false).forEachAssociationWithUserType { _, clazz ->
            clazz.acceptClass(object : TypeVisitor<Unit> {
                override fun visitPrimitive(clazz: PrimitiveClass) {
                    // no import
                }

                override fun visitCategory(clazz: CategoryClass) {
                    import.addDomainImport(clazz)
                }

                override fun visitDomain(clazz: DomainClass) {
                    import.addDomainControllerImport(clazz)
                }

                override fun visitConstant(clazz: Constant) {
                    // no import
                }

                override fun visitOther(clazz: IClass) {
                    // no import
                }

                override fun visitDummy(clazz: IClass) {
                    // no import
                }

            })
        }
        import.addDomainImport(clazz)
        import.addJavaImport(JavaImport.LOCAL_DATE)
        import.addCoreImport(CoreFramework.BCRYPT)
        replacer.insertAll(import.getimports())
    }
}
