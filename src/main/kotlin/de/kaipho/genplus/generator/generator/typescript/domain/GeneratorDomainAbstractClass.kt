package de.kaipho.genplus.generator.generator.typescript.domain

import de.kaipho.genplus.generator.common.convertPackageToFileSystem
import de.kaipho.genplus.generator.common.lowerFirstLetter
import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.core.obj.AssoziationTypeCollection
import de.kaipho.genplus.generator.core.obj.AssoziationTypeOptional
import de.kaipho.genplus.generator.core.obj.AssoziationTypeUser
import de.kaipho.genplus.generator.domain.obj.AbstractDomainClass
import de.kaipho.genplus.generator.domain.obj.PrimitiveClass
import de.kaipho.genplus.generator.generator.AbstractGenerator
import de.kaipho.genplus.generator.generator.core.File
import de.kaipho.genplus.generator.generator.core.Generator
import de.kaipho.genplus.generator.generator.getInlinedAssociations
import de.kaipho.genplus.generator.generator.recursiveAssociationFetcher
import de.kaipho.genplus.generator.generator.typescript.RelevantPathes
import de.kaipho.genplus.generator.generator.typescript.core.ImportHelp
import de.kaipho.genplus.generator.generator.typescript.core.Imports
import de.kaipho.genplus.generator.template.Templates
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerAssociations
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerClassName
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerComment
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerImports

@Generator(
        lang = Language.TYPESCRIPT,
        template = Templates.TS_DOMAIN_ABSTRACT_CLASS,
        appliedOn = AbstractDomainClass::class,
        file = File.TS_DOMAIN_ABSTRACT_CLASS
)
class GeneratorDomainAbstractClass : AbstractGenerator<AbstractDomainClass>() {
    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(meta.data.name)
    }

    override fun visit(replacer: TemplateReplacerComment) {
        replacer.insert(meta.data.comment)
    }

    override fun visit(replacer: TemplateReplacerImports) {
        replacer.insert(ImportHelp.createImport("DomainObject",
                                                ImportHelp.createRelativePath("domain${meta.data.prefix.convertPackageToFileSystem()}",
                                                                              RelevantPathes.DOMAIN_OBJECT.path),
                                                RelevantPathes.DOMAIN_OBJECT.file))
        replacer.insert(ImportHelp.createImport("${meta.data.name}Visitor",
                                                ImportHelp.createRelativePath("",
                                                                              ""),
                                                "${meta.data.name.lowerFirstLetter()}.visitor"))

        val imports = Imports("domain${meta.data.prefix.convertPackageToFileSystem()}")

        recursiveAssociationFetcher(meta.data).union(getInlinedAssociations(meta.data)).toList().forEach {
            if (it.type is AssoziationTypeUser) {
//                imports.addDomainImport(it.type.clazz)
            }
            if (it.type is AssoziationTypeCollection) {
                if (it.type.inner is AssoziationTypeUser)
                    imports.addDomainImport(it.type.inner.clazz)
            }
        }

        replacer.insertAll(imports.getImports())
    }

    override fun visit(replacer: TemplateReplacerAssociations) {
        recursiveAssociationFetcher(meta.data).union(getInlinedAssociations(meta.data)).toList().forEach {
            if (it.type !is AssoziationTypeUser && !(it.type is AssoziationTypeOptional && it.type.inner is AssoziationTypeUser)) {
                replacer.insert("${it.name}:${it.type.typescriptRep()};")
            }
            if (it.type is AssoziationTypeUser && it.type.clazz is PrimitiveClass) {
                replacer.insert("${it.name}:any;")
            }
        }
    }
}