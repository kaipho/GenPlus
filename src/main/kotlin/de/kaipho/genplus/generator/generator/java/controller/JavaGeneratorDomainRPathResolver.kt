package de.kaipho.genplus.generator.generator.java.controller

import de.kaipho.genplus.generator.common.lowerFirstLetter
import de.kaipho.genplus.generator.domain.obj.ExplicitDomainClass
import de.kaipho.genplus.generator.generator.java.core.ExternalJavaGenerator
import de.kaipho.genplus.generator.generator.java.persistence.ClassIdGenerator
import de.kaipho.genplus.generator.generator.java.service.Imports
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerImports
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLine

class JavaGeneratorDomainRPathResolver() : ExternalJavaGenerator() {
    override val templateString = "controller/domainr/ResolvePathVisitor.java"
    val classes = ClassStore.domain.domainClasses.filter { it is ExplicitDomainClass }

    override fun visit(replacer: TemplateReplacerImports) {
        val imports = Imports()
        classes.forEach { clazz ->
            imports.addDomainImport(clazz)
        }
        replacer.insertAll(imports.getimports())
    }

    override fun visit(replacer: TemplateReplacerLine) {
        classes.forEach { clazz ->
            replacer.insert("@Override")
            replacer.insert("public Long visit(${clazz.name} ${clazz.name.lowerFirstLetter()}) {")
            replacer.insert("   return ${ClassIdGenerator.getClassValName(clazz)};")
            replacer.insert("}")
        }
    }
}
