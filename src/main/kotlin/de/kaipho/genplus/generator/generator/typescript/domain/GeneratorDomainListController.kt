package de.kaipho.genplus.generator.generator.typescript.domain

import de.kaipho.genplus.generator.common.convertPackageToFileSystem
import de.kaipho.genplus.generator.common.lowerFirstLetter
import de.kaipho.genplus.generator.core.obj.AssoziationTypeUser
import de.kaipho.genplus.generator.domain.obj.DomainClass
import de.kaipho.genplus.generator.generator.typescript.ExternalTsGenerator
import de.kaipho.genplus.generator.generator.typescript.TsPath
import de.kaipho.genplus.generator.generator.typescript.core.GeneratorTsAppModule
import de.kaipho.genplus.generator.generator.typescript.core.Imports
import de.kaipho.genplus.generator.generator.typescript.core.RoutingHelp
import de.kaipho.genplus.generator.template.replacer.*


class GeneratorDomainListController(val clazz: DomainClass) : ExternalTsGenerator() {
    override val templateString = "src/app/components/domain/xxx/xxx.list.component.ts"
    override val replacer = arrayListOf(
            clazz.prefix.convertPackageToFileSystem().removeSuffix("/"),
            clazz.name.lowerFirstLetter()
    )

    override fun standardAction() {
        GeneratorTsAppModule.listComponents.add(clazz)
    }

    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(clazz.name)
    }

    override fun visit(replacer: TemplateReplacerLowerClassName) {
        replacer.insert(clazz.name.lowerFirstLetter())
    }

    override fun visit(replacer: TemplateReplacerImports) {
        val imports = Imports("components/domain/" + clazz.prefix.convertPackageToFileSystem())

        imports.addImport(TsPath.LIST_COMPONENT)
        imports.addImport(TsPath.TITLE_SERVICE)

        imports.addHttpImport(clazz)
        imports.addDomainImport(clazz)

        clazz.getAllCreateFunctions().forEach {
            val name = it.returnType.name
            imports.addImport(
                    "${it.owner.name}Create$name${name}Component",
                    "components/service${(it.returnType as AssoziationTypeUser).clazz.prefix.convertPackageToFileSystem()}",
                    "${it.owner.name}.create$name.$name.component"
            )
        }

        replacer.insertAll(imports.getImports())
    }

    override fun visit(replacer: TemplateReplacerName) {
        replacer.insert(clazz.prefix.convertPackageToFileSystem())
    }

    override fun visit(replacer: TemplateReplacerI18n) {
        replacer.insert("title${clazz.prefix}.list${clazz.name.lowerFirstLetter()}")
    }

    override fun visit(replacer: TemplateReplacerLine) {
        clazz.getAllCreateFunctions().forEach {
            val name = it.returnType.name
            """
            |create$name() {
            |    this.create(${it.owner.name}Create$name${name}Component, ${it.returnType.id})
            |}
            """.insertInto(replacer)
        }
    }

    override fun visit(replacer: TemplateReplacerSubpackage) {
        replacer.insert(clazz.prefix)
    }

    override fun visit(replacer: TemplateReplacerPath) {
        replacer.insert(RoutingHelp.getRouteToList(clazz))
    }
}