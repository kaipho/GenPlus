package de.kaipho.genplus.generator.generator.c_esp32.core

import de.kaipho.genplus.generator.common.convertPackageToFileSystem
import de.kaipho.genplus.generator.domain.obj.IClass
import java.util.*

/**
 * Created by tom on 26.07.16.
 */
class Imports {
    val imports: MutableSet<String> = HashSet()
    val self: String

    constructor(self: String) {
        this.self = self
    }

    fun addImport(import: CImports) {
        imports.add("#include <${import.rep}>")
    }
    fun addImport(import: FrameworkImports) {
        imports.add("#include <${import.rep}>")
    }

    fun addDomainImport(import: IClass) {
        imports.add("#include <domain${import.prefix.convertPackageToFileSystem()}/${import.name}.h>")
    }

    fun getimports(): List<String> = imports.toList()
}

enum class CImports(val rep: String) {
    STRING("string"),
    VECTOR("vector")
}
enum class FrameworkImports(val rep: String) {
    PERSISTENT_ELEMENT("domain/core/PersistentElement.h"),
}
