package de.kaipho.genplus.generator.generator.java.domain.category

import de.kaipho.genplus.generator.core.obj.options.Option
import de.kaipho.genplus.generator.generator.java.core.ExternalJavaGenerator
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.store.SettingsStore
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerId
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLine


class JavaGeneratorCategoryRole : ExternalJavaGenerator() {
    override val templateString = "domain/category/Role.java"

    override fun visit(replacer: TemplateReplacerId) {
        val role = ClassStore.categories.find { it.name == "Role" }
        if(role == null) throw RuntimeException("Language must be defined as category for framework!")
        replacer.insert(role.id.toString())
    }

    override fun visit(replacer: TemplateReplacerLine) {
        replacer.insert("public static Role ADMIN;")
        replacer.insert("public static Role EDIT;")
        SettingsStore.options.get(Option.SECURITY_ROLES).forEach {
            replacer.insert("public static Role ${it.toUpperCase()};")
        }
    }
}