package de.kaipho.genplus.generator.generator.java.domain.constant

import de.kaipho.genplus.generator.common.convertPackageToFileSystem
import de.kaipho.genplus.generator.domain.obj.Constant
import de.kaipho.genplus.generator.domain.obj.ConstantClass
import de.kaipho.genplus.generator.domain.obj.ConstantContainer
import de.kaipho.genplus.generator.domain.obj.ConstantVisitor
import de.kaipho.genplus.generator.generator.java.core.ExternalJavaGenerator
import de.kaipho.genplus.generator.generator.java.core.fetchHierarchy
import de.kaipho.genplus.generator.generator.java.core.fetchHierarchyWrongWay
import de.kaipho.genplus.generator.generator.java.core.fetchSimpleHierarchy
import de.kaipho.genplus.generator.template.replacer.*

class JavaGeneratorConstantInterface(val constant: Constant) : ExternalJavaGenerator() {
    override val templateString = "domain/constant/xxx/xxx.java"
    override val replacer = arrayListOf(constant.prefix.convertPackageToFileSystem().removeSuffix("/"), constant.name)


    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(constant.name)
    }

    override fun visit(replacer: TemplateReplacerImplements) {
        val hierarchy = fetchSimpleHierarchy(constant)
        if (fetchSimpleHierarchy(constant).isNotEmpty()) {
            val implementedClasses = hierarchy.joinToString()
            replacer.insert(", $implementedClasses ")
        } else {
            replacer.insertNothing()
        }
    }

    override fun visit(replacer: TemplateReplacerSubpackage) {
        replacer.insert(constant.prefix.removeSuffix("."))
    }

    override fun visit(replacer: TemplateReplacerLine) {
        constant.accept(object : ConstantVisitor<Unit> {
            override fun visit(constantClass: ConstantClass) {
                val parameter = constantClass.parameter.joinToString(transform = { "Object $it" })
                val parameterForCall = constantClass.parameter.joinToString()
                """
                |static ${constant.name} get($parameter) {
                |    return new ${constant.name}Impl($parameterForCall);
                |}

                |static I18nContext i18nModel() {
                |    return I18nModelContext.get(ID);
                |}
                """.insertInto(replacer)
            }

            override fun visit(constantContainer: ConstantContainer) {
                fetchHierarchyWrongWay(constant).forEach {
                    it.accept(object : ConstantVisitor<Unit> {
                        override fun visit(constantClass: ConstantClass) {
                            val parameter = constantClass.parameter.joinToString(transform = { "Object $it" })
                            val parameterForCall = constantClass.parameter.joinToString()
                            """
                            |static ${constant.name} get${constantClass.name}($parameter) {
                            |    return ${constantClass.name}.get($parameterForCall);
                            |}
                            """.insertInto(replacer)
                        }

                        override fun visit(constantContainer: ConstantContainer) {
                            // no create method
                        }
                    })
                }
            }
        })
    }

    override fun visit(replacer: TemplateReplacerId) {
        replacer.insert(constant.id.toString())
    }
}