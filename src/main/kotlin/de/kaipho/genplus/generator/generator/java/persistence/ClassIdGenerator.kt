package de.kaipho.genplus.generator.generator.java.persistence


import de.kaipho.genplus.generator.domain.obj.Association
import de.kaipho.genplus.generator.domain.obj.IClass
import de.kaipho.genplus.generator.store.SettingsStore
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardOpenOption
import java.util.*


object ClassIdGenerator {
    private val path = Paths.get(SettingsStore.runningPath).resolve("ids.txt")
    private val ids = HashMap<String, Int>()
    private val idsAss = HashMap<String, Int>()
    private var actualClass: Int = 0
    private var actualAssociation: Int = 0

    init {
        if (Files.exists(path)) {
            // load data
            Files.readAllLines(path).forEach {
                if (it.isEmpty())
                    return@forEach
                val elements = it.split(":")
                if (elements.size != 2) {
                    throw RuntimeException("Wring ID File!")
                }
                if (elements[0].contains(".")) {
                    idsAss.put(elements[0], elements[1].toInt())
                } else {
                    ids.put(elements[0], elements[1].toInt())
                }
            }
            this.actualClass = if (ids.values.max() == null) 1 else ids.values.max()!! + 1
            this.actualAssociation = if (idsAss.values.max() == null) 1 else idsAss.values.max()!! + 1
        }
    }

    fun finalize() {
        val builder = StringBuilder()
        ids.forEach { t, u ->
            builder.append("$t:$u\n")
        }
        idsAss.forEach { t, u ->
            builder.append("$t:$u\n")
        }
        Files.write(path, builder.lines(), StandardOpenOption.CREATE)
    }

    fun getIdForClass(clazz: String): Int {
        if (ids.containsKey(clazz))
            return ids[clazz]!!
        actualClass++
        ids.put(clazz, actualClass)
        return actualClass
    }

    fun getIdForAssiciation(clazz: String, association: String): Int {
        val name = "$clazz.$association"
        if (idsAss.containsKey(name))
            return idsAss[name]!!
        actualAssociation++
        idsAss.put(name, actualAssociation)
        return actualAssociation
    }

    fun getClassValName(clazz: IClass) = (clazz.prefix.replace(".", "_") + "_" + clazz.name).toUpperCase().removePrefix("_")
    fun getAssociationValName(association: Association) = getClassValName(association.belongsTo) + "_" + association.name.toUpperCase()
}