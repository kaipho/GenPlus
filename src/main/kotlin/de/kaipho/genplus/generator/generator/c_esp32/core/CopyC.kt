package de.kaipho.genplus.generator.generator.c_esp32.core

import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.core.obj.options.IOptions
import de.kaipho.genplus.generator.generator.AbstractGenerator
import de.kaipho.genplus.generator.generator.core.GeneratorType
import de.kaipho.genplus.generator.generator.java.core.ExternalGeneratorManager
import de.kaipho.genplus.generator.generator.java.core.SimpleExternalGenerator
import de.kaipho.genplus.generator.store.SettingsStore
import java.util.*

// Generators to simply copy the file

class CCopyFilesManager : ExternalGeneratorManager {
    val options: IOptions = SettingsStore.options

    override fun go() {
//        C_CopyPersistentElement().apply {
//            GenerationPool.addGenerator(this, this.templateString)
//        }
//        C_CopyPersistentService().apply {
//            GenerationPool.addGenerator(this, this.templateString)
//        }
    }
}

class C_CopyPersistentElement : ExternalCGenerator() {
    override val lang = Language.ESP32
    override val templateString = "domain/core/PersistentElement.h"
}

class C_CopyPersistentService : ExternalCGenerator() {
    override val lang = Language.ESP32
    override val templateString = "domain/core/PersistentService.h"
}

abstract class ExternalCGenerator : AbstractGenerator<Nothing>(), SimpleExternalGenerator {
    override val replacer: List<String> = ArrayList()
    override val mode: GeneratorType = GeneratorType.PROCESS
}
