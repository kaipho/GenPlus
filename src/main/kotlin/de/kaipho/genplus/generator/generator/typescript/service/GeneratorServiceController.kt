package de.kaipho.genplus.generator.generator.typescript.service

import de.kaipho.genplus.generator.common.convertPackageToFileSystem
import de.kaipho.genplus.generator.common.upperFirstLetter
import de.kaipho.genplus.generator.core.obj.*
import de.kaipho.genplus.generator.core.obj.service.ClassDummy
import de.kaipho.genplus.generator.core.obj.service.Function
import de.kaipho.genplus.generator.core.obj.service.Parameter
import de.kaipho.genplus.generator.domain.obj.*
import de.kaipho.genplus.generator.generator.core.GenerationPool
import de.kaipho.genplus.generator.generator.html.domain.GeneratorHtmlDomainList
import de.kaipho.genplus.generator.generator.typescript.ExternalTsGenerator
import de.kaipho.genplus.generator.generator.typescript.TsPath
import de.kaipho.genplus.generator.generator.typescript.core.Imports
import de.kaipho.genplus.generator.generator.typescript.domain.GeneratorDomainListController
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerClassName
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerImports
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLine
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLowerClassName

class GeneratorServiceController(val function: Function) : ExternalTsGenerator() {
    override val templateString = "src/app/components/service/xxx/xxx.component.ts"
    override val replacer = arrayListOf(
            function.owner.prefix.convertPackageToFileSystem().removeSuffix("/"),
            function.getQualifiedName()
    )

    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(function.getQualifiedClassName())
    }

    override fun visit(replacer: TemplateReplacerImports) {
        val imports = Imports("components/service/${function.getFilePrefix()}/")

        if (function.extends !is ClassDummy) {
            imports.addDomainImport(function.extends)
        }
        getUserParams().forEach { param ->
            imports.addDomainImport((param.type as AssoziationTypeUser).clazz)
            imports.addListComponentImport((param.type as AssoziationTypeUser).clazz)
        }

        if (function.params.isNotEmpty()) {
            imports.addImport("import { MatDialogConfig, MatDialog } from '@angular/material/dialog';")
            imports.addImport("import { ViewContainerRef } from '@angular/core';")
        }
        imports.addFunctionsHttpImport(function.owner)
        imports.addImport(TsPath.MENU_DIALOG_COMPONENT)

        replacer.insertAll(imports.getImports())
    }

    override fun visit(replacer: TemplateReplacerLowerClassName) {
        replacer.insert(function.getQualifiedName())
    }

    override fun visit(replacer: TemplateReplacerLine) {
        when (replacer.id) {
            0 -> insertConstructorParams(replacer)
            1 -> insertSelectMethods(replacer)
            2 -> insertOnInitMethods(replacer)
            3 -> insertForm(replacer)
        }
    }

    private fun insertForm(replacer: TemplateReplacerLine) {
        function.params.forEach { param ->
            if (param.type is AssoziationTypeBoolean)
                replacer.insert("${param.name}: [false, Validators.required],")
            else if (param.type is AssoziationTypeUser || param.type is AssociationTypeBlob)
                replacer.insert("${param.name}: [{}, Validators.required],")
            else
                replacer.insert("${param.name}: ['', Validators.required],")
        }
    }

    fun insertSelectMethods(replacer: TemplateReplacerLine) {
        getUserParams().forEach { param ->
            // Eventuell gibt es noch keine ListComponent
            (param.type as AssoziationTypeUser).acceptClass(object : UserClassVisitor<Unit> {
                override fun visitPrimitive(association: AssoziationTypeUser, clazz: PrimitiveClass) {
                    // no extra generation
                }

                override fun visitCategory(association: AssoziationTypeUser, clazz: CategoryClass) {
                    // no extra generation
                }

                override fun visitDomain(association: AssoziationTypeUser, clazz: DomainClass) {
                    if (!clazz.isExportRecursive()) {
                        val generatorTs = GeneratorDomainListController(clazz)
                        GenerationPool.addGenerator(generatorTs, "${param.type.name}ListComponent.ts")
                        val generatorHtml = GeneratorHtmlDomainList(clazz)
                        GenerationPool.addGenerator(generatorHtml, "${param.type.name}ListComponent.html")
                    }
                    """
                    |select${param.name.upperFirstLetter()}() {
                    |   let config = new MatDialogConfig();
                    |   config.viewContainerRef = this.viewContainerRef;
                    |   const ref = this.dialog.open(${param.type.name}ListComponent, config);
                    |   ref.afterClosed().subscribe(result => {
                    |       this.serviceForm.patchValue({${param.name}: result.id});
                    |   });
                    |}
                    """.insertInto(replacer)
                }

                override fun visitConstant(association: AssoziationTypeUser, clazz: Constant) {
                    // no extra generation
                }

            })
        }

        replacer.insert("submit() {")
        replacer.insert("   this.submitting = true;")
        replacer.insert("   this.http.${function.name}(")
        if (function.extends !is ClassDummy)
            replacer.insert("           this._actualId, ")
        function.params.forEach {
            replacer.insert("           this.serviceForm.get('${it.name}').value,")
        }
        replacer.insert("   ).subscribe(")
        if (function.returnType is AssoziationTypeUnit) {
            replacer.insert("       (ok) => {")
            replacer.insert("           this.submitting = false;")
            replacer.insert("           this.dialogRef.close(ok);")
            replacer.insert("       },")
        } else {
            replacer.insert("       (ok) => {")
            replacer.insert("           this.submitting = false;")
            replacer.insert("           if (this.closeSilent) this.dialogRef.close(ok);")
            replacer.insert("           this.serviceForm.patchValue({result: ok});")
            replacer.insert("           this._result = ok;")
            replacer.insert("       },")
        }
        replacer.insert("       (error) => {")
        replacer.insert("           this.submitting = false;")
        replacer.insert("           this._error = error.body.message;")
        replacer.insert("       }")
        replacer.insert("   );")
        replacer.insert("}")
    }

    private fun getParams(param: Parameter): String {
        if (param.type is AssoziationTypeUser)
            return "this._${param.name}.id"
        else
            return "this._${param.name}"
    }

    fun insertConstructorParams(replacer: TemplateReplacerLine) {
        val params = getUserParams()
        if (params.isNotEmpty())
            replacer.insert(", public http: ${function.owner.name}HttpService, public dialog: MatDialog, public viewContainerRef: ViewContainerRef")
        else
            replacer.insert(", public http: ${function.owner.name}HttpService")
    }

    fun insertOnInitMethods(replacer: TemplateReplacerLine) {
        if (function.params.isEmpty())
            replacer.insert("this.submit()")
    }

    fun getUserParams(): List<Parameter> {
        return function.params.filter { it.type is AssoziationTypeUser && (it.type as AssoziationTypeUser).clazz !is PrimitiveClass }
    }
}
