package de.kaipho.genplus.generator.generator.java.core

import de.kaipho.genplus.generator.common.lowerFirstLetter
import de.kaipho.genplus.generator.core.obj.service.ClassDummy
import de.kaipho.genplus.generator.core.obj.service.Function
import de.kaipho.genplus.generator.core.obj.service.Parameter
import de.kaipho.genplus.generator.domain.obj.IClass
import java.util.*

/**
 * Helper for generate java functions
 */
object Functions {

    /**
     * Creates a parameter list
     */
    fun getParams(params: ArrayList<Parameter>, extends: IClass? = null): String {
        var result = ""
        if (extends != null && extends !is ClassDummy) {
            result += "${extends.name} ${extends.name.lowerFirstLetter()}"
            if (params.isNotEmpty()) {
                result += ", "
            }
        }
        result += params.map { "${it.type.javaAbstractRep()} ${it.name}" }.joinToString()
        return result
    }


    fun getParamsForCall(params: ArrayList<Parameter>, extends: IClass? = null): String {
        var result = ""
        if (extends != null && extends !is ClassDummy) {
            result += extends.name.lowerFirstLetter()
            if (params.isNotEmpty()) {
                result += ", "
            }
        }
        result += params.map { it.name }.joinToString()
        return result
    }

    /**
     * Creates a operation header of this style: *returnType name(extends, params...);*
     */
    fun getOperationHeader(function: Function): String {
        return "${function.returnType.javaAbstractRep()} ${function.name}(${getParams(function.params, function.extends)});"
    }
}
