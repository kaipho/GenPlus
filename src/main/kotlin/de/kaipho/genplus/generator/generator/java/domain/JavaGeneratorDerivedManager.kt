package de.kaipho.genplus.generator.generator.java.domain

import de.kaipho.genplus.generator.common.lowerFirstLetter
import de.kaipho.genplus.generator.common.upperFirstLetter
import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.domain.obj.Association
import de.kaipho.genplus.generator.domain.obj.IClass
import de.kaipho.genplus.generator.generator.AbstractGenerator
import de.kaipho.genplus.generator.generator.core.File
import de.kaipho.genplus.generator.generator.core.Generator
import de.kaipho.genplus.generator.generator.core.GeneratorType
import de.kaipho.genplus.generator.generator.core.RuleHelp
import de.kaipho.genplus.generator.generator.java.service.Imports
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.store.SettingsStore
import de.kaipho.genplus.generator.template.Templates
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerImports
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLine
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerPackage
import java.util.*

@Generator(
        lang = Language.JAVA,
        type = GeneratorType.MERGE,
        file = File.JAVA_DOMAIN_DERIVED_MANAGER,
        appliedOn = ClassStore::class,
        template = Templates.JAVA_DOMAIN_DERIVED_MANAGER
)
class JavaGeneratorDerivedManager : AbstractGenerator<ClassStore>() {
    override fun visit(replacer: TemplateReplacerPackage) {
        lines.add(replacer.replaceOneLine(actual, "${SettingsStore.getPrefixPackage()}"))
    }

    override fun visit(replacer: TemplateReplacerImports) {
        val imports = Imports()

        getAllDerivedMethods().forEach { iClass, list ->
            list.forEach {
                imports.addAssociationImport(it)
            }
            imports.addDomainImport(iClass)
        }

        replacer.insertAll(imports.getimports())
    }

    override fun visit(replacer: TemplateReplacerLine) {
        getAllDerivedMethods().forEach { iClass, list ->
            list.forEach {
                replacer.insert("public ${it.type.javaAbstractRep()} derived${it.name.upperFirstLetter()}(${iClass.name} ${iClass.name.lowerFirstLetter()}) {")
                replacer.insert("   // TODO implement void toggle(...)")
                replacer.insert("   throw new RuntimeException(\"Not implemented\");")
                replacer.insert("}")
            }

        }
    }

    private fun getAllDerivedMethods(): Map<IClass, List<Association>> {
        val result = HashMap<IClass, List<Association>>()

        self().domain.domainClasses.forEach {
            val associations = it.associations.filter { it.isDerived() }.toList()
            if (associations.isNotEmpty()) {
                result.put(it, associations)
            }
        }

        return result
    }
}