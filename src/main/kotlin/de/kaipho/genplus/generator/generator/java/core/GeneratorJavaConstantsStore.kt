package de.kaipho.genplus.generator.generator.java.core

import de.kaipho.genplus.generator.common.upperFirstLetter
import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.core.obj.constants.ConstantStore
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLine
import de.kaipho.genplus.generator.generator.java.core.ExternalJavaGenerator

/**
 * Generator for the ConstantsStore
 */
class GeneratorJavaConstantsStore : ExternalJavaGenerator() {
    override val lang = Language.JAVA
    override val templateString = "core/constants/ConstantsStore.java"

    override fun visit(replacer: TemplateReplacerLine) {
        when (replacer.id) {
            0 -> insertPackageMethods(replacer)
            1 -> insertConstantClasses(replacer)
            2 -> insertPackageConstants(replacer)
            3 -> insertConstants(replacer)
            4 -> insertConstantsInPackage(replacer)
        }
    }

    private fun insertPackageMethods(replacer: TemplateReplacerLine) {
        ConstantStore.constants.keys.forEach {
            replacer.insert("public static ConstantsContainer${it.upperFirstLetter()} $it() {")
            replacer.insert("String lang = SecurityContext.getUser().getLang();", 1)
            replacer.insert("if (!constantsContainer.containsKey(lang)) {", 1)
            replacer.insert("if (!LANGS.containsKey(lang)) {", 2)
            replacer.insert("//LANGS.put(lang, Application.RepoStore.constantLanguageRepository.findOneByLanguage(lang));", 3)
            replacer.insert("}", 2)
            replacer.insert("ConstantLanguage constantLanguage = LANGS.get(lang);", 2)
            replacer.insert("constantsContainer.put(lang, new ConstantsContainer${it.upperFirstLetter()}(constantLanguage.getPackages()));", 2)
            replacer.insert("}", 1)
            replacer.insert("return (ConstantsContainer${it.upperFirstLetter()}) constantsContainer.get(lang);", 1)
            replacer.insert("}")
        }
    }


    private fun insertConstantClasses(replacer: TemplateReplacerLine) {
        ConstantStore.constants.keys.forEach { packagee ->
            replacer.insert("public static class ConstantsContainer${packagee.upperFirstLetter()} extends ConstantsContainer {")
            replacer.insert("ConstantsContainer${packagee.upperFirstLetter()}(List<ConstantPackage> constantPackage) {", 1)
            replacer.insert("super(constantPackage, ${getPackageQualifier(packagee)});", 2)
            replacer.insert("}", 1)

            val sector = ConstantStore.constants[packagee]!!
            sector.constants.keys.forEach { constant ->
                replacer.insert("public String ${constant.toUpperCase()}(${sector.getSector(constant).map { it -> "Object $it" }.joinToString()}) {", 1)
                replacer.insert("checkConstantExist(${getConstantQualifier(constant, packagee)});", 2)
                if (sector.getSector(constant).isEmpty()) {
                    replacer.insert("return getCache().get(${getConstantQualifier(constant, packagee)}).getValue();", 2)
                } else {
                    replacer.insert("return MessageFormat.format(getCache().get(${getConstantQualifier(constant, packagee)}).getValue(), ", 2)
                    replacer.insert("${sector.getSector(constant).map { it -> "$it.toString()" }.joinToString()});", 3)
                }
                replacer.insert("}", 1)
            }

            replacer.insert("}")
        }
    }

    private fun insertPackageConstants(replacer: TemplateReplacerLine) {
        ConstantStore.constants.keys.forEach {
            replacer.insert("private final static String P_${it.toUpperCase()} = \"$it\";")
        }
    }

    private fun insertConstants(replacer: TemplateReplacerLine) {
        ConstantStore.constants.keys.forEach { packagee ->
            ConstantStore.getSector(packagee).constants.keys.forEach { constant ->
                replacer.insert("private final static String C_${packagee.toUpperCase()}_${constant.toUpperCase()} = \"$constant\";")
            }
        }
    }

    private fun insertConstantsInPackage(replacer: TemplateReplacerLine) {
        ConstantStore.forEachPackage { packagee ->
            replacer.insert("case ${getPackageQualifier(packagee)}:")
            replacer.insert("return Arrays.asList(", 1)
            replacer.insert(ConstantStore.getSector(packagee).constants.keys.map {
                getConstantQualifier(it, packagee)
            }.joinToString(), 3)
            replacer.insert(");", 1)
        }
    }

    private fun getConstantQualifier(constant: String, packagee: String) = "C_${packagee.toUpperCase()}_${constant.toUpperCase()}"

    private fun getPackageQualifier(packagee: String) = "P_${packagee.toUpperCase()}"
}
