package de.kaipho.genplus.generator.generator.java.controller

import de.kaipho.genplus.generator.common.forEachAssociationWithUserType
import de.kaipho.genplus.generator.common.lowerFirstLetter
import de.kaipho.genplus.generator.common.upperFirstLetter
import de.kaipho.genplus.generator.core.obj.*
import de.kaipho.genplus.generator.domain.obj.*
import de.kaipho.genplus.generator.generator.java.core.ExternalJavaGenerator
import de.kaipho.genplus.generator.generator.java.service.Imports
import de.kaipho.genplus.generator.generator.recursiveAssociationFetcher
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerImports
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLine

class JavaGeneratorDomainRToJson() : ExternalJavaGenerator() {
    override val templateString = "controller/domainr/ToJsonVisitor.java"
    val classes = ClassStore.domain.domainClasses.filter { it is ExplicitDomainClass }

    override fun visit(replacer: TemplateReplacerImports) {
        val imports = Imports()
        classes.forEach { clazz ->
            imports.addDomainImport(clazz)
        }
        replacer.insertAll(imports.getimports())
    }

    override fun visit(replacer: TemplateReplacerLine) {
        classes.forEach { clazz ->
            val clazzname = clazz.name.lowerFirstLetter()
            replacer.insert("@Override")
            replacer.insert("public JsonObject visit(${clazz.name} $clazzname) {")
            replacer.insert("   JsonObjectBuilder obj = Json.createObjectBuilder();")
            replacer.insert("   if($clazzname.getId() != null) obj.add(\"id\", $clazzname.getId());")
            replacer.insert("   if($clazzname.getId() != null) obj.add(\"_rep\", $clazzname.toString());")
            replacer.insert("   obj.add(\"_type\", ${clazz.id});")
            replacer.insert("   obj.add(\"_self\", \"/domainr/${clazz.id}/\" + $clazzname.getId());")

            recursiveAssociationFetcher(clazz).filter {
                it.type !is AssoziationTypeUser
            }.filter {
                it.type !is AssoziationTypeCollection
            }.filter {
                it.type !is AssoziationTypeMap
            }.filter {
                it.type !is AssoziationTypeOptional && it.type !is AssociationTypeBlob
            }.forEach {
                val upperName = it.name.upperFirstLetter()
                if (it.type is AssoziationTypeDate)
                    replacer.insert("   if($clazzname.get$upperName() != null) obj.add(\"${it.name}\", $clazzname.get$upperName().format(Constants.DATE_FORMAT));")
                else
                    replacer.insert("   if($clazzname.get$upperName() != null) obj.add(\"${it.name}\", $clazzname.get$upperName());")
            }

            recursiveAssociationFetcher(clazz).filter {
                it.type is AssoziationTypeOptional && it.type.inner !is AssoziationTypeUser
            }.forEach {
                val upperName = it.name.upperFirstLetter()
                if ((it.type as AssoziationTypeOptional).inner is AssoziationTypeDate)
                    replacer.insert("   if($clazzname.get$upperName().isPresent()) obj.add(\"${it.name}\", $clazzname.get$upperName().get().format(Constants.DATE_FORMAT));")
                else
                    replacer.insert("   if($clazzname.get$upperName().isPresent()) obj.add(\"${it.name}\", $clazzname.get$upperName().get());")
            }

            recursiveAssociationFetcher(clazz).filter {
                it.type is AssociationTypeBlob
            }.forEach {
                val upperName = it.name.upperFirstLetter()
                replacer.insert("   if($clazzname.get$upperName() != null) obj.add(\"${it.name}\", visitBlob($clazzname.get$upperName()));")
            }

            recursiveAssociationFetcher(clazz).filter {
                it.type is AssoziationTypeOptional && it.type.inner is AssoziationTypeUser
            }.forEach {
                val upperName = it.name.upperFirstLetter()
                replacer.insert("   if($clazzname.get$upperName().isPresent()) obj.add(\"${it.name}\", visitPersistentElement($clazzname.get$upperName().get()));")
            }

            recursiveAssociationFetcher(clazz).forEachAssociationWithUserType { (_, _, name), iClass ->
                val upperName = name.upperFirstLetter()
                iClass.acceptClass(object : TypeVisitor<Unit> {
                    override fun visitPrimitive(clazz: PrimitiveClass) {
                        replacer.insert("   if($clazzname.get$upperName() != null) obj.add(\"$name\", visitPersistentPrimitive($clazzname.get$upperName()));")
                    }

                    override fun visitCategory(clazz: CategoryClass) {
                        replacer.insert("   if($clazzname.get$upperName() != null && !$clazzname.get$upperName().isEmpty()) obj.add(\"$name\", $clazzname.get$upperName().getValue());")
                    }

                    override fun visitDomain(clazz: DomainClass) {
                        // nothing
                    }

                    override fun visitConstant(clazz: Constant) {
                        // nothing
                    }

                    override fun visitOther(clazz: IClass) {
                        // nothing
                    }

                    override fun visitDummy(clazz: IClass) {
                        // nothing
                    }

                })
            }

            recursiveAssociationFetcher(clazz).filter {
                it.type is AssoziationTypeCollection
            }.forEach {
                val upperName = it.name.upperFirstLetter()
                val collection = it.type as AssoziationTypeCollection
                if(collection.inner is AssoziationTypeUser) {
                    collection.inner.acceptClass(object : UserClassVisitor<Unit> {
                        override fun visitPrimitive(association: AssoziationTypeUser, clazz: PrimitiveClass) {
                            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                        }

                        override fun visitCategory(association: AssoziationTypeUser, clazz: CategoryClass) {
                            replacer.insert("   JsonArrayBuilder ${it.name} = Json.createArrayBuilder();")
                            replacer.insert("   $clazzname.get$upperName().stream().map(PersistentCategory::getValue).forEach(${it.name}::add);")
                            replacer.insert("   obj.add(\"${it.name}\", ${it.name});")
                        }

                        override fun visitDomain(association: AssoziationTypeUser, clazz: DomainClass) {
                            replacer.insert("   JsonArrayBuilder ${it.name} = Json.createArrayBuilder();")
                            replacer.insert("   $clazzname.get$upperName()")
                            replacer.insert("                 .stream()")
                            replacer.insert("                 .map(this::visitPersistentElement)")
                            replacer.insert("                 .forEach(${it.name}::add);")
                            replacer.insert("   obj.add(\"${it.name}\", ${it.name});")
                        }

                        override fun visitConstant(association: AssoziationTypeUser, clazz: Constant) {
                            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                        }

                    })
                } else {
                    replacer.insert("   JsonArrayBuilder ${it.name} = Json.createArrayBuilder();")
                    replacer.insert("   $clazzname.get$upperName().forEach(${it.name}::add);")
                    replacer.insert("   obj.add(\"${it.name}\", ${it.name});")
                }
            }

            recursiveAssociationFetcher(clazz).filter {
                it.type is AssoziationTypeMap
            }.forEach {
                val upperName = it.name.upperFirstLetter()
                val collection = it.type as AssoziationTypeMap
                if(collection.value !is AssoziationTypeUser) {
                    replacer.insert("   JsonArrayBuilder ${it.name} = Json.createArrayBuilder();")
                    replacer.insert("   $clazzname.get$upperName().forEach(${it.name}::add);")
                    replacer.insert("   obj.add(\"${it.name}\", ${it.name});")
                } else {
                    replacer.insert("   JsonArrayBuilder ${it.name} = Json.createArrayBuilder();")
                    replacer.insert("   $clazzname.get$upperName().values()")
                    replacer.insert("                 .stream()")
                    replacer.insert("                 .map(this::visitPersistentElement)")
                    replacer.insert("                 .forEach(${it.name}::add);")
                    replacer.insert("   obj.add(\"${it.name}\", ${it.name});")
                }
            }

            replacer.insert("   return obj.build();")
            replacer.insert("}")
        }
    }
}
