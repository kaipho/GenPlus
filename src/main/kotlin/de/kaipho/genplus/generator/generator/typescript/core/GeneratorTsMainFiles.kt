package de.kaipho.genplus.generator.generator.typescript.core

import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.core.obj.options.Option
import de.kaipho.genplus.generator.generator.core.GeneratorType
import de.kaipho.genplus.generator.generator.typescript.ExternalTsGenerator
import de.kaipho.genplus.generator.store.SettingsStore
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerName

class GeneratorTsPackageJson : ExternalTsGenerator() {
    override val templateString = "package.json"

    override fun visit(replacer: TemplateReplacerName) {
        replacer.insert(SettingsStore.options.getFirst(Option.NAME))
    }
}

class GeneratorTsAngularCliJson : ExternalTsGenerator() {
    override val lang = Language.TYPESCRIPT
    override val templateString = "angular.json"

    override fun visit(replacer: TemplateReplacerName) {
        replacer.insert(SettingsStore.options.getFirst(Option.NAME))
    }
}

class GeneratorTsIndexHtml : ExternalTsGenerator() {
    override val lang = Language.TYPESCRIPT
    override val templateString = "src/index.html"

    override fun visit(replacer: TemplateReplacerName) {
        replacer.insert(SettingsStore.options.getFirst(Option.NAME))
    }
}

class GeneratorTsWithPath(override val templateString: String, override val mode: GeneratorType = GeneratorType.PROCESS) : ExternalTsGenerator() {
    override val lang = Language.TYPESCRIPT
}
