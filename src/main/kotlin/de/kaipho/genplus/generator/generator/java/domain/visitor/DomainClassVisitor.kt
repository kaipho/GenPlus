package de.kaipho.genplus.generator.generator.java.domain.visitor

import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.domain.obj.AbstractDomainClass
import de.kaipho.genplus.generator.generator.core.File
import de.kaipho.genplus.generator.generator.core.Generator
import de.kaipho.genplus.generator.generator.java.core.fetchAllAssociationsAndChildClasses
import de.kaipho.genplus.generator.generator.java.service.Imports
import de.kaipho.genplus.generator.template.Templates
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerImports
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLine

@Generator(
        lang = Language.JAVA,
        template = Templates.DOMAIN_CLASS_VISITOR,
        appliedOn = AbstractDomainClass::class,
        file = File.JAVA_DOMAIN_CLASS_VISITOR
)
class DomainClassVisitor : JavaGeneratorVisitor() {


    override fun visit(replacer: TemplateReplacerLine) {
        fetchAllAssociationsAndChildClasses(getData()).forEach {
            replacer.insert(visitorMethod("D", it.name))
        }
    }

    override fun visit(replacer: TemplateReplacerImports) {
        val imports = Imports()
        fetchAllAssociationsAndChildClasses(getData()).forEach {
            imports.addDomainImport(it)
        }
        replacer.insertAll(imports.getimports())
    }
}
