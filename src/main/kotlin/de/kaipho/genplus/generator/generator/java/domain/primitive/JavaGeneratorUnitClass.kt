package de.kaipho.genplus.generator.generator.java.domain.primitive

import de.kaipho.genplus.generator.domain.obj.PrimitiveClass
import de.kaipho.genplus.generator.generator.java.core.ExternalJavaGenerator
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerClassName
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerId
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLine
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLowerClassName

class JavaGeneratorUnitClass(val clazz: PrimitiveClass) : ExternalJavaGenerator() {
    override val templateString = "domain/primitive/unit/classes/xxx.java"
    override val replacer = arrayListOf(clazz.name)

    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(clazz.name)
    }

    override fun visit(replacer: TemplateReplacerLowerClassName) {
        replacer.insert(clazz.name.toLowerCase())
    }

    override fun visit(replacer: TemplateReplacerLine) {
        replacer.insert("defaultInstance = ${clazz.default!!.name}.getUnit();")
        clazz.units.forEach {
            replacer.insert("instances.add(${it.name}.getUnit());")
        }
    }

    override fun visit(replacer: TemplateReplacerId) {
        replacer.insert("${clazz.id}L")
    }
}