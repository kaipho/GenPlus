package de.kaipho.genplus.generator.generator.c_esp32.domain

import de.kaipho.genplus.generator.common.convertPackageToFileSystem
import de.kaipho.genplus.generator.common.upperFirstLetter
import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.core.obj.options.Option
import de.kaipho.genplus.generator.domain.obj.ExplicitDomainClass
import de.kaipho.genplus.generator.generator.AbstractGenerator
import de.kaipho.genplus.generator.generator.c_esp32.core.Imports
import de.kaipho.genplus.generator.generator.core.File
import de.kaipho.genplus.generator.generator.core.Generator
import de.kaipho.genplus.generator.generator.recursiveAssociationFetcher
import de.kaipho.genplus.generator.store.SettingsStore
import de.kaipho.genplus.generator.template.Templates
import de.kaipho.genplus.generator.template.replacer.*
import de.kaipho.genplus.generator.template.replacer.*


@Generator(
        lang = Language.ESP32,
        template = Templates.C_ESP32_DOMAIN_JSON_SERVICE,
        appliedOn = ExplicitDomainClass::class,
        file = File.C_ESP32_DOMAIN_JSON_SERVICE
)
class GeneratorDomainJsonService : AbstractGenerator<ExplicitDomainClass>() {
    override fun visit(replacer: TemplateReplacerUpperName) {
        replacer.insert("${SettingsStore.options.getFirst(Option.NAME)}_${getData().name}".toUpperCase())
    }

    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(getData().name)
    }

    override fun visit(replacer: TemplateReplacerLowerClassName) {
        replacer.insert(self().getLowerName())
    }

    override fun visit(replacer: TemplateReplacerImports) {
        val imports = Imports("domain${getData().prefix.convertPackageToFileSystem()}")

        imports.addDomainImport(getData())

        replacer.insertAll(imports.getimports())
    }

    override fun visit(replacer: TemplateReplacerLine) {
        when (replacer.id) {
            0 -> replacer.insert("#ifndef")
            1 -> replacer.insert("#define")
            2 -> replacer.insert("#endif")
            3 -> generateToJson(replacer)
            4 -> generateFromJson(replacer)
        }
    }

    private fun generateToJson(replacer: TemplateReplacerLine) {
        recursiveAssociationFetcher(self()).forEach {
            replacer.insert("root(\"${it.name}\", obj.get${it.name.upperFirstLetter()}());")
        }
    }

    private fun generateFromJson(replacer: TemplateReplacerLine) {
        recursiveAssociationFetcher(self()).forEach {
            replacer.insert("result.set${it.name.upperFirstLetter()}(root(\"${it.name}\"));")
        }
    }
}
