package de.kaipho.genplus.generator.generator.java.service

import de.kaipho.genplus.generator.core.obj.service.Function
import de.kaipho.genplus.generator.generator.java.service.Imports

fun getImportsForFunction(functions:List<Function>, imports: Imports) {
    functions.forEach { function ->
        imports.addDomainImport(function.extends)
        function.params.forEach {
            imports.addAssociationImport(it.type)
        }
        imports.addAssociationImport(function.returnType)
    }
}

fun getFunctionShortcut(f: Function): String {
    return "${f.owner.name.toUpperCase()}_${f.name.toUpperCase()}"
}
