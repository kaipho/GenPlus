package de.kaipho.genplus.generator.generator.typescript.domain

import de.kaipho.genplus.generator.common.convertPackageToFileSystem
import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.core.obj.AssoziationTypeCollection
import de.kaipho.genplus.generator.core.obj.AssoziationTypeMap
import de.kaipho.genplus.generator.core.obj.AssoziationTypeOptional
import de.kaipho.genplus.generator.core.obj.AssoziationTypeUser
import de.kaipho.genplus.generator.domain.obj.ExplicitDomainClass
import de.kaipho.genplus.generator.domain.obj.PrimitiveClass
import de.kaipho.genplus.generator.generator.AbstractGenerator
import de.kaipho.genplus.generator.generator.core.File
import de.kaipho.genplus.generator.generator.core.Generator
import de.kaipho.genplus.generator.generator.getInlinedAssociations
import de.kaipho.genplus.generator.generator.recursiveAssociationFetcher
import de.kaipho.genplus.generator.generator.typescript.TsPath
import de.kaipho.genplus.generator.generator.typescript.core.Imports
import de.kaipho.genplus.generator.template.Templates
import de.kaipho.genplus.generator.template.replacer.*

@Generator(
        lang = Language.TYPESCRIPT,
        template = Templates.TS_DOMAIN_CLASS,
        appliedOn = ExplicitDomainClass::class,
        file = File.TS_DOMAIN_CLASS
)
class GeneratorDomainClass : AbstractGenerator<ExplicitDomainClass>() {
    override fun visit(replacer: TemplateReplacerAssociations) {
        recursiveAssociationFetcher(meta.data).union(getInlinedAssociations(meta.data)).toList().forEach {
            if (it.type !is AssoziationTypeUser && !(it.type is AssoziationTypeOptional && it.type.inner is AssoziationTypeUser)) {
                replacer.insert("${it.name}:${it.type.typescriptRep()};")
            }
            if (it.type is AssoziationTypeUser && it.type.clazz is PrimitiveClass) {
                replacer.insert("${it.name}:any;")
            }
        }
    }

    override fun visit(replacer: TemplateReplacerImplements) {
        if (meta.data.extends.isNotEmpty()) {
            replacer.insert(meta.data.extends[0].name)
        } else {
            replacer.insert("DomainObject")
        }
    }

    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(meta.data.name)
    }

    override fun visit(replacer: TemplateReplacerComment) {
        replacer.insert(meta.data.comment)
    }

    override fun visit(replacer: TemplateReplacerImports) {
        val imports = Imports("domain${meta.data.prefix.convertPackageToFileSystem()}")

        recursiveAssociationFetcher(meta.data).union(getInlinedAssociations(meta.data)).toList().forEach {
            if (it.type is AssoziationTypeUser && it.type.clazz != self()) {
                // imports.addDomainImport(it.type.clazz)
            }
            if (it.type is AssoziationTypeCollection) {
                if (it.type.inner is AssoziationTypeUser && it.type.inner.clazz != self())
                    imports.addDomainImport(it.type.inner.clazz)
            }
            if (it.type is AssoziationTypeMap) {
                if (it.type.key is AssoziationTypeUser && it.type.key.clazz != self())
                    imports.addDomainImport(it.type.key.clazz)
                if (it.type.value is AssoziationTypeUser && it.type.value.clazz != self())
                    imports.addDomainImport(it.type.value.clazz)
            }

        }
        if (meta.data.extends.isNotEmpty()) {
            imports.addDomainImport(meta.data.extends[0])
        } else {
            imports.addImport(TsPath.DOMAIN_OBJECT)
        }
        imports.addImport(TsPath.DOMAIN_OBJECT_VISITOR)
        imports.addImport(TsPath.DOMAIN_REP_VISITOR)

        replacer.insertAll(imports.getImports())
    }
}