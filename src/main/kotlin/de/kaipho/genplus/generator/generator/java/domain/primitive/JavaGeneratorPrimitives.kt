package de.kaipho.genplus.generator.generator.java.domain.primitive

import de.kaipho.genplus.generator.generator.java.core.ExternalJavaGenerator
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.store.SettingsStore
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerImports
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLine

class JavaGeneratorPrimitives : ExternalJavaGenerator() {
    override val templateString = "service/primitive/Primitives.java"

    override fun visit(replacer: TemplateReplacerImports) {
        if (!ClassStore.primitives.isEmpty()) {
            replacer.insert("import ${SettingsStore.getPrefixPackage()}domain.primitive.unit.classes.*;")
        }
    }

    override fun visit(replacer: TemplateReplacerLine) {
        when(replacer.id) {
            0 -> {
                ClassStore.primitives.forEach {
                    replacer.insert("PRIMITIVE_CLASSES.put(${it.name}.getUnitClass().getId(), ${it.name}.getUnitClass());")
                }
            }
            1 -> {
                ClassStore.primitives.forEach {
                    replacer.insert("${it.name}.getUnitClass().getInstances().forEach(it -> PRIMITIVES.put(it.getId(), it));")
                }
            }
        }
    }
}
