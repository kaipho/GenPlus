package de.kaipho.genplus.generator.generator.java.core

import de.kaipho.genplus.generator.common.upperFirstLetter
import de.kaipho.genplus.generator.domain.obj.Association
import de.kaipho.genplus.generator.generator.ClassGenerator
import de.kaipho.genplus.generator.template.replacer.*
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerClassName
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerName
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerUpperName

/**
 * Created by tom on 17.07.16.
 */
class GetterSetterGenerator(domain: Association) : ClassGenerator<Association>(domain) {
    override fun visit(replacer: TemplateReplacerClassName) {
        replacer.insert(actual, lines, domain.type.javaAbstractRep())
    }

    override fun visit(replacer: TemplateReplacerName) {
        replacer.insert(actual, lines, domain.name)
    }

    override fun visit(replacer: TemplateReplacerUpperName) {
        replacer.insert(actual, lines, domain.name.upperFirstLetter())
    }
}
