package de.kaipho.genplus.generator.generator.java.core

import de.kaipho.genplus.generator.generator.java.service.getFunctionShortcut
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerLine

class JavaGeneratorCallable : ExternalJavaGenerator() {
    override val templateString = "core/transaction/Callable.java"

    override fun visit(replacer: TemplateReplacerLine) {
        ClassStore.services.forEach { service ->
            service.functions.forEach { function ->
                replacer.insert(",${getFunctionShortcut(function)}(${function.id})")
            }
        }
    }
}