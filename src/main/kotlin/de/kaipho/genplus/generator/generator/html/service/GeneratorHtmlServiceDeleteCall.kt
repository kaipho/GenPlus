package de.kaipho.genplus.generator.generator.html.service

import de.kaipho.genplus.generator.domain.obj.DomainClass
import de.kaipho.genplus.generator.generator.AbstractGenerator
import de.kaipho.genplus.generator.generator.i18n.JsonStore
import de.kaipho.genplus.generator.template.replacer.TemplateReplacerI18n

//@Generator(
//        lang = Language.TYPESCRIPT,
//        template = Templates.HTML_SERVICE_DELETE_COMPONENT,
//        appliedOn = DomainClass::class,
//        file = File.HTML_SERVICE_DELETE_COMPONENT
//)
class GeneratorHtmlServiceDeleteCall : AbstractGenerator<DomainClass>() {
    override fun standardAction() {
        JsonStore.add("button.delete")
        JsonStore.add("button.cancel")
        JsonStore.add("button.done")
    }

    override fun visit(replacer: TemplateReplacerI18n) {
        replacer.insert("functions${getData().prefix}.delete${getData().name}.title")
    }
}