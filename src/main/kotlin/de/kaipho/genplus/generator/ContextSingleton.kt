package de.kaipho.genplus.generator

/**
 * Annotation to mark a context item as resettable.
 * The reset Operation is called, after the generator finished a run.
 */
interface ContextSingleton {
    /**
     * Should clear all fields in the singleton.
     */
    fun reset()
}