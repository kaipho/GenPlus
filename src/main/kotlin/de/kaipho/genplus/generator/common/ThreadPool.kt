package de.kaipho.genplus.generator.common

import java.util.*

/**
 * Created by tom on 22.07.16.
 */
object ThreadPool {
    val threads: MutableList<Thread> = ArrayList()

    fun add(thread: Thread) {
        synchronized(this) {
            threads.add(thread)
        }
    }

    fun waitForAll() {
        threads.forEach(Thread::join)
    }
}