package de.kaipho.genplus.generator.common

/**
 * Erweiterungen für Strings
 */

fun String.upperFirstLetter(): String {
    return this.substring(0, 1).toUpperCase() + this.substring(1)
}

fun String.lowerFirstLetter(): String {
    return this.substring(0, 1).toLowerCase() + this.substring(1)
}

fun String.convertPackageToFileSystem(): String {
    return this.replace(".", "/")
}
