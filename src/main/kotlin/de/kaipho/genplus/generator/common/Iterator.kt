package de.kaipho.genplus.generator.common

import de.kaipho.genplus.generator.core.obj.AssoziationType
import de.kaipho.genplus.generator.core.obj.AssoziationTypeCollection
import de.kaipho.genplus.generator.core.obj.AssoziationTypeUser
import de.kaipho.genplus.generator.domain.obj.Association
import de.kaipho.genplus.generator.domain.obj.IClass

/**
 * Created by tom on 21.07.16.
 */

fun Iterator<String>.iterateUntil(lineContains: String, inline: (String) -> Unit) {
    while (this.hasNext()) {
        val next = this.next()
        if (next.contains(lineContains)) {
            break
        }
        inline(next)
    }
}

fun List<Association>.forEachAssociationWithUserType(inline: (Association, IClass) -> Unit) {
    this.forEach {
        if (it.type is AssoziationTypeUser) {
            inline.invoke(it, it.type.clazz)
        }
    }
}
fun List<Association>.forEachAssociationCollection(inline: (Association, AssoziationType) -> Unit) {
    this.forEach {
        if (it.type is AssoziationTypeCollection) {
            inline.invoke(it, it.type.inner)
        }
    }
}