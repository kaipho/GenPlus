package de.kaipho.genplus.generator.commands

import com.google.common.base.Stopwatch
import de.kaipho.genplus.generator.ContextSingleton
import de.kaipho.genplus.generator.constants.Reflection
import de.kaipho.genplus.generator.core.parser.MainParser
import de.kaipho.genplus.generator.core.parser.ParserException
import de.kaipho.genplus.generator.core.parser.Postprocessor
import de.kaipho.genplus.generator.core.parser.PostprocessorException
import de.kaipho.genplus.generator.core.scanner.Scanner
import de.kaipho.genplus.generator.io.FileSystem
import java.util.concurrent.TimeUnit

abstract class Command {
    fun execute(path: String) {
        val timer = Stopwatch.createStarted()

        Reflection.forEachClazzInstance(ContextSingleton::class.java) {
            it.reset()
        }

        executeImpl(path)

        println("Runtime -> ${timer.stop().elapsed(TimeUnit.MILLISECONDS)}ms")
    }

    protected abstract fun executeImpl(path: String)
}

annotation class GenCommand (
        val command: String,
        val shortcut: String,
        val description: String
)

internal fun scanFiles(files: List<Pair<String, String>>): MainParser {
    println("Models found: ")
    val parser = MainParser()
    files.forEach {
        try {
            val scanner = Scanner(it.second)
            scanner.scan()
            parser.parse(scanner.tokens)
            println(" - ${it.first} (Syntax OK)")
        } catch (ex: ParserException) {
            System.err.println("\u001B[31m - ${it.first} (${ex.message})\u001B[0m")
        }
    }
    try {
        Postprocessor.process()
    } catch (ex: PostprocessorException) {
        ex.printStackTrace()
        System.err.println("\u001B[31m - ${ex.message}\u001B[0m")
    }
    return parser
}