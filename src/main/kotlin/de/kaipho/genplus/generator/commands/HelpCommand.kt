package de.kaipho.genplus.generator.commands

import de.kaipho.genplus.Main
import de.kaipho.genplus.generator.constants.Reflection
import de.kaipho.genplus.generator.core.parser.MainParser
import de.kaipho.genplus.generator.core.parser.ParserException
import de.kaipho.genplus.generator.core.parser.Postprocessor
import de.kaipho.genplus.generator.core.parser.PostprocessorException
import de.kaipho.genplus.generator.core.scanner.Scanner
import de.kaipho.genplus.generator.io.FileSystem

@GenCommand("help", "h", "prints the version")
class HelpCommand : Command() {
    override fun executeImpl(path: String) {
        Reflection.forEachClazzInstance(Reflection.COMMANDS, Command::class.java) { _, annotation ->
            println("   ${annotation.command} | ${annotation.shortcut}: ${annotation.description}")
        }
    }
}