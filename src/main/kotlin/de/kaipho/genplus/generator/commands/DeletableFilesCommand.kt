package de.kaipho.genplus.generator.commands

import com.google.common.base.Stopwatch
import de.kaipho.genplus.generator.ContextSingleton
import de.kaipho.genplus.generator.common.ThreadPool
import de.kaipho.genplus.generator.constants.Reflection
import de.kaipho.genplus.generator.core.obj.AstElement
import de.kaipho.genplus.generator.core.obj.AstElementVisitorImpl
import de.kaipho.genplus.generator.core.obj.constants.ConstantStore
import de.kaipho.genplus.generator.core.parser.MainParser
import de.kaipho.genplus.generator.core.parser.ParserException
import de.kaipho.genplus.generator.core.parser.Postprocessor
import de.kaipho.genplus.generator.core.parser.PostprocessorException
import de.kaipho.genplus.generator.core.scanner.Scanner
import de.kaipho.genplus.generator.generator.GeneratorFassade
import de.kaipho.genplus.generator.generator.core.FileToCopy
import de.kaipho.genplus.generator.generator.core.GenerationPool
import de.kaipho.genplus.generator.generator.core.Ref
import de.kaipho.genplus.generator.generator.core.copyFiles
import de.kaipho.genplus.generator.generator.i18n.JsonStore
import de.kaipho.genplus.generator.io.AbstractFileSystem
import de.kaipho.genplus.generator.io.FileSystem
import de.kaipho.genplus.generator.io.MemoryFileSystem
import de.kaipho.genplus.generator.io.loader.FileStore
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.store.SettingsStore
import org.reflections.ReflectionUtils
import java.io.IOException
import java.nio.file.*
import java.nio.file.attribute.BasicFileAttributes
import java.util.*
import java.util.function.Function
import kotlin.reflect.KClass

@GenCommand("deletable", "dc", "shows all files, which will not regenerate")
class DeletableFilesCommand : Command() {

    override fun executeImpl(path: String) {
        JsonStore.load()

        val files = FileSystem.loadModels(path)
        val parser = scanFiles(files)
        val fileSystem = MemoryFileSystem()

        ClassStore.accept(AstGenerateInMemoryVisitor(fileSystem))
        copyFiles(FileToCopy.values(), fileSystem)

        GeneratorFassade.startManager()
        GenerationPool.runGenerators(fileSystem)
        ThreadPool.waitForAll()

        var existingFiles = HashSet<String>()

        Files.walkFileTree(Paths.get(SettingsStore.runningPath + "/server"), object : FileVisitor<Path> {
            override fun preVisitDirectory(dir: Path?, attrs: BasicFileAttributes?) = FileVisitResult.CONTINUE
            override fun visitFileFailed(file: Path?, exc: IOException?) = FileVisitResult.CONTINUE
            override fun postVisitDirectory(dir: Path?, exc: IOException?) = FileVisitResult.CONTINUE

            override fun visitFile(file: Path?, attrs: BasicFileAttributes?): FileVisitResult {
                if (isFileIgnored(file!!.toString().replace("\\","/"))) {
                    return FileVisitResult.CONTINUE
                }
                if (!file.toFile().isDirectory) {
                    existingFiles.add(file.toString().replace("\\","/"))
                }
                return FileVisitResult.CONTINUE
            }
        })
        Files.walkFileTree(Paths.get(SettingsStore.runningPath + "/web/src"), object : FileVisitor<Path> {
            override fun preVisitDirectory(dir: Path?, attrs: BasicFileAttributes?) = FileVisitResult.CONTINUE
            override fun visitFileFailed(file: Path?, exc: IOException?) = FileVisitResult.CONTINUE
            override fun postVisitDirectory(dir: Path?, exc: IOException?) = FileVisitResult.CONTINUE

            override fun visitFile(file: Path?, attrs: BasicFileAttributes?): FileVisitResult {
                if (isFileIgnored(file!!.toString().replace("\\","/"))) {
                    return FileVisitResult.CONTINUE
                }
                if (!file.toFile().isDirectory.and(!file.toString().contains("."))) {
                    existingFiles.add(file.toString().replace("\\","/"))
                }
                return FileVisitResult.CONTINUE
            }
        })
        fileSystem.files.keys.forEach { key ->
            existingFiles.remove(key)
        }
        existingFiles = existingFiles.filter { !it.contains(".DS_Store") }.toHashSet()
        val prettyPrint = HashMap<String, MutableList<String>>()
        println("Files are in folder: ${SettingsStore.runningPath}")
        existingFiles.forEach {
            val new = it.replace(SettingsStore.runningPath, "").removePrefix("/")
            var index = new.indexOf("/")

            if (!prettyPrint.contains(new.substring(0, index))) {
                prettyPrint.put(new.substring(0, index), ArrayList())
            }
            prettyPrint[new.substring(0, index)]!!.add(new.substring(index))
        }
        prettyPrint.keys.forEach {
            println(" -- " + it)
            prettyPrint[it]?.sorted()?.forEach {
                println("      + " + it)
            }
        }
        println("-> " + existingFiles.size)
    }

    fun isFileIgnored(s: String): Boolean {
        val contains = arrayListOf(
                "/target/",
                "backup",
                "node_modules",
                "/mvnw",
                "/.mvn/",
                "/resources/static/",
                "/.idea",
                "/src/test/",
                "/.classpath",
                "/.settings/",
                "/.project",
                "/src/assets/i18n/"
        )
        val endsWith = arrayListOf(
                ".iml",
                ".ipr",
                ".iws",
                "logo.png",
                "colors.scss",
                "Dockerfile",
                "docker.sh",
                ".log"
        )
        contains.forEach {
            if (s.contains(it)) {
                return true
            }
        }
        endsWith.forEach {
            if (s.endsWith(it)) {
                return true
            }
        }
        return false
    }

    class AstGenerateInMemoryVisitor(val fileSystem: AbstractFileSystem) : AstElementVisitorImpl() {
        override fun <D : AstElement> handle(clazz: KClass<out AstElement>, data: D) {
            ReflectionUtils.getAllSuperTypes(clazz.java).plus(clazz.java).forEach {
                GeneratorFassade.generators[it.name]?.forEach {
                    if ((it.second.dependsOn == Ref.NOTHING || it.second.dependsOn.method.call(data)) &&
                            (it.second.dependsOnNot == Ref.NOTHING || !it.second.dependsOnNot.method.call(data))) {
                        val instance = it.first.copyInstance(data)

                        GenerationPool.callGenerator(it.second, instance, fileSystem)
                    }
                }
            }
        }
    }
}
