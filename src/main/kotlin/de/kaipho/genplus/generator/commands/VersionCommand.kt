package de.kaipho.genplus.generator.commands

import de.kaipho.genplus.Main
import de.kaipho.genplus.generator.core.parser.MainParser
import de.kaipho.genplus.generator.core.parser.ParserException
import de.kaipho.genplus.generator.core.parser.Postprocessor
import de.kaipho.genplus.generator.core.parser.PostprocessorException
import de.kaipho.genplus.generator.core.scanner.Scanner
import de.kaipho.genplus.generator.io.FileSystem

@GenCommand("version", "v", "prints the version")
class VersionCommand : Command() {
    override fun executeImpl(path: String) {
        println("   generator: 0.3.0")
        println("   metamodel: 0.3.0")
        println("   to update use the 'update' command")
    }
}