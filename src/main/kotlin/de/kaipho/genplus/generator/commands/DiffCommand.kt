package de.kaipho.genplus.generator.commands

import com.google.common.base.Stopwatch
import de.kaipho.genplus.generator.ContextSingleton
import de.kaipho.genplus.generator.common.ThreadPool
import de.kaipho.genplus.generator.constants.Reflection
import de.kaipho.genplus.generator.core.obj.AstElement
import de.kaipho.genplus.generator.core.obj.AstElementVisitorImpl
import de.kaipho.genplus.generator.core.obj.constants.ConstantStore
import de.kaipho.genplus.generator.core.parser.MainParser
import de.kaipho.genplus.generator.core.parser.ParserException
import de.kaipho.genplus.generator.core.parser.Postprocessor
import de.kaipho.genplus.generator.core.parser.PostprocessorException
import de.kaipho.genplus.generator.core.scanner.Scanner
import de.kaipho.genplus.generator.generator.GeneratorFassade
import de.kaipho.genplus.generator.generator.core.FileToCopy
import de.kaipho.genplus.generator.generator.core.GenerationPool
import de.kaipho.genplus.generator.generator.core.Ref
import de.kaipho.genplus.generator.generator.core.copyFiles
import de.kaipho.genplus.generator.generator.i18n.JsonStore
import de.kaipho.genplus.generator.io.AbstractFileSystem
import de.kaipho.genplus.generator.io.FileSystem
import de.kaipho.genplus.generator.io.MemoryFileSystem
import de.kaipho.genplus.generator.io.loader.FileStore
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.store.SettingsStore
import org.reflections.ReflectionUtils
import java.io.IOException
import java.nio.file.*
import java.nio.file.attribute.BasicFileAttributes
import java.util.*
import java.util.function.Function
import kotlin.reflect.KClass

@GenCommand("diff", "-", "shows files, which will have different content after new generation")
class DiffCommand : Command() {

    override fun executeImpl(path: String) {
        JsonStore.load()

        val files = FileSystem.loadModels(path)
        val parser = scanFiles(files)

        val fileSystem = MemoryFileSystem()

        ClassStore.accept(AstGenerateInMemoryVisitor(fileSystem))
        copyFiles(FileToCopy.values(), fileSystem)

        GeneratorFassade.startManager()
        GenerationPool.runGenerators(fileSystem)
        ThreadPool.waitForAll()

        var existingFiles = HashSet<String>()

        Files.walkFileTree(Paths.get(SettingsStore.runningPath + "/server"), object : FileVisitor<Path> {
            override fun preVisitDirectory(dir: Path?, attrs: BasicFileAttributes?) = FileVisitResult.CONTINUE
            override fun visitFileFailed(file: Path?, exc: IOException?) = FileVisitResult.CONTINUE
            override fun postVisitDirectory(dir: Path?, exc: IOException?) = FileVisitResult.CONTINUE

            override fun visitFile(file: Path?, attrs: BasicFileAttributes?): FileVisitResult {
                if (isFileIgnored(file!!.toString().replace("\\","/"))) {
                    return FileVisitResult.CONTINUE
                }
                if (!file.toFile().isDirectory) {
                    existingFiles.add(file.toString().replace("\\","/"))
                }
                return FileVisitResult.CONTINUE
            }
        })
        Files.walkFileTree(Paths.get(SettingsStore.runningPath + "/web/src"), object : FileVisitor<Path> {
            override fun preVisitDirectory(dir: Path?, attrs: BasicFileAttributes?) = FileVisitResult.CONTINUE
            override fun visitFileFailed(file: Path?, exc: IOException?) = FileVisitResult.CONTINUE
            override fun postVisitDirectory(dir: Path?, exc: IOException?) = FileVisitResult.CONTINUE

            override fun visitFile(file: Path?, attrs: BasicFileAttributes?): FileVisitResult {
                if (isFileIgnored(file!!.toString().replace("\\","/"))) {
                    return FileVisitResult.CONTINUE
                }
                if (!file.toFile().isDirectory.and(!file.toString().contains("."))) {
                    existingFiles.add(file.toString().replace("\\","/"))
                }
                return FileVisitResult.CONTINUE
            }
        })
        fileSystem.files.forEach { key, value ->
            val path = Paths.get(key)
            if(Files.exists(path)) {
                if (Files.readAllLines(path) != value) {
                    println(key)
                }
            }
        }
    }

    fun isFileIgnored(s: String): Boolean {
        val contains = arrayListOf(
                "/target/",
                "backup",
                "node_modules",
                "/mvnw",
                "/.mvn/",
                "/resources/static/",
                "/.idea",
                "/src/test/",
                "/.classpath",
                "/.settings/",
                "/.project"
        )
        val endsWith = arrayListOf(
                ".iml",
                ".ipr",
                ".iws",
                "de.json",
                "logo.png",
                "colors.scss",
                "Dockerfile",
                "docker.sh",
                ".log"
        )
        contains.forEach {
            if (s.contains(it)) {
                return true
            }
        }
        endsWith.forEach {
            if (s.endsWith(it)) {
                return true
            }
        }
        return false
    }

    class AstGenerateInMemoryVisitor(val fileSystem: AbstractFileSystem) : AstElementVisitorImpl() {
        override fun <D : AstElement> handle(clazz: KClass<out AstElement>, data: D) {
            ReflectionUtils.getAllSuperTypes(clazz.java).plus(clazz.java).forEach {
                GeneratorFassade.generators[it.name]?.forEach {
                    if ((it.second.dependsOn == Ref.NOTHING || it.second.dependsOn.method.call(data)) &&
                            (it.second.dependsOnNot == Ref.NOTHING || !it.second.dependsOnNot.method.call(data))) {
                        val instance = it.first.copyInstance(data)

                        GenerationPool.callGenerator(it.second, instance, fileSystem)
                    }
                }
            }
        }
    }
}
