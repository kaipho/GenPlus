package de.kaipho.genplus.generator.commands

import de.kaipho.genplus.generator.core.parser.MainParser
import de.kaipho.genplus.generator.core.parser.ParserException
import de.kaipho.genplus.generator.core.parser.Postprocessor
import de.kaipho.genplus.generator.core.parser.PostprocessorException
import de.kaipho.genplus.generator.core.scanner.Scanner
import de.kaipho.genplus.generator.io.FileSystem

@GenCommand("check", "c", "check the model")
class CheckCommand : Command() {
    override fun executeImpl(path: String) {
        val files = FileSystem.loadModels(path)
        scanFiles(files)
    }
}