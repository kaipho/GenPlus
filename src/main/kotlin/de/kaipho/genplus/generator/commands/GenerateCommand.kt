package de.kaipho.genplus.generator.commands

import com.google.common.base.Stopwatch
import de.kaipho.genplus.generator.ContextSingleton
import de.kaipho.genplus.generator.Model
import de.kaipho.genplus.generator.common.ThreadPool
import de.kaipho.genplus.generator.constants.Reflection
import de.kaipho.genplus.generator.core.obj.constants.ConstantStore
import de.kaipho.genplus.generator.core.parser.MainParser
import de.kaipho.genplus.generator.core.parser.Postprocessor
import de.kaipho.genplus.generator.core.scanner.Scanner
import de.kaipho.genplus.generator.generator.GeneratorFassade
import de.kaipho.genplus.generator.generator.core.GenerationPool
import de.kaipho.genplus.generator.generator.i18n.JsonStore
import de.kaipho.genplus.generator.generator.java.persistence.ClassIdGenerator
import de.kaipho.genplus.generator.io.FileSystem
import de.kaipho.genplus.generator.io.loader.FileStore
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.store.SettingsStore
import java.util.concurrent.TimeUnit

@GenCommand("generate", "g", "generate files for the model")
class GenerateCommand : Command() {

    override fun executeImpl(path: String) {
        FileSystem.checkFolderStructure(path)


        val files = Model.loader.invoke()
        val parser = scanFiles(files)

        JsonStore.load()

        GeneratorFassade.generate(path)
        GeneratorFassade.startManager()
        GenerationPool.runGenerators(FileSystem)
        ThreadPool.waitForAll()
        JsonStore.finalize()
        ClassIdGenerator.finalize()
    }

}