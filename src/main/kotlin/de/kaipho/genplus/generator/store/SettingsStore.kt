package de.kaipho.genplus.generator.store

import de.kaipho.genplus.generator.ContextSingleton
import de.kaipho.genplus.generator.core.obj.AstElement
import de.kaipho.genplus.generator.core.obj.AstElementVisitor
import de.kaipho.genplus.generator.core.obj.options.IOptions
import de.kaipho.genplus.generator.core.obj.options.Option
import de.kaipho.genplus.generator.core.obj.options.Options

/**
 * Created by tom on 15.07.16.
 */
object SettingsStore : AstElement, ContextSingleton {
    override fun accept(visitor: AstElementVisitor) {
        visitor.visit(this)
    }

    override fun reset() {
        options.reset()
    }

    var runningPath = "./"

    val options: IOptions = Options()

    fun getPrefixPackage():String {
        return options.getFirst(Option.PREFIX) + "."
    }

    fun getPrefixFileSystem():String {
        return (options.getFirst(Option.PREFIX)  + ".").replace(".", "/")
    }

    fun securityEnabled():Boolean {
        return options.given(Option.SECURITY_MODE)
    }

    fun databaseEnabled():Boolean {
        return options.given(Option.DATABASE)
    }
}