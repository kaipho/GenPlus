package de.kaipho.genplus.generator.store

import de.kaipho.genplus.generator.ContextSingleton
import de.kaipho.genplus.generator.core.obj.AssoziationTypeUser
import de.kaipho.genplus.generator.core.obj.AstElement
import de.kaipho.genplus.generator.core.obj.AstElementVisitor
import de.kaipho.genplus.generator.core.obj.service.ClassDummy
import de.kaipho.genplus.generator.core.obj.service.Function
import de.kaipho.genplus.generator.core.obj.service.ServiceClass
import de.kaipho.genplus.generator.domain.obj.*
import java.util.*
import kotlin.collections.HashMap

/**
 * Store for all classes (e.g. Domain, Service, ...)
 */
object ClassStore : AstElement, ContextSingleton {
    val services = ArrayList<ServiceClass>()
    val primitives = ArrayList<PrimitiveClass>()
    val categories = ArrayList<CategoryClass>()
    val constants = ArrayList<Constant>()
    var domain: Domain = Domain()

    var functionIndex = HashMap<String, Function>()
    var createFunctions = HashMap<String, Function>()

    val all = ArrayList<DomainClass>()

    fun ServiceClass.addFunction(f: Function) {
        f.owner = this
        val index = f.getQualifiedClassName()

        if (functionIndex.contains(index)) {
            throw FunctionUniquesViolatedException("Uniques for function ${f.name} is violated!")
        }

        if (f.isCreateOperation()) {
            processCreateFunction(f)
        }
        functionIndex.put(index, f)
        this.functions.add(f)
    }

    fun getAllCreateFunctionsFor(clazz: DomainClass) : List<Function> {
        return createFunctions.values.filter { (it.returnType as AssoziationTypeUser).clazz.isSubClass(clazz) }
    }

    fun processCreateFunction(f: Function) {
        val nameWithoutCreate = f.name.substring(6)
        if(nameWithoutCreate != f.returnType.name) {
            throw FunctionUniquesViolatedException("Create Function '${f.name}' do not match name convention <returnType> create<returnType>(...)!")
        }
        if(f.extends !is ClassDummy) {
            throw FunctionUniquesViolatedException("Create Function '${f.name}' do not match name convention <returnType> create<returnType>(...)!")
        }
        if(createFunctions.contains(nameWithoutCreate)) {
            throw FunctionUniquesViolatedException("Only one create Function per type allowed!")
        }
        createFunctions.put(nameWithoutCreate, f)
    }


    init {
        reset()
    }

    /**
     * removes all data
     */
    override fun reset() {
        all.clear()

        domain = Domain()
        services.clear()
        primitives.clear()
        categories.clear()
        constants.clear()

        functionIndex.clear()
        createFunctions.clear()
    }

    override fun accept(visitor: AstElementVisitor) {
        visitor.visit(this)
        domain.accept(visitor)
        primitives.forEach {
            it.accept(visitor)
        }
        categories.forEach {
            it.accept(visitor)
        }
        constants.forEach {
            it.accept(visitor)
        }
        services.forEach {
            it.accept(visitor)
        }
    }

    fun getByName(name: String): DomainClass {
        if (all.isEmpty()) {
            all.addAll(domain.domainClasses)
            all.addAll(primitives)
            all.addAll(categories)
            all.addAll(constants)
        }
        val result = all.filter { it.name == name }
        if (result.isEmpty()) {
            throw RuntimeException("no class for $name found")
        } else if (result.size > 1) {
            throw RuntimeException("more than 1 class for $name found")
        }
        return result[0]
    }

    fun getAllClasses(): List<DomainClass> {
        if (all.isEmpty()) {
            all.addAll(domain.domainClasses)
            all.addAll(primitives)
            all.addAll(categories)
            all.addAll(constants)
        }
        return all
    }

    fun getClassOrPrimitiveByName(name: String): DomainClass {
        val result = domain.domainClasses.filter { it.name == name }
        if (result.isEmpty()) {
            val primitives = primitives.filter { it.name == name }
            if (primitives.isEmpty()) {
                throw RuntimeException("no class for $name found")
            } else if (primitives.size > 1) {
                throw RuntimeException("more than 1 class for $name found")
            }
            return primitives[0]
        } else if (result.size > 1) {
            throw RuntimeException("more than 1 class for $name found")
        }
        return result[0]
    }

    fun getAllNotAbstractDomainClasses(): List<ExplicitDomainClass> {
        return domain.domainClasses.filter { it is ExplicitDomainClass }.map { it as ExplicitDomainClass }
    }

    fun getAllAbstractDomainClasses(): List<AbstractDomainClass> {
        return domain.domainClasses.filter { it is AbstractDomainClass }.map { it as AbstractDomainClass }
    }

    fun getAllFunctionExtensionsFor(clazz: IClass): List<Function> {
        return services.map { it.functions }.flatten().filter { isEqualOrSubclass(clazz, it.extends) }
    }

    fun isEqualOrSubclass(clazz: IClass, superclazz: IClass): Boolean {
        return clazz.name == superclazz.name || clazz.extends.map { isEqualOrSubclass(it, superclazz) }.contains(true)
    }

    fun getDomainClassByName(name: String) : DomainClass {
        return domain.domainClasses.find { it.name == name}!!
    }
}
