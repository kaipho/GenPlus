package de.kaipho.genplus.generator.store

class FunctionUniquesViolatedException : RuntimeException {
    constructor(message: String?) : super(message)
}