package de.kaipho.genplus.generator.parser

import de.kaipho.genplus.generator.domain.obj.ConstantClass
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus_vscode.genplus.Constant
import de.kaipho.genplus_vscode.genplus.ConstantDeclaration
import de.kaipho.genplus_vscode.genplus.Model

object ConstantParser : AntlrParser<Model, Unit> {

    override fun parse(root: Model) {
        root.members
                .filter { it is ConstantDeclaration }
                .forEach { parseConstantDeclaration(it as ConstantDeclaration) }
    }

    private fun parseConstantDeclaration(constantDeclaration: ConstantDeclaration) {
        constantDeclaration.members.forEach {
            if (it is Constant) {
                parseConstant(it)
            }
        }
    }

    private fun parseConstant(constant: Constant) {
        val constantName = constant.name
        val result = ConstantClass(constantName)

        constant.params.forEach {
            result.parameter.add(it)
        }

        ClassStore.constants.add(result)
    }
}
