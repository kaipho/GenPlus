package de.kaipho.genplus.generator.parser

import de.kaipho.genplus_vscode.GenplusStandaloneSetup
import de.kaipho.genplus_vscode.genplus.Model
import org.eclipse.emf.common.util.URI
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.xtext.resource.XtextResourceSet
import org.eclipse.xtext.util.CancelIndicator
import org.eclipse.xtext.validation.CheckMode


object ParserFacade {


    fun parseFilesToClassStore(files: List<String>) {

        val injector = GenplusStandaloneSetup().createInjectorAndDoEMFRegistration()
        val resourceSet = injector.getInstance(XtextResourceSet::class.java)

        for (file in files) {
            val resource = resourceSet.getResource(URI.createFileURI(file), true)
            val parseResult = (resource as XtextResource).parseResult
            val root = parseResult.rootASTElement as Model

            ConstantParser.parse(root)
            DomainParser.parse(root)
        }
    }

    fun validateFiles(files: List<String>) : Map<String, List<String>> {
        val result = HashMap<String, List<String>>()

        val injector = GenplusStandaloneSetup().createInjectorAndDoEMFRegistration()
        val resourceSet = injector.getInstance(XtextResourceSet::class.java)

        for (file in files) {
            val resource = resourceSet.getResource(URI.createFileURI(file), true)
            val validator = (resource as XtextResource).resourceServiceProvider.resourceValidator
            val issues = validator.validate(resource, CheckMode.ALL, CancelIndicator.NullImpl)
            result[file] = issues.map { it.message }
        }
        return result
    }

}
