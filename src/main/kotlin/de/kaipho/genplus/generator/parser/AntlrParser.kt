package de.kaipho.genplus.generator.parser

interface AntlrParser<P, R> {

    fun parse(root: P) : R

}
