package de.kaipho.genplus.generator.parser

import de.kaipho.genplus_vscode.genplus.Path

/**
 * Parser für den Domain-Block
 * domain {
 *      types...
 * }
 */
object PathParser : AntlrParser<Path?, String> {

    override fun parse(root: Path?): String {
        if (root == null) return ""
        return root.parts.joinToString(separator = ".")
    }
}
