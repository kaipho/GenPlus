package de.kaipho.genplus.generator.parser

import de.kaipho.genplus.generator.domain.obj.AbstractDomainClass
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus_vscode.genplus.DomainDeclaration
import de.kaipho.genplus_vscode.genplus.Model
import de.kaipho.genplus_vscode.genplus.TypeDeclaration

/**
 * Parser für den Domain-Block
 * domain {
 *      types...
 * }
 */
object DomainParser : AntlrParser<Model, Unit> {

    override fun parse(root: Model) {
        root.members
                .filter { it is DomainDeclaration }
                .forEach { parseDomainDeclaration(it as DomainDeclaration) }
    }

    private fun parseDomainDeclaration(domainDeclaration: DomainDeclaration) {
        domainDeclaration.types.forEach {
            parseTypeDeclaration(it)
        }
    }

    private fun parseTypeDeclaration(typeDeclaration: TypeDeclaration) {
        val isAbstract = "abstract" == typeDeclaration.modifier
        val path = PathParser.parse(typeDeclaration.path)

        if (isAbstract) {
            buildAbstractDomainClass(
                    typeDeclaration.name,
                    typeDeclaration.comment,
                    path
            )
        }

    }

    private fun buildAbstractDomainClass(name: String, comment: String?, path: String?) {
        val domainClass = AbstractDomainClass()

        if (comment != null)
            domainClass.comment = comment
        if (path != null)
            domainClass.prefix = path
        domainClass.name = name

        ClassStore.domain.domainClasses.add(domainClass)
    }
}
