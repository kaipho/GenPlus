package de.kaipho.genplus.generator.domain.obj

import de.kaipho.genplus.generator.common.convertPackageToFileSystem
import de.kaipho.genplus.generator.common.lowerFirstLetter
import de.kaipho.genplus.generator.core.obj.*
import de.kaipho.genplus.generator.core.obj.UserClassVisitor
import de.kaipho.genplus.generator.core.obj.service.Function
import de.kaipho.genplus.generator.generator.java.persistence.ClassIdGenerator
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.store.SettingsStore
import java.util.*
import kotlin.collections.ArrayList

data class Domain(
        val domainClasses: MutableList<DomainClass> = ArrayList()
) : AstElement {
    override fun accept(visitor: AstElementVisitor) {
        visitor.visit(this)
        getInternalDomainClasses().forEach {
            it.accept(visitor)
        }
    }

    fun getInternalDomainClasses(): List<DomainClass> {
        return domainClasses.filter { it !is ExternalClass }
    }

    override fun toString(): String {
        return "#domain { $domainClasses \n }"
    }
}

interface IClass : AstElementWithRules {
    var comment: String
    var prefix: String
    var name: String
    var export: Boolean
    val extends: MutableList<AbstractClass>

    var id: Int

    fun hasDetailsPage(): Boolean
    fun isLocal(): Boolean

    fun exportRecursive(): Boolean {
        if (export) return true
        extends.forEach { if (it.exportRecursive()) return true }
        return false
    }

    /**
     * return true, if superClazz == this or this extends superClazz.
     */
    fun isSubClass(superClazz: IClass): Boolean {
        if(this.id == superClazz.id) {
            return true
        }
        return !extends.filter { it.isSubClass(superClazz) }.isEmpty()
    }

    fun <D> acceptClass(visitor: TypeVisitor<D>): D
}

fun IClass.isExportRecursive(): Boolean {
    return this.export || this.extends.map { it.export }.contains(true)
}

interface DomainClass : IClass, AstElement {
    val associations: MutableList<Association>
    var repository: DomainRepository
    var createOperation: Function?

    fun getAllCreateFunctions() : List<Function> {
        return ClassStore.getAllCreateFunctionsFor(this)
    }
}

abstract class ClassImpl(
        override var comment: String = "",
        override var prefix: String = "",
        override var name: String = "",
        override var export: Boolean = false,
        override val extends: MutableList<AbstractClass> = ArrayList(),
        override val rules: MutableList<RuleContainer> = ArrayList(),
        override var id: Int = -1
) : AstElement, IClass {
    fun getFilePrefix(): String {
        return prefix.convertPackageToFileSystem()
    }

    fun getLowerName(): String {
        return name.lowerFirstLetter()
    }

    override fun isLocal(): Boolean {
        return rules.filter { it is LocalRule }.isNotEmpty()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false

        other as ClassImpl

        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }

    override fun toString(): String {
        return this.name
    }

    // TODO WORKAROUND
    fun databaseEnabled(): Boolean {
        return SettingsStore.databaseEnabled()
    }
}

abstract class AbstractClass : ClassImpl() {
    val childClasses: MutableSet<IClass> = HashSet()

}

/**
 * Class to be replaced after parsing
 */
class AbstractClassDummy : AbstractClass() {
    override fun <D> acceptClass(visitor: TypeVisitor<D>): D {
        return visitor.visitDummy(this)
    }

    override fun hasDetailsPage(): Boolean = false

    override fun accept(visitor: AstElementVisitor) {
        visitor.visit(this)
    }
}

interface TypeVisitor<out R> {
    fun visitPrimitive(clazz: PrimitiveClass): R
    fun visitCategory(clazz: CategoryClass): R
    fun visitDomain(clazz: DomainClass): R
    fun visitConstant(clazz: Constant): R
    fun visitOther(clazz: IClass): R
    fun visitDummy(clazz: IClass): R
}

abstract class Type(
        override var name: String,
        override var id: Int
) : AbstractClass(), DomainClass {
    override val associations: MutableList<Association> = ArrayList()
    override var repository: DomainRepository = DomainRepository()
    override var createOperation: Function? = null

    override fun hasDetailsPage(): Boolean = false
}


// Categories / Observations
open class CategoryClass(
        name: String,
        id: Int = ClassIdGenerator.getIdForClass(name)
) : Type(name, id) {
    var hierarchy: MutableList<CategoryClass> = ArrayList()

    override fun <D> acceptClass(visitor: TypeVisitor<D>): D {
        return visitor.visitCategory(this)
    }

    override fun accept(visitor: AstElementVisitor) {
        visitor.visit(this)
    }
}

class CategoryClassDummy(
        name: String,
        id: Int = -1
) : CategoryClass(name, id)

// Constants

interface Constant : DomainClass {
    fun <D> accept(visitor: ConstantVisitor<D>): D
}

interface ConstantVisitor<out R> {
    fun visit(constantClass: ConstantClass): R
    fun visit(constantContainer: ConstantContainer): R

}

open class ConstantClass(
        name: String,
        id: Int = ClassIdGenerator.getIdForClass(name)
) : Type(name, id), Constant {
    var parameter: MutableList<String> = ArrayList()

    override fun <D> acceptClass(visitor: TypeVisitor<D>): D {
        return visitor.visitConstant(this)
    }

    override fun <D> accept(visitor: ConstantVisitor<D>): D {
        return visitor.visit(this)
    }

    override fun accept(visitor: AstElementVisitor) {
        visitor.visit(this)
    }
}

class ConstantContainer(
        name: String,
        id: Int = ClassIdGenerator.getIdForClass(name)
) : Type(name, id), Constant {
    var constants: MutableList<Constant> = ArrayList()

    override fun <D> acceptClass(visitor: TypeVisitor<D>): D {
        return visitor.visitConstant(this)
    }

    override fun <D> accept(visitor: ConstantVisitor<D>): D {
        return visitor.visit(this)
    }

    override fun accept(visitor: AstElementVisitor) {
        visitor.visit(this)
    }
}

class ConstantClassDummy(
        name: String,
        id: Int = -1
) : ConstantClass(name, id) {
    override fun <D> accept(visitor: ConstantVisitor<D>): D {
        throw RuntimeException("Dummy visited!")
    }
}

// Primitives

class PrimitiveClass(
        name: String,
        id: Int
) : Type(name, id) {
    val units: MutableList<PrimitiveType> = ArrayList()
    var default: PrimitiveType? = null

    override fun <D> acceptClass(visitor: TypeVisitor<D>): D {
        return visitor.visitPrimitive(this)
    }

    override fun hasDetailsPage(): Boolean = false

    override fun accept(visitor: AstElementVisitor) {
        visitor.visit(this)
    }
}

class PrimitiveType(val name: String, val id: Int, val isDefault: Boolean)

open class ExplicitDomainClass(
        override val associations: MutableList<Association> = ArrayList(),
        override var repository: DomainRepository = DomainRepository(),
        override var export: Boolean = true,
        override var createOperation: Function? = null
) : ClassImpl(), DomainClass {
    override fun <D> acceptClass(visitor: TypeVisitor<D>): D {
        return visitor.visitDomain(this)
    }

    override fun accept(visitor: AstElementVisitor) {
        visitor.visit(this)
        associations.forEach {
            it.accept(visitor)
        }
    }

    override fun hasDetailsPage(): Boolean {
        return this.associations.filter {
            it.type is AssoziationTypeUser && it.type.acceptClass(object : UserClassVisitor<Boolean> {
                override fun visitPrimitive(association: AssoziationTypeUser, clazz: PrimitiveClass): Boolean {
                    return false
                }

                override fun visitCategory(association: AssoziationTypeUser, clazz: CategoryClass): Boolean {
                    return false
                }

                override fun visitDomain(association: AssoziationTypeUser, clazz: DomainClass): Boolean {
                    return true
                }

                override fun visitConstant(association: AssoziationTypeUser, clazz: Constant): Boolean {
                    return false
                }

            })
        }.isEmpty()
    }
}

class FrameworkClass(associations: MutableList<Association>, repository: DomainRepository) : ExplicitDomainClass(associations, repository) {
    var isExtendedByClient = false
    override fun accept(visitor: AstElementVisitor) {
        visitor.visit(this)
        associations.forEach {
            it.accept(visitor)
        }
    }
}

class AbstractDomainClass(
        override val associations: MutableList<Association> = ArrayList(),
        override var repository: DomainRepository = DomainRepository()
) : AbstractClass(), DomainClass {
    override var createOperation: Function? = null

    override fun accept(visitor: AstElementVisitor) {
        visitor.visit(this)
        associations.forEach {
            it.accept(visitor)
        }
    }

    override fun <D> acceptClass(visitor: TypeVisitor<D>): D {
        return visitor.visitDomain(this)
    }

    override fun hasDetailsPage(): Boolean = export || extends.map { it.hasDetailsPage() }.contains(true)
}

class ExternalClass(
        override val associations: MutableList<Association> = ArrayList(),
        override var repository: DomainRepository = DomainRepository()
) : AbstractClass(), DomainClass {
    override var createOperation: Function? = null

    override fun accept(visitor: AstElementVisitor) {

    }

    override fun <D> acceptClass(visitor: TypeVisitor<D>): D {
        return visitor.visitDomain(this)
    }

    override fun hasDetailsPage(): Boolean = export || extends.map { it.hasDetailsPage() }.contains(true)
}


data class DomainRepository(
        val functions: MutableList<Function> = ArrayList()
) {
    override fun toString(): String {
        return "\n\t\t#Repository { $functions }"
    }
}

interface AstElementWithRules {
    val rules: MutableList<RuleContainer>
}

enum class AssociationSpecialisation {
    DERIVED {
        override fun <R> accept(visitor: AssociationSpecialisationVisitor<R>): R {
            return visitor.visitDerived(this);
        }
    },
    TIMED {
        override fun <R> accept(visitor: AssociationSpecialisationVisitor<R>): R {
            return visitor.visitTimed(this); }
    },
    NAMED {
        override fun <R> accept(visitor: AssociationSpecialisationVisitor<R>): R {
            return visitor.visitNamed(this); }
    },
    NORMAL {
        override fun <R> accept(visitor: AssociationSpecialisationVisitor<R>): R {
            return visitor.visitNormal(this); }
    };

    abstract fun <R> accept(visitor: AssociationSpecialisationVisitor<R>): R
}

interface AssociationSpecialisationVisitor<out R> {
    fun visitDerived(specialisation: AssociationSpecialisation): R
    fun visitTimed(specialisation: AssociationSpecialisation): R
    fun visitNamed(specialisation: AssociationSpecialisation): R
    fun visitNormal(specialisation: AssociationSpecialisation): R
}

data class Association(
        val belongsTo: DomainClass,
        val type: AssoziationType,
        val name: String,
        val id: Int,
        override val rules: MutableList<RuleContainer> = ArrayList(),
        var isInline: Boolean = true,
        var specialisation: AssociationSpecialisation = AssociationSpecialisation.NORMAL
) : AstElement, AstElementWithRules {
    override fun toString(): String {
        return "\n\t\t#Association(type: $type, name: $name) $rules"
    }

    override fun accept(visitor: AstElementVisitor) {
        visitor.visit(this)
    }

    fun isDerived(): Boolean {
        return this.specialisation == AssociationSpecialisation.DERIVED
    }
}

