package de.kaipho.genplus.generator.domain.obj

import de.kaipho.genplus.generator.constants.STRINGS
import java.util.*

abstract class RuleContainer(
        val name: String,
        val rule: MutableList<Rule> = ArrayList(),
        val params: MutableList<String> = ArrayList()
) {
    override fun toString(): String {
        return "$name$rule"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false

        other as RuleContainer

        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }
}

fun printErr(err: String) {
    println("\u001B[31m - $err\u001B[0m")
}

class ValidateRule : RuleContainer(STRINGS.RULE_VALIDATE)
class InlineRule : RuleContainer(STRINGS.RULE_INLINE) {
    init {
        printErr("inline ist nicht mehr aktiv unterstützt!")
    }
}
class LocalRule : RuleContainer(STRINGS.RULE_INLINE)
class DerivedRule : RuleContainer(STRINGS.RULE_DERIVED) {
    init {
        printErr("derived ist nicht mehr aktiv unterstützt!")
    }
}
class SecuredRule : RuleContainer(STRINGS.RULE_SECURED)
class CustomRule(val customName:String) : RuleContainer(customName)

/**
 *  Definition einer Regel für Assoziationen
 */
abstract class Rule {
    val name: String

    constructor(name: String) {
        if(name == "allowedValues") {
            printErr("allowedValues ist nicht mehr aktiv unterstützt!")
        }
        this.name = name
    }

    abstract fun accept(visitor: RuleVisitor)
}

/**
 *  Definition einer Regel für Assoziationen mit Assoziationen
 */
class RuleWithParam(
        name: String,
        val params: List<String> = ArrayList()
) : Rule(name) {
    override fun accept(visitor: RuleVisitor) {
        visitor.visit(this)
    }

    override fun toString(): String {
        return "$name$params"
    }
}

/**
 *  Regel für eine Mindestlänge eines Strings
 */
class MinRule(name:String) : Rule(name) {
    override fun accept(visitor: RuleVisitor) {
        visitor.visit(this)
    }

    override fun toString(): String {
        return "$name[]"
    }
}

interface RuleVisitor {
    fun visit(rule: MinRule)
    fun visit(rule: RuleWithParam)
}
