package de.kaipho.genplus.generator.io.merger.impl

import de.kaipho.genplus.generator.common.iterateUntil
import de.kaipho.genplus.generator.constants.FileLoaderConstants
import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.io.merger.FileMerger
import de.kaipho.genplus.generator.io.merger.ConcreteFileMerger
import java.util.*
import java.util.regex.*

@FileMerger(Language.TYPESCRIPT)
class TypescriptFileMerger : ConcreteFileMerger {

    override fun merge(old: Map<String, List<String>>, new: MutableList<String>): MutableList<String> {
        val iterator = new.listIterator()
        mergeImports(old, iterator)
        mergeFunctions(old[FileLoaderConstants.EDITABLE_AREA]!!.toMutableList(), new)
        return new
    }

    /**
     * Reads the new imports and add every import which does not exist in the new but in the old
     */
    private fun mergeImports(old: Map<String, List<String>>, new: MutableListIterator<String>) {
        val oldImports = old[FileLoaderConstants.IMPORTS]!!
        val newImports = HashSet<String>()

        new.iterateUntil(FileLoaderConstants.IMPORTS_END) {
            newImports.add(it.replace(" ", ""))
        }
        new.previous()
        oldImports.filter {
            !newImports.contains(it.replace(" ", ""))
        }.forEach {
            new.add(it)
        }
        new.next()
    }

    private fun mergeFunctions(old: MutableList<String>, new: MutableList<String>) {
        val oldMethods = extractMethodSignatures(old)
        val newMethods = extractMethodSignatures(new)

        oldMethods.forEach {
            if (newMethods.contains(it.key)) {
                newMethods.remove(it.key)
            } else {
                for (i in it.value.first..it.value.second) {
                    old[i] = "// ${old[i]}"
                }
            }
        }
        val oldIterator = old.listIterator()
        newMethods.forEach {
            for (i in it.value.first..it.value.second) {
                oldIterator.add(new[i])
            }
        }
        val result = new.listIterator()
        result.iterateUntil(FileLoaderConstants.EDITABLE_AREA_START) {}
        while (result.hasNext()) {
            val next = result.next()
            if (next.contains(FileLoaderConstants.EDITABLE_AREA_END)) {
                break
            }
            result.remove()
        }
        result.previous()
        old.forEach {
            result.add(it)
        }
    }

    val regex = Pattern.compile(".*public.*(.*):.*")!!

    private fun extractMethodSignatures(file: List<String>): MutableMap<String, Pair<Int, Int>> {
        val map = HashMap<String, Pair<Int, Int>>()
        var method = ""
        var lineStart = 0
        var openBracket = 0

        // TODO falls { und } in einer Zeile

        file.forEachIndexed { index, line ->
            if (method.isEmpty()) {
                if (!line.startsWith("//") && regex.matcher(line).matches()) {
                    method = line
                    lineStart = index
                    openBracket = 1
                }
            } else {
                if (line.contains("{") && !line.contains("`")) {
                    openBracket++
                } else if (line.contains("}") && !line.contains("`")) {
                    openBracket--
                    if (openBracket == 0) {
                        map.put(method.replace(" ", ""), lineStart to index)
                        method = ""
                        lineStart = 0
                        openBracket = 0
                    }
                }
            }
        }
        return map
    }
}