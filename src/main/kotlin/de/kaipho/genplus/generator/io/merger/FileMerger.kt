package de.kaipho.genplus.generator.io.merger

import de.kaipho.genplus.generator.constants.Language

@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE)
@Retention(AnnotationRetention.RUNTIME)
annotation class FileMerger(
        val value: Language
)