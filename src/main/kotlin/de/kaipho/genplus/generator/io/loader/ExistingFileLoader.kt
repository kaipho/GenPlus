package de.kaipho.genplus.generator.io.loader

/**
 * Loader to fetch existing files from the filesystem.
 */
interface ExistingFileLoader {

    /**
     * Load all areas with user code.
     * returns a map containing the area as key and the lines as value
     */
    fun loadCustomParts(file: List<String>): Map<String, List<String>>

}