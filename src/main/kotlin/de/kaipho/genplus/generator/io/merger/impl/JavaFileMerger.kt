package de.kaipho.genplus.generator.io.merger.impl

import de.kaipho.genplus.generator.common.iterateUntil
import de.kaipho.genplus.generator.constants.FileLoaderConstants
import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.io.merger.FileMerger
import de.kaipho.genplus.generator.io.merger.ConcreteFileMerger
import java.util.*
import java.util.regex.*

@FileMerger(Language.JAVA)
class JavaFileMerger : ConcreteFileMerger {

    override fun merge(old: Map<String, List<String>>, new: MutableList<String>): MutableList<String> {
        val iterator = new.listIterator()
        mergeImports(old, iterator)
        mergeFunctions(old[FileLoaderConstants.EDITABLE_AREA]!!.toMutableList(), new)
        copyPrivateArea(old[FileLoaderConstants.PRIVATE_AREA]!!.toMutableList(), new)
        return new
    }

    /**
     * Reads the new imports and add every import which does not exist in the new but in the old
     */
    private fun mergeImports(old: Map<String, List<String>>, new: MutableListIterator<String>) {
        val oldImports = old[FileLoaderConstants.IMPORTS]!!
        val newImports = HashSet<String>()

        if (new.hasNext()) {
            new.next()
        }
        new.iterateUntil(FileLoaderConstants.IMPORTS_END) {
            newImports.add(it.replace(" ", "").replace("\t", ""))
        }
        new.previous()
        oldImports.filter {
            !newImports.contains(it.replace(" ", "").replace("\t", ""))
        }.forEach {
            new.add(it)
        }
        new.next()
    }

    private fun mergeFunctions(old: MutableList<String>, new: MutableList<String>) {
        val oldMethods = extractMethodSignatures(old)
        val cache = ArrayList<String>()
        val interator = new.listIterator()
        interator.iterateUntil(FileLoaderConstants.EDITABLE_AREA_START) {}
        while (interator.hasNext()) {
            val next = interator.next()
            if (next.contains(FileLoaderConstants.EDITABLE_AREA_END)) {
                break
            }
            cache.add(next)
        }
        val newMethods = extractMethodSignatures(cache)

        oldMethods.forEach {
            if (newMethods.contains(it.key)) {
                val newMethod = newMethods[it.key]!!
                if (it.value.first != it.value.third) {
                    var counter = 0
                    for (i in it.value.first..it.value.third - 1) {
                        if ((i - it.value.first) >= (newMethod.third - newMethod.first)) {
                            old.removeAt(i)
                            oldMethods.forEach { s, triple ->
                                if(triple.first > it.value.first) {
                                    oldMethods[s] = Triple(triple.first - 1, triple.second - 1, triple.third - 1)
                                }
                                if(triple.first == it.value.first) {
                                    oldMethods[s] = Triple(triple.first, triple.second - 1, triple.third - 1)
                                }
                            }
                        }
                        old[i] = cache[newMethod.first + counter]
                        counter++
                    }
                    counter = 0
                    if ((it.value.first - it.value.third) > (newMethod.first - newMethod.third)) {
                        for (i in (newMethod.first + (it.value.third - it.value.first))..newMethod.third - 1) {
                            old.add(it.value.first + (it.value.third - it.value.first) + counter, cache[i])
                            oldMethods.forEach { s, triple ->
                                if(triple.first > it.value.first) {
                                    oldMethods[s] = Triple(triple.first + 1, triple.second + 1, triple.third + 1)
                                }
                                if(triple.first == it.value.first) {
                                    oldMethods[s] = Triple(triple.first, triple.second + 1, triple.third + 1)
                                }
                            }
                            counter++
                        }
                    }
                }
                newMethods.remove(it.key)
            } else {
                for (i in it.value.first..it.value.second) {
                    old[i] = "// ${old[i]}"
                }
            }
        }
        val oldIterator = old.listIterator()
        newMethods.forEach {
            for (i in it.value.first..it.value.second) {
                oldIterator.add(cache[i])
            }
        }
        val result = new.listIterator()
        result.iterateUntil(FileLoaderConstants.EDITABLE_AREA_START) {}
        while (result.hasNext()) {
            val next = result.next()
            if (next.contains(FileLoaderConstants.EDITABLE_AREA_END)) {
                break
            }
            result.remove()
        }
        result.previous()
        old.forEach {
            result.add(it)
        }
    }

    private fun copyPrivateArea(old: MutableList<String>, new: MutableList<String>) {
        val iteratorNew = new.listIterator()
        iteratorNew.iterateUntil(FileLoaderConstants.PRIVATE_AREA_START) {}
        iteratorNew.iterateUntil(FileLoaderConstants.PRIVATE_AREA_END) {
            iteratorNew.previous()
            iteratorNew.remove()
            old.forEach {
                iteratorNew.add(it)
            }
        }
    }

    val regex = Pattern.compile(".*public .* .*\\(.*\\).*")!!

    private fun extractMethodSignatures(file: List<String>): MutableMap<String, Triple<Int, Int, Int>> {
        val map = HashMap<String, Triple<Int, Int, Int>>()
        var method = ""
        var lineStart = -1
        var openBracket = 0
        var methodStart = 0

        // TODO falls { und } in einer Zeile


        file.forEachIndexed { index, line ->
            if (method.isEmpty()) {
                if (line.trim().startsWith("/**")) {
                    lineStart = index
                } else if (!line.startsWith("//") && regex.matcher(line).matches()) {
                    method = line
                    if (lineStart == -1)
                        lineStart = index
                    methodStart = index
                    openBracket = 1
                }
            } else {
                val count = bracketCount(line)
                if (count > 0) {
                    openBracket += count
                } else if (count < 0) {
                    openBracket += count
                    if (openBracket == 0) {
                        map.put(method.replace(" ", "").replace("\t", ""), Triple(lineStart, index, methodStart))
                        method = ""
                        lineStart = -1
                        openBracket = 0
                        methodStart = 0
                    }
                }
            }
        }
        return map
    }

    private fun bracketCount(line: String): Int {
        var result = 0
        line.forEach {
            if (it == '{') {
                result++
            } else if (it == '}') {
                result--
            }
        }
        return result
    }
}