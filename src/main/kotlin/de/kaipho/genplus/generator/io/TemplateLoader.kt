package de.kaipho.genplus.generator.io

import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.template.Template
import java.io.InputStream
import java.util.*

/**
 * Klasse um ein Template vom Dateisystem zu laden.
 */
object TemplateLoader {

    fun loadTemplate(file: Template): List<String> {
        val lines: ArrayList<String> = ArrayList()
        var stream: InputStream? = null

        var path = "/templates/"

        if (file.lang == Language.SQL) {
            path += "java/resources/database/" + file.path
        } else if (file.path.contains(".")) {
            path += file.lang.name.toLowerCase()
            path += "/" + file.path
        } else {
            path += "${file.path}.txt"
        }
        path = path.replace("//", "/")

        synchronized(this) {
            stream = javaClass.getResourceAsStream(path)
        }
        if (stream == null) {
            println(path)
        }
        val scanner = Scanner(stream!!)
        while (scanner.hasNextLine()) {
            lines.add(scanner.nextLine())
        }
        return lines
    }
}
