package de.kaipho.genplus.generator.io

/**
 * Created by tom on 10.08.16.
 */
interface AbstractFileSystem {
    fun writeJavaFile(pathPrefix: String, path: String, file: String, content: List<String>)
    fun writeTypescriptFile(pathPrefix: String, path: String, file: String, content: List<String>)
    fun writeJsonFile(pathPrefix: String, path: String, file: String, content: List<String>)
    fun writeHtmlFile(pathPrefix: String, path: String, file: String, content: List<String>)
    fun writeFile(pathPrefix: String, path: String, file: String, content: List<String>)
    fun exist(pathPrefix: String, path: String, file: String): Boolean
}