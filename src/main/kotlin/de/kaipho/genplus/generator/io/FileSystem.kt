package de.kaipho.genplus.generator.io

import de.kaipho.genplus.generator.constants.STRINGS
import de.kaipho.genplus.generator.store.SettingsStore
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.nio.file.*
import java.nio.file.attribute.BasicFileAttributes
import java.util.*
import java.util.stream.Collectors

/**
 * Created by tom on 14.07.16.
 */
object FileSystem : AbstractFileSystem {
    private var backup = 0

    /**
     * Überprüft die Verzeichnisstruktur. Folgende Ordner werden auf höchstem level benötigt und angelegt, falls diese nicht existieren:
     * - model
     * - server
     * - web
     */
    fun checkFolderStructure(path: String) {
        Paths.get(path).toFile().mkdirs();
        for (actual in arrayOf("/server", "/web")) {
            if (!Files.exists(Paths.get(path + actual))) {
                println(STRINGS.FOLDER_NOT_EXIST(actual))
                Files.createDirectory(Paths.get(path + actual))
            }
        }
    }

    fun setBackupSequence(path: String) {
        backup = 0
        (0..31).filter { Files.exists(Paths.get("$path${STRINGS.BACKUP_DIR}/$it/")) }
                .forEach { backup = it + 1 }
        if (backup == 31) {
            backup = 0
        }
    }

    /**
     * Creates the file if it does not exist.
     */
    fun createFile(pathPrefix: String, path: String, file: String, suffix: String) {
        if (!Files.exists(Paths.get("$pathPrefix/$path"))) {
            Files.createDirectories(Paths.get("$pathPrefix/$path"))
        }
        if (!Files.exists(Paths.get("$pathPrefix/$path/$file$suffix"))) {
            Files.createFile(Paths.get("$pathPrefix/$path/$file$suffix"))
        }
    }

    override fun writeJavaFile(pathPrefix: String, path: String, file: String, content: List<String>) {
        writeFile(pathPrefix, path, file, content, ".java")
    }

    override fun writeTypescriptFile(pathPrefix: String, path: String, file: String, content: List<String>) {
        writeFile(pathPrefix, path, file, content, ".ts")
    }

    override fun writeJsonFile(pathPrefix: String, path: String, file: String, content: List<String>) {
        writeFile(pathPrefix, path, file, content, ".json")
    }

    override fun writeHtmlFile(pathPrefix: String, path: String, file: String, content: List<String>) {
        writeFile(pathPrefix, path, file, content, ".html")
    }

    override fun writeFile(pathPrefix: String, path: String, file: String, content: List<String>) {
        writeFile(pathPrefix, path, file, content, "")
    }


    private fun writeFile(pathPrefix: String, path: String, file: String, content: List<String>, suffix: String) {
        val replacedPath = path.replace(STRINGS.PREFIX_PLACEHOLDER, SettingsStore.getPrefixFileSystem())
        val lFile = "$file$suffix"
        val lPath = "$pathPrefix/$replacedPath/"
        Paths.get(lPath + lFile).apply {
            if (Files.exists(this)) {
                val lines = Files.readAllLines(this)
                if (lines.size > 0 && lines[0].contains("keep")) {
                    // Keep the old file...
                    return
                }
            } else {
                Files.createDirectories(this.parent)
            }
            Files.write(this, content)
        }
    }

    override fun exist(pathPrefix: String, path: String, file: String): Boolean {
        val replacedPath = path.replace(STRINGS.PREFIX_PLACEHOLDER, SettingsStore.getPrefixFileSystem())
        val lFile = file
        val lPath = "$pathPrefix/$replacedPath/"
        Paths.get(lPath + lFile).apply {
            return Files.exists(this)
        }
        return false
    }

    fun loadModels(path: String): List<Pair<String, String>> {
        // WatchService um Änderungen mitzubekommen
        val result: MutableList<Pair<String, String>> = ArrayList()

        Files.walkFileTree(Paths.get(path + STRINGS.MODEL_DIR), object : FileVisitor<Path> {
            override fun postVisitDirectory(dir: Path?, exc: IOException?): FileVisitResult {
                return FileVisitResult.CONTINUE
            }

            override fun preVisitDirectory(dir: Path?, attrs: BasicFileAttributes?): FileVisitResult {
                return FileVisitResult.CONTINUE
            }

            override fun visitFileFailed(file: Path?, exc: IOException?): FileVisitResult {
                throw exc!!
            }

            override fun visitFile(file: Path?, attrs: BasicFileAttributes?): FileVisitResult {
                if(file?.toFile()?.name == "framework") throw RuntimeException("model file could not named framework!")
                val content = Files.readAllBytes(file)
                result.add(Pair(file!!.fileName.toString(), String(content)))
                return FileVisitResult.CONTINUE
            }

        })

        val framework = BufferedReader(InputStreamReader(javaClass.getResourceAsStream("/model/framework"))).lines().collect(Collectors.joining("\n"))
        result.add(Pair("framework", framework))
        return result
    }
}