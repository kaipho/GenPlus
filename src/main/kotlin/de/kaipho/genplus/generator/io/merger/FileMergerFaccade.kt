package de.kaipho.genplus.generator.io.merger

import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.constants.Reflection
import de.kaipho.genplus.generator.io.loader.FileStore
import de.kaipho.genplus.generator.io.merger.ConcreteFileMerger
import java.util.*

/**
 * Created by tom on 21.07.16.
 */
object FileMergerFaccade {
    val merger: MutableMap<Language, ConcreteFileMerger>

    init {
        merger = HashMap()
        Reflection.forEachClazzInstance(Reflection.MERGER, ConcreteFileMerger::class.java) { merger, annotation ->
            this.merger.put(annotation.value, merger)
        }
    }

    fun merge(name: String, lang: Language, fileLines: MutableList<String>): MutableList<String> {
        val oldFileData = FileStore.getFile(name, lang)
        if (oldFileData != null)
            return merger[lang]!!.merge(oldFileData, fileLines)
        else return fileLines
    }
}