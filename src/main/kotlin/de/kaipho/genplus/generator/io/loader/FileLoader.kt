package de.kaipho.genplus.generator.io.loader

import de.kaipho.genplus.generator.constants.Language

@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE)
@Retention(AnnotationRetention.RUNTIME)
annotation class FileLoader(
        val value: Language
)