package de.kaipho.genplus.generator.io.merger

/**
 * Merges data from an existing file into a new generated.
 */
interface ConcreteFileMerger {

    /**
     * gets the loaded and extracted data from the aol file and inserts it into the new file.
     */
    fun merge(old: Map<String, List<String>>, new: MutableList<String>): MutableList<String>

}