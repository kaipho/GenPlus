package de.kaipho.genplus.generator.io

import de.kaipho.genplus.generator.constants.STRINGS
import de.kaipho.genplus.generator.store.SettingsStore
import java.util.*

/**
 * File System, which stores all changes in a inMemory FileSystem.
 */
class MemoryFileSystem : AbstractFileSystem {
    val files = HashMap<String, List<String>>()

    override fun exist(pathPrefix: String, path: String, file: String): Boolean {
        return false
    }

    override fun writeJavaFile(pathPrefix: String, path: String, file: String, content: List<String>) {
        writeFile(pathPrefix, path, file, content, ".java")
    }

    override fun writeTypescriptFile(pathPrefix: String, path: String, file: String, content: List<String>) {
        writeFile(pathPrefix, path, file, content, ".ts")
    }

    override fun writeJsonFile(pathPrefix: String, path: String, file: String, content: List<String>) {
        writeFile(pathPrefix, path, file, content, ".json")
    }

    override fun writeHtmlFile(pathPrefix: String, path: String, file: String, content: List<String>) {
        writeFile(pathPrefix, path, file, content, ".html")
    }

    override fun writeFile(pathPrefix: String, path: String, file: String, content: List<String>) {
        writeFile(pathPrefix, path, file, content, "")
    }

    private fun writeFile(pathPrefix: String, path: String, file: String, content: List<String>, suffix: String) {
        val replacedPath = path.replace(STRINGS.PREFIX_PLACEHOLDER, SettingsStore.getPrefixFileSystem())
        val lFile = "$file$suffix"
        val lPath = "$pathPrefix/$replacedPath/"
        files.put((lPath + lFile).replace("//", "/"), content)
    }
}