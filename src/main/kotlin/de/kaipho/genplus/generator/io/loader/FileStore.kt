package de.kaipho.genplus.generator.io.loader


import de.kaipho.genplus.generator.ContextSingleton
import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.constants.Reflection
import de.kaipho.genplus.generator.store.SettingsStore
import de.kaipho.genplus.generator.io.loader.ExistingFileLoader
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.*

object FileStore : ContextSingleton {
    private val loader: MutableMap<Language, ExistingFileLoader>
    private val parsedFiles: MutableMap<String, Map<String, List<String>>> = HashMap()

    init {
        loader = HashMap()
        Reflection.forEachClazzInstance(Reflection.LOADER, ExistingFileLoader::class.java) { loader, annotation ->
            this.loader.put(annotation.value, loader)
        }
    }

    override fun reset() {
        parsedFiles.clear()
    }

    /**
     * returns the saved value for this File
     */
    fun getFile(name: String, lang: Language): Map<String, List<String>>? {
        if (parsedFiles[name] != null) {
            return parsedFiles[name]
        } else {
            if (Files.exists(getFullPath(Pair(lang, name))))
                return loader[lang]!!.loadCustomParts(Files.readAllLines(getFullPath(Pair(lang, name))))
            else
                return null
        }
    }

    private fun getFullPath(file: Pair<Language, String>): Path {
        when (file.first) {
            Language.TYPESCRIPT -> return Paths.get("${SettingsStore.runningPath}/web/src/app/${file.second}")
            Language.JAVA -> {
                if(file.second.endsWith("Test.java"))
                    return Paths.get("${SettingsStore.runningPath}/server/src/test/java/${SettingsStore.getPrefixFileSystem()}/${file.second}")
                else
                    return Paths.get("${SettingsStore.runningPath}/server/src/main/java/${SettingsStore.getPrefixFileSystem()}/${file.second}")
            }
            else -> throw RuntimeException("unsupported Lang ${file.first}")
        }
    }
}