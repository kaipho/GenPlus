package de.kaipho.genplus.generator.io.loader.impl

import de.kaipho.genplus.generator.constants.FileLoaderConstants
import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.io.loader.ExistingFileLoader
import de.kaipho.genplus.generator.io.loader.FileLoader
import java.util.*

@FileLoader(Language.JAVA)
class JavaFileLoader : ExistingFileLoader {
    override fun loadCustomParts(file: List<String>): Map<String, List<String>> {
        val result: MutableMap<String, List<String>> = HashMap()

        val iterator = file.iterator()
        result.put(FileLoaderConstants.IMPORTS, loadImports(iterator))

        while (iterator.hasNext()) {
            if (iterator.next().contains(FileLoaderConstants.EDITABLE_AREA_START)) {
                break
            }
        }
        result.put(FileLoaderConstants.EDITABLE_AREA, loadMethods(iterator))
        while (iterator.hasNext()) {
            if (iterator.next().contains(FileLoaderConstants.PRIVATE_AREA_START)) {
                break
            }
        }
        result.put(FileLoaderConstants.PRIVATE_AREA, loadPrivateArea(iterator))
        return result
    }

    private fun loadImports(iterator: Iterator<String>): List<String> {
        val result: MutableList<String> = ArrayList()
        if (iterator.hasNext()) {
            iterator.next()
        }
        while (iterator.hasNext()) {
            val next = iterator.next()
            if (next.contains(FileLoaderConstants.IMPORTS_END)) {
                break
            }
            result.add(next)
        }
        return result
    }

    private fun loadMethods(iterator: Iterator<String>): List<String> {
        val result: MutableList<String> = ArrayList()
        while (iterator.hasNext()) {
            val next = iterator.next()
            if (next.contains(FileLoaderConstants.EDITABLE_AREA_END)) {
                break
            }
            result.add(next)
        }
        return result
    }

    private fun loadPrivateArea(iterator: Iterator<String>): List<String> {
        val result: MutableList<String> = ArrayList()
        while (iterator.hasNext()) {
            val next = iterator.next()
            if (next.contains(FileLoaderConstants.PRIVATE_AREA_END)) {
                break
            }
            result.add(next)
        }
        return result
    }
}