package de.kaipho.genplus.generator

/**
 * Created by Neo on 09.02.17.
 */
object Model {
    var loader: () -> List<Pair<String, String>> = Model::defaultLoaderImpl

    fun defaultLoaderImpl(): List<Pair<String, String>> {
        throw RuntimeException("No model-loader created!")
    }
}