package de.kaipho.genplus.generator.core.scanner.token.simpleTypes

/**
 * Created by tom on 13.07.16.
 */
interface SimpleTypeVisitor<out E> {
    fun visit(simpleType: TokenSimpleTypeString): E
    fun visit(simpleType: TokenSimpleTypeDate): E
    fun visit(simpleType: TokenSimpleTypeInteger): E
    fun visit(simpleType: TokenSimpleTypeRepository): E
    fun visit(simpleType: TokenSimpleTypeBoolean): E
    fun visit(simpleType: TokenSimpleTypeCollection): E
    fun visit(simpleType: TokenSimpleTypeUnit): E
    fun visit(simpleType: TokenSimpleTypeDouble): E
    fun visit(simpleType: TokenSimpleTypeBigNumber): E
    fun visit(simpleType: TokenSimpleTypeText): E
    fun visit(simpleType: TokenSimpleTypeOptional): E
    fun visit(simpleType: TokenSimpleTypeService): E
    fun visit(simpleType: TokenSimpleTypePassword): E
    fun visit(simpleType: TokenSimpleTypeSecure): E
    fun visit(tokenSimpleTypeMap: TokenSimpleTypeMap): E
    fun visit(tokenSimpleTypeBlob: TokenSimpleTypeBlob): E
}