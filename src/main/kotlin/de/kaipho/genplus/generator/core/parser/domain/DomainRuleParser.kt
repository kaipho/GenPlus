package de.kaipho.genplus.generator.core.parser.domain

import de.kaipho.genplus.generator.core.parser.Parser
import de.kaipho.genplus.generator.core.scanner.token.*
import de.kaipho.genplus.generator.core.scanner.token.rules.*
import de.kaipho.genplus.generator.core.scanner.token.simpleTypes.TokenSimpleType
import de.kaipho.genplus.generator.core.scanner.token.simpleTypes.TokenSimpleTypeRepository
import de.kaipho.genplus.generator.domain.obj.*
import de.kaipho.genplus.generator.core.parser.BracketParser
import de.kaipho.genplus.generator.core.parser.CurlyParser
import de.kaipho.genplus.generator.core.parser.domain.DomainRuleFunctionParser
import de.kaipho.genplus.generator.core.parser.domain.DomainRuleInnerParser
import de.kaipho.genplus.generator.core.scanner.token.*
import de.kaipho.genplus.generator.core.scanner.token.rules.*
import de.kaipho.genplus.generator.domain.obj.*
import java.util.*

class DomainRuleParser(result: AstElementWithRules) : Parser<AstElementWithRules>(listOf(TokenString("Rule")), result) {
    var rule: RuleContainer? = null

    override fun parseImpl() {
        actual.accept(this)
    }

    /**
     * SimpleType ist erlaubt, aber hier nicht beachtet
     */
    override fun visit(token: TokenSimpleType) {

    }

    override fun visit(tokenDerived: TokenDerived) {
    }

    override fun visit(tokenTimed: TokenTimed) {
    }

    override fun visit(tokenNamed: TokenNamed) {
    }

    /**
     * OK, aber nicht beachtet
     */
    override fun visit(token: TokenString) {

    }

    override fun visit(token: TokenComment) {

    }

    override fun visit(token: TokenRule) {
        rule = token.accept(object : TokenRuleVisitor<RuleContainer> {
            override fun visit(rule: TokenRuleCustom): RuleContainer = CustomRule(rule.name)
            override fun visit(rule: TokenRuleSecured): RuleContainer = SecuredRule()
            override fun visit(rule: TokenRuleDerived): RuleContainer = DerivedRule()
            override fun visit(rule: TokenRuleLocal): RuleContainer = LocalRule()
            override fun visit(rule: TokenRuleValidator): RuleContainer = ValidateRule()
            override fun visit(rule: TokenRuleInline): RuleContainer = InlineRule()
        })

        result.rules.add(rule!!)
        removeFirst()
        this.parse(tokenList)
    }

    /**
     * Ok aber nicht beachtet
     */
    override fun visit(token: TokenSimpleTypeRepository) {

    }

    override fun visit(token: CurlyOpenBracket) {
        CurlyParser(DomainRuleInnerParser(rule!!)).parse(tokenList)
        this.parse(tokenList)
    }

    override fun visit(token: OpenBracket) {
        val parser = DomainRuleFunctionParser(ArrayList())
        BracketParser(parser).parse(tokenList)

        rule?.params?.addAll(parser.result)

        this.parse(tokenList)
    }

    override fun visit(token: CloseBracket) {

    }

    /**
     * Ok aber nicht beachtet
     */
    override fun visit(token: CurlyCloseBracket) {

    }

    override fun visit(token: NewLine) {
        tokenList.removeAt(0)
        parse(tokenList)
    }
}