package de.kaipho.genplus.generator.core.scanner.token.simpleTypes

import de.kaipho.genplus.generator.core.scanner.token.AbstractToken
import de.kaipho.genplus.generator.core.scanner.token.TokenVisitor
import de.kaipho.genplus.generator.core.scanner.token.simpleTypes.SimpleTypeVisitor

/**
 * AbstractToken for build in Types
 */
abstract class TokenSimpleType : AbstractToken() {
    override fun accept(visitor: TokenVisitor) {
        visitor.visit(this)
    }

    abstract fun <E> accept(visitor: SimpleTypeVisitor<E>): E
}