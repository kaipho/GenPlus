package de.kaipho.genplus.generator.core.parser.domain

import de.kaipho.genplus.generator.core.obj.AssoziationTypeString
import de.kaipho.genplus.generator.core.parser.CurlyParser
import de.kaipho.genplus.generator.core.parser.Parser
import de.kaipho.genplus.generator.core.parser.ParserExtends
import de.kaipho.genplus.generator.core.parser.core.NameHelper
import de.kaipho.genplus.generator.core.scanner.token.*
import de.kaipho.genplus.generator.domain.obj.*
import de.kaipho.genplus.generator.generator.java.persistence.ClassIdGenerator

/**
 * Created by tom on 12.07.16.
 */
class DomainMainParser(domain: Domain) : Parser<Domain>(listOf(TokenString("export | Klassenname | }")), domain) {
    var domainClass: DomainClass = ExplicitDomainClass()

    override fun parseImpl() {
        actual.accept(this)
    }

    /**
     * export Befehl einer Klasse
     */
    override fun visit(token: TokenExport) {
        printErr("export im domain Abschnitt ist nicht mehr aktiv unterstützt!")
        domainClass.export = true
        removeFirst()
        this.parse(tokenList)
    }

    /**
     * Kommentar
     */
    override fun visit(token: TokenComment) {
        domainClass.comment = token.value
        removeFirst()
        this.parse(tokenList)
    }

    /**
     * Klassenname
     */
    override fun visit(token: TokenString) {
        val (prefix, name) = NameHelper.splitClassName(token.value)
        if(domainClass.name == "ext") {
            val rep = Association(domainClass, AssoziationTypeString, "rep",  ClassIdGenerator.getIdForAssiciation(name, "rep"), mutableListOf(DerivedRule()))
            domainClass.associations.add(rep)
        }
        domainClass.name = name
        domainClass.prefix = prefix
        result.domainClasses.add(domainClass)
        removeFirst()
        this.parse(tokenList)
    }

    override fun visit(tokenDots: TokenDots) {
        removeFirst()
        ParserExtends(domainClass).parse(tokenList)
        this.parse(tokenList)
    }

    override fun visit(token: CurlyOpenBracket) {
        CurlyParser(DomainVariablesParser(domainClass)).parse(tokenList)
        domainClass = ExplicitDomainClass()
        this.parse(tokenList)
    }

    /**
     * abstrakte klasse
     */
    override fun visit(token: TokenAbstractClass) {
        val newDomainClass = AbstractDomainClass()
        newDomainClass.comment = domainClass.comment
        newDomainClass.export = domainClass.export
        domainClass = newDomainClass
        parseNext()
    }

    override fun visit(tokenExternal: TokenExternal) {
        // TODO explicit class
        // id, String rep, Object obj
        val newDomainClass = ExplicitDomainClass()
        newDomainClass.comment = domainClass.comment
        newDomainClass.export = domainClass.export
        newDomainClass.name = "ext"
        domainClass = newDomainClass
        parseNext()
    }

    override fun visit(token: CurlyCloseBracket) {

    }

    override fun visit(token: NewLine) {
        removeFirst()
        this.parse(tokenList)
    }
}