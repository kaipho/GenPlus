package de.kaipho.genplus.generator.core.scanner.token

import de.kaipho.genplus.generator.core.scanner.token.AbstractToken

/**
 * Ein AbstractToken für den Scanner mit Wert
 */
abstract class TokenWithValue<out E>(
        val value: E
) : AbstractToken() {}