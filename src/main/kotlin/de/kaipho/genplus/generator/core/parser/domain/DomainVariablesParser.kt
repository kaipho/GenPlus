package de.kaipho.genplus.generator.core.parser.domain

import de.kaipho.genplus.generator.core.obj.AssoziationType
import de.kaipho.genplus.generator.core.obj.AssoziationTypeUser
import de.kaipho.genplus.generator.core.obj.service.Function
import de.kaipho.genplus.generator.core.obj.service.ServiceClass
import de.kaipho.genplus.generator.core.parser.*
import de.kaipho.genplus.generator.core.parser.service.ServiceFunctionParams
import de.kaipho.genplus.generator.core.scanner.token.*
import de.kaipho.genplus.generator.core.scanner.token.rules.*
import de.kaipho.genplus.generator.core.scanner.token.simpleTypes.TokenSimpleType
import de.kaipho.genplus.generator.core.scanner.token.simpleTypes.TokenSimpleTypeRepository
import de.kaipho.genplus.generator.domain.obj.*
import de.kaipho.genplus.generator.generator.java.persistence.ClassIdGenerator
import de.kaipho.genplus.generator.store.ClassStore.addFunction
import java.util.*

/**
 * Created by tom on 12.07.16.
 */
class DomainVariablesParser(domainClass: DomainClass) : Parser<DomainClass>(listOf(TokenString("Type | Variablenname")), domainClass) {
    var type: AssoziationType? = null
    var specialisation: AssociationSpecialisation = AssociationSpecialisation.NORMAL

    override fun parseImpl() {
        actual.accept(this)
    }

    override fun visit(tokenDerived: TokenDerived) {
        specialisation = AssociationSpecialisation.DERIVED
        parseNext()
    }

    override fun visit(tokenTimed: TokenTimed) {
        specialisation = AssociationSpecialisation.TIMED
        parseNext()
    }

    override fun visit(tokenNamed: TokenNamed) {
        specialisation = AssociationSpecialisation.NAMED
        parseNext()
    }

    /**
     * variablenname
     */
    override fun visit(token: TokenString) {
        if (token.value[0].isUpperCase()) {
            if (type == null) {
                // Möglicher UserType
                type = AssoziationTypeUser(token.value)
                removeFirst()
                this.parse(tokenList)
                return
            } else
                throw ParserException(token, ParserExceptionTexts.VARIABLE_NAMES_LOW)
        }
        if (type == null) {
            throw ParserException(token, ParserExceptionTexts.NEED_TYPE)
        }
        val association = Association(result, type!!, token.value, ClassIdGenerator.getIdForAssiciation(result.name, token.value), specialisation = this.specialisation)
        result.associations.add(association)
        removeFirst()
        DomainRuleParser(association).parse(tokenList)
        DomainVariablesParser(result).parse(tokenList)
    }

    /**
     * varieblentyp
     */
    override fun visit(token: TokenSimpleType) {
        if (type != null) {
            throw ParserException(token)
        }
        removeFirst()
        type = token.accept(mapper)
        tokenList[0].accept(this)
    }

    override fun visit(token: TokenRule) {
        DomainRuleParser(this.result).parse(tokenList)
        parse(tokenList)
    }

    /**
     * Repository
     */
    override fun visit(token: TokenSimpleTypeRepository) {
        throw RuntimeException("Repository not longer supported!")
    }

    override fun visit(token: CurlyCloseBracket) {

    }

    /**
     * Falls kein Typ im zwischenspeicher ist OK, aber nicht beachtet
     */
    override fun visit(token: CloseBracket) {
        if (type != null) {
            throw ParserException(token)
        }
    }

    override fun visit(token: NewLine) {
        tokenList.removeAt(0)
        parse(tokenList)
    }


}

class FunctionParser(result: ServiceClass) : Parser<ServiceClass>(listOf(TokenString("Comment, Funktion")), result) {
    override fun parseImpl() {
        actual.accept(this)
    }

    var comment = ""

    override fun visit(token: TokenComment) {
        comment = token.value
        parseNext()
    }

    override fun visit(token: TokenString) {
        val function = Function()
        function.comment = getAndResetComment()
        function.returnType = AssoziationTypeUser(token.value)
        removeFirst()
        FunctionNameParser(function).parse(tokenList)
        result.addFunction(function)
    }

    override fun visit(token: TokenSimpleType) {
        val function = Function()
        function.comment = this.getAndResetComment()
        removeFirst()
        function.returnType = token.accept(mapper)
        this.actual = tokenList[0]
        FunctionNameParser(function).parse(tokenList)
        result.addFunction(function)
    }

    override fun visit(token: NewLine) {
        // ^
    }

    override fun visit(token: CurlyCloseBracket) {
        // ^
    }

    fun getAndResetComment(): String {
        val result = comment
        comment = ""
        return result
    }
}

class FunctionNameParser(result: Function) : Parser<Function>(listOf(TokenString("Comment, Funktion")), result) {
    override fun parseImpl() {
        actual.accept(this)
    }

    override fun visit(token: TokenString) {
        result.name = token.value
        parseNext()
    }

    override fun visit(token: OpenBracket) {
        BracketParser(ServiceFunctionParams(result)).parse(tokenList)
    }

    override fun visit(token: NewLine) {
        // ^
    }

    override fun visit(token: CurlyCloseBracket) {
        // ^
    }
}
