package de.kaipho.genplus.generator.core.scanner.token.simpleTypes


class TokenSimpleTypePassword : TokenSimpleType() {
    override fun toString(): String {
        return "*Password"
    }

    override fun <E> accept(visitor: SimpleTypeVisitor<E>): E {
        return visitor.visit(this)
    }
}