package de.kaipho.genplus.generator.core.parser.core

import de.kaipho.genplus.generator.core.parser.ParserException
import de.kaipho.genplus.generator.core.parser.ParserExceptionTexts
import de.kaipho.genplus.generator.core.scanner.token.TokenString

object NameHelper {
    /**
     * split the classname into the prefix and name.
     */
    fun splitClassName(className: String): Pair<String, String> {
        if (className.split(".").last()[0].isLowerCase()) {
            throw ParserException(TokenString(className), ParserExceptionTexts.CLASSNAMES_BIG)
        }
        val name = className.split(".").last()
        if (className.indexOf('.') > 0) {
            return Pair("." + className.substring(0, className.lastIndexOf(".")), name)
        }
        return Pair("", name)
    }

    fun splitFunctionName(functionName: String): Pair<String, String> {
        if (functionName.contains(".")) {
            val parts = functionName.split(".")
            if (parts.size != 2) {
                throw ParserException(TokenString(functionName), ParserExceptionTexts.FUNCTION_NAME_TO_MAY_DOTS)
            }
            return Pair(parts[0], parts[1])
        }
        return Pair("", functionName)
    }
}