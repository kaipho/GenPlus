package de.kaipho.genplus.generator.core.obj.options

import java.util.*

/**
 * Created by tom on 27.07.16.
 */
class Options : IOptions {
    val values = HashMap<Option, MutableList<String>>()

    init {
        Option.values().forEach {
            values.put(it, ArrayList())
        }
    }

    override fun get(option: Option): List<String> = values[option]!!
    override fun getFirst(option: Option): String {
        if(values[option]!!.isEmpty()) {
            throw RuntimeException("Option für " + option.name + " nicht hinterlegt.")
        }
        return values[option]!![0]
    }
    override fun isSelected(option: Option, value: String) = values[option]!!.contains(value)
    override fun add(option: Option, value: String) = values[option]!!.add(value)
    override fun given(option: Option) = values[option] != null && values[option]!!.isNotEmpty()

    // Konstanten

    companion object {
        val SECURITY_JWT = "JWT"
    }

    override fun reset() {
        values.clear()
        Option.values().forEach {
            values.put(it, ArrayList())
        }
    }
}

interface IOptions {
    fun get(option: Option): List<String>
    fun getFirst(option: Option): String
    fun isSelected(option: Option, value: String): Boolean
    fun add(option: Option, value: String): Boolean
    fun given(option: Option): Boolean
    fun reset()
}

enum class Option {
    LANGS,
    NAME,
    PREFIX,
    USER_LANGS,
    DATABASE,
    DEFAULTS,

    // Style
    COLOR_MAIN,
    COLOR_SELECTED,
    COLOR_ERROR,
    COLOR_ACCENT,

    // Security
    SECURITY_MODE,
    SECURITY_ROLES
}