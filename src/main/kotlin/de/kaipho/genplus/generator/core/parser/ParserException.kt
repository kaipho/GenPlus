package de.kaipho.genplus.generator.core.parser

import de.kaipho.genplus.generator.core.scanner.token.AbstractToken

/**
 * Created by tom on 12.07.16.
 */
class ParserException : RuntimeException {
    constructor(found: AbstractToken, expected: List<AbstractToken>) : super(
            "Found $found but expected $expected (Line ${found.pos?.first}:${found.pos?.second})") {

    }

    constructor(found: AbstractToken) : super("Already get the keyword $found") {

    }

    constructor(token: AbstractToken, error: String) : super("$error (get $token)") {

    }
}