package de.kaipho.genplus.generator.core.scanner.token.simpleTypes

import de.kaipho.genplus.generator.core.scanner.token.simpleTypes.SimpleTypeVisitor
import de.kaipho.genplus.generator.core.scanner.token.simpleTypes.TokenSimpleType

/**
 * Created by tom on 13.07.16.
 */
class TokenSimpleTypeDate : TokenSimpleType() {

    override fun toString(): String {
        return "*Date"
    }

    override fun <E> accept(visitor: SimpleTypeVisitor<E>): E {
        return visitor.visit(this)
    }
}