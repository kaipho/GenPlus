package de.kaipho.genplus.generator.core.parser

/**
 * Created by tom on 13.07.16.
 */
object ParserExceptionTexts {
    val CLASSNAMES_BIG = "IClass names should be start with an upper case letter"
    val VARIABLE_NAMES_LOW = "Variable names should be start with an lower case letter"
    val NEED_TYPE = "Expect Type"
    val FUNCTION_NAME_TO_MAY_DOTS = "The function Name has too many dots"
}