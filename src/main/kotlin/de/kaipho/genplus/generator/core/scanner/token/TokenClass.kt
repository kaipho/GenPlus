package de.kaipho.genplus.generator.core.scanner.token

import de.kaipho.genplus.generator.core.scanner.Token


class TokenExport : AbstractToken() {
    override fun accept(visitor: TokenVisitor) {
        visitor.visit(this)
    }
}

class TokenDerived : AbstractToken() {
    override fun accept(visitor: TokenVisitor) {
        visitor.visit(this)
    }
}

class TokenTimed : AbstractToken() {
    override fun accept(visitor: TokenVisitor) {
        visitor.visit(this)
    }
}

class TokenNamed : AbstractToken() {
    override fun accept(visitor: TokenVisitor) {
        visitor.visit(this)
    }
}

class TokenContainer : AbstractToken() {
    override fun accept(visitor: TokenVisitor) {
        visitor.visit(this)
    }
}

class TokenDefault : AbstractToken() {
    override fun accept(visitor: TokenVisitor) {
        visitor.visit(this)
    }
}

@Token("abstract")
class TokenAbstractClass : AbstractToken() {
    override fun accept(visitor: TokenVisitor) {
        visitor.visit(this)
    }
}

@Token("external")
class TokenExternal : AbstractToken() {
    override fun accept(visitor: TokenVisitor) {
        visitor.visit(this)
    }
}

class TokenDots : AbstractToken() {
    override fun accept(visitor: TokenVisitor) {
        visitor.visit(this)
    }
}

class TokenSeperator : AbstractToken() {
    override fun accept(visitor: TokenVisitor) {
        visitor.visit(this)
    }
}