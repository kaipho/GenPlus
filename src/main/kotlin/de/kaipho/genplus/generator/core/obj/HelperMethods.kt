package de.kaipho.genplus.generator.core.obj

import de.kaipho.genplus.generator.domain.obj.AbstractClass
import de.kaipho.genplus.generator.domain.obj.IClass
import java.util.*

fun IClass.getConcreteClasses(): List<IClass> {
    if (this is AbstractClass) {
        return this.childClasses.map(IClass::getConcreteClasses).flatten()
    } else {
        return listOf(this)
    }
}

fun IClass.getAllNonAbstractChildClasses(): List<IClass> {
    if (this is AbstractClass) {
        return this.childClasses.map(IClass::getAllNonAbstractChildClasses).flatten()
    } else {
        return listOf(this)
    }
}

fun IClass.getAbstractChildClasses(): List<IClass> {
    if (this is AbstractClass) {
        return this.childClasses.filter { it is AbstractClass }
    } else {
        return ArrayList()
    }
}

fun IClass.getNonAbstractChildClasses(): List<IClass> {
    if (this is AbstractClass) {
        return this.childClasses.filter { it !is AbstractClass }
    } else {
        return ArrayList()
    }
}