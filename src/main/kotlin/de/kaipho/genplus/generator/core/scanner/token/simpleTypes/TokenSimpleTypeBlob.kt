package de.kaipho.genplus.generator.core.scanner.token.simpleTypes


class TokenSimpleTypeBlob : TokenSimpleType() {
    override fun toString(): String {
        return "*BLOB"
    }

    override fun <E> accept(visitor: SimpleTypeVisitor<E>): E {
        return visitor.visit(this)
    }
}