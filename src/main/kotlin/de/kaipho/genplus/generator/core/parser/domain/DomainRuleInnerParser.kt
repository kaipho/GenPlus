package de.kaipho.genplus.generator.core.parser.domain

import de.kaipho.genplus.generator.core.parser.Parser
import de.kaipho.genplus.generator.core.scanner.token.CurlyCloseBracket
import de.kaipho.genplus.generator.core.scanner.token.NewLine
import de.kaipho.genplus.generator.core.scanner.token.TokenString
import de.kaipho.genplus.generator.domain.obj.MinRule
import de.kaipho.genplus.generator.domain.obj.RuleContainer
import de.kaipho.genplus.generator.domain.obj.RuleWithParam
import de.kaipho.genplus.generator.core.parser.BracketParser
import de.kaipho.genplus.generator.core.parser.domain.DomainRuleFunctionParser
import java.util.*

/**
 * Created by tom on 12.07.16.
 */
class DomainRuleInnerParser(rule: RuleContainer) : Parser<RuleContainer>(listOf(TokenString("Function")), rule) {
    override fun parseImpl() {
        actual.accept(this)
    }

    /**
     * funktionaname
     */
    override fun visit(token: TokenString) {
        removeFirst()
        val parser = DomainRuleFunctionParser(ArrayList())
        BracketParser(parser).parse(tokenList)
        if (parser.result.isEmpty()) {
            result.rule.add(MinRule(token.value))
        } else {
            result.rule.add(RuleWithParam(token.value, parser.result))
        }
        this.parse(tokenList)
    }

    /**
     * OK, aber nicht beachtet
     */
    override fun visit(token: CurlyCloseBracket) {

    }

    override fun visit(token: NewLine) {
        tokenList.removeAt(0)
        parse(tokenList)
    }
}