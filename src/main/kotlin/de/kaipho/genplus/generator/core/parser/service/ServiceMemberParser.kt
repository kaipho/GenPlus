package de.kaipho.genplus.generator.core.parser.service

import de.kaipho.genplus.generator.core.obj.AssoziationTypeUser
import de.kaipho.genplus.generator.core.obj.service.*
import de.kaipho.genplus.generator.core.obj.service.Function
import de.kaipho.genplus.generator.core.parser.BracketParser
import de.kaipho.genplus.generator.core.parser.Parser
import de.kaipho.genplus.generator.core.parser.ParserException
import de.kaipho.genplus.generator.core.parser.core.NameHelper
import de.kaipho.genplus.generator.core.parser.domain.DomainRuleParser
import de.kaipho.genplus.generator.core.scanner.token.CurlyCloseBracket
import de.kaipho.genplus.generator.core.scanner.token.NewLine
import de.kaipho.genplus.generator.core.scanner.token.TokenComment
import de.kaipho.genplus.generator.core.scanner.token.TokenString
import de.kaipho.genplus.generator.core.scanner.token.simpleTypes.TokenSimpleType
import de.kaipho.genplus.generator.core.scanner.token.simpleTypes.TokenSimpleTypeRepository
import de.kaipho.genplus.generator.core.scanner.token.simpleTypes.TokenSimpleTypeService
import de.kaipho.genplus.generator.store.ClassStore.addFunction

class ServiceMemberParser(val clazz: ServiceClass) : Parser<Unit>(listOf(TokenString("association | function")), Unit) {
    var comment = ""

    override fun parseImpl() {
        actual.accept(this)
    }

    /**
     * repository association
     */
    override fun visit(token: TokenSimpleTypeRepository) {
        parseInjectable(InjectableRepository(), token)
    }
    /**
     * service association
     */
    override fun visit(token: TokenSimpleTypeService) {
        parseInjectable(InjectableService(), token)
    }

    private fun parseInjectable(injectable: Injectable, token: TokenSimpleType) {
        removeFirst()
        if (actual is TokenString) {
            val (prefix, name) = NameHelper.splitClassName((actual as TokenString).value)
            injectable.forClass.name = name
            injectable.forClass.prefix = prefix
            clazz.injectable.add(injectable)
        } else {
            super.visit(token)
        }
        parseNext()
    }

    override fun visit(token: TokenComment) {
        comment = token.value
        parseNext()
    }

    /**
     * function
     */
    override fun visit(token: TokenString) {
        val function = Function()
        function.comment = getAndResetComment()
        function.returnType = AssoziationTypeUser(token.value)
        removeFirst()
        parseFunction(function)
    }

    /**
     * function
     */
    override fun visit(token: TokenSimpleType) {
        val function = Function()
        function.comment = getAndResetComment()
        removeFirst()
        function.returnType = token.accept(mapper)
        this.actual = tokenList[0]
        parseFunction(function)
    }

    override fun visit(token: NewLine) = parseNext()
    override fun visit(token: CurlyCloseBracket) {
    }

    private fun parseFunction(function: Function) {
        if (actual is TokenString) {
            val (extends, name) = NameHelper.splitFunctionName((actual as TokenString).value)
            function.extends.name = extends
            function.name = name
        } else {
            throw ParserException(actual, expected)
        }
        removeFirst()
        BracketParser(ServiceFunctionParams(function)).parse(tokenList)
        DomainRuleParser(function).parse(tokenList)
        clazz.addFunction(function)
        this.parse(tokenList)
    }

    fun getAndResetComment(): String {
        val result = comment
        comment = ""
        return result
    }
}