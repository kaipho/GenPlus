package de.kaipho.genplus.generator.core.parser.options

import de.kaipho.genplus.generator.core.obj.options.Option
import de.kaipho.genplus.generator.core.parser.Parser
import de.kaipho.genplus.generator.core.parser.ParserException
import de.kaipho.genplus.generator.core.scanner.token.*
import de.kaipho.genplus.generator.core.parser.CurlyParser
import de.kaipho.genplus.generator.core.scanner.token.*
import de.kaipho.genplus.generator.store.SettingsStore

class OptionsParser : Parser<Unit>(listOf(TokenString("key: value, value, ...")), Unit) {
    override fun parseImpl() {
        actual.accept(this)
    }

    override fun visit(token: TokenString) {
        val key = token.value
        removeFirst()
        if (actual is TokenDots) {
            removeFirst()
            ValueParser(key).parse(tokenList)
        } else if (actual is CurlyOpenBracket) {
            CurlyParser(OptionsParser()).parse(tokenList)
        } else {
            throw ParserException(actual, expected)
        }
        this.parse(tokenList)
    }

    override fun visit(token: CurlyCloseBracket) {
    }

    override fun visit(token: NewLine) = parseNext()
}

class ValueParser(val key: String, var next: Boolean = true) : Parser<Unit>(listOf(TokenString("value, value, ...")), Unit) {
    override fun parseImpl() {
        actual.accept(this)
    }

    override fun visit(token: TokenString) {
        if (next) {
            next = false
            SettingsStore.options.add(Option.valueOf(key.toUpperCase()), token.value)
            parseNext()
        }
    }

    override fun visit(tokenSeperator: TokenSeperator) {
        next = true
        parseNext()
    }

    override fun visit(token: CurlyCloseBracket) {
    }

    override fun visit(token: NewLine) = parseNext()
}