package de.kaipho.genplus.generator.core.scanner.response

import de.kaipho.genplus.generator.core.scanner.Scanner
import de.kaipho.genplus.generator.core.scanner.states.SeperatorState
import de.kaipho.genplus.generator.core.scanner.response.ResponsableStateChecker

/**
 * Created by tom on 11.07.16.
 */
class SeperatorChecker(scanner: Scanner, next: ResponsableStateChecker?) : ResponsableStateChecker(scanner, next) {

    override fun isResponsable(c: Char) {
        if (c == ',')
            scanner.state = SeperatorState(scanner)
        else
            next?.isResponsable(c)
    }
}
