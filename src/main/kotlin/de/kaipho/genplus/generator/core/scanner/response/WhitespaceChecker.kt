package de.kaipho.genplus.generator.core.scanner.response

import de.kaipho.genplus.generator.core.scanner.Scanner
import de.kaipho.genplus.generator.core.scanner.states.WhitespaceState
import de.kaipho.genplus.generator.core.scanner.response.ResponsableStateChecker

/**
 * Created by tom on 11.07.16.
 */
class WhitespaceChecker(scanner: Scanner, next: ResponsableStateChecker?) : ResponsableStateChecker(scanner, next) {

    override fun isResponsable(c: Char) {
        if (c.isWhitespace())
            scanner.state = WhitespaceState(scanner)
        else
            next?.isResponsable(c)
    }
}
