package de.kaipho.genplus.generator.core.obj.service

import de.kaipho.genplus.generator.common.convertPackageToFileSystem
import de.kaipho.genplus.generator.common.lowerFirstLetter
import de.kaipho.genplus.generator.common.upperFirstLetter
import de.kaipho.genplus.generator.core.obj.*
import de.kaipho.genplus.generator.domain.obj.*

class ServiceClass : ClassImpl() {
    override fun <D> acceptClass(visitor: TypeVisitor<D>): D {
        return visitor.visitOther(this)
    }

    val functions = ArrayList<Function>()
    val injectable = ArrayList<Injectable>()
    var scope = Scope.SINGLETON

    override fun hasDetailsPage(): Boolean {
        return false
    }

    override fun accept(visitor: AstElementVisitor) {
        visitor.visit(this)
        injectable.forEach {
            it.accept(visitor)
        }
        functions.forEach {
            it.accept(visitor)
        }
    }
}

class Function : AstElement, AstElementWithRules {
    override val rules: MutableList<RuleContainer> = ArrayList()
    var extends: IClass = ClassDummy()
    var name = ""
    val params = ArrayList<Parameter>()
    var returnType: AssoziationType = AssoziationTypeDummy
    var comment = ""

    var id = 0

    var owner: IClass = ClassDummy()

    fun getFilePrefix(): String {
        if (owner is ClassDummy) {
            return "/core"
        }
        return owner.prefix.convertPackageToFileSystem()
    }

    fun isWithExport(): Boolean {
        return owner.export
    }

    fun getQualifiedName() : String {
        return "${this.owner.name}.${this.name}.${this.returnType.name}"
    }
    fun getQualifiedClassName() : String {
        return "${this.owner.name}${this.name.upperFirstLetter()}${this.returnType.name}"
    }

    fun isCreateOperation() : Boolean {
        return name.startsWith("create")
    }

    override fun accept(visitor: AstElementVisitor) = visitor.visit(this)

    /**
     * calculates a list with the extends class if present followed by the real parameters
     */
    fun getAllParams(): List<Parameter> {
        val allParameter = ArrayList<Parameter>()

        if(extends !is ClassDummy) {
            val parameterExtends = Parameter()
            parameterExtends.name = extends.name.lowerFirstLetter()
            parameterExtends.type = AssoziationTypeUser(extends.name)
            (parameterExtends.type as AssoziationTypeUser).clazz = extends
            allParameter.add(parameterExtends)
        }

        allParameter.addAll(params)
        return allParameter
    }
}

class Parameter {
    var name = ""
    var type: AssoziationType = AssoziationTypeDummy

    fun name(name: String): Parameter {
        this.name = name
        return this
    }

    fun type(type: AssoziationType): Parameter {
        this.type = type
        return this
    }
}

abstract class Injectable : AstElement {
    var forClass: IClass = ClassDummy()
    abstract fun accept(visitor: InjectableVisitor)
    override fun accept(visitor: AstElementVisitor) = visitor.visit(this)
}

class InjectableRepository : Injectable() {
    init {
        printErr("Repository ist nicht mehr aktiv unterstützt!")
    }

    override fun accept(visitor: InjectableVisitor) = visitor.visit(this)
}

class InjectableService : Injectable() {
    override fun accept(visitor: InjectableVisitor) = visitor.visit(this)
}

interface InjectableVisitor {
    fun visit(injectable: InjectableService)
    fun visit(injectable: InjectableRepository)
}

enum class Scope {
    SINGLETON, REQUEST
}

class ClassUnit : ClassDummy() {
    override var id: Int = -9
    override var name: String = "GenUnit"
}

open class ClassDummy : ClassImpl() {
    override fun <D> acceptClass(visitor: TypeVisitor<D>): D {
        return visitor.visitOther(this)
    }

    override fun hasDetailsPage(): Boolean {
        return false
    }

    override fun accept(visitor: AstElementVisitor) {
        visitor.visit(this)
    }
}
