package de.kaipho.genplus.generator.core.scanner.token.simpleTypes


class TokenSimpleTypeSecure : TokenSimpleType() {
    override fun toString(): String {
        return "*Secure"
    }

    override fun <E> accept(visitor: SimpleTypeVisitor<E>): E {
        return visitor.visit(this)
    }
}