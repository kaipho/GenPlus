package de.kaipho.genplus.generator.core.parser

import de.kaipho.genplus.generator.core.parser.constants.ConstantsParser
import de.kaipho.genplus.generator.core.parser.domain.DomainMainParser
import de.kaipho.genplus.generator.core.parser.observations.ObservationsParser
import de.kaipho.genplus.generator.core.parser.options.OptionsParser
import de.kaipho.genplus.generator.core.parser.primitives.PrimitivesParser
import de.kaipho.genplus.generator.core.parser.service.ServiceParser
import de.kaipho.genplus.generator.core.scanner.token.NewLine
import de.kaipho.genplus.generator.core.scanner.token.TokenEOF
import de.kaipho.genplus.generator.core.scanner.token.TokenString
import de.kaipho.genplus.generator.store.ClassStore

/**
 * Created by tom on 12.07.16.
 */
class MainParser : Parser<Any>(listOf(TokenString("domain")), Any()) {
    val store = ClassStore

    val tokens = hashMapOf(
            "domain" to DomainMainParser(store.domain),
            "service" to ServiceParser(),
            "options" to OptionsParser(),
            "constants" to ConstantsParser(),
            "primitives" to PrimitivesParser(),
            "observations" to ObservationsParser()
    )

    override fun parseImpl() {
        actual.accept(this)
    }

    override fun visit(token: TokenString) {
        if (!tokens.contains(token.value))
            super.visit(token)
        removeFirst()
        CurlyParser(tokens[token.value]!!).parse(tokenList)
        parse(tokenList)
    }

    override fun visit(token: NewLine) {
        tokenList.removeAt(0)
        parse(tokenList)
    }

    override fun visit(tokenEOF: TokenEOF) {

    }

    override fun toString(): String {
        return "${store.domain} \n"
    }
}