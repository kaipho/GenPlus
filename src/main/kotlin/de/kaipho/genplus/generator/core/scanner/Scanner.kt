package de.kaipho.genplus.generator.core.scanner

import de.kaipho.genplus.generator.core.scanner.states.DecisionState
import de.kaipho.genplus.generator.core.scanner.states.ScannerState
import de.kaipho.genplus.generator.core.scanner.token.AbstractToken
import de.kaipho.genplus.generator.core.scanner.token.TokenEOF
import java.util.*

/**
 *
 */
class Scanner {
    var input: String
    val tokens: MutableList<AbstractToken>
    var state: ScannerState = DecisionState(this)
        set(value) {
            field = value
            if (!tokens.isEmpty())
                tokens.last().pos = this.pos
        }

    // Line, Column
    var pos = Pair(1, 0)

    constructor(input: String) {
        this.input = input
        this.tokens = ArrayList()
    }

    fun getNextChar(): Char {
        if (input.isEmpty()) {
            return '^'
        }
        val cache = input.substring(0, 1)
        input = input.substring(1)

        if (cache[0] == '\n') {
            pos = Pair(pos.first + 1, 0)
        } else {
            pos = Pair(pos.first, pos.second + 1)
        }

        return cache[0]
    }

    fun peekNextChar(): Char {
        if (input.isEmpty()) {
            return '^'
        }
        return input.toCharArray()[0]
    }

    fun scan() {
        while (!input.isEmpty())
            state.scan()
        tokens.add(TokenEOF())
    }
}