package de.kaipho.genplus.generator.core.parser

import de.kaipho.genplus.generator.core.obj.AssoziationType
import de.kaipho.genplus.generator.core.obj.AssoziationTypeUser
import de.kaipho.genplus.generator.core.scanner.token.AngleCloseBracket
import de.kaipho.genplus.generator.core.scanner.token.NewLine
import de.kaipho.genplus.generator.core.scanner.token.TokenSeperator
import de.kaipho.genplus.generator.core.scanner.token.TokenString
import de.kaipho.genplus.generator.core.scanner.token.simpleTypes.TokenSimpleType

/**
 * Created by tom on 12.07.16.
 */
class GenericParser() : Parser<Unit>(listOf(TokenString("AssoziationType")), Unit) {
    /**
     * The last generic type in the brackets: <>
     * Like <T1, T2> -> T2
     */
    var associationType: AssoziationType? = null

    /**
     * All generic types in the brackets: <>
     * Like <T1, T2> -> listOf(T1, T2)
     */
    val generics = ArrayList<AssoziationType>()

    override fun parseImpl() {
        actual.accept(this)
    }

    /**
     * UserType
     */
    override fun visit(token: TokenString) {
        if (token.value[0].isUpperCase()) {
            // Möglicher UserType
            associationType = AssoziationTypeUser(token.value)
            generics.add(associationType!!)
            parseNext()
        } else {
            throw ParserException(token, ParserExceptionTexts.CLASSNAMES_BIG)
        }
    }

    /**
     * SimpleType
     */
    override fun visit(token: TokenSimpleType) {
        associationType = token.accept(mapper)
        generics.add(associationType!!)
        parseNext()
    }

    override fun visit(tokenSeperator: TokenSeperator) {
        parseNext()
    }

    override fun visit(token: AngleCloseBracket) {

    }

    override fun visit(token: NewLine) {
        tokenList.removeAt(0)
        parse(tokenList)
    }
}