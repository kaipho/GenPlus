package de.kaipho.genplus.generator.core.scanner.token

import de.kaipho.genplus.generator.core.scanner.token.TokenVisitor

/**
 * Created by tom on 11.07.16.
 */
abstract class AbstractToken {
    var pos:Pair<Int, Int>? = null

    abstract fun accept(visitor: TokenVisitor)
}