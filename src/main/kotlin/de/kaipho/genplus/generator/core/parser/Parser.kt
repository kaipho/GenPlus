package de.kaipho.genplus.generator.core.parser

import de.kaipho.genplus.generator.core.obj.AssoziationType
import de.kaipho.genplus.generator.core.obj.AssoziationTypeCollection
import de.kaipho.genplus.generator.core.obj.AssoziationTypeMap
import de.kaipho.genplus.generator.core.obj.AssoziationTypeOptional
import de.kaipho.genplus.generator.core.parser.mapper.TokenSimpleTypeToAssoziationType
import de.kaipho.genplus.generator.core.scanner.token.*
import de.kaipho.genplus.generator.core.scanner.token.rules.TokenRule
import de.kaipho.genplus.generator.core.scanner.token.simpleTypes.*

/**
 * Created by tom on 12.07.16.
 */
abstract class Parser<out E>(val expected: List<AbstractToken>, val result: E) : TokenVisitor {
    lateinit var actual: AbstractToken
    lateinit var tokenList: MutableList<AbstractToken>

    fun parse(token: MutableList<AbstractToken>) {
        if (token.size < 1) {
            throw ParserException(TokenError("EndOfInput"), expected)
        }
        actual = token[0]
        tokenList = token
        parseImpl()
    }

    open fun parseImpl() {
        actual.accept(this)
    }

    fun removeFirst(): MutableList<AbstractToken> {
        if (tokenList.size < 1) {
            throw ParserException(TokenError("EndOfInput"), expected)
        }
        tokenList.removeAt(0)
        actual = tokenList[0]
        return tokenList
    }

    fun parseNext() {
        removeFirst()
        this.parse(tokenList)
    }

    override fun visit(token: TokenString) {
        throw ParserException(actual, expected)
    }

    override fun visit(token: TokenInteger) {
        throw ParserException(actual, expected)
    }

    override fun visit(token: TokenDigit) {
        throw ParserException(actual, expected)
    }

    override fun visit(token: AngleOpenBracket) {
        throw ParserException(actual, expected)
    }

    override fun visit(token: AngleCloseBracket) {
        throw ParserException(actual, expected)
    }

    override fun visit(token: CurlyOpenBracket) {
        throw ParserException(actual, expected)
    }

    override fun visit(token: CurlyCloseBracket) {
        throw ParserException(actual, expected)
    }

    override fun visit(token: OpenBracket) {
        throw ParserException(actual, expected)
    }

    override fun visit(token: CloseBracket) {
        throw ParserException(actual, expected)
    }

    override fun visit(token: Hashtag) {
        throw ParserException(actual, expected)
    }

    override fun visit(token: TokenExport) {
        throw ParserException(actual, expected)
    }

    override fun visit(token: TokenSimpleType) {
        throw ParserException(actual, expected)
    }

    override fun visit(token: TokenRule) {
        throw ParserException(actual, expected)
    }

    override fun visit(token: TokenSimpleTypeRepository) {
        throw ParserException(actual, expected)
    }

    override fun visit(token: TokenError) {
        throw ParserException(actual, expected)
    }

    override fun visit(token: TokenComment) {
        throw ParserException(actual, expected)
    }

    override fun visit(token: TokenAbstractClass) {
        throw ParserException(actual, expected)
    }

    override fun visit(tokenDots: TokenDots) {
        throw ParserException(actual, expected)
    }

    override fun visit(tokenSeperator: TokenSeperator) {
        throw ParserException(actual, expected)
    }

    override fun visit(tokenEOF: TokenEOF) {
        throw ParserException(actual, expected)
    }

    override fun visit(token: TokenSimpleTypeService) {
        throw ParserException(actual, expected)
    }

    override fun visit(tokenExternal: TokenExternal) {
        throw ParserException(actual, expected)
    }

    override fun visit(tokenDefault: TokenDefault) {
        throw ParserException(actual, expected)
    }

    override fun visit(tokenContainer: TokenContainer) {
        throw ParserException(actual, expected)
    }

    override fun visit(tokenDerived: TokenDerived) {
        throw ParserException(actual, expected)
    }

    override fun visit(tokenTimed: TokenTimed) {
        throw ParserException(actual, expected)
    }

    override fun visit(tokenNamed: TokenNamed) {
        throw ParserException(actual, expected)
    }

    override fun visit(token: NewLine) = parseNext()

    val mapper = object : TokenSimpleTypeToAssoziationType() {
        override fun visit(tokenSimpleTypeMap: TokenSimpleTypeMap): AssoziationType {
            val parser = GenericParser()
            AngleParser(parser).parse(tokenList)

            return AssoziationTypeMap(parser.generics[0], parser.generics[1])
        }

        override fun visit(simpleType: TokenSimpleTypeCollection): AssoziationType {
            val parser = GenericParser()
            AngleParser(parser).parse(tokenList)

            return AssoziationTypeCollection(parser.associationType!!)
        }

        override fun visit(simpleType: TokenSimpleTypeOptional): AssoziationType {
            val parser = GenericParser()
            AngleParser(parser).parse(tokenList)

            return AssoziationTypeOptional(parser.associationType!!)
        }
    }
}