package de.kaipho.genplus.generator.core.scanner.states

import de.kaipho.genplus.generator.core.scanner.Scanner
import de.kaipho.genplus.generator.core.scanner.states.DecisionState
import de.kaipho.genplus.generator.core.scanner.states.ScannerState
import de.kaipho.genplus.generator.core.scanner.token.AngleCloseBracket

/**
 * Created by tom on 11.07.16.
 */
class AngleBracketCloseState(scanner: Scanner) : ScannerState(scanner) {
    override fun scan() {
        scanner.getNextChar()
        scanner.tokens.add(AngleCloseBracket())
        scanner.state = DecisionState(scanner)
    }

}