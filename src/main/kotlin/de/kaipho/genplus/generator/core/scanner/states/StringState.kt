package de.kaipho.genplus.generator.core.scanner.states

import de.kaipho.genplus.generator.core.scanner.KeywordHolder
import de.kaipho.genplus.generator.core.scanner.Scanner
import de.kaipho.genplus.generator.core.scanner.token.TokenString
import de.kaipho.genplus.generator.core.scanner.states.DecisionState
import de.kaipho.genplus.generator.core.scanner.states.ScannerState

/**
 * Created by tom on 11.07.16.
 */
class StringState(scanner: Scanner) : ScannerState(scanner) {
    var result: String = ""
    var complexString = false

    override fun scan() {
        if(scanner.peekNextChar() == '"' && result == "") {
            scanner.getNextChar()
            complexString = true
            return
        }
        if(scanner.peekNextChar() == '"' && result != "") {
            scanner.getNextChar()
            scanner.tokens.add(TokenString(result))
            scanner.state = DecisionState(scanner)
            return
        }
        result += scanner.getNextChar()

        if (isLetter() && !complexString) {
            if (KeywordHolder.generics.contains(result)) {
                scanner.tokens.add(KeywordHolder.generics[result]!!)
            } else if (KeywordHolder.keywords.contains(result)) {
                scanner.tokens.add(KeywordHolder.keywords[result]!!)
            } else if (KeywordHolder.simpleTypes.contains(result)) {
                scanner.tokens.add(KeywordHolder.simpleTypes[result]!!)
            } else {
                scanner.tokens.add(TokenString(result))
            }
            scanner.state = DecisionState(scanner)
        }
    }

    fun isLetter() = !scanner.peekNextChar().isLetter() && !scanner.peekNextChar().isDigit() && scanner.peekNextChar() != '_' && scanner.peekNextChar() != '.'
}