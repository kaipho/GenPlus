package de.kaipho.genplus.generator.core.scanner

@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE)
@Retention(AnnotationRetention.RUNTIME)
annotation class Token(
        val rep: String
)