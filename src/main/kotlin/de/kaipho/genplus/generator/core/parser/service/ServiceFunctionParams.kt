package de.kaipho.genplus.generator.core.parser.service

import de.kaipho.genplus.generator.core.obj.AssoziationTypeUser
import de.kaipho.genplus.generator.core.obj.service.Function
import de.kaipho.genplus.generator.core.obj.service.Parameter
import de.kaipho.genplus.generator.core.parser.Parser
import de.kaipho.genplus.generator.core.parser.ParserException
import de.kaipho.genplus.generator.core.scanner.token.CloseBracket
import de.kaipho.genplus.generator.core.scanner.token.NewLine
import de.kaipho.genplus.generator.core.scanner.token.TokenSeperator
import de.kaipho.genplus.generator.core.scanner.token.TokenString
import de.kaipho.genplus.generator.core.scanner.token.simpleTypes.TokenSimpleType

class ServiceFunctionParams(val function: Function) : Parser<Unit>(listOf(TokenString("parameter")), Unit) {
    override fun parseImpl() {
        actual.accept(this)
    }

    /**
     * param
     */
    override fun visit(token: TokenString) {
        val param = Parameter()
        param.type = AssoziationTypeUser(token.value)
        removeFirst()
        getParameterName(param)
    }

    /**
     * param
     */
    override fun visit(token: TokenSimpleType) {
        val param = Parameter()
        removeFirst()
        param.type = token.accept(mapper)
        actual = tokenList[0]
        getParameterName(param)
    }

    private fun getParameterName(param: Parameter) {
        if (actual is TokenString) {
            param.name = (actual as TokenString).value
        } else {
            throw ParserException(actual, expected)
        }
        removeFirst()
        function.params.add(param)
        this.parse(tokenList)
    }

    override fun visit(tokenSeperator: TokenSeperator) {
        removeFirst()
        this.parse(tokenList)
    }

    override fun visit(token: CloseBracket) {
    }

    override fun visit(token: NewLine) = parseNext()
}