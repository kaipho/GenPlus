package de.kaipho.genplus.generator.core.scanner.states

import de.kaipho.genplus.generator.core.scanner.Scanner
import de.kaipho.genplus.generator.core.scanner.states.ScannerState
import de.kaipho.genplus.generator.core.scanner.states.DecisionState

/**
 * Created by tom on 11.07.16.
 */
class NewLineState(scanner: Scanner) : ScannerState(scanner) {
    override fun scan() {
        scanner.getNextChar()
        scanner.state = DecisionState(scanner)
    }

}