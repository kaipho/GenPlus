package de.kaipho.genplus.generator.core.scanner.token

import de.kaipho.genplus.generator.core.scanner.token.TokenVisitor
import de.kaipho.genplus.generator.core.scanner.token.TokenWithValue

/**
 * Created by tom on 11.07.16.
 */
class TokenError(value: String) : TokenWithValue<String>(value) {

    override fun accept(visitor: TokenVisitor) {
        visitor.visit(this)
    }

    override fun toString(): String {
        return "ERROR: $value"
    }
}