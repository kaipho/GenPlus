package de.kaipho.genplus.generator.core.exceptions

class NotSupported : RuntimeException("Not supported!")