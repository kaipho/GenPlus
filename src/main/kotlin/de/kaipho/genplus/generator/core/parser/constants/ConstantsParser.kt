package de.kaipho.genplus.generator.core.parser.constants

import de.kaipho.genplus.generator.core.obj.constants.ConstantStore
import de.kaipho.genplus.generator.core.obj.constants.Sektor
import de.kaipho.genplus.generator.core.parser.BracketParser
import de.kaipho.genplus.generator.core.parser.CurlyParser
import de.kaipho.genplus.generator.core.parser.Parser
import de.kaipho.genplus.generator.core.parser.ParserException
import de.kaipho.genplus.generator.core.parser.domain.DomainRuleFunctionParser
import de.kaipho.genplus.generator.core.scanner.token.*
import de.kaipho.genplus.generator.domain.obj.ConstantClass
import de.kaipho.genplus.generator.domain.obj.ConstantClassDummy
import de.kaipho.genplus.generator.domain.obj.ConstantContainer
import de.kaipho.genplus.generator.store.ClassStore

/**
 * Parser für Konstantendefinition
 */
class ConstantsParser : Parser<Unit>(listOf(TokenString("package {}")), Unit) {
    override fun parseImpl() {
        actual.accept(this)
    }

    override fun visit(token: TokenString) {
        val key = token.value
        this.removeFirst()
        if (actual is CurlyOpenBracket) {
            CurlyParser(ConstantsParserInner(key)).parse(tokenList)
        } else {
            throw ParserException(actual, expected)
        }
        this.parse(tokenList)
    }

    /**
     * End of constants
     */
    override fun visit(token: CurlyCloseBracket) = Unit
}

class ConstantsParserInner(val pack: String) : Parser<Unit>(listOf(TokenString("variablenname | variablenname(placeholder) | container")), Unit) {
    override fun visit(token: TokenString) {
        val value = token.value
        if (!value[0].isUpperCase()) {
            throw RuntimeException(value + " does not start with upper-case letter!")
        }
        val constant = ConstantClass(value)
        constant.prefix = pack + "."
        this.removeFirst()

        if (tokenList.first() is OpenBracket) {
            val parser = DomainRuleFunctionParser(constant.parameter)
            BracketParser(parser).parse(tokenList)
        }

        ClassStore.constants.add(constant)
        this.parse(tokenList)
    }

    override fun visit(tokenContainer: TokenContainer) {
        this.removeFirst()
        ConstantsContainerParser(pack).parse(this.tokenList)
        this.parse(this.tokenList)
    }

    /**
     * End of constants
     */
    override fun visit(token: CurlyCloseBracket) = Unit
}

class ConstantsContainerParser(val pack: String) : Parser<Unit>(listOf(TokenString("container ConstantContainer")), Unit) {
    override fun visit(token: TokenString) {
        val value = token.value
        if (!value[0].isUpperCase()) {
            throw RuntimeException(value + " does not start with upper-case letter!")
        }
        val constantContainer = ConstantContainer(value)
        constantContainer.prefix = pack + "."
        this.removeFirst()

        if(tokenList[0] is TokenDots) {
            this.removeFirst()
            ConstantsContainerContentParser(constantContainer).parse(this.tokenList)
        } else {
            throw RuntimeException(": expected, gets" + value)
        }

        ClassStore.constants.add(constantContainer)
    }
}


class ConstantsContainerContentParser(val constantContainer: ConstantContainer) : Parser<Unit>(listOf(TokenString(": Constant|ConstantContainer")), Unit) {
    override fun visit(token: TokenString) {
        val value = token.value
        if (!value[0].isUpperCase()) {
            throw RuntimeException(value + " does not start with upper-case letter!")
        }
        constantContainer.constants.add(ConstantClassDummy(value))
        this.removeFirst()

        if(tokenList[0] is TokenSeperator) {
            this.removeFirst()
            ConstantsContainerContentParser(this.constantContainer).parse(this.tokenList)
        }
    }

    /**
     * End of constants
     */
    override fun visit(token: CurlyCloseBracket) = Unit
}
