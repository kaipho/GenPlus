package de.kaipho.genplus.generator.core.obj

import de.kaipho.genplus.generator.core.obj.AssoziationType
import java.util.*

/**
 * Eine Funktion aus dem Model.
 */
data class Function(
        val returnType: AssoziationType,
        val functionName:String,
        val parameter:List<Pair<AssoziationType, String>> = ArrayList()
) {}
