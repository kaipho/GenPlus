package de.kaipho.genplus.generator.core.parser.mapper

import de.kaipho.genplus.generator.core.obj.*
import de.kaipho.genplus.generator.core.scanner.token.simpleTypes.*
import de.kaipho.genplus.generator.core.obj.*
import de.kaipho.genplus.generator.core.scanner.token.simpleTypes.*

/**
 * Created by tom on 13.07.16.
 */
abstract class TokenSimpleTypeToAssoziationType : SimpleTypeVisitor<AssoziationType> {
    override fun visit(simpleType: TokenSimpleTypeBoolean): AssoziationType {
        return AssoziationTypeBoolean
    }

    override fun visit(simpleType: TokenSimpleTypeSecure): AssoziationType {
        return AssoziationTypeSecure
    }

    override fun visit(simpleType: TokenSimpleTypeDate): AssoziationType {
        return AssoziationTypeDate
    }

    override fun visit(simpleType: TokenSimpleTypeBigNumber): AssoziationType {
        return AssoziationTypeBigNumber
    }

    override fun visit(simpleType: TokenSimpleTypeText): AssoziationType {
        return AssoziationTypeText
    }

    override fun visit(simpleType: TokenSimpleTypeBlob): AssoziationType {
        return AssociationTypeBlob
    }

    override fun visit(simpleType: TokenSimpleTypeRepository): AssoziationType {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
    override fun visit(simpleType: TokenSimpleTypeService): AssoziationType {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun visit(simpleType: TokenSimpleTypeInteger): AssoziationType {
        return AssoziationTypeInteger
    }

    override fun visit(simpleType: TokenSimpleTypeDouble): AssoziationType {
        return AssoziationTypeDouble
    }

    override fun visit(simpleType: TokenSimpleTypeString): AssoziationType {
        return AssoziationTypeString
    }

    override fun visit(simpleType: TokenSimpleTypeUnit): AssoziationType {
        return AssoziationTypeUnit
    }

    override fun visit(simpleType: TokenSimpleTypePassword): AssoziationType {
        return AssoziationTypePassword
    }
}