package de.kaipho.genplus.generator.core.parser

import de.kaipho.genplus.generator.core.parser.Parser
import de.kaipho.genplus.generator.core.scanner.token.*
import de.kaipho.genplus.generator.core.scanner.token.CloseBracket
import de.kaipho.genplus.generator.core.scanner.token.NewLine
import de.kaipho.genplus.generator.core.scanner.token.OpenBracket
import de.kaipho.genplus.generator.core.scanner.token.TokenString

/**
 * Created by tom on 12.07.16.
 */
class BracketParser(val next: Parser<Any>) : Parser<Unit>(listOf(TokenString("("), TokenString(")")), Unit) {
    override fun parseImpl() {
        actual.accept(this)
    }

    override fun visit(token: OpenBracket) {
        removeFirst()
        next.parse(tokenList)
        if(tokenList[0] is CloseBracket) {
            removeFirst()
        } else {
            super.visit(token)
        }
    }

    override fun visit(token: NewLine) {
        tokenList.removeAt(0)
        parse(tokenList)
    }
}