package de.kaipho.genplus.generator.core.parser.domain

import de.kaipho.genplus.generator.core.parser.Parser
import de.kaipho.genplus.generator.core.scanner.token.*

/**
 * Created by tom on 12.07.16.
 */
class DomainRuleFunctionParser(params: MutableList<String>) : Parser<MutableList<String>>(listOf(TokenString("Parameter")), params) {
    override fun parseImpl() {
        actual.accept(this)
    }

    /**
     * Parameter
     */
    override fun visit(token: TokenString) {
        result.add(token.value)
        removeFirst()
        parse(tokenList)
    }

    /**
     * Parameter
     */
    override fun visit(token: TokenInteger) {
        result.add("" + token.value)
        removeFirst()
        parse(tokenList)
    }

    /**
     * Separator for next element, ok
     */
    override fun visit(tokenSeperator: TokenSeperator) {
        parseNext()
    }

    /**
     * OK, aber nicht beachtet
     */
    override fun visit(token: CloseBracket) {

    }

    override fun visit(token: NewLine) {
        tokenList.removeAt(0)
        parse(tokenList)
    }
}