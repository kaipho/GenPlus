package de.kaipho.genplus.generator.core.scanner.response

import de.kaipho.genplus.generator.core.scanner.Scanner


/**
 * Überprüft, welcher Zustand für das Objekt zuständig ist.
 */
abstract class ResponsableStateChecker(
        val scanner: Scanner,
        val next: ResponsableStateChecker?
) {

    abstract fun isResponsable(c: Char)
}