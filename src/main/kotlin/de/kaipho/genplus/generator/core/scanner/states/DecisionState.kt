package de.kaipho.genplus.generator.core.scanner.states

import de.kaipho.genplus.generator.core.scanner.Scanner
import de.kaipho.genplus.generator.core.scanner.response.*
import de.kaipho.genplus.generator.core.scanner.states.ScannerState
import de.kaipho.genplus.generator.core.scanner.response.*

/**
 * Created by tom on 11.07.16.
 */
class DecisionState : ScannerState {
    val checker: ResponsableStateChecker

    constructor(scanner: Scanner) : super(scanner) {
        val complexChecker = StringChecker(scanner, DigitChecker(scanner, HashtagChecker(scanner, CommentChecker(scanner, ErrorChecker(scanner)))))
        val curlyChecker = CurlyBracketOpenChecker(scanner, CurlyBracketCloseChecker(scanner, complexChecker))
        val angleChecker = AngleBracketOpenChecker(scanner, AngleBracketCloseChecker(scanner, curlyChecker))
        val bracketChecker = BracketOpenChecker(scanner, BracketCloseChecker(scanner, angleChecker))
        val classChecker = DotsChecker(scanner, SeperatorChecker(scanner, bracketChecker))

        checker = LineBreakChecker(scanner, WhitespaceChecker(scanner, classChecker))
    }

    override fun scan() {
        val actual: Char = scanner.peekNextChar()
        checker.isResponsable(actual)
    }
}
