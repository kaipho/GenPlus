package de.kaipho.genplus.generator.core.scanner.token.simpleTypes

import de.kaipho.genplus.generator.core.scanner.token.simpleTypes.SimpleTypeVisitor
import de.kaipho.genplus.generator.core.scanner.token.simpleTypes.TokenSimpleType


class TokenSimpleTypeText : TokenSimpleType() {
    override fun toString(): String {
        return "*Unit"
    }

    override fun <E> accept(visitor: SimpleTypeVisitor<E>): E {
        return visitor.visit(this)
    }
}