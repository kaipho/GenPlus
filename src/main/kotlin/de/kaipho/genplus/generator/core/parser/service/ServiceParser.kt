package de.kaipho.genplus.generator.core.parser.service

import de.kaipho.genplus.generator.core.obj.service.ServiceClass
import de.kaipho.genplus.generator.core.parser.Parser
import de.kaipho.genplus.generator.core.parser.core.NameHelper
import de.kaipho.genplus.generator.core.scanner.token.*
import de.kaipho.genplus.generator.core.parser.CurlyParser
import de.kaipho.genplus.generator.core.parser.service.ServiceMemberParser
import de.kaipho.genplus.generator.core.scanner.token.*
import de.kaipho.genplus.generator.store.ClassStore

/**
 * Created by tom on 27.07.16.
 */
class ServiceParser : Parser<Unit>(listOf(TokenString("export | Klassenname | }")), Unit) {
    var serviceClass = ServiceClass()

    override fun parseImpl() {
        actual.accept(this)
    }

    /**
     * export Befehl einer Klasse
     */
    override fun visit(token: TokenExport) {
        serviceClass.export = true
        parseNext()
    }

    /**
     * comment
     */
    override fun visit(token: TokenComment) {
        serviceClass.comment = token.value
        parseNext()
    }

    /**
     * classname
     */
    override fun visit(token: TokenString) {
        val (prefix, name) = NameHelper.splitClassName(token.value)
        serviceClass.name = name
        serviceClass.prefix = prefix
        ClassStore.services.add(serviceClass)
        parseNext()
    }

    /**
     * inner class
     */
    override fun visit(token: CurlyOpenBracket) {
        CurlyParser(ServiceMemberParser(this.serviceClass)).parse(tokenList)
        this.serviceClass = ServiceClass()
        this.parse(tokenList)
    }


    /**
     * end of class
     */
    override fun visit(token: CurlyCloseBracket) {
    }

    override fun visit(token: NewLine) = parseNext()
}