package de.kaipho.genplus.generator.core.parser.primitives

import de.kaipho.genplus.generator.core.parser.CurlyParser
import de.kaipho.genplus.generator.core.parser.Parser
import de.kaipho.genplus.generator.core.parser.ParserException
import de.kaipho.genplus.generator.core.scanner.token.*
import de.kaipho.genplus.generator.domain.obj.PrimitiveClass
import de.kaipho.genplus.generator.domain.obj.PrimitiveType
import de.kaipho.genplus.generator.generator.java.persistence.ClassIdGenerator
import de.kaipho.genplus.generator.store.ClassStore

class PrimitivesParser : Parser<Unit>(listOf(TokenString("Unit Class")), Unit) {
    override fun parseImpl() {
        actual.accept(this)
    }

    override fun visit(token: TokenString) {
        val key = token.value
        if (!key[0].isUpperCase()) {
            throw RuntimeException(key + " does not start with upper-case letter!")
        }
        val primitive = PrimitiveClass(key, ClassIdGenerator.getIdForClass(key))
        ClassStore.primitives.add(primitive)
        removeFirst()
        if (actual is CurlyOpenBracket) {
            CurlyParser(UnitParser(primitive)).parse(tokenList)
        } else {
            throw ParserException(actual, expected)
        }
        this.parse(tokenList)
    }

    override fun visit(token: CurlyCloseBracket) {
    }

    override fun visit(token: NewLine) = parseNext()
}

class UnitParser(val clazz: PrimitiveClass) : Parser<Unit>(listOf(TokenString("<default> Unit")), Unit) {
    var isDefault = false

    override fun parseImpl() {
        actual.accept(this)
    }

    override fun visit(token: TokenString) {
        val key = token.value
        if (!key[0].isUpperCase()) {
            throw RuntimeException(key + " does not start with upper-case letter!")
        }
        if (isDefault && clazz.default != null) {
            throw RuntimeException("Default must be unique, violated for ${clazz.name}!")
        }
        val type: PrimitiveType = PrimitiveType(key, ClassIdGenerator.getIdForClass(key), isDefault)
        if (isDefault) {
            clazz.default = type
            isDefault = false
        }
        clazz.units.add(type)
        parseNext()
    }

    override fun visit(token: TokenDefault) {
        isDefault = true
        parseNext()
    }

    override fun visit(token: CurlyCloseBracket) {
    }

    override fun visit(token: NewLine) = parseNext()
}