package de.kaipho.genplus.generator.core.scanner.response

import de.kaipho.genplus.generator.core.scanner.Scanner
import de.kaipho.genplus.generator.core.scanner.response.ResponsableStateChecker
import de.kaipho.genplus.generator.core.scanner.states.DigitState

/**
 * Created by tom on 11.07.16.
 */
class DigitChecker(scanner: Scanner, next: ResponsableStateChecker?) : ResponsableStateChecker(scanner, next) {
    override fun isResponsable(c: Char) {
        if(c.isDigit())
            scanner.state = DigitState(scanner)
        else
            next?.isResponsable(c)
    }
}