package de.kaipho.genplus.generator.core.scanner.states

import de.kaipho.genplus.generator.core.scanner.Scanner


/**
 * Created by tom on 11.07.16.
 */
abstract class ScannerState(
        val scanner: Scanner
) {
    abstract fun scan()
}