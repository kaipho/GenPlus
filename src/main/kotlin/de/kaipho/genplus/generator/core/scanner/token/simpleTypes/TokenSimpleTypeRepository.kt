package de.kaipho.genplus.generator.core.scanner.token.simpleTypes

import de.kaipho.genplus.generator.core.scanner.token.TokenVisitor
import de.kaipho.genplus.generator.core.scanner.token.simpleTypes.SimpleTypeVisitor
import de.kaipho.genplus.generator.core.scanner.token.simpleTypes.TokenSimpleType

/**
 * Created by tom on 13.07.16.
 */
class TokenSimpleTypeRepository : TokenSimpleType() {
    override fun toString(): String {
        return "#Repository"
    }

    override fun <E> accept(visitor: SimpleTypeVisitor<E>): E {
        return visitor.visit(this)
    }

    override fun accept(visitor: TokenVisitor) {
        visitor.visit(this)
    }
}
class TokenSimpleTypeService : TokenSimpleType() {
    override fun toString(): String {
        return "#Service"
    }

    override fun <E> accept(visitor: SimpleTypeVisitor<E>): E {
        return visitor.visit(this)
    }

    override fun accept(visitor: TokenVisitor) {
        visitor.visit(this)
    }
}