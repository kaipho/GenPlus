package de.kaipho.genplus.generator.core.scanner.response

import de.kaipho.genplus.generator.core.scanner.Scanner
import de.kaipho.genplus.generator.core.scanner.response.ResponsableStateChecker
import de.kaipho.genplus.generator.core.scanner.states.ErrorState

/**
 * Created by tom on 11.07.16.
 */
class ErrorChecker(scanner: Scanner) : ResponsableStateChecker(scanner, null) {
    override fun isResponsable(c: Char) {
        scanner.state = ErrorState(scanner)
    }
}