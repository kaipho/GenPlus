package de.kaipho.genplus.generator.core.obj

import de.kaipho.genplus.generator.core.obj.AstElementVisitor

/**
 * An element from the model
 */
interface AstElement {
    fun accept(visitor: AstElementVisitor)
}