package de.kaipho.genplus.generator.core.parser.observations

import de.kaipho.genplus.generator.core.parser.Parser
import de.kaipho.genplus.generator.core.scanner.token.*
import de.kaipho.genplus.generator.domain.obj.CategoryClass
import de.kaipho.genplus.generator.domain.obj.CategoryClassDummy
import de.kaipho.genplus.generator.store.ClassStore

class ObservationsParser : Parser<Unit>(listOf(TokenString("Observation Class")), Unit) {
    override fun parseImpl() {
        actual.accept(this)
    }

    override fun visit(token: TokenString) {
        val value = token.value
        if (!value[0].isUpperCase()) {
            throw RuntimeException(value + " does not start with upper-case letter!")
        }
        val categoryClass = CategoryClass(value)
        ClassStore.categories.add(categoryClass)
        this.removeFirst()
        if(this.tokenList.first() is TokenDots) {
            this.removeFirst()
            ObservationsHierarchyParser(categoryClass).parse(this.tokenList)
        }
        this.parse(this.tokenList)
    }

    override fun visit(token: CurlyCloseBracket) {
    }

    override fun visit(token: NewLine) = parseNext()
}

class ObservationsHierarchyParser(val categoryClass: CategoryClass) : Parser<Unit>(listOf(TokenString("Observation Hierachie")), Unit) {
    override fun parseImpl() {
        actual.accept(this)
    }

    override fun visit(token: TokenString) {
        val value = token.value
        if (!value[0].isUpperCase()) {
            throw RuntimeException(value + " does not start with upper-case letter!")
        }
        categoryClass.hierarchy.add(CategoryClassDummy(value))
        this.removeFirst()
        if(this.tokenList.first() is TokenSeperator) {
            this.removeFirst()
            ObservationsHierarchyParser(categoryClass).parse(this.tokenList)
        }
    }

    override fun visit(token: CurlyCloseBracket) {
    }

    override fun visit(token: NewLine) = parseNext()
}