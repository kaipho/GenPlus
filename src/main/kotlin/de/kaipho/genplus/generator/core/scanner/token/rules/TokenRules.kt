package de.kaipho.genplus.generator.core.scanner.token.rules

import de.kaipho.genplus.generator.core.scanner.token.rules.TokenRuleVisitor
import de.kaipho.genplus.generator.constants.STRINGS.RULE_DERIVED
import de.kaipho.genplus.generator.constants.STRINGS.RULE_INLINE
import de.kaipho.genplus.generator.constants.STRINGS.RULE_LOCAL
import de.kaipho.genplus.generator.constants.STRINGS.RULE_SECURED
import de.kaipho.genplus.generator.constants.STRINGS.RULE_VALIDATE
import de.kaipho.genplus.generator.core.scanner.Token
import de.kaipho.genplus.generator.core.scanner.token.rules.TokenRule

@Token(RULE_VALIDATE)
class TokenRuleValidator : TokenRule() {
    override fun <E> accept(visitor: TokenRuleVisitor<E>): E {
        return visitor.visit(this)
    }

    override fun toString(): String = RULE_VALIDATE
}

@Token(RULE_INLINE)
class TokenRuleInline : TokenRule() {
    override fun <E> accept(visitor: TokenRuleVisitor<E>): E {
        return visitor.visit(this)
    }

    override fun toString(): String = RULE_INLINE
}

@Token(RULE_LOCAL)
class TokenRuleLocal : TokenRule() {
    override fun <E> accept(visitor: TokenRuleVisitor<E>): E {
        return visitor.visit(this)
    }

    override fun toString(): String = RULE_LOCAL
}

@Token(RULE_DERIVED)
class TokenRuleDerived : TokenRule() {
    override fun <E> accept(visitor: TokenRuleVisitor<E>): E {
        return visitor.visit(this)
    }

    override fun toString(): String = RULE_DERIVED
}

@Token(RULE_SECURED)
class TokenRuleSecured : TokenRule() {
    override fun <E> accept(visitor: TokenRuleVisitor<E>): E {
        return visitor.visit(this)
    }

    override fun toString(): String = RULE_SECURED
}

class TokenRuleCustom(val name:String) : TokenRule() {
    override fun <E> accept(visitor: TokenRuleVisitor<E>): E {
        return visitor.visit(this)
    }

    override fun toString(): String = name
}