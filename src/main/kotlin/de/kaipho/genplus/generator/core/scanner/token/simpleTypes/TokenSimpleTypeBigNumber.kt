package de.kaipho.genplus.generator.core.scanner.token.simpleTypes

import de.kaipho.genplus.generator.core.scanner.token.TokenVisitor

/**
 * Created by tom on 13.07.16.
 */
class TokenSimpleTypeBigNumber : TokenSimpleType() {
    override fun toString(): String {
        return "#Decimal"
    }

    override fun <E> accept(visitor: SimpleTypeVisitor<E>): E {
        return visitor.visit(this)
    }

    override fun accept(visitor: TokenVisitor) {
        visitor.visit(this)
    }
}
