package de.kaipho.genplus.generator.core.scanner.states

import de.kaipho.genplus.generator.core.scanner.Scanner
import de.kaipho.genplus.generator.core.scanner.states.ScannerState
import de.kaipho.genplus.generator.core.scanner.token.TokenDigit
import de.kaipho.genplus.generator.core.scanner.token.TokenInteger
import de.kaipho.genplus.generator.core.scanner.states.DecisionState

/**
 * Created by tom on 11.07.16.
 */
class DigitState(scanner: Scanner) : ScannerState(scanner) {
    var result: String = ""

    override fun scan() {
        result += scanner.getNextChar()

        if (!scanner.peekNextChar().isDigit() && scanner.peekNextChar() != '.') {
            if(result.contains('.'))
                scanner.tokens.add(TokenDigit(result.toDouble()))
            else
                scanner.tokens.add(TokenInteger(result.toInt()))
            scanner.state = DecisionState(scanner)
        }
    }

}