package de.kaipho.genplus.generator.core.parser

import de.kaipho.genplus.generator.core.parser.Parser
import de.kaipho.genplus.generator.core.scanner.token.CurlyCloseBracket
import de.kaipho.genplus.generator.core.scanner.token.CurlyOpenBracket
import de.kaipho.genplus.generator.core.scanner.token.NewLine
import de.kaipho.genplus.generator.core.scanner.token.TokenString

/**
 * Created by tom on 12.07.16.
 */
class CurlyParser(val next: Parser<Any>) : Parser<Unit>(listOf(TokenString("{"), TokenString("}")), Unit) {
    override fun parseImpl() {
        actual.accept(this)
    }

    override fun visit(token: CurlyOpenBracket) {
        tokenList.removeAt(0)
        next.parse(tokenList)
        while (tokenList[0] is NewLine) {
            removeFirst()
        }
        if (tokenList[0] is CurlyCloseBracket) {
            removeFirst()
        } else {
            super.visit(TokenString(tokenList[0].toString()))
        }
    }

    override fun visit(token: NewLine) {
        tokenList.removeAt(0)
        parse(tokenList)
    }
}