package de.kaipho.genplus.generator.core.scanner.states

import de.kaipho.genplus.generator.core.scanner.Scanner
import de.kaipho.genplus.generator.core.scanner.states.DecisionState
import de.kaipho.genplus.generator.core.scanner.states.ScannerState
import de.kaipho.genplus.generator.core.scanner.token.OpenBracket

/**
 * Created by tom on 11.07.16.
 */
class BracketOpenState(scanner: Scanner) : ScannerState(scanner) {
    override fun scan() {
        scanner.getNextChar()
        scanner.tokens.add(OpenBracket())
        scanner.state = DecisionState(scanner)
    }

}