package de.kaipho.genplus.generator.core.scanner.token.rules

import de.kaipho.genplus.generator.core.scanner.token.AbstractToken
import de.kaipho.genplus.generator.core.scanner.token.TokenVisitor
import de.kaipho.genplus.generator.core.scanner.token.rules.TokenRuleVisitor

/**
 * Created by tom on 13.07.16.
 */
abstract class TokenRule : AbstractToken() {
    override fun accept(visitor: TokenVisitor) {
        visitor.visit(this)
    }

    abstract fun <E> accept(visitor: TokenRuleVisitor<E>): E
}