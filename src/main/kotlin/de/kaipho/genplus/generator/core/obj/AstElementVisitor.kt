package de.kaipho.genplus.generator.core.obj

import de.kaipho.genplus.generator.core.obj.service.ClassDummy
import de.kaipho.genplus.generator.core.obj.service.Function
import de.kaipho.genplus.generator.core.obj.service.Injectable
import de.kaipho.genplus.generator.core.obj.service.ServiceClass
import de.kaipho.genplus.generator.domain.obj.*
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.store.SettingsStore

/**
 * A visitor for the AST element
 */
interface AstElementVisitor {
    fun visit(explicitDomainClass: ExplicitDomainClass)
    fun visit(frameworkClass: FrameworkClass)

    fun visit(domainClass: Domain)

    fun visit(abstractDomainClass: AbstractDomainClass)

    fun visit(abstractClassDummy: AbstractClassDummy)

    fun visit(association: Association)

    fun visit(serviceClass: ServiceClass)

    fun visit(classDummy: ClassDummy)

    fun visit(injectable: Injectable)

    fun visit(function: Function)

    fun visit(classStore: ClassStore)

    fun visit(settingsStore: SettingsStore)
    fun visit(primitiveClass: PrimitiveClass)
    fun visit(categoryClass: CategoryClass)
    fun visit(constantClass: ConstantClass)
    fun visit(constantContainer: ConstantContainer)
}