package de.kaipho.genplus.generator.core.obj

import de.kaipho.genplus.generator.constants.Language
import de.kaipho.genplus.generator.core.obj.AstElementVisitorImpl
import de.kaipho.genplus.generator.domain.obj.FrameworkClass
import de.kaipho.genplus.generator.generator.GeneratorFassade
import de.kaipho.genplus.generator.generator.core.GenerationPool
import de.kaipho.genplus.generator.generator.core.Ref
import de.kaipho.genplus.generator.io.FileSystem
import de.kaipho.genplus.generator.core.obj.AstElement
import org.reflections.ReflectionUtils
import kotlin.reflect.KClass

/**
 * A visitor for the AST element
 */
class AstElementStandardVisitor : AstElementVisitorImpl() {
    override fun <D : AstElement> handle(clazz: KClass<out AstElement>, data: D) {
        ReflectionUtils.getAllSuperTypes(clazz.java).plus(clazz.java).forEach {
            GeneratorFassade.generators[it.name]?.forEach {
                if (clazz.java != FrameworkClass::class.java || it.second.lang == Language.TYPESCRIPT) {
                    if ((it.second.dependsOn == Ref.NOTHING || it.second.dependsOn.method.call(data)) &&
                            (it.second.dependsOnNot == Ref.NOTHING || !it.second.dependsOnNot.method.call(data))) {
                        val instance = it.first.copyInstance(data)

                        GenerationPool.callGenerator(it.second, instance, FileSystem)
                    }
                }
            }
        }
    }
}