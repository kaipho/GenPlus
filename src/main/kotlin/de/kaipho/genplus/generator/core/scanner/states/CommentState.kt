package de.kaipho.genplus.generator.core.scanner.states

import de.kaipho.genplus.generator.core.scanner.Scanner
import de.kaipho.genplus.generator.core.scanner.states.DecisionState
import de.kaipho.genplus.generator.core.scanner.states.ScannerState
import de.kaipho.genplus.generator.core.scanner.token.TokenComment

/**
 * Created by tom on 11.07.16.
 */
class CommentState(scanner: Scanner) : ScannerState(scanner) {
    var result: String = ""

    override fun scan() {
        result += scanner.getNextChar()

        if (scanner.peekNextChar() == '/') {
            result += scanner.getNextChar()
            scanner.tokens.add(TokenComment(result))
            scanner.state = DecisionState(scanner)
        }
    }

}