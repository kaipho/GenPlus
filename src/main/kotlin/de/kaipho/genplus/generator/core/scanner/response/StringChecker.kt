package de.kaipho.genplus.generator.core.scanner.response

import de.kaipho.genplus.generator.core.scanner.Scanner
import de.kaipho.genplus.generator.core.scanner.states.StringState
import de.kaipho.genplus.generator.core.scanner.response.ResponsableStateChecker

/**
 * Created by tom on 11.07.16.
 */
class StringChecker(scanner: Scanner, next: ResponsableStateChecker?) : ResponsableStateChecker(scanner, next) {
    override fun isResponsable(c: Char) {
        if(c.isLetter() || c == '"')
            scanner.state = StringState(scanner)
        else
            next?.isResponsable(c)
    }
}