package de.kaipho.genplus.generator.core.parser

import de.kaipho.genplus.generator.core.parser.Parser
import de.kaipho.genplus.generator.core.parser.ParserException
import de.kaipho.genplus.generator.core.parser.ParserExceptionTexts
import de.kaipho.genplus.generator.core.scanner.token.CurlyOpenBracket
import de.kaipho.genplus.generator.core.scanner.token.TokenSeperator
import de.kaipho.genplus.generator.core.scanner.token.TokenString
import de.kaipho.genplus.generator.domain.obj.AbstractClassDummy
import de.kaipho.genplus.generator.domain.obj.IClass

class ParserExtends(clazz: IClass) : Parser<IClass>(listOf(TokenString("ClassName"), TokenString(",")), clazz) {
    override fun parseImpl() {
        actual.accept(this)
    }

    /**
     * Abstract class
     */
    override fun visit(token: TokenString) {
        val cache = AbstractClassDummy()
        if (token.value.split(".").last()[0].isLowerCase()) {
            throw ParserException(token, ParserExceptionTexts.CLASSNAMES_BIG)
        }
        cache.name = token.value.split(".").last()
        if (token.value.indexOf('.') > 0) {
            cache.prefix = "." + token.value.substring(0, token.value.lastIndexOf("."))
        }
        result.extends.add(cache)
        removeFirst()
        this.parse(tokenList)
    }

    /**
     * No more abstract classes
     */
    override fun visit(token: CurlyOpenBracket) {
    }

    /**
     * Next abstract Class
     */
    override fun visit(tokenSeperator: TokenSeperator) {
        removeFirst()
        this.parse(tokenList)
    }
}