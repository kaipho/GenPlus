package de.kaipho.genplus.generator.core.parser

/**
 * Class should be called to check the AST
 */
class PostprocessorException(message: String) : RuntimeException("Error while postprocessing: $message") {

}

fun POSTPROCESSOR_CLASS_NOT_EXIST(clazz: String, context: String) = "Class $clazz does not exist in context $context!"
fun POSTPROCESSOR_CLASS_NOT_ABSTRACT(clazz: String, context: String) = "Class $clazz must be abstract in context $context!"