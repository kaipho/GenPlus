package de.kaipho.genplus.generator.core.scanner

import de.kaipho.genplus.generator.constants.Reflection
import de.kaipho.genplus.generator.core.scanner.token.*
import de.kaipho.genplus.generator.core.scanner.token.rules.TokenRuleCustom
import de.kaipho.genplus.generator.core.scanner.token.simpleTypes.*

/**
 * Keywords to map on specific Tokens
 */
object KeywordHolder {
    val RULE_COMPARATOR: String = "#comparator"
    val RULE_TRANSIENT: String = "#transient"
    val RULE_USER_ATTRIBUTE: String = "#userattribute"
    val RULE_CONSTANTS: String = "#constants"
    val RULE_VIEWMODEL: String = "#viewmodel"

    val keywords = hashMapOf(
            "export" to TokenExport(),
            "timed" to TokenTimed(),
            "named" to TokenNamed(),
            "derived" to TokenDerived(),
            "container" to TokenContainer(),
            "default" to TokenDefault(),
            "Repository" to TokenSimpleTypeRepository(),
            "Service" to TokenSimpleTypeService(),
            RULE_COMPARATOR to TokenRuleCustom(RULE_COMPARATOR),
            RULE_TRANSIENT to TokenRuleCustom(RULE_TRANSIENT),
            RULE_USER_ATTRIBUTE to TokenRuleCustom(RULE_USER_ATTRIBUTE),
            RULE_CONSTANTS to TokenRuleCustom(RULE_CONSTANTS),
            RULE_VIEWMODEL to TokenRuleCustom(RULE_VIEWMODEL)
    )
    val generics = hashMapOf(
            "Collection" to TokenSimpleTypeCollection(),
            "Map" to TokenSimpleTypeMap(),
            "Optional" to TokenSimpleTypeOptional()
    )
    val simpleTypes = hashMapOf(
            "String" to TokenSimpleTypeString(),
            "Integer" to TokenSimpleTypeInteger(),
            "Double" to TokenSimpleTypeDouble(),
            "Date" to TokenSimpleTypeDate(),
            "Boolean" to TokenSimpleTypeBoolean(),
            "Unit" to TokenSimpleTypeUnit(),
            "Decimal" to TokenSimpleTypeBigNumber(),
            "Text" to TokenSimpleTypeText(),
            "Password" to TokenSimpleTypePassword(),
            "Secure" to TokenSimpleTypeSecure(),
            "BLOB" to TokenSimpleTypeBlob()
    )

    init {
        Reflection.forEachClazzInstance(Reflection.TOKEN, AbstractToken::class.java) { token, annotation ->
            keywords.put(annotation.rep, token)
        }
    }
}
