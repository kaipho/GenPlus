package de.kaipho.genplus.generator.core.scanner.token

import de.kaipho.genplus.generator.core.scanner.token.TokenVisitor
import de.kaipho.genplus.generator.core.scanner.token.AbstractToken

/**
 * Created by tom on 11.07.16.
 */
class TokenEOF : AbstractToken() {

    override fun accept(visitor: TokenVisitor) {
        visitor.visit(this)
    }

    override fun toString(): String {
        return "EOF"
    }
}