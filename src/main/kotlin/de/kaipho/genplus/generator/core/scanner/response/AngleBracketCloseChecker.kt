package de.kaipho.genplus.generator.core.scanner.response

import de.kaipho.genplus.generator.core.scanner.Scanner
import de.kaipho.genplus.generator.core.scanner.response.ResponsableStateChecker
import de.kaipho.genplus.generator.core.scanner.states.AngleBracketCloseState

/**
 * Created by tom on 11.07.16.
 */
class AngleBracketCloseChecker(scanner: Scanner, next: ResponsableStateChecker?) : ResponsableStateChecker(scanner, next) {

    override fun isResponsable(c: Char) {
        if (c == '>')
            scanner.state = AngleBracketCloseState(scanner)
        else
            next?.isResponsable(c)
    }
}
