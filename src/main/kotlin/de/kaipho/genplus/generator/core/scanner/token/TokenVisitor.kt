package de.kaipho.genplus.generator.core.scanner.token

import de.kaipho.genplus.generator.core.scanner.token.rules.TokenRule
import de.kaipho.genplus.generator.core.scanner.token.simpleTypes.TokenSimpleType
import de.kaipho.genplus.generator.core.scanner.token.simpleTypes.TokenSimpleTypeRepository
import de.kaipho.genplus.generator.core.scanner.token.simpleTypes.TokenSimpleTypeService

/**
 * Created by tom on 12.07.16.
 */
interface TokenVisitor {
    fun visit(token: TokenInteger)
    fun visit(token: TokenDigit)
    fun visit(token: AngleOpenBracket)
    fun visit(token: AngleCloseBracket)
    fun visit(token: CurlyOpenBracket)
    fun visit(token: CurlyCloseBracket)
    fun visit(token: OpenBracket)
    fun visit(token: CloseBracket)
    fun visit(token: Hashtag)
    fun visit(token: NewLine)
    fun visit(token: TokenComment)

    // String mit Spezialisierungen
    fun visit(token: TokenString)
    fun visit(token: TokenExport)
    fun visit(token: TokenAbstractClass)

    // BuildInType
    fun visit(token: TokenSimpleType)
    fun visit(token: TokenSimpleTypeRepository)
    fun visit(token: TokenSimpleTypeService)

    // Rules
    fun visit(token: TokenRule)

    // ERROR
    fun visit(token: TokenError)

    fun visit(tokenDots: TokenDots)
    fun visit(tokenSeperator: TokenSeperator)

    fun visit(tokenEOF: TokenEOF)
    fun visit(tokenExternal: TokenExternal)
    fun visit(tokenDefault: TokenDefault)
    fun visit(tokenContainer: TokenContainer)
    fun visit(tokenDerived: TokenDerived)
    fun visit(tokenTimed: TokenTimed)
    fun visit(tokenNamed: TokenNamed)
}