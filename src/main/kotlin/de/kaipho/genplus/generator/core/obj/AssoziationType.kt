package de.kaipho.genplus.generator.core.obj

import de.kaipho.genplus.generator.core.exceptions.NotSupported
import de.kaipho.genplus.generator.domain.obj.*

/**
 * AssoziationType beschreibt alle eingebauten Typen für Assoziationen.
 */
abstract class AssoziationType {
    val name: String
    open val id: Int

    constructor(name: String, id: Int) {
        this.name = name
        this.id = id
    }

    constructor(name: String) {
        this.name = name
        this.id = -999
    }

    override fun toString(): String {
        return name
    }

    abstract fun <D> accept(visitor: AssociationVisitor<D>): D

    abstract fun javaRep(): String
    abstract fun cRep(): String
    open fun javaAbstractRep(): String = javaRep()
    abstract fun typescriptRep(): String
}

object AssoziationTypeDummy : AssoziationType("Dummy", -999) {
    override fun cRep(): String = "Dummy"

    override fun <D> accept(visitor: AssociationVisitor<D>): D {
        return visitor.visit(this)
    }

    override fun typescriptRep(): String {
        return "Dummy"
    }

    override fun javaRep(): String {
        return "Dummy"
    }
}

object AssoziationTypeUnit : AssoziationType("Unit", -9) {
    override fun <D> accept(visitor: AssociationVisitor<D>): D {
        return visitor.visit(this)
    }

    override fun typescriptRep(): String {
        return "void"
    }

    override fun javaRep(): String {
        return "void"
    }

    override fun cRep(): String {
        return "void"
    }
}

object AssoziationTypeString : AssoziationType("String", -2) {
    override fun <D> accept(visitor: AssociationVisitor<D>): D {
        return visitor.visit(this)
    }

    override fun typescriptRep(): String {
        return "string"
    }

    override fun javaRep(): String {
        return "String"
    }

    override fun cRep(): String {
        return "std::string"
    }
}

object AssoziationTypePassword : AssoziationType("Password", -8) {
    override fun <D> accept(visitor: AssociationVisitor<D>): D {
        return visitor.visit(this)
    }

    override fun typescriptRep(): String {
        return "string"
    }

    override fun javaRep(): String {
        return "String"
    }

    override fun cRep(): String {
        return "std::string"
    }
}

object AssoziationTypeInteger : AssoziationType("Integer", -4) {
    override fun <D> accept(visitor: AssociationVisitor<D>): D {
        return visitor.visit(this)
    }

    override fun typescriptRep(): String {
        return "number"
    }

    override fun javaRep(): String {
        return "Long"
    }

    override fun cRep(): String {
        return "int"
    }
}

object AssoziationTypeBigNumber : AssoziationType("BigNumber", -999) {
    override fun <D> accept(visitor: AssociationVisitor<D>): D {
        return visitor.visit(this)
    }

    override fun typescriptRep(): String {
        return "string"
    }

    override fun javaRep(): String {
        return "BigDecimal"
    }

    override fun cRep(): String {
        return "std::string"
    }
}

object AssoziationTypeText : AssoziationType("Text", -7) {
    override fun <D> accept(visitor: AssociationVisitor<D>): D {
        return visitor.visit(this)
    }

    override fun typescriptRep(): String {
        return "string"
    }

    override fun javaRep(): String {
        return "String"
    }

    override fun cRep(): String {
        return "std::string"
    }
}

object AssociationTypeBlob : AssoziationType("BLOB", -10) {
    override fun <D> accept(visitor: AssociationVisitor<D>): D {
        return visitor.visit(this)
    }

    override fun typescriptRep(): String {
        return "any"
    }

    override fun javaRep(): String {
        return "Blob"
    }

    override fun cRep(): String {
        throw RuntimeException()
    }
}

object AssoziationTypeDouble : AssoziationType("Double", -3) {
    override fun <D> accept(visitor: AssociationVisitor<D>): D {
        return visitor.visit(this)
    }

    override fun typescriptRep(): String {
        return "number"
    }

    override fun javaRep(): String {
        return "Double"
    }

    override fun cRep(): String {
        return "double"
    }
}

object AssoziationTypeDate : AssoziationType("Date", -5) {
    override fun <D> accept(visitor: AssociationVisitor<D>): D {
        return visitor.visit(this)
    }

    override fun typescriptRep(): String {
        return "string"
    }

    override fun javaRep(): String {
        return "LocalDate"
    }

    override fun cRep(): String {
        return "std::string"
    }
}

object AssoziationTypeSecure : AssoziationType("Secure", -999) {
    override fun <D> accept(visitor: AssociationVisitor<D>): D {
        return visitor.visit(this)
    }

    override fun typescriptRep(): String {
        return "string"
    }

    override fun javaRep(): String {
        return "String"
    }

    override fun cRep(): String {
        return "std::string"
    }
}

object AssoziationTypeBoolean : AssoziationType("Boolean", -6) {
    override fun <D> accept(visitor: AssociationVisitor<D>): D {
        return visitor.visit(this)
    }

    override fun typescriptRep(): String {
        return "boolean"
    }

    override fun javaRep(): String {
        return "Boolean"
    }

    override fun cRep(): String {
        return "bool"
    }
}

class AssoziationTypeCollection(inner: AssoziationType) : GenericAssoziationType("Collection", inner) {
    override fun <D> accept(visitor: AssociationVisitor<D>): D {
        return visitor.visit(this)
    }

    override fun typescriptRep(): String {
        return "Array<${inner.typescriptRep()}>"
    }

    override fun javaRep(): String {
        return "ArrayList<${inner.javaRep()}>"
    }

    override fun javaAbstractRep(): String {
        return "List<${inner.javaRep()}>"
    }

    override fun cRep(): String {
        return "std::vector<${inner.javaRep()}>"
    }
}

class AssoziationTypeMap(val key: AssoziationType, val value: AssoziationType) : AssoziationType("Collection", -999) {
    override fun <D> accept(visitor: AssociationVisitor<D>): D {
        return visitor.visit(this)
    }

    override fun typescriptRep(): String {
        return "Map<${key.typescriptRep()}, ${value.typescriptRep()}>"
    }

    override fun javaRep(): String {
        return "HashMap<${key.javaRep()}, ${value.javaRep()}>"
    }

    override fun javaAbstractRep(): String {
        return "Map<${key.javaRep()}, ${value.javaRep()}>"
    }

    override fun cRep(): String {
        throw NotSupported()
    }
}

class AssoziationTypeOptional(inner: AssoziationType) : GenericAssoziationType("Collection", inner) {
    override fun <D> accept(visitor: AssociationVisitor<D>): D {
        return visitor.visit(this)
    }

    override fun typescriptRep(): String {
        return inner.typescriptRep()
    }

    override fun javaRep(): String {
        return inner.javaRep()
    }

    override fun javaAbstractRep(): String {
        return "Optional<${inner.javaRep()}>"
    }

    override fun cRep(): String {
        return inner.javaRep()
    }
}

abstract class GenericAssoziationType(s: String, val inner: AssoziationType) : AssoziationType(s) {
    override val id: Int get() {
        return this.inner.id
    }
}

class AssoziationTypeUser(name: String) : AssoziationType(name) {
    override val id: Int get() {
        return this.clazz.id
    }

    var clazz: IClass = AbstractClassDummy()

    fun primitive(): Boolean {
        return this.clazz is PrimitiveClass
    }

    fun noClazz(): Boolean {
        when (clazz) {
            is PrimitiveClass -> return true
            is Constant -> return true
            is CategoryClass -> return true
            is ExplicitDomainClass -> return false
            is AbstractDomainClass -> return false
        }
        throw RuntimeException("Type could not matched!")
    }

    fun <D> acceptClass(visitor: UserClassVisitor<D>): D {
        when (clazz) {
            is PrimitiveClass -> return visitor.visitPrimitive(this, clazz as PrimitiveClass)
            is Constant -> return visitor.visitConstant(this, clazz as Constant)
            is CategoryClass -> return visitor.visitCategory(this, clazz as CategoryClass)
            is ExplicitDomainClass -> return visitor.visitDomain(this, clazz as DomainClass)
            is AbstractDomainClass -> return visitor.visitDomain(this, clazz as DomainClass)
        }
        throw RuntimeException("Type could not matched!")
    }

    override fun <D> accept(visitor: AssociationVisitor<D>): D {
        return visitor.visit(this)
    }

    override fun typescriptRep(): String {
        return clazz.acceptClass(object : TypeVisitor<String> {
            override fun visitPrimitive(clazz: PrimitiveClass): String {
                return "PersistentPrimitive<$name>"
            }

            override fun visitCategory(clazz: CategoryClass): String {
                return "string"
            }

            override fun visitDomain(clazz: DomainClass): String {
                return name
            }

            override fun visitConstant(clazz: Constant): String {
                return "string"
            }

            override fun visitOther(clazz: IClass): String {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun visitDummy(clazz: IClass): String {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

        })
    }

    override fun javaRep(): String {
        return clazz.acceptClass(object : TypeVisitor<String> {
            override fun visitPrimitive(clazz: PrimitiveClass): String {
                return "PersistentPrimitive<$name>"
            }

            override fun visitCategory(clazz: CategoryClass): String {
                return name
            }

            override fun visitDomain(clazz: DomainClass): String {
                return name
            }

            override fun visitConstant(clazz: Constant): String {
                return name
            }

            override fun visitOther(clazz: IClass): String {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun visitDummy(clazz: IClass): String {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

        })
    }

    override fun cRep(): String {
        return name
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false

        other as AssoziationTypeUser

        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }
}

interface UserClassVisitor<out R> {
    fun visitPrimitive(association: AssoziationTypeUser, clazz: PrimitiveClass): R
    fun visitCategory(association: AssoziationTypeUser, clazz: CategoryClass): R
    fun visitDomain(association: AssoziationTypeUser, clazz: DomainClass): R
    fun visitConstant(association: AssoziationTypeUser, clazz: Constant): R
}

interface AssociationVisitor<out D> {
    fun visit(association: AssoziationTypeString): D
    fun visit(association: AssoziationTypeInteger): D
    fun visit(association: AssoziationTypeDate): D
    fun visit(association: AssoziationTypeBoolean): D
    fun visit(association: AssoziationTypeUser): D
    fun visit(association: AssoziationTypeCollection): D
    fun visit(association: AssoziationTypeDummy): D
    fun visit(association: AssoziationTypeUnit): D
    fun visit(association: AssoziationTypeDouble): D
    fun visit(association: AssoziationTypeBigNumber): D
    fun visit(association: AssoziationTypeText): D
    fun visit(association: AssoziationTypeOptional): D
    fun visit(association: AssoziationTypePassword): D
    fun visit(association: AssoziationTypeSecure): D
    fun visit(association: AssoziationTypeMap): D
    fun visit(associationTypeBlob: AssociationTypeBlob): D
}
