package de.kaipho.genplus.generator.core.scanner.token.rules

import de.kaipho.genplus.generator.core.scanner.token.rules.*

/**
 * Created by Tom Schmidt (A13530) on 13.07.2016.
 */
interface TokenRuleVisitor<out E> {
    fun visit(rule: TokenRuleValidator): E
    fun visit(rule: TokenRuleInline): E
    fun visit(rule: TokenRuleLocal): E
    fun visit(rule: TokenRuleDerived): E
    fun visit(rule: TokenRuleSecured): E
    fun visit(rule: TokenRuleCustom): E
}