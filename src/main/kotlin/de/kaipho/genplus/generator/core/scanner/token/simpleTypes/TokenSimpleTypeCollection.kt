package de.kaipho.genplus.generator.core.scanner.token.simpleTypes

/**
 * Created by tom on 13.07.16.
 */
class TokenSimpleTypeCollection : TokenSimpleType() {
    lateinit var generic: TokenSimpleType

    override fun toString(): String {
        return "*Collection"
    }

    override fun <E> accept(visitor: SimpleTypeVisitor<E>): E {
        return visitor.visit(this)
    }
}
class TokenSimpleTypeOptional : TokenSimpleType() {
    lateinit var generic: TokenSimpleType

    override fun toString(): String {
        return "*Optional"
    }

    override fun <E> accept(visitor: SimpleTypeVisitor<E>): E {
        return visitor.visit(this)
    }
}
class TokenSimpleTypeMap : TokenGeneric() {
    override fun toString(): String {
        return "*Map"
    }

    override fun <E> accept(visitor: SimpleTypeVisitor<E>): E {
        return visitor.visit(this)
    }
}

abstract class TokenGeneric : TokenSimpleType() {
    val generics= ArrayList<TokenSimpleType>()
}