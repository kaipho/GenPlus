package de.kaipho.genplus.generator.core.parser

import de.kaipho.genplus.generator.core.obj.*
import de.kaipho.genplus.generator.core.obj.service.*
import de.kaipho.genplus.generator.core.obj.service.Function
import de.kaipho.genplus.generator.domain.obj.*
import de.kaipho.genplus.generator.generator.java.persistence.ClassIdGenerator
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.store.SettingsStore

/**
 * Class should be called to check the AST
 */
object Postprocessor {

    @Throws(PostprocessorException::class)
    fun process() {
        ClassStore.accept(visitor)

        // resolve create Functions
        resolveCreateFunctions()
    }

    /**
     * Resolves a create function for every domain class.
     * Also resolves parameters inherited by 1..1 associations.
     */
    fun resolveCreateFunctions() {
        ClassStore.createFunctions.forEach { key, value ->
            val owner: DomainClass = (value.returnType as AssoziationTypeUser).clazz as DomainClass
            if (owner.createOperation == null) {
                resolveCreateFunction(value, owner)
            }
        }
    }

    fun resolveCreateFunction(f: Function, owner: DomainClass) {
        if (owner.createOperation != null) {
            return
        }
        val userAssociations = owner.associations.filter { it.type is AssoziationTypeUser }.map { it.type as AssoziationTypeUser }
        owner.createOperation = f

        userAssociations
                .filter { ClassStore.createFunctions.contains(it.clazz.name) }
                .map {
                    val value = ClassStore.createFunctions.get(it.clazz.name)
                    val ownerInner: DomainClass = (value!!.returnType as AssoziationTypeUser).clazz as DomainClass
                    resolveCreateFunction(value, ownerInner)

                    value.params
                }
                .forEach {
                    f.params.addAll(it)
                }

    }

    val visitor = object : AstElementVisitor {
        override fun visit(constantClass: ConstantClass) {
            // nothing to do
        }

        override fun visit(constantContainer: ConstantContainer) {
            val noDummies: MutableList<Constant> = constantContainer.constants.map {
                ClassStore.getByName(it.name) as? Constant ?: throw RuntimeException("${it.name} is no Constant!")
            }.toMutableList()
            constantContainer.constants = noDummies
        }

        override fun visit(categoryClass: CategoryClass) {
            val noDummies: MutableList<CategoryClass> = categoryClass.hierarchy.map {
                ClassStore.getByName(it.name) as? CategoryClass ?: throw RuntimeException("${it.name} is no CategoryClass!")
            }.toMutableList()
            categoryClass.hierarchy = noDummies
        }

        override fun visit(primitiveClass: PrimitiveClass) {
            // TODO?!
        }

        override fun visit(settingsStore: SettingsStore) {
            throw UnsupportedOperationException("not implemented")
        }

        override fun visit(frameworkClass: FrameworkClass) {
            processExtends(frameworkClass)
            if (frameworkClass.export) {
                processAssociationInline(frameworkClass)
            }
        }

        override fun visit(classStore: ClassStore) {

        }

        override fun visit(function: Function) {
            function.extends = getRealClass(function.extends)
            function.params.forEach { param ->
                replaceUserType(param.type)
            }
            replaceUserType(function.returnType)

            function.id = ClassIdGenerator.getIdForAssiciation(function.owner.name, function.name)

        }

        override fun visit(injectable: Injectable) {
            injectable.accept(object : InjectableVisitor {
                override fun visit(injectable: InjectableService) {
                    injectable.forClass = getRealServiceClass(injectable.forClass)
                }

                override fun visit(injectable: InjectableRepository) {
                    injectable.forClass = getRealClass(injectable.forClass)
                }

            })
        }

        override fun visit(serviceClass: ServiceClass) {
            processExtends(serviceClass)
            processFunctions(serviceClass)
        }

        override fun visit(classDummy: ClassDummy) {
        }

        override fun visit(association: Association) {
            processUserTypeAssociations(association)
        }

        override fun visit(explicitDomainClass: ExplicitDomainClass) {
            processExtends(explicitDomainClass)
            generateId(explicitDomainClass)
            if (explicitDomainClass.export) {
                processAssociationInline(explicitDomainClass)
            }
            explicitDomainClass.repository.functions.forEach {
                it.accept(this)
            }
        }

        override fun visit(domainClass: Domain) {
        }

        override fun visit(abstractDomainClass: AbstractDomainClass) {
            processExtends(abstractDomainClass)
            generateId(abstractDomainClass)
            if (abstractDomainClass.export) {
                abstractDomainClass.getConcreteClasses().forEach {
                    processAssociationInline(it as DomainClass)
                }
            }
            abstractDomainClass.repository.functions.forEach {
                it.accept(this)
            }
        }

        override fun visit(abstractClassDummy: AbstractClassDummy) {
        }
    }

    fun generateId(clazz: IClass) {
        clazz.id = ClassIdGenerator.getIdForClass(clazz.name)
    }

    fun processExtends(clazz: IClass) {
        clazz.extends.forEachIndexed { index, extension ->
            val resultList = ClassStore.domain.domainClasses.filter { it.name == extension.name }
            // TODO check prefix if result.size > 1
            if (resultList.size == 0 || resultList.size > 1) {
                throw PostprocessorException(POSTPROCESSOR_CLASS_NOT_EXIST(clazz.name, extension.name))
            }
            val result = resultList[0]
            if (result is AbstractClass) {
                result.childClasses.add(clazz)
                clazz.extends[index] = result
            } else {
                throw PostprocessorException(POSTPROCESSOR_CLASS_NOT_ABSTRACT(clazz.name, extension.name))
            }
        }
    }

    fun processAssociationInline(clazz: DomainClass) {
        clazz.associations.forEach {
            if (it.type is AssoziationTypeUser) {
                it.isInline = false
            }
        }
    }

    fun processUserTypeAssociations(association: Association) {
        replaceUserType(association.type, association)
        if (association.type is AssoziationTypeCollection) {
            replaceUserType(association.type.inner)
        }
        if (association.type is AssoziationTypeOptional) {
            replaceUserType(association.type.inner)
        }
        if (association.type is AssoziationTypeMap) {
            replaceUserType(association.type.key)
            replaceUserType(association.type.value)
        }
    }

    private fun getRealClass(clazz: IClass): IClass {
        if (clazz is ClassDummy && clazz.name == "") {
            return ClassUnit() // TODO?!
        }
        if (clazz is ClassUnit) {
            return clazz
        }
        val resultList = ClassStore.domain.domainClasses.filter { it.name == clazz.name }
        // TODO check prefix if result.size > 1
        if (resultList.isEmpty() || resultList.size > 1) {
            throw PostprocessorException(POSTPROCESSOR_CLASS_NOT_EXIST("", clazz.name))
        }
        return resultList.first()
    }

    private fun getRealServiceClass(clazz: IClass): IClass {
        if (clazz is ClassDummy && clazz.name == "") {
            return clazz // TODO?!
        }
        val resultList = ClassStore.services.filter { it.name == clazz.name }
        // TODO check prefix if result.size > 1
        if (resultList.isEmpty() || resultList.size > 1) {
            throw PostprocessorException(POSTPROCESSOR_CLASS_NOT_EXIST("", clazz.name))
        }
        return resultList.first()
    }

    private fun replaceUserType(type: AssoziationType, association: Association? = null) {
        if (type is AssoziationTypeCollection && type.inner is AssoziationTypeUser) {
            type.inner.clazz = ClassStore.getByName(type.inner.name)
        }
        if (type is AssoziationTypeOptional && type.inner is AssoziationTypeUser) {
            type.inner.clazz = ClassStore.getByName(type.inner.name)
        }
        if (type is AssoziationTypeUser) {
            val result = ClassStore.getByName(type.name)

            if (result is ExternalClass) {
                association?.rules?.add(DerivedRule())
            }

            type.clazz = result
        }
    }

    private fun processFunctions(serviceClass: ServiceClass) {
        serviceClass.functions.forEach {
            it.owner = serviceClass
            it.extends = getRealClass(it.extends)
            it.params.forEach {
                replaceUserType(it.type)
            }
            replaceUserType(it.returnType)
        }
    }
}