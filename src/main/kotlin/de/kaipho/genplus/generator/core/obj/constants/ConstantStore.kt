package de.kaipho.genplus.generator.core.obj.constants

import de.kaipho.genplus.generator.ContextSingleton
import java.util.*

/**
 * Store for constants. A constant has a name and placeholder.
 */
object ConstantStore : ContextSingleton {
    val constants: MutableMap<String, Sektor> = HashMap()

    fun getSector(key: String): Sektor {
        if(constants.containsKey(key)) {
            return constants[key]!!
        }
        val map = Sektor()
        constants.put(key, map)
        return map
    }

    fun forEachPackage(inline: (String)->Unit) {
        constants.keys.forEach {
            inline(it)
        }
    }

    override fun reset() {
        constants.clear()
    }
}

class Sektor {
    val constants: MutableMap<String, MutableList<String>> = HashMap()

    fun getSector(key: String): MutableList<String> {
        if(constants.containsKey(key)) {
            return constants[key]!!
        }
        val list = ArrayList<String>()
        constants.put(key, list)
        return list
    }

    fun forEachConstant(inline: (String)->Unit) {
        constants.keys.forEach {
            inline(it)
        }
    }
}