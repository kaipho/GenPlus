package de.kaipho.genplus.generator.core.obj

import de.kaipho.genplus.generator.core.obj.service.ClassDummy
import de.kaipho.genplus.generator.core.obj.service.Function
import de.kaipho.genplus.generator.core.obj.service.Injectable
import de.kaipho.genplus.generator.core.obj.service.ServiceClass
import de.kaipho.genplus.generator.domain.obj.*
import de.kaipho.genplus.generator.generator.AbstractGenerator
import de.kaipho.genplus.generator.generator.core.Wrapper
import de.kaipho.genplus.generator.store.ClassStore
import de.kaipho.genplus.generator.store.SettingsStore
import kotlin.reflect.KClass

abstract class AstElementVisitorImpl : AstElementVisitor {
    override fun visit(classStore: ClassStore) {
        handle(ClassStore::class, classStore)
    }

    override fun visit(settingsStore: SettingsStore) {
        handle(SettingsStore::class, settingsStore)
    }

    override fun visit(function: Function) {
        handle(Function::class, function)
    }

    override fun visit(constantClass: ConstantClass) {
    }

    override fun visit(constantContainer: ConstantContainer) {

    }

    override fun visit(injectable: Injectable) {

    }

    override fun visit(classDummy: ClassDummy) {

    }

    override fun visit(categoryClass: CategoryClass) {

    }

    override fun visit(serviceClass: ServiceClass) {
        handle(ServiceClass::class, serviceClass)
    }

    override fun visit(association: Association) {

    }

    override fun visit(abstractClassDummy: AbstractClassDummy) {
        handle(AbstractClassDummy::class, abstractClassDummy)
    }

    override fun visit(frameworkClass: FrameworkClass) {
        handle(FrameworkClass::class, frameworkClass)
    }

    override fun visit(abstractDomainClass: AbstractDomainClass) {
        handle(AbstractDomainClass::class, abstractDomainClass)
    }

    override fun visit(explicitDomainClass: ExplicitDomainClass) {
        handle(ExplicitDomainClass::class, explicitDomainClass)
    }

    override fun visit(primitiveClass: PrimitiveClass) {
        // handle(PrimitiveClass::class, primitiveClass)
    }

    override fun visit(domainClass: Domain) {
        handle(Domain::class, domainClass)
    }

    abstract fun <D : AstElement> handle(clazz: KClass<out AstElement>, data: D)

    fun <D> AbstractGenerator<*>.copyInstance(data: D): AbstractGenerator<D> {
        val instance = this.javaClass.newInstance() as AbstractGenerator<D>
        instance.meta = Wrapper(data)
        return instance
    }
}