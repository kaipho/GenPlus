package de.kaipho.genplus.generator.core.scanner.states

import de.kaipho.genplus.generator.core.scanner.Scanner
import de.kaipho.genplus.generator.core.scanner.states.DecisionState
import de.kaipho.genplus.generator.core.scanner.states.ScannerState
import de.kaipho.genplus.generator.core.scanner.token.CurlyCloseBracket

/**
 * Created by tom on 11.07.16.
 */
class CurlyBracketCloseState(scanner: Scanner) : ScannerState(scanner) {
    override fun scan() {
        scanner.getNextChar()
        scanner.tokens.add(CurlyCloseBracket())
        scanner.state = DecisionState(scanner)
    }

}