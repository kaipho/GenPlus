package de.kaipho.genplus

import de.kaipho.genplus.generator.Model
import de.kaipho.genplus.generator.commands.*
import de.kaipho.genplus.generator.constants.Reflection
import de.kaipho.genplus.generator.io.FileSystem
import de.kaipho.genplus.generator.store.SettingsStore
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader

object Main {
    var path = ""
        get

    private val br = BufferedReader(InputStreamReader(System.`in`))
    private val commandMap: MutableMap<String, Command> = HashMap()

    @JvmStatic
    fun main(args: Array<String>) {
        var line = init(args)

        Reflection.forEachClazzInstance(Reflection.COMMANDS, Command::class.java) { command, annotation ->
            commandMap.put(annotation.command, command)
            commandMap.put(annotation.shortcut, command)
        }

        Model.loader = { FileSystem.loadModels(path) }

        while (true) {
            if (line == null) line = printGenerator()

            if (commandMap.containsKey(line)) {
                commandMap[line]?.execute(path)
            } else {
                println("   Unknown command. Type 'help' to see commands")
            }
            line = null
        }
    }

    fun init(args: Array<String>): String? {
        path = System.getProperty("user.dir").replace("\\", "/")
        if (args.isNotEmpty()) {
            path = args[0].replace("\\", "/")
        }
        SettingsStore.runningPath = path;
        println("GeneratorFassade runs in " + path)
        FileSystem.checkFolderStructure(path)
        if (args.size > 1) {
            return args[1]
        }
        return null
    }

    private fun printGenerator(): String? {
        println()
        print("\$generator: ")
        return br.readLine()
    }
}
