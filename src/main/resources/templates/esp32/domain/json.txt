#line(0)# #upper_name#JSONSERVICE_H
#line(1)# #upper_name#JSONSERVICE_H

#include <Arduino.h>
#include <ArduinoJson.h>
#imports#

class #class_name#JsonService {
private:

public:
	char* toJson(#class_name# obj) {
		StaticJsonBuffer<200> jsonBuffer;
        JsonObject& root = jsonBuffer.createObject();

		#line(3)#

		JsonObject& links = root.createNestedObject("_links");

		JsonObject& self1 = links.createNestedObject("self");
		self1.set("href", "/#lower_class_name#s/" + obj.getTableRow());
		JsonObject& self2 = links.createNestedObject("#lower_class_name#");
		self2.set("href", "/#lower_class_name#s/" + obj.getTableRow());

        char buffer[root.measureLength()];
        root.printTo(buffer, sizeof(buffer));
        return buffer;
	}

	#class_name# fromJson(String json) {
		StaticJsonBuffer<200> jsonBuffer;
        JsonObject& root = jsonBuffer.parseObject(json);

        #class_name# result = #class_name#();

        #line(4)#

        return result;
	}
};

#line(2)# //#upper_name#JSONSERVICE_H