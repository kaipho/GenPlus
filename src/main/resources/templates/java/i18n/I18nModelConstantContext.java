package #package#i18n;

import #package#domain.category.Language;

import java.util.Map;

/**
 * i18n context for constants.
 */
public class I18nModelConstantContext extends I18nModelContext {
    private final Map<String, Object> data;

    public static I18nModelConstantContext get(Long forModelId, Map<String, Object> data) {
        return new I18nModelConstantContext(forModelId, data);
    }

    private I18nModelConstantContext(Long forModelId, Map<String, Object> data) {
        super(forModelId);
        this.data = data;
    }

    @Override
    public void update(Language language, String newRep) {
        super.update(language, newRep);
    }

    @Override
    public String get(Language language) {
        String result = super.get(language);
        return new I18nParser(result, data).parseString();
    }
}
