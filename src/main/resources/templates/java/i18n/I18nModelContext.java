package #package#i18n;

import #package#core.db.DbConnector;
import #package#core.db.DbFacade;
import #package#core.db.cache.SingletonCache;
import #package#core.security.SecurityContext;
import #package#domain.category.Language;

public class I18nModelContext implements I18nContext {
    final Long forModelId;

    public static I18nModelContext get(Long forModelId) {
        return new I18nModelContext(forModelId);
    }

    I18nModelContext(Long forModelId) {
        this.forModelId = forModelId;
    }


    @Override
    public void update(Language language, String newRep) {
        Long languageId = DbFacade.saveString(language.getValue());
        Long newRepId = DbFacade.saveString(newRep);
        DbConnector.getConnector()
                   .getBuilderFor("INSERT INTO internationalisation(rep, lang, translates) VALUES (?, ?, ?)")
                   .withLong(newRepId)
                   .withLong(languageId)
                   .withLong(forModelId)
                   .submit();
    }

    @Override
    public String get() {
        return get(SecurityContext.getUser().getLang());
    }

    @Override
    public String get(Language language) {
        if(language.isEmpty()) {
            throw new UserHasNoLanguageDefined();
        }
        Long rep = DbConnector.getConnector()
                              .getBuilderFor("SELECT rep FROM internationalisation WHERE lang = ? AND translates = ?")
                              .withLong(DbFacade.saveString(language.getValue()))
                              .withLong(forModelId)
                              .forSingle(row -> row.getLong("rep"));
        return SingletonCache.getInstance().getString(rep);
    }
}
