-- model
CREATE TABLE IF NOT EXISTS class (
  id       INT PRIMARY KEY           NOT NULL,
  name     VARCHAR(255)              NOT NULL,
  abstract INT                       NOT NULL,
  type     VARCHAR(255)              NOT NULL
);

CREATE TABLE IF NOT EXISTS hierarchy (
  super   INT NOT NULL,
  sub     INT NOT NULL,
  derived INT NOT NULL,
  CONSTRAINT fk_super FOREIGN KEY (super) REFERENCES class ON DELETE CASCADE,
  CONSTRAINT fk_sub FOREIGN KEY (sub) REFERENCES class ON DELETE CASCADE,
  CONSTRAINT pk_hierarchy PRIMARY KEY (super, sub)
);

CREATE TABLE IF NOT EXISTS association (
  id     INT          NOT NULL,
  source INT          NOT NULL,
  target INT          NOT NULL,
  name   VARCHAR(255) NOT NULL,
  CONSTRAINT fk_source FOREIGN KEY (source) REFERENCES class ON DELETE CASCADE,
  CONSTRAINT fk_target FOREIGN KEY (target) REFERENCES class ON DELETE CASCADE,
  CONSTRAINT pk_association PRIMARY KEY (id, source, target, name)
);

CREATE TABLE IF NOT EXISTS callable (
  id     INT          NOT NULL,
  source INT          NOT NULL,
  target INT          NOT NULL,
  name   VARCHAR(255) NOT NULL,
  CONSTRAINT fk_callable_source FOREIGN KEY (source) REFERENCES class ON DELETE CASCADE,
  CONSTRAINT fk_callable_target FOREIGN KEY (target) REFERENCES class ON DELETE CASCADE,
  CONSTRAINT pk_callable_association PRIMARY KEY (id)
);

DELETE FROM class;
DELETE FROM hierarchy;
DELETE FROM association;
INSERT INTO class (id, name, abstract, type) VALUES (-1, 'PersistentElement', 1, 'class');
INSERT INTO class (id, name, abstract, type) VALUES (-2, 'GenString', 0, 'gen-primitive');
INSERT INTO class (id, name, abstract, type) VALUES (-3, 'GenDouble', 0, 'gen-primitive');
INSERT INTO class (id, name, abstract, type) VALUES (-4, 'GenInteger', 0, 'gen-primitive');
INSERT INTO class (id, name, abstract, type) VALUES (-5, 'GenDate', 0, 'gen-primitive');
INSERT INTO class (id, name, abstract, type) VALUES (-6, 'GenBoolean', 0, 'gen-primitive');
INSERT INTO class (id, name, abstract, type) VALUES (-7, 'GenText', 0, 'gen-primitive');
INSERT INTO class (id, name, abstract, type) VALUES (-8, 'GenPassword', 0, 'gen-primitive');
INSERT INTO class (id, name, abstract, type) VALUES (-9, 'GenUnit', 0, 'gen-primitive');
INSERT INTO class (id, name, abstract, type) VALUES (-10, 'GenBlob', 0, 'gen-primitive');

INSERT INTO association (id, source, target, name) VALUES (-1, -1, -4, 'id');
INSERT INTO callable (id, source, target, name) VALUES (0, -9, -9, 'default');

#line#

-- instance
CREATE SEQUENCE globalseq;

CREATE TABLE IF NOT EXISTS longTransaction (
  id       INT DEFAULT globalseq.nextval PRIMARY KEY NOT NULL,
  userId   INT                                       NOT NULL,
  commited TIMESTAMP
);

CREATE TABLE IF NOT EXISTS shortTransaction (
  id         INT DEFAULT globalseq.nextval PRIMARY KEY NOT NULL,
  instanceof INT                                       NOT NULL,
  belongsTo  INT                                       NOT NULL,
  CONSTRAINT fk_sTaBelongsTo FOREIGN KEY (belongsTo) REFERENCES longTransaction ON DELETE CASCADE,
  CONSTRAINT fk_sTaInstanceOf FOREIGN KEY (instanceof) REFERENCES callable ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS accessed (
  type        CHAR NOT NULL,
  transaction INT  NOT NULL,
  object      INT  NOT NULL,
  CONSTRAINT fk_accessed_transaction FOREIGN KEY (transaction) REFERENCES shortTransaction ON DELETE CASCADE,
  CONSTRAINT pk_accessed PRIMARY KEY (transaction, object)
);

CREATE TABLE IF NOT EXISTS obj (
  id         INT DEFAULT globalseq.nextval PRIMARY KEY NOT NULL,
  instanceof INT                                       NOT NULL,
  createdBy  INT                                       NOT NULL,
  CONSTRAINT fk_objCreatedBy FOREIGN KEY (createdBy) REFERENCES shortTransaction ON DELETE CASCADE
);
CREATE INDEX i_obj_instanceof
  ON obj (instanceof);

CREATE TABLE IF NOT EXISTS val_integer (
  id  INT DEFAULT globalseq.nextval PRIMARY KEY NOT NULL,
  val INT,
);
CREATE UNIQUE INDEX i_val_integer
  ON val_integer (val);

CREATE TABLE IF NOT EXISTS val_string (
  id  INT DEFAULT globalseq.nextval PRIMARY KEY NOT NULL,
  val VARCHAR(2000),
);
CREATE UNIQUE INDEX i_val_string
  ON val_string (val);

CREATE TABLE IF NOT EXISTS val_datetime (
  id  INT DEFAULT globalseq.nextval PRIMARY KEY NOT NULL,
  val DATETIME,
);
CREATE UNIQUE INDEX i_val_datetime
  ON val_datetime (val);

CREATE TABLE IF NOT EXISTS val_double (
  id  INT DEFAULT globalseq.nextval PRIMARY KEY NOT NULL,
  val DOUBLE,
);
CREATE UNIQUE INDEX i_val_double
  ON val_double (val);

CREATE TABLE IF NOT EXISTS val_blob (
  id  INT DEFAULT globalseq.nextval PRIMARY KEY NOT NULL,
  val BLOB,
);

CREATE TABLE IF NOT EXISTS val_rational (
  id          INT DEFAULT globalseq.nextval PRIMARY KEY NOT NULL,
  numerator   BIGINT,
  denominator BIGINT
);
CREATE UNIQUE INDEX i_val_rational
  ON val_rational (numerator, denominator);

CREATE TABLE IF NOT EXISTS persistent_primitive (
  id       INT DEFAULT globalseq.nextval PRIMARY KEY NOT NULL,
  fraction INT,
  unit     INT
);

CREATE TABLE IF NOT EXISTS category (
  instanceOf INT NOT NULL,
  fk_string  INT NOT NULL,
  CONSTRAINT fk_category_string FOREIGN KEY (fk_string) REFERENCES val_string ON DELETE CASCADE,
  CONSTRAINT pk_category PRIMARY KEY (instanceOf, fk_string)
);

CREATE TABLE IF NOT EXISTS internationalisation (
  rep        INT NOT NULL,
  lang       INT NOT NULL,
  translates INT NOT NULL,
  CONSTRAINT fk_in_rep FOREIGN KEY (rep) REFERENCES val_string ON DELETE CASCADE,
  CONSTRAINT fk_in_lang FOREIGN KEY (lang) REFERENCES val_string ON DELETE CASCADE,
  CONSTRAINT pk_in PRIMARY KEY (translates, lang)
);

CREATE TABLE IF NOT EXISTS link (
  id         INT DEFAULT globalseq.nextval PRIMARY KEY NOT NULL,
  pos        INT                                       NOT NULL,
  fromObj    INT                                       NOT NULL,
  toObj      INT,
  named      INT,
  instanceof INT                                       NOT NULL,
  createdBy  INT                                       NOT NULL,
  CONSTRAINT fk_fromObj FOREIGN KEY (fromObj) REFERENCES obj ON DELETE CASCADE,
  CONSTRAINT fk_createdBy FOREIGN KEY (createdBy) REFERENCES shortTransaction ON DELETE CASCADE
);

CREATE INDEX i_link_instanceof
  ON link (instanceof);
CREATE INDEX i_fromObj
  ON link (fromObj);
CREATE INDEX i_toObj
  ON link (toObj);

CREATE VIEW transactionCommitted AS
  SELECT
    LONGTRANSACTION.commited,
    SHORTTRANSACTION.id
  FROM LONGTRANSACTION, SHORTTRANSACTION
  WHERE SHORTTRANSACTION.BELONGSTO = LONGTRANSACTION.ID
        AND LONGTRANSACTION.COMMITED IS NOT NULL;

CREATE VIEW transactionOpen AS
  SELECT
    LONGTRANSACTION.ID  AS LTA_ID,
    SHORTTRANSACTION.id AS STA_ID
  FROM LONGTRANSACTION, SHORTTRANSACTION
  WHERE SHORTTRANSACTION.BELONGSTO = LONGTRANSACTION.ID
        AND LONGTRANSACTION.COMMITED IS NULL;

CREATE VIEW linksNotDeleted AS
  SELECT LINK.id
  FROM transactionCommitted, LINK
  WHERE LINK.createdBy = transactionCommitted.id
        AND LINK.id NOT IN (SELECT OBJECT
                            FROM ACCESSED, transactionCommitted
                            WHERE type = 'D' AND transactionCommitted.id = ACCESSED.transaction);