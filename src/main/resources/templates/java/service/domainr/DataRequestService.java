package #package#service.domainr;

import #package#controller.domainr.ListVM;
import #package#controller.domainr.ObjVM;
import #package#core.db.DbException;
import #package#domain.core.PersistentElement;

import java.util.HashMap;
import java.util.List;

#imports#

/**
 * Service definition to make requesting/deleting data possible.
 * Create and update actions will be avaible while manipulation the object via a proxy.
 */
public interface DataRequestService<T extends PersistentElement> {
    /**
     * Gets all elements of the given type.
     * TODO paging
     */
    ListVM<T> findAll() throws DbException;

    /**
     * Limits the search with an check on one association.
     */
    ListVM<T> findAllWithAssociationLike(Long associationId, Object val) throws DbException;

    /**
     * Finds the element to the given id.
     */
    ObjVM<T> findSingle(Long id) throws DbException;

    // TODO delete

    HashMap<Class<? extends PersistentElement>, DataRequestService> requestServices = new HashMap<>();

    public static <R extends PersistentElement> DataRequestService<R> getInstance(Class<R> forClass) {
        if(requestServices.isEmpty()) {
            #line#
        }
        return requestServices.get(forClass);
    }
}
