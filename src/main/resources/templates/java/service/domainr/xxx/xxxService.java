package #package#service.domainr#path#;

import #package#controller.domainr.ListVM;
import #package#controller.domainr.ObjVM;
import #package#core.db.DbException;
import #package#service.domainr.DataRequestService;

#imports#

/**
 * Implements a DataRequestService for Azubi
 */
public class #class_name#Service implements DataRequestService<#class_name#> {
    @Override
    public ListVM<#class_name#> findAll() throws DbException {
        ListVM<#class_name#> result = new ListVM<>();

        #class_name#.findAll().forEach(it ->
            #line#
        ));

        return result;
    }

    @Override
    public ListVM<#class_name#> findAllWithAssociationLike(Long associationId, Object val) throws DbException {
        ListVM<#class_name#> result = new ListVM<>();

        #class_name#.findBy(associationId, val).forEach(it ->
            #line#
        ));

        return result;
    }

    @Override
    public ObjVM<#class_name#> findSingle(Long id) throws DbException {
        return new ObjVM<>(#class_name#.findOneById(id));
    }
}
