package #package#service.core;

public class CategoryAlreadyExistInHierarchyException extends RuntimeException {
    public CategoryAlreadyExistInHierarchyException(String category) {
        super(category);
    }
}
