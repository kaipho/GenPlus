package #package#service.core;

public class CategoryNotExistInClassException extends RuntimeException {
    public CategoryNotExistInClassException(String category) {
        super(category);
    }
}
