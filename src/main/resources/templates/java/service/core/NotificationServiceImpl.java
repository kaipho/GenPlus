package #package#service.core;

import #package#core.security.SecurityContext;
import org.springframework.stereotype.Service;

import java.util.List;

import #package#domain.core.Notification;

import java.util.ArrayList;

import #package#domain.security.User;

import static #package#Application.WEB_SOCKET;
// imports_end

@Service
public class NotificationServiceImpl implements NotificationService {

    private NotificationService self() {
        return this;
    }

    // editable_area_start

    public void addNotification(User user, Notification notification) {
        user.addSingleToNotifications(notification);
        if (WEB_SOCKET != null) {
            WEB_SOCKET.convertAndSend("/notifications/" + user.getId(), notification);
        }
    }

    public List<Notification> getNotifications() {
        return User.findOneById(SecurityContext.getId()).getNotifications();
    }

    // editable_area_end

    // private_area_start

    // private_area_end
}
