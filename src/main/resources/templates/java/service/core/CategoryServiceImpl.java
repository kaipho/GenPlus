package #package#service.core;

import #package#core.db.DbFacade;
import #package#domain.core.CategoryClassVM;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

import java.util.List;
// imports_end

@Service
public class CategoryServiceImpl implements CategoryService {

    private CategoryService self() {
        return this;
    }

    // editable_area_start

    /**
     * Loads all categories of the categoryClass
     */
    public List<String> getValuesFor(Long categoryClassId) {
        return DbFacade.loadCategoriesInstanceOfCategoryClass(categoryClassId);
    }

    /**
     * Adds a single value to the categoryClass
     */
    public void addValueTo(String value, Long categoryClassId) {
        DbFacade.addCategoryToCategoryClass(categoryClassId, value);
    }

    /**
     * Deletes a value from the categoryClass
     */
    public void deleteValue(String value, Long categoryClassId) {
        DbFacade.deletesCategoryFromCategoryClass(categoryClassId, value);
    }

    public List<CategoryClassVM> getAllCategoryClasses() {
        return DbFacade.loadAllCategoryClasses().stream().map(it -> {
            it.setValues(DbFacade.loadCategoriesInstanceOfCategoryClass(it.getName()));
            return it;
        }).collect(Collectors.toList());
    }

    // editable_area_end

    // private_area_start

    // private_area_end
}
