package #package#service.primitive;

import #package#domain.core.primitive.UnitClass;
import #package#domain.core.primitive.UnitInstance;
#imports#

import java.util.HashMap;
import java.util.Map;

public class Primitives {

    public static Map<Long, UnitClass<?>> PRIMITIVE_CLASSES = new HashMap<>();

    static {
        #line(0)#
    }


    public static Map<Long, UnitInstance<?>> PRIMITIVES = new HashMap<>();

    static {
        #line(1)#
    }
}
