package #package#service.transaction;

import #package#core.db.DbFacade;
import #package#core.db.DbStatistic;
import #package#core.security.SecurityContext;
import #package#core.transaction.LongTransaction;
import #package#core.transaction.TransactionContext;
import #package#domain.transaction.LongTransactionVM;
import #package#domain.db.StatistikVM;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.ArrayList;
import java.util.stream.Collectors;

import static #package#Application.WEB_SOCKET;
// imports_end

@Service
public class TransactionServiceImpl implements TransactionService {

    private TransactionService self() {
        return this;
    }

    // editable_area_start

    /**
     * Commits the actual long-transaction from the users session.
     */
    public void commit() {
        if (TransactionContext.isInLongTransaction()) {
            LongTransaction longTransaction = TransactionContext.getLongTransaction();
            LongTransactionVM transactionVM = LongTransactionVM.createUnchecked();
            transactionVM.setLongTaId(longTransaction.getId());

            longTransaction.commit();

            Long userId = SecurityContext.getId();
            if (WEB_SOCKET != null) {
                WEB_SOCKET.convertAndSend("/transactions/" + userId, transactionVM);
            }
        }
    }

    @Override
    public void startTransaction() {
        TransactionContext.startLongTransaction();

        LongTransaction longTransaction = TransactionContext.getLongTransaction();
        LongTransactionVM transactionVM = LongTransactionVM.createUnchecked();
        transactionVM.setLongTaId(longTransaction.getId());

        Long userId = SecurityContext.getId();

        if (WEB_SOCKET != null) {
            WEB_SOCKET.convertAndSend("/transactions/" + userId, transactionVM);
        }
    }

    /**
     * Loads all uncommitted transactions of the user.
     */
    public List<LongTransactionVM> openTransactions() {
        return DbFacade.loadOpenLongTransactions()
                       .stream()
                       .map(it -> {
                           LongTransactionVM longTa = LongTransactionVM.createUnchecked();
                           longTa.setLongTaId(it);
                           return longTa;
                       })
                       .collect(Collectors.toList());
    }

    @Override
    public List<StatistikVM> transactionStatistics() {
        List<StatistikVM> result = new ArrayList<>();
        StatistikVM uncommitted = StatistikVM.createUnchecked();
        uncommitted.setName("uncommitted");
        uncommitted.setValue(DbStatistic.fetchOpenLongTransactions());
        result.add(uncommitted);
        StatistikVM committed = StatistikVM.createUnchecked();
        committed.setName("committed");
        committed.setValue(DbStatistic.fetchCommitedLongTransactions());
        result.add(committed);
        return result;
    }

    // editable_area_end

    // private_area_start

    // private_area_end
}
