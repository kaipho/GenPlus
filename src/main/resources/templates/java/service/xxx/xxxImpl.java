package #package#service#subpackage#;

import org.springframework.stereotype.Service;
#imports#
// imports_end

@Service
public class #class_name#Impl implements #class_name# {
    #associations#

    private #class_name# self() {
        return this;
    }

    // editable_area_start

    #line#

    // editable_area_end

    // private_area_start

    // private_area_end
}