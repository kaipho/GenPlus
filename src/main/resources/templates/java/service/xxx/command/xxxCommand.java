package #package#service#subpackage#.command;

import #package#core.Command;
import #package#core.CommandVisitor;

#imports#

import static #package#core.transaction.Callable.#name#;

public abstract class #class_name#Command extends Command<#generic#> {
    #line(1)#

    protected #class_name#Command(
            #line(2)#
        ) {
        super(#name#);
        #line(3)#
    }

    #line(4)#

    @Override
    public <R> R accept(CommandVisitor<R> visitor) {
        return visitor.visit(this);
    }
}
