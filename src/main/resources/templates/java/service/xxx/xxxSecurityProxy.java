package #package#service#subpackage#;

import #package#core.exceptions.UnauthorizedException;
import #package#core.security.SecurityContext;

#imports#

class #class_name#SecurityProxy implements #class_name# {

    private final #class_name# real;
    
    #class_name#SecurityProxy(#class_name# real) {
        this.real = real;
    }

    #line#
}