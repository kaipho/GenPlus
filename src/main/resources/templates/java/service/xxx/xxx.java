package #package#service#subpackage#;

#imports#

public interface #class_name# {

    #line#

    public static #class_name# getInstance() {
        #class_name# instance = new #class_name#Impl();
        return new #class_name#SecurityProxy(instance);
    }
}