package #package#service#subpackage#;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;


// imports_end

public class #class_name#Test {
    private #class_name# #lower_class_name#;

    @Before
    public void init() {
        #lower_class_name# = #class_name#.getInstance();
    }

    // editable_area_start

    #line#

    // editable_area_end
}