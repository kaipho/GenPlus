package #package#;

import #package#.core.db.DbConnector;
import #package#.domain.MetadataKeys;
import #package#.core.transaction.Transaction;
import #package#.core.transaction.Callable;
import #package#.domain.category.Language;
import #package#.domain.category.Role;
import #package#.domain.security.TechnicalUser;
import #package#.domain.security.User;
import #package#.service.security.BCryptProvider;
import #package#.service.core.CategoryService;
import #package#.service.core.CategoryNotExistInClassException;
import #package#.core.security.SecurityContext;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Context to load all needed data into the db.
 */
public class BootstrapContext {

    /**
     * boots up the application.
     */
    public static void bootstrap() {
        DbConnector.createInMemoryDbConnection();
        Transaction.getInstance()
                   .run(Callable.DEFAULT, () -> {
                       SecurityContext.setUser(TechnicalUser.create("tu", BCryptProvider.getInstance().hashPassword("tu"), Language.empty(), ""));
                       initLangs();
                       initRoles();
                       initAdminUser();
                       return null;
                   })
                   .resolveAndCommit();
    }

    private static void initAdminUser() {
        String name = "admin";
        List<User> founding = User.findBy(MetadataKeys.SECURITY_USER_USERNAME, name);
        if (founding.isEmpty()) {
            TechnicalUser user = TechnicalUser.create(name, BCryptProvider.getInstance().hashPassword(name), Language.empty(), "");
            user.addSingleToRoles(Role.ADMIN);
            user.addSingleToRoles(Role.EDIT);
        }
    }

    private static void initLangs() {
        #line(0)#
    }

    private static Language initLang(String name) {
        try {
            return Language.from(name);
        } catch (CategoryNotExistInClassException e) {
            CategoryService.getInstance().addValueTo(name, Language.ID);
            return Language.from(name);
        }
    }

    private static void initRoles() {
        #line(1)#
    }

    private static Role initRole(String name) {
        try {
            return Role.from(name);
        } catch (CategoryNotExistInClassException e) {
            CategoryService.getInstance().addValueTo(name, Role.ID);
            return Role.from(name);
        }
    }
}
