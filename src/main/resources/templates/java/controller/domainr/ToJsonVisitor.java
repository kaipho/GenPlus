package #package#controller.domainr;

#imports#

import #package#domain.category.PersistentCategory;
import #package#domain.core.PersistentElement;
import #package#core.Constants;
import #package#domain.core.PersistentElementCompleteVisitor;
import #package#domain.core.primitive.PersistentPrimitive;
import #package#core.db.blob.Blob;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonArrayBuilder;

/**
 * Visitor to map a persistent element to its json representation.
 */
public class ToJsonVisitor implements PersistentElementCompleteVisitor<JsonObject> {
    private static ToJsonVisitor INSTANCE = new ToJsonVisitor(false);
    private static ToJsonVisitor INNER_INSTANCE = new ToJsonVisitor(true);
    public static ToJsonVisitor getInstance() {
        return INSTANCE;
    }

    private final boolean isInner;

    private ToJsonVisitor(boolean isInner) {
        this.isInner = isInner;
    }

    private JsonObjectBuilder visitPersistentPrimitive(PersistentPrimitive primitive) {
        JsonObjectBuilder obj = Json.createObjectBuilder();
        obj.add("unit_id", primitive.getUnitInstance().getId());
        obj.add("unit_class_id", primitive.getUnitClass().getId());
        obj.add("value", primitive.resolve().toDouble());
        obj.add("id", primitive.getId());
        return obj;
    }

    private JsonObjectBuilder visitBlob(Blob blob) {
        JsonObjectBuilder obj = Json.createObjectBuilder();
        obj.add("id", blob.getId());
        if(blob.getName() != null) obj.add("name", blob.getName());
        if(blob.getBlobId() != null) obj.add("blobId", blob.getBlobId());
        return obj;
    }

    private JsonObject visitPersistentElement(PersistentElement persistentElement) {
        if(isInner) {
            JsonObjectBuilder obj = Json.createObjectBuilder();
            if(persistentElement.getId() != null) obj.add("id", persistentElement.getId());
            if(persistentElement.getId() != null) obj.add("_rep", persistentElement.toString());
            obj.add("_type", 6);
            obj.add("_self", "/domainr/6/" + persistentElement.getId());
            return obj.build();
        }
        return persistentElement.accept(INNER_INSTANCE);
    }

    #line#
}
