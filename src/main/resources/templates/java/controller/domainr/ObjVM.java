package #package#controller.domainr;

import java.util.HashMap;
import java.util.Map;

/**
 * TODO
 */
public class ObjVM<T> {
    private T obj;
    private Map<String, String> _links;

    public ObjVM(T obj) {
        this.obj = obj;
        this._links = new HashMap<>();
    }

    public T getObj() {
        return obj;
    }

    public Map<String, String> get_links() {
        return _links;
    }
}
