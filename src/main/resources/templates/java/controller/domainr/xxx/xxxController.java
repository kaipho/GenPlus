package #package#controller.domainr#line(0)#;

import #package#controller.domainr.JsonConverter;
import #package#controller.domainr.ListVM;
import #package#controller.domainr.ObjVM;
import #package#core.db.DbException;
import #package#service.domainr.DataRequestService;
import #package#controller.domainr.ToJsonVisitor;
import #package#controller.domainr.ResolvePathVisitor;
import #package#core.transaction.Transaction;
import #package#core.converter.*;

import org.springframework.web.bind.annotation.*;

import javax.json.*;
import java.util.HashMap;
import java.util.Map;

#imports#

import static #package#core.converter.Updater.*;
import static #package#core.converter.UpdaterCategory.updaterCategory;
import static #package#domain.MetadataKeys.*;

@RestController
@RequestMapping("api/domainr/#path#")
public class #class_name#Controller {
    private DataRequestService<#class_name#> requestService = DataRequestService.getInstance(#class_name#.class);

    @GetMapping
    public String findAll() throws DbException {
        ListVM<#class_name#> objects = requestService.findAll();
        return JsonConverter.listVmToJson(objects);
    }

    @GetMapping("/find")
    public String findAllWithAssociationLike(Long associationId, @RequestParam("val") Object val) throws DbException {
        ListVM<#class_name#> objects = requestService.findAllWithAssociationLike(associationId, val);
        return JsonConverter.listVmToJson(objects);
    }

    @GetMapping("/{id}")
    public String findSingle(@PathVariable Long id) throws DbException {
        #class_name# it = #class_name#.findOneById(id);
        JsonObject obj = it.accept(ToJsonVisitor.getInstance());
        JsonObjectBuilder links = Json.createObjectBuilder();
        links.add("self", "domainr/" + it.accept(new ResolvePathVisitor()) + "/" + it.getId());
        #line(2)#

        JsonObjectBuilder objVm = Json.createObjectBuilder().add("_embedded", obj)
                                                            .add("_links", links.build());
        return objVm.build().toString();
    }

    private static final HashMap<String, CrudUpdater<#class_name#>> updater = new HashMap<>();

    static {
        #line(3)#
    }

    @PutMapping("/{id}")
    public void updateSingle(@PathVariable("id") Long id, @RequestBody HashMap<String, Object> obj) throws DbException {
        Transaction.getInstance()
                    .run(()->Updater.updateAll(#class_name#.findOneById(id),obj,updater))
                    .resolve();
    }

    #line(1)#
}
