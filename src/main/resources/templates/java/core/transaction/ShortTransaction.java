package #package#core.transaction;

/**
 * Representation of a short transaction from the metamodell.
 */
public class ShortTransaction {
    private final long id;
    private final LongTransaction belongsTo;

    public ShortTransaction(long id, LongTransaction belongsTo) {
        this.id = id;
        this.belongsTo = belongsTo;
    }

    public long getId() {
        return id;
    }

    public LongTransaction getBelongsTo() {
        return belongsTo;
    }
}
