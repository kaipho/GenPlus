package #package#core.transaction;

import #package#core.db.DbConnector;
import #package#core.db.DbException;
import #package#core.db.DbFacade;
import #package#core.security.SecurityContext;
import #package#domain.security.User;

import java.util.function.Supplier;

public class TransactionExecutor<R> {
    private final Supplier<R> logic;
    private final LongTransaction runsInTransaction;
    private final Callable instanceOf;
    private Thread t;

    private R result;
    private RuntimeException exception1;
    private Error exception2;

    TransactionExecutor(Supplier<R> logic, LongTransaction runsInTransaction, Callable instanceOf) {
        this.logic = logic;
        this.runsInTransaction = runsInTransaction;
        this.instanceOf = instanceOf;
    }

    public LongTransaction getRunsInTransaction() {
        return runsInTransaction;
    }

    void execute() {
        User user = SecurityContext.getUser();
        t = new Thread(() -> {
            ShortTransaction shortTransaction = DbFacade.createShortTransaction(runsInTransaction, instanceOf.id);
            TransactionContext.setTransaction(shortTransaction);
            SecurityContext.setUser(user);
            try {
                DbConnector.getConnector().startTransaction();
                this.result = logic.get();
                DbConnector.getConnector().commitTransaction();
            } catch (RuntimeException e) {
                this.exception1 = e;
                DbConnector.getConnector().rollback();
            } catch (Error e) {
                this.exception2 = e;
                DbConnector.getConnector().rollback();
            }
        });
        t.start();
    }


    public R resolveAndCommit() {
        R result = resolve();
        runsInTransaction.commit();
        DbConnector.getConnector().commit();
        return result;
    }

    public R resolve() {
        try {
            t.join();
        } catch (InterruptedException e) {
            throw new DbException(e);
        }
        if (exception1 != null) {
            throw this.exception1;
        }
        if (exception2 != null) {
            throw this.exception2;
        }
        return result;
    }
}
