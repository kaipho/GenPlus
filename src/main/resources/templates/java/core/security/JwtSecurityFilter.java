package #package#core.security;

import #package#core.Constants;
import #package#domain.security.AnonymUser;
import #package#domain.security.User;
import #package#service.security.JwtTokenService;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.SignatureException;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtSecurityFilter extends GenericFilterBean {

    private JwtTokenService tokenService;

    public JwtSecurityFilter() {
        this.tokenService = JwtTokenService.getInstance();
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        try {
            HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
            String jwt = resolveToken(httpServletRequest);
            if (StringUtils.hasText(jwt)) {
                try {
                    User user = this.tokenService.parseToken(jwt);
                    SecurityContext.setUser(user);
                } catch (SignatureException e) {
                    SecurityContext.setUser(AnonymUser.createUnchecked());
                }
            } else {
                SecurityContext.setUser(AnonymUser.createUnchecked());
            }
            filterChain.doFilter(servletRequest, servletResponse);
        } catch (ExpiredJwtException eje) {
            ((HttpServletResponse) servletResponse).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }

    private String resolveToken(HttpServletRequest request){
        String bearerToken = request.getHeader(Constants.AUTHORIZATION_HEADER);
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith(Constants.BEARER_TOKEN)) {
            return bearerToken.substring(7, bearerToken.length());
        }
        return null;
    }
}
