package #package#core.security;

import #package#domain.security.AnonymUser;
import #package#domain.security.User;
import #package#domain.security.impl.AnonymUserImpl;

/**
 * Context to store user information. The information should be set at incomming request.
 * The information is stored only for the thread!
 */
public class SecurityContext {
    private static ThreadLocal<User> user = ThreadLocal.withInitial(AnonymUserImpl::new);

    public static User getUser() {
        return SecurityContext.user.get();
    }

    public static void setUser(User user) {
        SecurityContext.user = ThreadLocal.withInitial(() -> user);
    }

    public static boolean isLoggedIn() {
        return getUser() != null;
    }

    public static Long getId() {
        if (SecurityContext.isLoggedIn() && !(getUser() instanceof AnonymUser))
            return SecurityContext.user.get().getId();
        return -1L;
    }
}