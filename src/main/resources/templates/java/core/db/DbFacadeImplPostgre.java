package #package#core.db;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Types;

/**
 * Created by Neo on 08.02.17.
 */
public class DbFacadeImplPostgre implements DbFacadeImpl {
    @Override
    public void deleteObj(Long id) {
        try {
            CallableStatement function = DbConnector.getConnector()
                                                    .getFunctionCall("{ ? = call deleteobj(?)}");
            function.registerOutParameter(1, Types.OTHER);
            function.setInt(2, id.intValue());
            function.execute();
        } catch (SQLException e) {
            throw new DbException(e);
        }
    }
}
