package #package#core.db.primitives;

import #package#domain.core.PersistentElement;
import #package#domain.core.primitive.PersistentPrimitive;
import #package#domain.core.primitive.UnitClass;

public interface PrimitivesStore {
    static #package#core.db.primitives.PrimitivesStore getStore() {
        return PrimitivesStoreH2.INSTANCE;
    }

    void savePrimitive(PersistentPrimitive<?> primitive, PersistentElement owner, Long linkInstanceOf);
    <RANGE extends UnitClass> PersistentPrimitive<RANGE> loadPersistentPrimitive(Long id, RANGE range);
    <RANGE extends UnitClass> PersistentPrimitive<RANGE> loadPersistentPrimitive(PersistentElement owner, Long linkInstanceOf, RANGE range);
}
