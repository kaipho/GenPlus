package #package#core.db.primitives;

import #package#core.db.DbConnector;
import #package#core.db.DbException;
import #package#core.db.cache.PrimitivesCache;
import #package#core.transaction.TransactionContext;
import #package#domain.core.PersistentElement;
import #package#domain.core.primitive.PersistentPrimitive;
import #package#domain.core.primitive.Rational;
import #package#domain.core.primitive.UnitClass;
import #package#domain.core.primitive.UnitInstance;
import #package#service.primitive.Primitives;

import java.util.List;
import java.util.Optional;

/**
 * Primitive store implementation for H2 db.
 */
public class PrimitivesStoreH2 implements #package#core.db.primitives.PrimitivesStore {
    static #package#core.db.primitives.PrimitivesStoreH2 INSTANCE = new #package#core.db.primitives.PrimitivesStoreH2();

    @Override
    public void savePrimitive(PersistentPrimitive<?> primitive, PersistentElement owner, Long linkInstanceOf) {
        Long rationalId = saveRational(primitive.resolve()); // TODO hier wird ausgerechnet, also den primitive auch intern korrekt setzen?
        savePrimitiveInDefaultUnit(primitive, owner, linkInstanceOf, rationalId);
    }

    /**
     * Tries to find the rational in the cache and the db. If no rational exist, a new one will be created.
     *
     * @return the id of the rational
     */
    private Long saveRational(Rational r) {
        // Check if primitive exist in Cache
        Optional<Long> existingCacheVal = PrimitivesCache.getInstance()
                                                         .findRational(r.getNumerator(), r.getDenominator());
        if (existingCacheVal.isPresent()) {
            return existingCacheVal.get();
        }

        // Check if primitive exist in DB
        List<Long> existing = DbConnector.getConnector()
                                         .getBuilderFor("SELECT id FROM val_rational WHERE numerator = ? AND denominator = ?")
                                         .withLong(r.getNumerator())
                                         .withLong(r.getDenominator())
                                         .asQuery()
                                         .forEach(it -> it.getLong(1));

        if (existing.isEmpty()) {
            Long id = DbConnector.getConnector()
                                 .getBuilderFor("INSERT INTO val_rational (numerator, denominator) VALUES (?, ?)")
                                 .withLong(r.getNumerator())
                                 .withLong(r.getDenominator())
                                 .asManipulation()
                                 .resolveKey();
            PrimitivesCache.getInstance().cacheRational(id, r);
            return id;
        } else {
            if (existing.size() > 1) throw new DbException("Uniques of the rational number not guaranteed");
            PrimitivesCache.getInstance().cacheRational(existing.get(0), r);
            return existing.get(0);
        }
    }

    private void savePrimitiveInDefaultUnit(PersistentPrimitive<?> primitive, PersistentElement owner, Long linkInstanceOf, Long rationalId) {
        Long unitId = primitive.getUnitInstance().getId();
        // 1. look if a link already exist
        Optional<Long> toObj = DbConnector.getConnector()
                                          .getBuilderFor("SELECT toobj FROM link WHERE instanceof = ? AND fromobj = ?")
                                          .withLong(linkInstanceOf)
                                          .withLong(owner.getId())
                                          .asQuery()
                                          .forSingleOptional(it -> it.getLong(1));
        if (toObj.isPresent()) {
            // recycle existing primitive
            primitive.setId(toObj.get());
            DbConnector.getConnector()
                       .getBuilderFor("UPDATE persistent_primitive SET fraction = ?, unit = ? WHERE id = ?")
                       .withLong(rationalId)
                       .withLong(unitId)
                       .withLong(toObj.get())
                       .asManipulation()
                       .submit();
        } else {
            // create new primitive
            Long primitiveId = DbConnector.getConnector()
                                          .getBuilderFor("INSERT INTO persistent_primitive (fraction, unit) VALUES (?, ?)")
                                          .withLong(rationalId)
                                          .withLong(unitId)
                                          .asManipulation()
                                          .resolveKey();
            primitive.setId(primitiveId);
            DbConnector.getConnector()
                        .getBuilderFor("INSERT INTO link (pos, fromobj, toobj, instanceof, CREATEDBY) VALUES (-1, ?, ?, ?, ?)")
                        .withLong(owner.getId())
                        .withLong(primitiveId)
                        .withLong(linkInstanceOf)
                        .withLong(TransactionContext.getTransaction().getId())
                        .asManipulation()
                        .submit();
        }
    }

    @Override
    public <RANGE extends UnitClass> PersistentPrimitive<RANGE> loadPersistentPrimitive(Long id, RANGE range) {
        return DbConnector.getConnector()
                          .getBuilderFor("SELECT unit, fraction FROM persistent_primitive WHERE id = ?")
                          .withLong(id)
                          .asQuery()
                          .forSingle(row -> createPrimitive(id, row.getLong("fraction"), row.getLong("unit")));
    }

    @Override
    public <RANGE extends UnitClass> PersistentPrimitive<RANGE> loadPersistentPrimitive(PersistentElement owner, Long linkInstanceOf, RANGE range) {
        Optional<Long> primitiveId = DbConnector.getConnector()
                                                .getBuilderFor("SELECT toobj FROM link WHERE instanceof = ? AND fromobj = ?")
                                                .withLong(linkInstanceOf)
                                                .withLong(owner.getId())
                                                .asQuery()
                                                .forSingleOptional(it -> it.getLong(1));
        if (primitiveId.isPresent())
            return loadPersistentPrimitive(primitiveId.get(), range);
        PersistentPrimitive<RANGE> primitive = PersistentPrimitive.empty(range);
        savePrimitive(primitive, owner, linkInstanceOf);
        return loadPersistentPrimitive(owner, linkInstanceOf, range);
    }

    private <RANGE extends UnitClass> PersistentPrimitive<RANGE> createPrimitive(Long primitiveId, Long rationalId, Long unitId) {
        Optional<Rational> possibleRational = PrimitivesCache.getInstance().findRational(rationalId);

        Rational rational;
        if (possibleRational.isPresent()) {
            rational = possibleRational.get();
        } else {
            rational = DbConnector.getConnector()
                                  .getBuilderFor("SELECT numerator, denominator FROM val_rational WHERE id = ?")
                                  .withLong(rationalId)
                                  .asQuery()
                                  .forSingle(row -> new Rational(row.getLong("numerator"), row.getLong("denominator")));
            PrimitivesCache.getInstance().cacheRational(rationalId, rational);
        }
        if (!Primitives.PRIMITIVES.containsKey(unitId)) {
            throw new DbException("No Primitive Type with id " + unitId);
        }

        UnitInstance unit = Primitives.PRIMITIVES.get(unitId);
        return new PersistentPrimitive<RANGE>(primitiveId, rational, unit);
    }
}
