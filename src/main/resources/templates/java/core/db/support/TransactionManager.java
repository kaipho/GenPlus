package #package#core.db.support;

import #package#core.db.DbConnector;

import java.util.function.Consumer;
import java.util.function.Function;

public class TransactionManager {

    public static <R> #package#core.db.support.TransactionContext<R> execute(Function<#package#core.db.support.TransactionContext<R>, R> executable) {
        throw new RuntimeException("Not Implemented!");
    }

    public static #package#core.db.support.TransactionContext<Void> execute(String name, Consumer<#package#core.db.support.TransactionContext<Void>> executable) {
        DbConnector.getConnector().startTransaction();
        System.out.println("Transaction gestartet");
        #package#core.db.support.TransactionContext<Void> context = new #package#core.db.support.TransactionContext<>();

        try {
            executable.accept(context);
            System.out.println("commit");
            DbConnector.getConnector().commitTransaction();
            return context;
        } catch (RuntimeException e) {
            context.setE(e);
            e.printStackTrace();
            System.out.println("rollback");
            DbConnector.getConnector().rollback();
            return context;
        }
    }

}
