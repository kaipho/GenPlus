package #package#core.db.support;

public class TransactionContext<R> {
    private R result;
    private RuntimeException e;

    public R getResult() {
        if(e != null) {
            throw e;
        }
        return result;
    }

    void setResult(R result) {
        this.result = result;
    }

    void setE(RuntimeException e) {
        this.e = e;
    }
}
