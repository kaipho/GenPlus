package #package#core.db;

/**
 * A link persisted in the database.
 */
public class DbLink {
    private Long id;
    private Long fromObj;
    private Long toObj;
    private Long instanceOf;
    /**
     * Position information for sequences. Is a total order.
     */
    private Long pos;
    /**
     * Set for named associations.
     */
    private Long name;

    public DbLink(Long id, Long fromObj, Long toObj, Long instanceOf, Long pos, Long name) {
        this.id = id;
        this.fromObj = fromObj;
        this.toObj = toObj;
        this.instanceOf = instanceOf;
        this.pos = pos;
        this.name = name;
    }

    public DbLink(Long id, Long toObj, Long instanceOf, Long pos, Long name) {
        this.id = id;
        this.toObj = toObj;
        this.instanceOf = instanceOf;
        this.pos = pos;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFromObj() {
        return fromObj;
    }

    public void setFromObj(Long fromObj) {
        this.fromObj = fromObj;
    }

    public Long getToObj() {
        return toObj;
    }

    public void setToObj(Long toObj) {
        this.toObj = toObj;
    }

    public Long getInstanceOf() {
        return instanceOf;
    }

    public void setInstanceOf(Long instanceOf) {
        this.instanceOf = instanceOf;
    }

    public Long getPos() {
        return pos;
    }

    public void setPos(Long pos) {
        this.pos = pos;
    }

    public Long getName() {
        return name;
    }

    public void setName(Long name) {
        this.name = name;
    }
}
