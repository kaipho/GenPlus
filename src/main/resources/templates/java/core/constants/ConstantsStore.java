package #package#core.constants;

import #package#Application;
import #package#core.security.SecurityContext;
import #package#domain.core.Constant;
import #package#domain.core.ConstantLanguage;
import #package#domain.core.ConstantPackage;

import java.text.MessageFormat;
import java.util.*;

/**
 * This is the central store to access constants.
 * Constants are stores in the db and loaded on demand.
 * <p>
 * Loaded constants get cached local and if updated thou the admin interface, the cache get updated too.
 * If the constants gets changed direct, the clearCache method will help.
 */
public class ConstantsStore {
    public static final String CONSTANT_NOT_DEFINED = "Constant not defined!";
    private static Map<String, ConstantLanguage> LANGS = new HashMap<>();

    private static final Map<String, ConstantsContainer> constantsContainer = new HashMap<>();

    #line(0)#

    /**
     * Method to clear the local cache. Needed if constants changed in the db to reload them on next access.
     */
    public void clearCache() {
        constantsContainer.clear();
    }

    static abstract class ConstantsContainer {
        private final Map<String, Constant> cache;
        private final ConstantPackage constantPackage;

        ConstantsContainer(List<ConstantPackage> constantPackage, String packagee) {
            this.constantPackage = constantPackage.stream()
                                                  .filter((it) -> it.getName().equals(packagee))
                                                  .findFirst()
                                                  .orElseThrow(ConstantNotDefinedException::new);
            this.cache = new HashMap<>();
        }

        /**
         * Checks if the constant exist. If not or its value is the default value, en error gets thrown.
         */
        void checkConstantExist(String constant) throws ConstantNotDefinedException {
            if (!getCache().containsKey(constant)) {
                Constant finding = getConstantPackage().getConstants()
                                                       .stream()
                                                       .filter((it) -> it.getName().equals(constant))
                                                       .findFirst()
                                                       .orElseThrow(ConstantNotDefinedException::new);
                getCache().put(constant, finding);
            }
            if (getCache().get(C_MESSAGE_ORT_VHV).getValue().equals(CONSTANT_NOT_DEFINED)) {
                throw new ConstantNotDefinedException();
            }
        }

        Map<String, Constant> getCache() {
            return cache;
        }

        ConstantPackage getConstantPackage() {
            return constantPackage;
        }
    }

    #line(1)#

    // Packages
    #line(2)#

    // Constants
    #line(3)#

    public static List<String> packages() {
        return Arrays.asList(P_MESSAGE);
    }

    public static List<String> constantsInPackage(String s) {
        switch (s) {
            #line(4)#
        }
        return Collections.emptyList();
    }
}