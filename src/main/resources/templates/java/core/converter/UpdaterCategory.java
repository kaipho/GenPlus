package #package#core.converter;


import #package#domain.category.PersistentCategory;

import java.util.function.BiConsumer;
import java.util.function.Function;

public class UpdaterCategory<O, T extends PersistentCategory> implements CrudUpdater<O> {
    private final Function<String, T> categoryCreator;
    private final BiConsumer<O, T> setter;
    private final Function<O, T> getter;

    private UpdaterCategory(Function<String, T> categoryCreator, BiConsumer<O, T> setter, Function<O, T> getter) {
        this.categoryCreator = categoryCreator;
        this.setter = setter;
        this.getter = getter;
    }

    public static <O, T extends PersistentCategory> CrudUpdater<O> updaterCategory(Function<String, T> categoryCreator, BiConsumer<O, T> setter, Function<O, T> getter) {
        return new #package#core.converter.UpdaterCategory<>(categoryCreator, setter, getter);
    }

    @Override
    public void execute(O obj, Object value) {
        if (value == null) {
            setter.accept(obj, categoryCreator.apply(null));
            return;
        }
        if (!value.equals(getter.apply(obj))) {
            setter.accept(obj, categoryCreator.apply(Updater.UPDATER_STRING.apply(value)));
        }
    }
}