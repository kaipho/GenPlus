package #package#core;

import #package#core.transaction.Callable;

import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

public abstract class Command<T> {
    private CompletableFuture<T> result = new CompletableFuture<>();
    private Error error;

    private final Callable callableId;

    protected Command(Callable callableId) {
        this.callableId = callableId;
    }

    public Command<T> execute() {
        try {
            this.result.complete(executeImpl());
        } catch (Error t) {
            this.error = t;
        }
        return this;
    }

    protected abstract T executeImpl();

    public T getResult() throws Error {
        try {
            T futureResult = result.get();
            if(error != null) {
                throw error;
            }
            return futureResult;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Throwable getError() {
        return error;
    }

    public Callable getCallableId() {
        return callableId;
    }

    public void then(Consumer<T> onResult, Consumer<Throwable> onError) {
        try {
            T futureResult = result.get();
            if(error != null) {
                onError.accept(error);
            }
            onResult.accept(futureResult);
        } catch (Exception e) {
            onError.accept(new RuntimeException(e));
        }
    }

    public abstract <R> R accept(CommandVisitor<R> visitor);
}
