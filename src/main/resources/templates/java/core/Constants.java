package #package#core;

import java.time.format.DateTimeFormatter;

public enum Constants {
    ;

    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String TRANSACTION_HEADER = "Transaction";
    public static final String BEARER_TOKEN = "Bearer ";
    public static final String AUTHORITIES_KEY = "auth";
    public static final String SECURITY_KEY = "abcdefghijk";

    public static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public static final String LANG_DEFAULT = "default";
}