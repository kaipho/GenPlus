package #package#repository;

import #package#domain.core.AllowedValue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "allowedValues")
public interface AllowedValueRepository extends JpaRepository<AllowedValue, String> {

}