package #package#domain.constant.core;

public class PersistentConstant {
    private final Long id;

    public PersistentConstant(Long id) {
        this.id = id;
    }
}
