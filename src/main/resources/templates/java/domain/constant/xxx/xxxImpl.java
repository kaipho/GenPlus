package #package#domain.constant.#subpackage#;

import #package#domain.constant.core.PersistentConstant;
import #package#i18n.I18nContext;
import #package#i18n.I18nModelConstantContext;

import java.util.HashMap;
import java.util.Map;

class #class_name#Impl extends PersistentConstant implements #class_name# {
    private final Map<String, Object> data;

    #class_name#Impl(#params#) {
        super(#class_name#.ID);
        data = new HashMap<>();
        #line#
    }

    @Override
    public I18nContext i18n() {
        return I18nModelConstantContext.get(ID, data);
    }
}
