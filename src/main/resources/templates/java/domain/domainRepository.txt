package #package#repository;

import org.springframework.data.jpa.repository.JpaRepository;
#imports#

#comment#
#annotations#
public interface #class_name#Repository extends JpaRepository<#class_name#, Long> {
	#line#
}
