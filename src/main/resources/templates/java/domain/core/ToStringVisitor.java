package #package#domain.core;

import java.util.Optional;
import java.util.function.Function;

public class ToStringVisitor implements PersistentElementCompleteVisitor<String> {
    public static final ToStringVisitor INSTANCE = new ToStringVisitor();
    public static final String NULL_REPRESENTATION = "-";

    private <T> String getNullSafe(T obj, Function<T, Object> f) {
        if(obj == null)return NULL_REPRESENTATION;
        Object result = f.apply(obj);
        if (result == null) return NULL_REPRESENTATION;
        return result.toString();
    }

    private <T> String getNullSafe(Optional<T> optional, Function<T, Object> f){
        if(optional == null)
            return NULL_REPRESENTATION;
        return optional.map(x -> getNullSafe(x, f)).orElse(NULL_REPRESENTATION);
    }
}
