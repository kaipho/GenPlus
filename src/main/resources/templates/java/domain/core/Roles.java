package #package#domain.core;

/**
 * All roles defined in the model for the security system. Each Role starts with 'ROLE_'.
 * Does not define add Roles at this place!
 */
public enum Roles {
    ;
    private static final String ROLE = "ROLE_";

    public static final String ADMIN = ROLE + "ADMIN";
    public static final String EDIT = ROLE + "EDIT";

    #line#
}
