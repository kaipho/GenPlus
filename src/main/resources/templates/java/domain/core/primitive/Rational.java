package #package#domain.core.primitive;

public class Rational implements Comparable<#package#domain.core.primitive.Rational> {
    public static #package#domain.core.primitive.Rational ZERO = new #package#domain.core.primitive.Rational(0L, 1L);
    public static #package#domain.core.primitive.Rational ONE = new #package#domain.core.primitive.Rational(1L, 1L);

    public static #package#domain.core.primitive.Rational NO_VAL = new #package#domain.core.primitive.Rational(0L, 1L) {
        @Override
        public #package#domain.core.primitive.Rational times(#package#domain.core.primitive.Rational b) {
            return this;
        }

        @Override
        public #package#domain.core.primitive.Rational plus(#package#domain.core.primitive.Rational b) {
            return this;
        }

        @Override
        public #package#domain.core.primitive.Rational negate() {
            throw new NullPointerException();
        }

        @Override
        public #package#domain.core.primitive.Rational minus(#package#domain.core.primitive.Rational b) {
            return this;
        }

        @Override
        public #package#domain.core.primitive.Rational reciprocal() {
            throw new NullPointerException();
        }

        @Override
        public #package#domain.core.primitive.Rational divides(#package#domain.core.primitive.Rational b) {
            return this;
        }

        @Override
        public int compareTo(#package#domain.core.primitive.Rational b) {
            return -1;
        }

        @Override
        public boolean equals(Object y) {
            return y == NO_VAL;
        }

        @Override
        public double toDouble() {
            return 0;
        }

        @Override
        public String toString() {
            return "NO_VAL";
        }
    };

    private static final String FractionStroke = "/";
    private static final String Separator = ".";

    private Long numerator;
    private Long denominator;

    public Rational(Long numerator, Long denominator) {
        if (denominator == 0) {
            throw new ArithmeticException("denominator must not be zero");
        }
        long g = gcd(numerator, denominator);

        this.numerator = numerator;
        this.denominator = denominator;
    }

    public Long getNumerator() {
        return numerator;
    }

    public Long getDenominator() {
        return denominator;
    }

    public #package#domain.core.primitive.Rational times(#package#domain.core.primitive.Rational b) {
        if(b == NO_VAL) return b;
        #package#domain.core.primitive.Rational a = this;

        // reduce p1/q2 and p2/q1, then multiply, where a = p1/q1 and b = p2/q2
        #package#domain.core.primitive.Rational c = new #package#domain.core.primitive.Rational(a.numerator, b.denominator);
        #package#domain.core.primitive.Rational d = new #package#domain.core.primitive.Rational(b.numerator, a.denominator);
        return new #package#domain.core.primitive.Rational(c.numerator * d.numerator, c.denominator * d.denominator);
    }


    // return a + b, staving off overflow
    public #package#domain.core.primitive.Rational plus(#package#domain.core.primitive.Rational b) {
        if(b == NO_VAL) return b;
        #package#domain.core.primitive.Rational a = this;

        // special cases
        if (a.compareTo(ZERO) == 0) return b;
        if (b.compareTo(ZERO) == 0) return a;

        // Find gcd of numeratorerators and denominatorominators
        long f = gcd(a.numerator, b.numerator);
        long g = gcd(a.denominator, b.denominator);

        // add cross-product terms for numeratorerator
        #package#domain.core.primitive.Rational s = new #package#domain.core.primitive.Rational((a.numerator / f) * (b.denominator / g) + (b.numerator / f) * (a.denominator / g),
                lcm(a.denominator, b.denominator));

        // multiply back in
        s.numerator *= f;
        return s;
    }

    // return -a
    public #package#domain.core.primitive.Rational negate() {
        return new #package#domain.core.primitive.Rational(-numerator, denominator);
    }

    // return a - b
    public #package#domain.core.primitive.Rational minus(#package#domain.core.primitive.Rational b) {
        if(b == NO_VAL) return b;
        #package#domain.core.primitive.Rational a = this;
        return a.plus(b.negate());
    }


    public #package#domain.core.primitive.Rational reciprocal() {
        return new #package#domain.core.primitive.Rational(denominator, numerator);
    }

    // return a / b
    public #package#domain.core.primitive.Rational divides(#package#domain.core.primitive.Rational b) {
        if(b == NO_VAL) return b;
        #package#domain.core.primitive.Rational a = this;
        return a.times(b.reciprocal());
    }

    private static long gcd(long m, long n) {
        if (m < 0) m = -m;
        if (n < 0) n = -n;
        if (0 == n) return m;
        else return gcd(n, m % n);
    }

    private static long lcm(long m, long n) {
        if (m < 0) m = -m;
        if (n < 0) n = -n;
        return m * (n / gcd(m, n));    // parentheses important to avoid overflow
    }

    @Override
    public int compareTo(#package#domain.core.primitive.Rational b) {
        #package#domain.core.primitive.Rational a = this;
        long lhs = a.numerator * b.denominator;
        long rhs = a.denominator * b.numerator;
        if (lhs < rhs) return -1;
        if (lhs > rhs) return +1;
        return 0;
    }

    public boolean equals(Object y) {
        if (y == null) return false;
        if (y.getClass() != this.getClass()) return false;
        #package#domain.core.primitive.Rational b = (#package#domain.core.primitive.Rational) y;
        return compareTo(b) == 0;
    }


    public int hashCode() {
        return this.toString()
                   .hashCode();
    }

    public double toDouble() {
        return (double) numerator / denominator;
    }

    public static #package#domain.core.primitive.Rational of(String s) {
        Long numerator = 1L;
        Long denominator = 1L;
        if (s == null) return ZERO;
        int fractionStrokePosition = s.indexOf(FractionStroke);
        if (fractionStrokePosition >= 0) {
            String enumeratorText = s.substring(0, fractionStrokePosition);
            if (enumeratorText.length() > 0) numerator = Long.parseLong(enumeratorText);
            String denominatorText = s.substring(fractionStrokePosition + FractionStroke.length(), s.length());
            if (denominatorText.length() > 0) denominator = Long.parseLong(denominatorText);
        } else {
            int commaPosition = s.indexOf(Separator);
            if (commaPosition >= 0) {
                String inFrontOfComma = s.substring(0, commaPosition);
                String behindComma = s.substring(commaPosition + Separator.length(), s.length());
                numerator = Long.parseLong(inFrontOfComma + behindComma);
                denominator = (long) Math.pow(10.0, behindComma.length());
            } else {
                numerator = Long.parseLong(s);
            }
        }
        if (denominator.equals(0L)) throw new ArithmeticException("denominator must not be zero");
        return new #package#domain.core.primitive.Rational(numerator, denominator);
    }

    public static #package#domain.core.primitive.Rational of(Double d) {
        String s = String.valueOf(d);
        long digitsDec = s.length() - 1 - s.indexOf('.');

        long denominator = 1;
        for (int i = 0; i < digitsDec; i++) {
            d *= 10;
            denominator *= 10;
        }
        long numerator = Math.round(d);
        return new #package#domain.core.primitive.Rational(numerator, denominator);
    }

    public static #package#domain.core.primitive.Rational of(Integer anzahlNoten) {
        return new #package#domain.core.primitive.Rational((long) anzahlNoten, 1L);
    }
}
