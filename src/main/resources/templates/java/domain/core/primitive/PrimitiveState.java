package #package#domain.core.primitive;

/**
 * Defines if the primitive is combined or elementary.
 */
public abstract class PrimitiveState<RANGE extends #package#domain.core.primitive.UnitClass> {
    protected final PersistentPrimitive<RANGE> of;

    PrimitiveState(PersistentPrimitive<RANGE> of) {
        this.of = of;
    }

    public abstract PersistentPrimitive<RANGE> to(UnitInstance<RANGE> unit);

    /**
     * Calculates the value in the default Unit of RANGE if the primitive is combined. Else return the stored value.
     */
    public abstract #package#domain.core.primitive.Rational resolve();

    public abstract PersistentPrimitive<RANGE> add(PersistentPrimitive<RANGE> primitive);
    public abstract PersistentPrimitive<?> mult(PersistentPrimitive<?> primitive);
    public abstract PersistentPrimitive<?> minus(PersistentPrimitive<?> primitive);
    public abstract PersistentPrimitive<?> div(PersistentPrimitive<?> primitive);

    /**
     * Normalizes the unit instance to the default unit if the primitive is combined. Otherwise returns the actual instance.
     */
    public abstract UnitInstance<RANGE> getUnitInstance();

    public abstract <R> R visit(PrimitiveStateVisitor<R> visitor);
}
