package #package#domain.core.primitive;

public interface PrimitiveStateVisitor<R> {
    R accept(#package#domain.core.primitive.PersistentCombinedPrimitive rangePersistentCombinedPrimitive);
    R accept(#package#domain.core.primitive.PersistentElementaryPrimitive rangePersistentElementaryPrimitive);
    R accept(#package#domain.core.primitive.PersistentEmptyPrimitive rangePersistentEmptyPrimitive);
}
