package #package#domain.core.primitive;

/**
 * Created by Neo on 03.03.17.
 */
public class PersistentElementaryPrimitive<RANGE extends #package#domain.core.primitive.UnitClass> extends #package#domain.core.primitive.PrimitiveState<RANGE> {
    private final #package#domain.core.primitive.Rational fraction;
    private final #package#domain.core.primitive.UnitInstance<RANGE> unit;

    PersistentElementaryPrimitive(#package#domain.core.primitive.Rational fraction, #package#domain.core.primitive.UnitInstance<RANGE> unit, #package#domain.core.primitive.PersistentPrimitive<RANGE> of) {
        super(of);
        this.fraction = fraction;
        this.unit = unit;
    }

    @Override
    public #package#domain.core.primitive.PersistentPrimitive<RANGE> to(#package#domain.core.primitive.UnitInstance<RANGE> unit) {
        return new #package#domain.core.primitive.PersistentPrimitive<>(unit.fromDefault(this.unit.toDefault(fraction)), unit);
    }

    @Override
    public #package#domain.core.primitive.Rational resolve() {
        return fraction;
    }

    @Override
    public #package#domain.core.primitive.PersistentPrimitive<RANGE> add(#package#domain.core.primitive.PersistentPrimitive<RANGE> primitive) {
        return primitive.visit(new PrimitiveStateVisitor<#package#domain.core.primitive.PersistentPrimitive<RANGE>>() {
            @Override
            public #package#domain.core.primitive.PersistentPrimitive<RANGE> accept(#package#domain.core.primitive.PersistentCombinedPrimitive rangePersistentCombinedPrimitive) {
                return null;
            }

            @Override
            public #package#domain.core.primitive.PersistentPrimitive<RANGE> accept(#package#domain.core.primitive.PersistentElementaryPrimitive rangePersistentElementaryPrimitive) {
                return new #package#domain.core.primitive.PersistentPrimitive<>(fraction.plus(rangePersistentElementaryPrimitive.fraction), unit);
            }

            @Override
            public #package#domain.core.primitive.PersistentPrimitive<RANGE> accept(#package#domain.core.primitive.PersistentEmptyPrimitive rangePersistentEmptyPrimitive) {
                return primitive;
            }
        });
    }

    @Override
    public #package#domain.core.primitive.PersistentPrimitive<?> mult(#package#domain.core.primitive.PersistentPrimitive<?> primitive) {
        return primitive.visit(new PrimitiveStateVisitor<#package#domain.core.primitive.PersistentPrimitive<?>>() {
            @Override
            public #package#domain.core.primitive.PersistentPrimitive<?> accept(#package#domain.core.primitive.PersistentCombinedPrimitive rangePersistentCombinedPrimitive) {
                return null;
            }

            @Override
            public #package#domain.core.primitive.PersistentPrimitive<?> accept(#package#domain.core.primitive.PersistentElementaryPrimitive rangePersistentElementaryPrimitive) {
                // TODO units do not match
                return new #package#domain.core.primitive.PersistentPrimitive<>(fraction.times(rangePersistentElementaryPrimitive.fraction), unit);
            }

            @Override
            public #package#domain.core.primitive.PersistentPrimitive<?> accept(#package#domain.core.primitive.PersistentEmptyPrimitive rangePersistentEmptyPrimitive) {
                return primitive;
            }
        });
    }

    @Override
    public #package#domain.core.primitive.PersistentPrimitive<?> minus(#package#domain.core.primitive.PersistentPrimitive<?> primitive) {
        return primitive.visit(new PrimitiveStateVisitor<#package#domain.core.primitive.PersistentPrimitive<?>>() {
            @Override
            public #package#domain.core.primitive.PersistentPrimitive<?> accept(#package#domain.core.primitive.PersistentCombinedPrimitive rangePersistentCombinedPrimitive) {
                return null;
            }

            @Override
            public #package#domain.core.primitive.PersistentPrimitive<?> accept(#package#domain.core.primitive.PersistentElementaryPrimitive rangePersistentElementaryPrimitive) {
                // TODO units do not match
                return new #package#domain.core.primitive.PersistentPrimitive<>(fraction.minus(rangePersistentElementaryPrimitive.fraction), unit);
            }

            @Override
            public #package#domain.core.primitive.PersistentPrimitive<?> accept(#package#domain.core.primitive.PersistentEmptyPrimitive rangePersistentEmptyPrimitive) {
                return primitive;
            }
        });
    }

    @Override
    public #package#domain.core.primitive.PersistentPrimitive<?> div(#package#domain.core.primitive.PersistentPrimitive<?> primitive) {
        return primitive.visit(new PrimitiveStateVisitor<#package#domain.core.primitive.PersistentPrimitive<?>>() {
            @Override
            public #package#domain.core.primitive.PersistentPrimitive<?> accept(#package#domain.core.primitive.PersistentCombinedPrimitive rangePersistentCombinedPrimitive) {
                return null;
            }

            @Override
            public #package#domain.core.primitive.PersistentPrimitive<?> accept(#package#domain.core.primitive.PersistentElementaryPrimitive rangePersistentElementaryPrimitive) {
                // TODO units do not match
                return new #package#domain.core.primitive.PersistentPrimitive<>(fraction.divides(rangePersistentElementaryPrimitive.fraction), unit);
            }

            @Override
            public #package#domain.core.primitive.PersistentPrimitive<?> accept(#package#domain.core.primitive.PersistentEmptyPrimitive rangePersistentEmptyPrimitive) {
                return primitive;
            }
        });
    }


    @Override
    public #package#domain.core.primitive.UnitInstance<RANGE> getUnitInstance() {
        return unit;
    }

    @Override
    public String toString() {
        return fraction.toDouble() + " (" + unit.toString() + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        #package#domain.core.primitive.PersistentElementaryPrimitive<?> that = (#package#domain.core.primitive.PersistentElementaryPrimitive<?>) o;

        if (fraction != null ? !fraction.equals(that.fraction) : that.fraction != null) return false;
        return unit != null ? unit.equals(that.unit) : that.unit == null;
    }

    @Override
    public int hashCode() {
        int result = fraction != null ? fraction.hashCode() : 0;
        result = 31 * result + (unit != null ? unit.hashCode() : 0);
        return result;
    }

    @Override
    public <R> R visit(PrimitiveStateVisitor<R> visitor) {
        return visitor.accept(this);
    }
}
