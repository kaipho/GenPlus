package #package#domain.core.primitive;

/**
 * A persistent primitive is a rational number combined with a unit.
 * eg 12.4 km
 */
public class PersistentPrimitive<RANGE extends #package#domain.core.primitive.UnitClass> {
    private final RANGE unitClass;
    private #package#domain.core.primitive.PrimitiveState<RANGE> state;
    private Long id;

    public static <R extends #package#domain.core.primitive.UnitClass> #package#domain.core.primitive.PersistentPrimitive<R> getIdentity(R range) {
        return new #package#domain.core.primitive.PersistentPrimitive<>(range);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public static <RANGE extends #package#domain.core.primitive.UnitClass> #package#domain.core.primitive.PersistentPrimitive<RANGE> empty(RANGE range) {
        return new #package#domain.core.primitive.PersistentPrimitive<>(range);
    }

    private PersistentPrimitive(RANGE range) {
        this.unitClass = range;
        this.state = new #package#domain.core.primitive.PersistentEmptyPrimitive<>(this, range.getDefaultInstance());
    }

    public PersistentPrimitive(#package#domain.core.primitive.Rational value, UnitInstance<RANGE> unit) {
        if(!value.equals(#package#domain.core.primitive.Rational.NO_VAL) && !unit.isCovered(value)) {
            throw new #package#domain.core.primitive.UnitNotCovered("primitives.UnitNotCovered");
        }
        this.unitClass = unit.getInstanceOf();
        this.state = new PersistentElementaryPrimitive<>(value, unit, this);
    }

    public PersistentPrimitive(Long id, #package#domain.core.primitive.Rational value, UnitInstance<RANGE> unit) {
        this(value, unit);
        this.id = id;
    }

    public #package#domain.core.primitive.PersistentPrimitive<RANGE> to(UnitInstance<RANGE> unit) {
        return state.to(unit);
    }

    /**
     * Calculates the value in the default Unit of RANGE if the primitive is combined. Else return the stored value.
     */
    public #package#domain.core.primitive.Rational resolve() {
        return state.resolve();
    }

    public #package#domain.core.primitive.PersistentPrimitive<RANGE> add(#package#domain.core.primitive.PersistentPrimitive<RANGE> primitive) {
        return state.add(primitive);
    }
    public #package#domain.core.primitive.PersistentPrimitive<?> mult(#package#domain.core.primitive.PersistentPrimitive<?> primitive) {
        return state.mult(primitive);
    }
    public #package#domain.core.primitive.PersistentPrimitive<?> minus(#package#domain.core.primitive.PersistentPrimitive<?> primitive) {
        return state.minus(primitive);
    }
    public #package#domain.core.primitive.PersistentPrimitive<?> div(#package#domain.core.primitive.PersistentPrimitive<?> primitive) {
        return state.div(primitive);
    }

    public RANGE getUnitClass() {
        return unitClass;
    }

    /**
     * Normalizes the unit instance to the default unit if the primitive is combined. Otherwise returns the actual instance.
     */
    public UnitInstance<RANGE> getUnitInstance() {
        return state.getUnitInstance();
    }

    @Override
    public String toString() {
        return state.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        #package#domain.core.primitive.PersistentPrimitive<?> that = (#package#domain.core.primitive.PersistentPrimitive<?>) o;

        if (unitClass != null ? !unitClass.equals(that.unitClass) : that.unitClass != null) return false;
        if (state != null ? !state.equals(that.state) : that.state != null) return false;
        return id != null ? id.equals(that.id) : that.id == null;
    }

    @Override
    public int hashCode() {
        int result = unitClass != null ? unitClass.hashCode() : 0;
        result = 31 * result + (state != null ? state.hashCode() : 0);
        result = 31 * result + (id != null ? id.hashCode() : 0);
        return result;
    }

    public <R> R visit(PrimitiveStateVisitor<R> visitor) {
        return this.state.visit(visitor);
    }
}
