package #package#domain.core.primitive;

import java.util.Set;

public abstract class UnitClass<RANGE extends #package#domain.core.primitive.UnitClass> {
    protected UnitInstance<RANGE> defaultInstance;
    protected Set<UnitInstance<RANGE>> instances;
    private final Long id;

    protected UnitClass(Long id) {
        this.id = id;
    }

    public abstract String toString();

    public Long getId() {
        return id;
    }

    public Set<UnitInstance<RANGE>> getInstances() {
        return instances;
    }

    public UnitInstance<RANGE> getDefaultInstance() {
        return defaultInstance;
    }
}
