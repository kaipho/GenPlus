package #package#domain.primitive.unit.classes;

import #package#domain.core.primitive.UnitClass;
import #package#domain.primitive.unit.instances.#lower_class_name#.*;

import java.util.HashSet;

public class #class_name# extends UnitClass<#class_name#> {

    public static #class_name# getUnitClass() {
        return new #class_name#();
    }

    public #class_name#() {
        super(#id#);
        instances = new HashSet<>();

        #line#
    }

    @Override
    public String toString() {
        return "#class_name#";
    }
}
