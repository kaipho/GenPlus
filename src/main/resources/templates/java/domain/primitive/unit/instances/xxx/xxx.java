// keep
package #package#domain.primitive.unit.instances.#lower_class_name#;

import #package#domain.core.primitive.Rational;
import #package#domain.core.primitive.UnitInstance;
import #package#domain.primitive.unit.classes.#name#;

public class #class_name# extends UnitInstance<#name#> {
    private static #class_name# instance = new #class_name#();

    private #class_name#() {
        super(#name#.getUnitClass(), #id#L);
    }

    public static #class_name# getUnit() {
        return instance;
    }

    @Override
    public boolean isCovered(Rational r) {
        throw new RuntimeException("TODO");
    }

    @Override
    public String toString() {
        return "#class_name#";
    }
}
