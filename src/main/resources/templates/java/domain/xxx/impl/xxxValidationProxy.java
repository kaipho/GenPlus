package #package#domain#subpackage#.impl;

import #package#core.exceptions.ValidationException;
import #package#domain.MetadataKeys;
import #package#domain.core.PersistentElementCompleteVisitor;
import #package#domain.core.ToStringVisitor;
import #package#domain#subpackage#.*;
import #package#domain.core.PersistentElement;

import java.util.regex.Pattern;
import java.util.List;

#imports#

#comment#
public class #class_name#ValidationProxy implements #class_name# {
    private #class_name# real;

    /**
     * Creates a proxy for validation. So the setter may throw a ValidationException
     */
    public #class_name#ValidationProxy(#class_name# real) {
        this.real = real;
    }

    #getter#
    #setter#


    @Override
    public Long getId() {
        return real.getId();
    }

    #accept#

    @Override
    public <T> T accept(PersistentElementCompleteVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return ToStringVisitor.INSTANCE.visit(this);
    }
}