package #package#domain.category;

public class Role extends PersistentCategory {
    public final static Long ID = #id#L;

    private Role(String value) {
        super(ID, value);
    }

    public static Role empty() {
        return new Role(null);
    }

    public static Role from(String s) {
        return new Role(s);
    }

    #line#
}
