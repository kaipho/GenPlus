package #package#domain.category;

public class #class_name# extends PersistentCategory {
    public final static Long ID = #id#L;

    private #class_name#(String value) {
        super(ID, value);
    }

    public static #class_name# empty() {
        return new #class_name#(null);
    }

    public static #class_name# from(String s) {
        return new #class_name#(s);
    }
}
