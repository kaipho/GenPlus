package #package#domain.category;

public class Language extends PersistentCategory {
    public final static Long ID = #id#L;

    private Language(String value) {
        super(ID, value);
    }

    public static Language empty() {
        return new Language(null);
    }

    public static Language from(String s) {
        return new Language(s);
    }

    #line#

}
