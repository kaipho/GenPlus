import {Injectable} from "@angular/core";
import {LoginService} from "./login.service";
import {TransactionServiceHttpService} from "../functions/http/transactionService.http.service";
import {LongTransactionVM} from "../../domain/transaction/longTransactionVM";
import {StompService} from "@stomp/ng2-stompjs";
import {Message} from '@stomp/stompjs';

/**
 * Store to save Transaction IDs
 */
@Injectable()
export class TransactionStore {
    _transaction: any;
    _transactions: Array<LongTransactionVM> = [];
    private subscription: any;
    transactionService: TransactionServiceHttpService;

    public get transactions(): any {
        return this._transactions;
    }

    public response = (ta) => {
        console.log(ta);
        const result = this._transactions.find(it => it.longTaId == ta.longTaId);
        if (result != null) {
            this._transactions = this._transactions.filter(it => it.longTaId != ta.longTaId);
        } else {
            this._transactions.push(ta)
        }
    };

    constructor(private loginService: LoginService,
                private stompService: StompService) {
        loginService.userChange.subscribe(user => {
            this.transactionService.openTransactions().subscribe(
                transactions => this._transactions = transactions
            );

            this.subscription = stompService.subscribe('/transactions/' + user.id);
            this.subscription.map((message: Message) => JSON.parse(message.body)).subscribe(this.response)
        });

        loginService.userLogout.subscribe(user => {
            this._transactions = [];
            this.stompService.disconnect();
        });
    }

    inTransaction(): boolean {
        return this._transaction != null && "" != this._transaction
    }
}
