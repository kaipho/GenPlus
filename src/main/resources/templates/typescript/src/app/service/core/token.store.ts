import { Injectable } from "@angular/core";

/**
 * Store to save Tokens
 */
@Injectable()
export class TokenStore {
    _token:string; get; set;
}
