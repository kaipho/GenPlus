import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { TokenStore } from "./token.store";
import { LoginService } from "./login.service";
import { Router } from "@angular/router";
import { environment } from "../../../environments/environment";
import { TransactionStore } from "./transaction.store";
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import { catchError, map } from 'rxjs/operators';

@Injectable()
export class HttpService {
    _http: HttpClient;


    constructor(_http: HttpClient, public token: TokenStore, private login: LoginService, private router: Router, private transaction: TransactionStore) {
        this._http = _http;
    }


    get(url: string): Observable<HttpResponse<any>> {
        return this._http.get(this._getUrl(url), {headers: this._createHeader(), observe: 'response'})
        .pipe(
            catchError((error) => this.handleError(error)),
            map((ok: HttpResponse<any>) => {
                this.transaction._transaction = ok.headers.get("transaction");
                return ok;
            })
        );
    }

    post(url: string, body: any): Observable<HttpResponse<any>> {
        return this._http.post(this._getUrl(url), JSON.stringify(body), {headers: this._createHeader(), observe: 'response'})
        .pipe(
            catchError((error) => this.handleError(error)),
            map((ok: HttpResponse<any>) => {
                this.transaction._transaction = ok.headers.get("transaction");
                return ok;
            })
        );
    }

    put(url: string, body: any): Observable<HttpResponse<any>> {
        return this._http.put(this._getUrl(url), JSON.stringify(body), {headers: this._createHeader(), observe: 'response'})
        .pipe(
            catchError((error) => this.handleError(error)),
            map((ok: HttpResponse<any>) => {
                this.transaction._transaction = ok.headers.get("transaction");
                return ok;
            })
        );
    }

    delete(url: string): Observable<HttpResponse<any>> {
        return this._http.delete(this._getUrl(url), {headers: this._createHeaderText(), observe: 'response'})
        .pipe(
            catchError((error) => this.handleError(error)),
            map((ok: HttpResponse<any>) => {
                this.transaction._transaction = ok.headers.get("transaction");
                return ok;
            })
        );
    }

    patch(url: string, body: any): Observable<HttpResponse<any>> {
        return this._http.patch(this._getUrl(url), JSON.stringify(body), {headers: this._createHeader(), observe: 'response'})
        .pipe(
            catchError((error) => this.handleError(error)),
            map((ok: HttpResponse<any>) => {
                this.transaction._transaction = ok.headers.get("transaction");
                return ok;
            })
        );
    }

    _createHeader(): HttpHeaders {
        return new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': this.token._token || '',
            'transaction': (<string>this.transaction._transaction || '')
        });
    }

    _createHeaderText(): HttpHeaders {
        return new HttpHeaders({
            'Authorization': this.token._token || '',
            'transaction': (<string>this.transaction._transaction || '')
        });
    }

    _getUrl(url: string): string {
        if (url.startsWith("/")) {
            return environment.server + url;
        } else {
            return url;
        }
    }

    handleError(error: any) {
        if (error.status == 401) {
            this.login.logout();
            this.router.navigate(['']);
            return throwError(error);
        } else {
            return throwError(error);
        }
    }
}
