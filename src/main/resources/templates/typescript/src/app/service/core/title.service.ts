import { Injectable } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";

/**
 * Service to store the Title. Has support for multiple stacked titles.
 */
@Injectable()
export class TitleService {
    _title:Array<Title> = [];
    _translate:TranslateService;

    constructor(_translate:TranslateService) {
        this._translate = _translate;
    }

    /**
     * Setzt den Titel und löscht alle bereits vorhandenen Titel.
     */
    setTitle(title:string, url?:Array<string>) {
        this._title = [];
        const translatedTitle = this._translate.get(title).subscribe(
            ok => this._title.push({
                name: ok,
                link: url
            })
        )
    }

    /**
     * Setzt den Titel als neuen aktuellen ohne den Verlauf zu löschen.
     */
    addTitle(title:string, level:number, url?:Array<string>) {
        const translatedTitle = this._translate.get(title).subscribe(
            ok => this._title.splice(level, 1, {
                name: ok,
                link: url
            })
        )
    }

    /**
     * @returns {Array<string>} Alle gespeicherten Titel
     */
    getTitle():Array<Title> {
        return this._title;
    }
}

export interface Title {
    name:string,
    link:Array<string>
}
