import { EventEmitter, Injectable } from "@angular/core";
import { User } from "../../domain/security/user";
import { AnonymUser } from "../../domain/security/anonymUser";
import { TokenStore } from "./token.store";
import { LocalStorage } from "ngx-webstorage";
import { Router } from "@angular/router";
import { TechnicalUser } from "../../domain/security/technicalUser";

/**
 * Service zum speichern der Nutzerinfos und für die Anmeldung.
 */
@Injectable()
export class LoginService {
    public _user: User;
    public authenticated: boolean;

    @LocalStorage('token')
    public boundValue;

    public userChange: EventEmitter<User> = new EventEmitter();
    public userLogout: EventEmitter<User> = new EventEmitter();

    constructor(public token: TokenStore, private router: Router) {
        this.authenticated = false;
        this.token._token = '';
        this.user = <TechnicalUser>{};
    }

    set user(user: User) {
        this._user = user;
        if (user.username != null)
            this.userChange.emit(user);
    }

    logout() {
        this.userLogout.emit(this.user);
        this.authenticated = false;
        this.user = new AnonymUser();
        this.boundValue = '';
        this.token._token = '';
        this.router.navigate(['']);
    }

    isAuthenticated(): boolean {

        return this.authenticated;
    }

    canWrite(): boolean {
        if (this.user.roles == null) {
            return false;
        }
        return this.user.roles.find(it => (<any>it).value === 'EDIT') != null
    }

    hasRole(roles: Array<string>): boolean {
        if (this.user.roles == null) {
            return false;
        }
        const contains = this.user.roles.filter(
            actual => {
                return roles.indexOf((<any>actual).value) > -1;
            }
        );
        return contains.length > 0;
    }

    getUserName(): string {
        if (this.user == null) {
            return 'Gast';
        }
        return this.user.username;
    }

    get user(): User {
        return this._user;
    }
}
