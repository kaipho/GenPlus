import {HttpService} from "../../core/http.service";
import { Observable, throwError } from "rxjs";
import {Pageable} from "./pageable";
import {Router} from "@angular/router";

#imports#

export abstract class AbstractHttpService<D> {
    http: HttpService;
    router: Router;

    constructor(http: HttpService, router: Router) {
        this.http = http;
        this.router = router;
    }

    abstract getFirst(): Observable<Pageable<D>>;

    abstract getAll(link: string): Observable<Pageable<D>>;

    abstract getNext(data: Pageable<D>): Observable<Pageable<D>>;

    abstract getPrev(data: Pageable<D>): Observable<Pageable<D>>;

    abstract get(link: string): Observable<D>;

    abstract create(type: number): Observable<String>;

    abstract update(data: D): Observable<D>;

    abstract delete(data: D): Observable<any>;

    abstract find(id: number, searchText: string): Observable<Pageable<D>>;

    static extractOne(data: any, obj: any): any {
        let _this = data;
        if (data._embedded)
            _this = data._embedded;

        for (let property in _this) {
            if (_this.hasOwnProperty(property)) {
                if (Object.prototype.toString.call(_this[property]) === '[object Object]') {
                    const _type: number = _this[property]._type;
                    if (_type == null) {
                        obj[property] = _this[property];
                    } else
                        obj[property] = AbstractHttpService.mapTo(_type, _this[property]);
                } else if (AbstractHttpService.typedArray(_this[property])) {
                    const array = [];
                    _this[property].forEach(item => {
                        const _type: number = item._type;
                        array.push(AbstractHttpService.mapTo(_type, item));
                    });
                    obj[property] = array;
                } else {
                    obj[property] = _this[property] || null;
                }
            }
        }
        if (data._links)
            obj.self = data._links.self;
        return obj;
    }

    static typedArray(data: any): boolean {
        return Object.prototype.toString.call(data) === '[object Array]' && data.length > 0 && Object.prototype.toString.call(data[0]) === '[object Object]';
    }

    static mapTo(type: number, item: any): any {
        switch (type) {
            #line#
        }
    }

    handleError(error: any) {
        return throwError(error);
    }
}
