import { Component, Input } from "@angular/core";
import { HttpService } from "../../../service/core/http.service";
import { MatDialog, MatDialogRef } from "@angular/material/dialog";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

@Component({
    selector: 'gen-simple-obj-list',
    template: `
	<div class="full-width">
	    <label>{{placeholder}}: </label>
	    <mat-chip-list [selectable]="true">
	        <mat-chip *ngFor="let value of data">{{value}}</mat-chip>
	        <mat-chip color="primary" (click)="addElement()"><mat-icon>add</mat-icon> {{'button.addElement' | translate}}</mat-chip>
	    </mat-chip-list>
	</div>
    `,
    styles: [`
         .material-icons {
            font-size: 12px !important;
        }
        mat-icon {
            height: 12px !important;
            width: 12px !important;
            vertical-align: middle;
        }
    `]
})
export class GenSimpleObjListComponent {
    @Input()
    data: Array<any>;

    @Input()
    placeholder: string;

    @Input()
    link:string;
    @Input()
    assId:number;

    constructor(private dialog: MatDialog) {
    }

    addElement() {
        const ref = this.dialog.open(AddElementComponent, {
            disableClose: true
        });
        ref.componentInstance.link = this.link + '/' + this.assId;
        ref.afterClosed().subscribe(result => {
            if (result != null && result != '') this.data.push(result);
        });
    }
}

@Component({
    template: `
<form [formGroup]="serviceForm">
    <mat-toolbar color="primary">
        <span>{{ 'button.addElement' | translate }}</span>
    </mat-toolbar>
    <mat-card-content class="form-fields">
        <div>
            <mat-form-field class="service"><input matInput formControlName="newValue" [placeholder]="'button.addElement' | translate"/></mat-form-field>
        </div>

        <button mat-button *ngIf="_error" style="color: darkred;width: 100%">
            <mat-icon>error_outline</mat-icon>
            {{_error}}
        </button>
    </mat-card-content>
    <mat-card-actions align="end">
        <div >
            <div>
                <button mat-raised-button [disabled]="!serviceForm.valid" type="submit" color="primary" (click)="submit()">
                    <mat-icon>check</mat-icon>
                    {{ 'button.ok' | translate }}
                </button>
                <button mat-button mat-dialog-close>
                    <mat-icon>close</mat-icon>
                    {{ 'button.cancel' | translate }}
                </button>
            </div>
        </div>
    </mat-card-actions>
</form>
    `,
})
export class AddElementComponent {
    dialogRef: MatDialogRef<AddElementComponent>;
    serviceForm: FormGroup;
    _error: string;

    link: string;

    constructor(dialogRef: MatDialogRef<AddElementComponent>, builder: FormBuilder, private http: HttpService) {
        this.dialogRef = dialogRef;

        this.serviceForm = builder.group({
            newValue: ['', Validators.required],
        })
    }

    ngOnInit() {
    }

    submit() {
        this.http.post(
            '/api/' + this.link + '/add', {newValue: this.serviceForm.get('newValue').value}
        ).subscribe(
            () => {
                this.dialogRef.close(this.serviceForm.get('newValue').value)
            },
            (error) => this._error = error.body.message
        );
    }
}
