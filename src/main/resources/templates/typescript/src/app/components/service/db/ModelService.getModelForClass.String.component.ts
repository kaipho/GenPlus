import { Component } from "@angular/core";
import { MatDialogRef } from "@angular/material/dialog";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { GenDialogComponent } from '../../framework/gen-dialog/gen-dialog.component';
import { ModelServiceHttpService } from '../../../service/functions/http/modelService.http.service';

@Component({
    templateUrl: './ModelService.getModelForClass.String.component.html',
})
export class ModelServiceGetModelForClassStringComponent extends GenDialogComponent {
    dialogRef: MatDialogRef<ModelServiceGetModelForClassStringComponent>;
    serviceForm: FormGroup;

    submitting: boolean = false;

    getModelForClassOptions: Array<String> = [];

    constructor(dialogRef: MatDialogRef<ModelServiceGetModelForClassStringComponent>, builder: FormBuilder
        , public http: ModelServiceHttpService) {
        super();
        this.dialogRef = dialogRef;

        http.getAllClasses().subscribe(
            data => this.getModelForClassOptions = data
        );

        this.serviceForm = builder.group({
            result: [{value: '', disabled: true}],
            clazz: ['', Validators.required],
        })
    }

    ngOnInit() {
    }

    close() {
        setTimeout(() =>
                this.dialogRef.close(this._result)
            , 200);
    }

    submit() {
        this.submitting = true;
        this.http.getModelForClass(
            this.serviceForm.get('clazz').value,
        ).subscribe(
            (ok) => {
                this.submitting = false;
                this.serviceForm.patchValue({result: ok});
                this._result = ok;
            },
            (error) => {
                this.submitting = false;
                this._error = error.body.message;
            }
        );
    }

    public _toString(obj: any) {
        if (obj == null)
            return 'Nichts ausgewählt';
        return obj.toString();
    }
}
