import { Component, EventEmitter, Input, Optional, Self } from "@angular/core";
import { ControlValueAccessor, NgControl } from "@angular/forms";
import { Primitive } from "../../../domain/primitive";
import { PrimitivesServiceHttpService } from "../../../service/functions/http/primitivesService.http.service";

@Component({
    selector: 'gen-primitive',
    template: `
        <mat-form-field>
            <input matInput type="number" [ngModel]="number | async" (change)="update($event)" [placeholder]="placeholder"/>
        </mat-form-field>
        <mat-select [placeholder]=" getPlaceholder() | translate " [ngModel]="unit | async" (change)="convert($event)">
            <mat-option *ngFor="let item of units" [value]="item">{{ getPlaceholderForUnit(item) | translate}}</mat-option>
        </mat-select>
    `,
    styleUrls: ['./gen-primitive.scss']
})
export class GenPrimitiveComponent implements ControlValueAccessor {
    @Input()
    placeholder: string;

    @Input()
    unitClass: number;
    @Input()
    units: Array<number>;

    @Input()
    required:boolean;

    value: Primitive;
    _onChange = (value: any) => {
    };

    number: EventEmitter<number> = new EventEmitter();
    unit: EventEmitter<number> = new EventEmitter();

    constructor(@Self() @Optional() public control: NgControl, public http: PrimitivesServiceHttpService) {
        this.control.valueAccessor = this;
        // this.required = control
    }

    writeValue(obj: any): void {
        this.value = obj;
        this.number.emit(this.value.value);
        this.unit.emit(this.value.unit_id);
    }

    registerOnChange(fn: (value: any) => void): void {
        this._onChange = fn;
    }

    registerOnTouched(fn: any): void {
    }

    update(event: any) {
        const value = event.target.value;
        this.value.value = value;
        this._onChange(this.value);
        this.number.emit(value);
    }

    getPlaceholder(): string {
        if (this.value == null) return '';
        return `primitives.${this.unitClass}.name`
    }

    getPlaceholderForUnit(id: number): string {
        return `primitives.${this.unitClass}.units.${id}`
    }

    convert(newUnit: any) {
        this.http.convertTo(this.value.id, newUnit.value).subscribe(
            ok => {
                this.value.value = ok;
                this.number.emit(ok);
                this._onChange(this.value);
            },
            error => null // TODO!!!
        );
        this.value.unit_id = newUnit.value;
    }
}
