import { Directive, OnInit } from '@angular/core';

@Directive()
export abstract class GenDialogComponent implements OnInit {
  abstract ngOnInit(): void;

  public _actualId: number;
  public _error: string;

  public _result: any;

  public set actualId(id: number) {
    this._actualId = id;
  }
}
