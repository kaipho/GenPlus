import { Component, EventEmitter, Input, Output } from "@angular/core";
import { Router } from "@angular/router";

@Component({
    selector: 'gen-obj',
    template: `
<mat-card>
    <mat-toolbar color="primary">{{ name | translate }}</mat-toolbar>
    <mat-nav-list>
        <mat-list-item (click)="openObj()" *ngIf="obj">
            <h2 mat-line> {{obj._rep.split("|")[0]}} </h2>
            <p mat-line> {{obj._rep.split("|")[1]}} </p>
        </mat-list-item>
		<mat-list-item *ngIf="!obj">
				{{ 'noObj' | translate }}
		</mat-list-item>
	</mat-nav-list></mat-card>
    `,
    styles: ['mat-card { margin: 0; }']
})
export class GenObjComponent {
    @Input()
    name: string;
    @Input()
    obj: any;

    constructor(private router:Router) {
    }

    public openObj() {
        this.router.navigateByUrl("/web" + this.obj._self);
    }
}
