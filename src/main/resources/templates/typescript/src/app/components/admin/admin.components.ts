import { MasterviewComponent } from "./masterview/masterview.component";
import { TransactionsComponent } from "./transactions/transactions.component";
import { StatisticComponent } from "./statistic/statistic.component";


export const ADMIN_COMPONENTS:any[] = [
    MasterviewComponent,
    TransactionsComponent,
    StatisticComponent
];
