import { Component, EventEmitter, Input, Output } from "@angular/core";
import { Router } from "@angular/router";
import { Pageable } from "../../../service/domain/http/pageable";
import { DomainObject } from "../../../domain/domain.object";

@Component({
    selector: 'gen-domainr-list',
    template: `
        <mat-nav-list *ngIf="obj.data.length != 0">
            <div *ngFor="let actualClass of obj.data">
                <mat-list-item (click)="select.emit(actualClass)">
                    <h2 mat-line> {{actualClass._rep.split("|")[0]}} </h2>
                    <p mat-line> {{actualClass._rep.split("|")[1]}} </p>
                </mat-list-item>
                <mat-divider></mat-divider>
            </div>
        </mat-nav-list>
        <mat-nav-list *ngIf="obj.data.length == 0">
            <mat-list-item>
                Keine Einträge gefunden!
            </mat-list-item>
        </mat-nav-list>
    `
})
export class GenDomainrListComponent {
    @Input()
    obj: Pageable<DomainObject>;
    @Output()
    select: EventEmitter<Number> = new EventEmitter<Number>();

    constructor(private router:Router) {
    }

    toString(obj): string {
        return obj.toString();
    }

    public _emitSelect(index: number) {
        this.router.navigateByUrl("/web" + this.obj[index]._self);
    }
}
