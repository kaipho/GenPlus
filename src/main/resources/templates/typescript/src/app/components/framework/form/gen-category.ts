import { Component, Input, OnInit, Optional, Self } from "@angular/core";
import { ControlValueAccessor, NgControl } from "@angular/forms";
import { CategoryServiceHttpService } from "../../../service/functions/http/categoryService.http.service";

@Component({
    selector: 'gen-category',
    template: `
        <mat-select [ngModel]="value" (change)="change($event)" [placeholder]="placeholder" [disabled]="disabled">
            <mat-option [value]="it" *ngFor="let it of options">{{it}}</mat-option>
        </mat-select>
    `
})
export class GenCategoryComponent implements OnInit, ControlValueAccessor {
    @Input()
    placeholder: string;

    @Input()
    clazz: number;

    disabled: boolean = false;

    _onChange = (value: any) => {
    };

    value: string;

    options: Array<string>;

    constructor(@Self() @Optional() public _control: NgControl,
                public categoriesHttp: CategoryServiceHttpService) {
        this._control.valueAccessor = this;
    }

    ngOnInit(): void {
        this.categoriesHttp.getValuesFor(this.clazz).subscribe(
            data => this.options = data
        );
    }

    change(event: any) {
        this._onChange(event.value)
    }

    setDisabledState(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }

    writeValue(obj: any): void {
        this.value = obj;
    }

    registerOnChange(fn: (value: any) => void): void {
        this._onChange = fn;
    }

    registerOnTouched(fn: any): void {
    }
}
