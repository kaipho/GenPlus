import { Component } from "@angular/core";
import { MatDialogRef } from "@angular/material/dialog";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

#imports#

@Component({
    templateUrl: './#lower_class_name#.component.html',
})
export class #class_name#Component extends GenDialogComponent {
    dialogRef: MatDialogRef<#class_name#Component>;
    serviceForm: FormGroup;

    submitting: boolean = false;
    closeSilent: boolean = false;

    constructor(dialogRef:MatDialogRef<#class_name#Component>, builder:FormBuilder
            #line(0)#) {
        super();
        this.dialogRef = dialogRef;

        this.serviceForm = builder.group({
            result: [{value: '', disabled: true}],
            #line(3)#
        })
    }

    ngOnInit() {
        #line(2)#
    }

    close() {
        this.dialogRef.close(this._result);
    }

    #line(1)#

    public _toString(obj: any) {
        if(obj == null)
            return 'Nichts ausgewählt';
        return obj.toString();
    }
}
