import { Component, EventEmitter, Input, Optional, Self, ViewChild } from "@angular/core";
import { ControlValueAccessor, NgControl } from "@angular/forms";
import { PrimitivesServiceHttpService } from "../../../service/functions/http/primitivesService.http.service";
import { BLOB } from "../../../domain/blob";
import { environment } from "../../../../environments/environment";


@Component({
    selector: 'gen-blob',
    template: `
        <div layout="row">
            <button mat-icon-button [disabled]="!value.blobId" (click)="clear()">
                <mat-icon>cancel</mat-icon>
            </button>
            <td-file-input style="display: none" [(ngModel)]="file" #fileInput
                           name="file"></td-file-input>
            <button mat-icon-button [disabled]="!file" (click)="uploadBlob()">
                <mat-icon>file_upload</mat-icon>
            </button>
            <button mat-icon-button [disabled]="!value.blobId" (click)="downloadBlob()">
                <mat-icon>file_download</mat-icon>
            </button>
        </div>
    `
})
export class GenBlobComponent implements ControlValueAccessor {
    @Input()
    placeholder: string;

    file: File;

    value: BLOB;

    _onChange = (value: any) => {
    };

    constructor(@Self() @Optional() public control: NgControl,
                public http: PrimitivesServiceHttpService) {
        this.control.valueAccessor = this;
    }

    writeValue(obj: any): void {
        this.value = obj;
    }

    registerOnChange(fn: (value: any) => void): void {
        this._onChange = fn;
    }

    registerOnTouched(fn: any): void {
    }

    selectFile(event: File) {
        this.file = event;
        this._onChange({name: event.name, id: this.value.id, blobId: this.value.blobId});
        this.writeValue({name: event.name, id: this.value.id, blobId: this.value.blobId})
    }

    /**
     * Uploads the file to the server and stored it to the DB. Returns the ID (blobId) the blob is stored under.
     */
    uploadBlob() {

    }

    downloadBlob() {
        window.open(`${environment.server}/api/file/${this.value.blobId}`)
    }

    /**
     *  Clears the FileInput and the linking to the BLOB.
     */
    clear() {
        this.value.name = '';
        this.value.blobId = null;
    }
}
