import { AfterContentInit, Component, Input, Optional, Self } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ControlValueAccessor, NgControl } from "@angular/forms";
import { SecureValueAccessorEncryptStringComponent } from "../../service/security/SecureValueAccessor.encrypt.String.component";
import { SecureValueAccessorDecryptStringComponent } from "../../service/security/SecureValueAccessor.decrypt.String.component";

@Component({
    selector: 'gen-secure',
    templateUrl: './gen-secure.component.html'
})
export class GenSecureComponent implements AfterContentInit, ControlValueAccessor {

    @Input()
    placeholder: string;

    value: string;
    _onChange = (value: any) => {
    };

    constructor(@Self() @Optional() public _control: NgControl, private dialog: MatDialog) {
        this._control.valueAccessor = this;
    }

    ngAfterContentInit() {
        this.value = this._control.value;
    }

    writeValue(obj: any): void {
        this.value = obj;
    }

    registerOnChange(fn: (value: any) => void): void {
        this._onChange = fn;
    }

    registerOnTouched(fn: any): void {
    }

    encryptValue() {
        this.dialog.open(SecureValueAccessorEncryptStringComponent, {
            disableClose: true
        }).afterClosed().subscribe(result => {
            if (result == null || result == '')
                return;
            this._control.valueAccessor.writeValue(result);
            this._onChange(result.toString())
        });
    }

    decryptValue() {
        const dialog = this.dialog.open(SecureValueAccessorDecryptStringComponent, {
            disableClose: true
        });
        dialog.componentInstance.serviceForm.patchValue({'text': this._control.value});
        dialog.componentInstance.serviceForm.get('text').disable();
    }
}
