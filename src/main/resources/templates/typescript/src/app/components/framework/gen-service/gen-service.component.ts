import { Component, OnInit, ViewContainerRef } from "@angular/core";
import { MatDialog, MatDialogConfig, MatDialogRef } from "@angular/material/dialog";
import { MenuService } from "./menu.service";

@Component({
    selector: 'gen-service',
    template: `
        <button mat-icon-button [mat-menu-trigger-for]="menu" *ngIf="_menuService.entries.length">
           <mat-icon>more_vert</mat-icon>
        </button>
        <mat-menu #menu x-position="before">
           <button mat-menu-item *ngFor="let actual of _menuService.entries" (click)="openDialog(actual.dialog)">
            <mat-icon *ngIf="actual.icon">{{actual.icon}}</mat-icon>
            {{ actual.name | translate }}
           </button>
        </mat-menu>
    `,
})
export class GenService implements OnInit {
    _menuService: MenuService;

    dialogRef: MatDialogRef<any>;

    constructor(menuService: MenuService, public dialog: MatDialog, public viewContainerRef: ViewContainerRef) {
        this._menuService = menuService;
    }

    ngOnInit() {
    }

    openDialog(dialog: any) {
        let config = new MatDialogConfig();
        config.viewContainerRef = this.viewContainerRef;

        this.dialogRef = this.dialog.open(dialog, config);
        this.dialogRef.componentInstance.actualId = this._menuService.actualId;
        this.dialogRef.componentInstance.data = this._menuService.data;

        this.dialogRef.afterClosed().subscribe(() => {
            this.dialogRef = null;
            this._menuService.component.ngOnInit();
        });
    }
}
