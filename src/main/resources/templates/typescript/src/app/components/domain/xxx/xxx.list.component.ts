import { Component, OnInit, Optional } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormBuilder } from "@angular/forms";

#imports#

@Component({
    templateUrl: './#lower_class_name#.list.component.html'
})
export class #class_name#ListComponent extends ListComponent<#class_name#> implements OnInit {

    constructor(titleService: TitleService,
        http: #class_name#HttpService,
        router: Router,
        route: ActivatedRoute,
        @Optional() dialogRef: MatDialogRef<#class_name#ListComponent>,
        snackBar: MatSnackBar,
        builder: FormBuilder,
        dialog: MatDialog) {
        super(titleService, http, builder, snackBar, router, route, dialogRef, dialog);
    }

    ngOnInit() {
        this.loadAll();
    }

    #line#
}
