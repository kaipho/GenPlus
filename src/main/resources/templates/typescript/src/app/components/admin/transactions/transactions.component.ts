import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { MatSnackBar } from '@angular/material/snack-bar';
import { StatistikVM } from "../../../domain/db/statistikVM";
import { TransactionServiceHttpService } from "../../../service/functions/http/transactionService.http.service";

@Component({
    selector: 'app-transactions',
    templateUrl: './transactions.component.html',
    styleUrls: ['./transactions.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class TransactionsComponent implements OnInit {
    data: Array<StatistikVM> = [];

    constructor(public html: TransactionServiceHttpService,
                public snackBar: MatSnackBar) {

    }

    blueScheme: any = {
        domain: [
            '#0D47A1',
            '#1976D2',
            '#039BE5',
            '#29B6F6',
            '#81D4FA',
            '#B2EBF2'],
    };

    view: any[] = [850, 400];

    ngOnInit(): void {
        this.html.transactionStatistics().subscribe(
            result => this.data = result,
            error => {
                this.snackBar.open(error)
            }
        );
    }
}
