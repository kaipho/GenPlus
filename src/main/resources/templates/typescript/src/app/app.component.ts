import { Component } from "@angular/core";
import { TitleService } from "./service/core/title.service";
import { LoginService } from "./service/core/login.service";
import { TranslateService } from "@ngx-translate/core";
import { MatDialog } from "@angular/material/dialog";
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserServiceChangePasswordUnitComponent } from "./components/service/security/UserService.changePassword.Unit.component";
import { NotificationServiceHttpService } from "./service/functions/http/notificationService.http.service";
import { Notification } from "./domain/core/notification";
import { Router } from "@angular/router";
import { NotificationHttpService } from "./service/domain/http/notification.http.service";
import { TransactionStore } from "./service/core/transaction.store";
import { TransactionServiceHttpService } from "./service/functions/http/transactionService.http.service";
import { LongTransactionVM } from "./domain/transaction/longTransactionVM";
import { environment } from "../environments/environment";
import { StompService } from "@stomp/ng2-stompjs";
import { Message } from '@stomp/stompjs';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    sidenavOpen;

    _loginService: LoginService;
    _titleService: TitleService;

    private subscription: any;

    notifications: Array<Notification> = [];

    constructor(titleService: TitleService,
                loginService: LoginService,
                translate: TranslateService,
                public router: Router,
                public dialog: MatDialog,
                public notificationService: NotificationServiceHttpService,
                private crud: NotificationHttpService,
                public snackBar: MatSnackBar,
                public transaction: TransactionStore,
                public transactionService: TransactionServiceHttpService,
                private stompService: StompService) {
        this._loginService = loginService;
        this._titleService = titleService;
        this.sidenavOpen = true;

        transaction.transactionService = this.transactionService;

        translate.setDefaultLang('de');
        translate.use('de');

        const location = environment.server == '' ? window.location.host : environment.server;

        loginService.userChange.subscribe(user => {
            this.notificationService.getNotifications().subscribe(
                notifications => this.notifications = notifications
            );

            this.subscription = stompService.subscribe('/notifications/' + user.id)
            this.subscription.map((message: Message) => JSON.parse(message.body)).subscribe(this.response)

        });

        loginService.userLogout.subscribe(user => {
            this.notifications = [];
            this.stompService.disconnect();
        });
    }

    public response = (notification) => {
        this.notifications.push(notification)
    };

    openNotification(notification: Notification) {
        this.crud.update(<Notification>{self: '/api/domainr/13/' + notification.id, isRead: true}).subscribe(
            ok => this.router.navigate(['/web/domainr/13/', notification.id]),
            error => this.snackBar.open(error, 'close')
        );
    }

    toggleSidenav() {
        this.sidenavOpen = !this.sidenavOpen
    }

    logout() {
        this._loginService.logout();
        this.router.navigate(['']);
    }

    unreadNotifications(): number {
        return this.notifications.filter(it => !it.isRead).length
    }

    changePassword() {
        this.dialog.open(UserServiceChangePasswordUnitComponent, {
            disableClose: true
        });
    }

    commit() {
        this.transactionService.commit().subscribe();
    }

    startTransactions() {
        this.transactionService
            .startTransaction()
            .subscribe();
    }

    detachTransactions() {
        this.transaction._transaction = null;
    }

    connectToTransaction(id: number) {
        this.transaction._transaction = id;
    }

    format(s: string): string {
        return new Date(s).toLocaleDateString();
    }
}
