export interface BLOB {
    id: number,
    name: string;
    blobId: number;
}
