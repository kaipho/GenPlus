export interface Primitive {
    id: number,
    unit_class_id: number,
    unit_id: number,
    value: number
}
