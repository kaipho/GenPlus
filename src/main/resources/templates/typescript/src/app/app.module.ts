import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatCommonModule, MatOptionModule } from '@angular/material/core';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDividerModule } from '@angular/material/divider';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatListModule } from '@angular/material/list';

import { SERVICE_COMPONENTS } from "./components/components";
import { ROUTING } from "./app.routes";
import { AuthenticationGuard } from "./guard/authentication.guard";
import { SaveDialog, SaveGuard } from "./guard/save.guard";
import { GENERATED_SERVICES } from "./service/domain/http/services";
import { TitleService } from "./service/core/title.service";
import { SidenavComponent } from './components/core/sidenav/sidenav.component';
import { RouterModule } from '@angular/router';
import { LoginService } from './service/core/login.service';
import { TokenStore } from "./service/core/token.store";
import { MenuService } from "./components/framework/gen-service/menu.service";
import { GenService } from "./components/framework/gen-service/gen-service.component";
import { GenDomainrListComponent } from "./components/framework/gen-obj-list/gen-domainr-list.component";
import { GenObjListComponent } from "./components/framework/gen-obj-list/gen-obj-list.component";
import { FormService } from "./components/framework/form/form.service";
import { SafeHtml } from "./components/framework/pipes/safeHtml.pipe";
import { GenSecureComponent } from "./components/framework/gen-secure/gen-secure.component";
import { MonacoEditorComponent } from './components/framework/editor/monaco-editor.component';
import { GenPrimitiveComponent } from "./components/framework/gen-obj/gen-primitive";
import { GenPrimitiveForServiceComponent } from "./components/framework/gen-obj/gen-primitive-s";
import { GenBlobComponent } from "./components/framework/gen-obj/gen-blob";
import { GenCategoryComponent } from "./components/framework/form/gen-category";
import { GenObjComponent } from "./components/framework/gen-obj/gen-obj.component";
import { AddElementComponent, GenSimpleObjListComponent } from "./components/framework/gen-obj-list/gen-simple-obj-list.component";
import {
    AddElementToCategoryComponent,
    GenCategoryObjListComponent
} from "./components/framework/gen-obj-list/gen-category-obj-list.component";
import { TransactionStore } from "./service/core/transaction.store";
import { SidenavAreaComponent } from "./components/core/sidenav/sidenav-area.component";
import { ADMIN_COMPONENTS } from "./components/admin/admin.components";
import { environment } from "../environments/environment";
import { StompConfig, StompService } from '@stomp/ng2-stompjs';
import { NgxWebstorageModule } from 'ngx-webstorage';


#imports#

export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

let location = environment.server == '' ? "ws://" + window.location.host : environment.server;
location = location.replace('http://', "ws://");
location = location.replace('https://', "wss://");
const stompConfig: StompConfig = {
    url:  location + "/ws",
    headers: {},
    heartbeat_in: 0,
    heartbeat_out: 20000,
    reconnect_delay: 5000,
    debug: true
};

@NgModule({
    imports: [
        HttpClientModule,
        FormsModule,
        BrowserModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient]
            }
        }),
        NgxWebstorageModule.forRoot(),
        RouterModule.forRoot(ROUTING),
        MatButtonModule,
        MatChipsModule,
        MatCardModule,
        MatExpansionModule,
        MatDialogModule,
        MatCheckboxModule,
        MatCommonModule,
        MatIconModule,
        MatInputModule,
        MatDatepickerModule,
        MatDividerModule,
        MatOptionModule,
        MatMenuModule,
        MatToolbarModule,
        MatTooltipModule,
        MatTabsModule,
        MatSelectModule,
        MatSidenavModule,
        MatListModule,
        MatSnackBarModule
    ],
    declarations: [
        ADMIN_COMPONENTS,
        MonacoEditorComponent,
        GenSecureComponent,
        GenPrimitiveComponent,
        GenPrimitiveForServiceComponent,
        GenBlobComponent,
        GenCategoryComponent,
        GenObjComponent,
        SafeHtml,
        AppComponent,
        SidenavComponent,
        SidenavAreaComponent,
        GenService,
        SaveDialog,
        GenDomainrListComponent,
        GenObjListComponent,
        GenSimpleObjListComponent,
        AddElementComponent,
        GenCategoryObjListComponent,
        AddElementToCategoryComponent,
        SERVICE_COMPONENTS,
        #line(0)#
    ],
    providers: [
        AuthenticationGuard,
        SaveGuard,
        LoginService,
        GENERATED_SERVICES,
        TitleService,
        TokenStore,
        TransactionStore,
        FormService,
        MenuService,
        StompService,
        {
            provide: StompConfig,
            useValue: stompConfig
        }
    ],
    bootstrap: [AppComponent],
    entryComponents: [
        AddElementComponent,
        AddElementToCategoryComponent,
        SaveDialog,
        SERVICE_COMPONENTS
    ]})
export class AppModule {}
