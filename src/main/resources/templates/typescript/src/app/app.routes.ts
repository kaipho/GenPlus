import { Routes } from '@angular/router';
import { AuthenticationGuard } from './guard/authentication.guard';
import { MasterviewComponent } from "./components/admin/masterview/masterview.component";
import { TransactionsComponent } from "./components/admin/transactions/transactions.component";
import { StatisticComponent } from "./components/admin/statistic/statistic.component";

#imports#

export const ROUTING: Routes = [
    {
        path: '',
        redirectTo: "/web/admin/categories",
        pathMatch: 'full'
    },
    {
        path: 'web/admin/categories',
        canActivate: [AuthenticationGuard],
        component: MasterviewComponent
    },
    {
        path: 'web/admin/transactions',
        canActivate: [AuthenticationGuard],
        component: TransactionsComponent
    },
    {
        path: 'web/admin/statistics',
        canActivate: [AuthenticationGuard],
        component: StatisticComponent
    },
    {
        path: 'web',
        canActivate: [AuthenticationGuard],
        children: [
            #line#
        ]
    }
];
