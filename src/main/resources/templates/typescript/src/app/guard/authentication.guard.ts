import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { map } from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { LoginService } from '../service/core/login.service';
import { MatDialog } from "@angular/material/dialog";
import { UserServiceLoginLoginVMComponent } from "../components/service/security/UserService.login.LoginVM.component";
import { CustomUser } from "../domain/security/customUser";
import { LocalStorage } from "ngx-webstorage";
import { UserServiceHttpService } from "../service/functions/http/userService.http.service";
import { User } from "../domain/security/user";

/**
 * Guard, der Routes mit einer authentifizierung ausstattet.
 * Falls keine Anmeldung vorhanden ist, wird der Nutzer auf die Anmeldeseite weitergeleitet.
 */
@Injectable()
export class AuthenticationGuard implements CanActivate {
    constructor(public router: Router, public dialog: MatDialog, public loginService: LoginService, private http: UserServiceHttpService) {

    }

    @LocalStorage('token')
    public boundValue;

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>|boolean {
        if (this.boundValue && this.boundValue !== '') {
            return this.http.getUserFromToken(this.boundValue).pipe(
                map(result => {
                    this.loginService.token._token = result.token;
                    this.loginService.user = <User>result.user;
                    this.loginService.authenticated = true;
                    return true;
                })
            );
        }
        if (this.loginService.isAuthenticated()) {
            return true;
        } else {
            return Observable.create(observer => {
                let dialogRef = this.dialog.open(UserServiceLoginLoginVMComponent, {
                    disableClose: true
                });
                dialogRef.componentInstance.closeSilent = true;
                return dialogRef.afterClosed().subscribe(result => {
                    if (result == null) {
                        observer.next(false);
                        observer.complete();
                        return;
                    }
                    this.loginService.token._token = result.token;
                    this.boundValue = result.token;
                    this.loginService.user = <User>result.user;
                    this.loginService.authenticated = true;
                    dialogRef = null;
                    observer.next(true);
                    observer.complete();
                    return;
                });
            });
        }
    }
}
