import { DomainObjectVisitor } from "./domain.object.visitor";
#imports#

/**
 * Visitor to get the Type as string for the object
 */
export class TypeVisitor implements DomainObjectVisitor<#generic#> {
    #line#
}