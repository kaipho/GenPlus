import {DomainObjectVisitor} from "./domain.object.visitor";

/**
 * Interface for every domain class. Defines am accept method for the DomainObjectVisitor.
 */
export interface DomainObject {
    accept<D>(visitor: DomainObjectVisitor<D>): D;
    toString():string;
    self:string;
    id:number;
    _rep:string;
}